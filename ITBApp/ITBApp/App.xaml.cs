﻿//-----------------------------------------------------------------------
// <copyright file="App.xaml.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Linq;
using System.Threading.Tasks;
using ITBApp.Helpers;
using ITBApp.Listeners;
using ITBApp.Services;
using ITBApp.UI;
using Xamarin.Auth;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Microsoft.Azure.Mobile.Crashes;
using Microsoft.Azure.Mobile;
using Microsoft.Azure.Mobile.Analytics;
using ITBApp.Interfaces;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace ITBApp
{
    public partial class App : Application
	{
		static NavigationPage navigationPage;
		public static MasterDetailPage MasterDetailPage;
		public static PageId CurrentPage;
		public static int DaysPosition = DateHelper.GetDayOfWeekAsIndex();
        public static string lastLocationId = "1";
        private static string cleanDataVersion = "2.5.26";
        public static bool IsTesting = false;

		public App()
		{
			InitializeComponent();

            if (DependencyService.Get<IAppMethods>().getAppVersion() == cleanDataVersion &&
               ! Current.Properties.ContainsKey(ServiceConstants.APP_VERSION))
			{
				LogoutAndRelogin();
			}

            if (Current.Properties.ContainsKey(ServiceConstants.APP_VERSION))
			{
				string oldVersion = Current.Properties[ServiceConstants.APP_VERSION].ToString();
				if (DependencyService.Get<IAppMethods>().getAppVersion() != oldVersion)
                {
                    if (DependencyService.Get<IAppMethods>().getAppVersion() == cleanDataVersion)
                    {
                        LogoutAndRelogin();
                    }
                    else{
						new LoginService().ClearUserData();
                    }
                }
			}

            if (Current.Properties.ContainsKey("tokenExpiration"))
            {
                LogoutAndRelogin();
            }

			var appStyle = new Style(typeof(Label))
			{
				BaseResourceKey = Xamarin.Forms.Device.Styles.SubtitleStyleKey,
				Setters = {
					new Setter { Property = Label.TextColorProperty, Value = Color.Green }
				}
			};
			Application.Current.Resources.Add(AppSettings.AppStyleAttribute, appStyle);

			var boxStyle = new Style(typeof(BoxView))
			{
				Setters = {
					new Setter { Property = BoxView.ColorProperty, Value = Color.Aqua }
				}
			};
			Application.Current.Resources.Add(boxStyle);

			NavigationPage localNavPage;
			if (IsLoggedIn)
			{
                try
                {
                    string fromPush = (string)DependencyService.Get<IAppGroupSettings>().Get("from_push");
                    if (!string.IsNullOrEmpty(fromPush))
                    {
                        localNavPage = MainMenuItem.NotificationsItem.NavigationPage;
                        DependencyService.Get<IAppGroupSettings>().Set("from_push", null);
                    }
                    else
                    {
                        localNavPage = MainMenuItem.HomeItem.NavigationPage;
                    }
                }
                catch {
                    localNavPage = MainMenuItem.HomeItem.NavigationPage;
                }

				MasterDetailPage = createHamburgerMenu(localNavPage);
				MainPage = MasterDetailPage;
			}
			else
			{
				if (Xamarin.Forms.Device.RuntimePlatform == Xamarin.Forms.Device.iOS) 
                    localNavPage = new NavigationPage(new GoogleLogin());
                else localNavPage = new NavigationPage(new BackgroundPage());

				MainPage = localNavPage;
			}

			localNavPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
			localNavPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

			navigationPage = localNavPage;
		}

		public static MasterDetailPage createHamburgerMenu(Page detailPage)
		{
            return new MasterDetailPage
            {
                Master = new MenuPage()
                {
                    Icon = AppSettings.MasterDetailImageName,
                    AutomationId = AppSettings.MasterDetailAutomationId
                },
                Detail = detailPage,
                MasterBehavior = Xamarin.Forms.Device.RuntimePlatform == Xamarin.Forms.Device.UWP 
                    && DependencyService.Get<IAppMethods>().GetWidth() > 600 ? MasterBehavior.Split : MasterBehavior.Popover
            };
		}

        public static Func<Task<bool?>> HardwareBackPressed
        {
        	private get;  set;
        }

        public static async Task<bool?> CallHardwareBackPressed()
        {
        	Func<Task<bool?>> backPressed = HardwareBackPressed;
        	if (backPressed != null)
        	{
        		return await backPressed();
        	}

        	return true;
        }

		public static bool IsLoggedIn
		{
			get
			{
				var account = AccountStore.Create().FindAccountsForService(AppSettings.AppSignInName).FirstOrDefault();

				if (account != null)
				{
                    if (Xamarin.Forms.Device.RuntimePlatform == Xamarin.Forms.Device.iOS 
                            && AppSettings.RemovePreviousAccount)
					{
						AccountStore.Create().Delete(account, AppSettings.AppSignInName);
						account = null;
						AppSettings.RemovePreviousAccount = false;
					}
				}

				return account != null;
			}
		}

		public static async Task<string> LogoutAndRelogin()
		{
			new LoginService().ClearUserData();

			var account = AccountStore.Create().FindAccountsForService(AppSettings.AppSignInName).FirstOrDefault();
			AccountStore.Create().Delete(account, AppSettings.AppSignInName);
			DependencyService.Get<ILogoutListener>().DidLogout();

            Page localNavPage = null;

            if (Xamarin.Forms.Device.RuntimePlatform == Xamarin.Forms.Device.iOS) localNavPage = new GoogleLogin();
            else localNavPage = new GoogleLoginPage();

			localNavPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
			localNavPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

			await Current.MainPage.Navigation.PushModalAsync(localNavPage);

			return String.Empty;
		}

		public static async Task<string> SaveTokenAsync(Account account)
		{
			try
			{
				LoginService loginService = new LoginService();
				string token = account.Properties[ServiceConstants.ACCESS_TOKEN];
				await loginService.loginWithTokenAsync(token);

				AccountStore.Create().Save(account, AppSettings.AppSignInName);
				TokenHolder.SetExpirationInSeconds(Int32.Parse(account.Properties[ServiceConstants.EXPIRES_IN]));

                if (Xamarin.Forms.Device.RuntimePlatform == Xamarin.Forms.Device.Android)
                {
                    DependencyService.Get<IAppMethods>().RestartApp();
                }
                else
                {
                    //await navigationPage.Navigation.PopModalAsync();

                    var localNavPage = new NavigationPage(new HomePage());
                    MasterDetailPage = createHamburgerMenu(localNavPage);
                    Application.Current.MainPage = MasterDetailPage;
                    localNavPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                    localNavPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                }

				return token;
			}
			catch (AuthenticationError e)
			{
				await navigationPage.Navigation.PopModalAsync();
				await navigationPage.Navigation.PushModalAsync(new LoginFail(e.Message));
			}

			return String.Empty;
		}


		/**
		 * Para usar desde IOS (integrado con nuevo SDK de Sign-in de Google)
		*/
		public static async Task<string> SaveTokenAsync(string token, string refreshToken, int expirationInSeconds)
		{
			Account account = new Account();
			account.Properties[ServiceConstants.ACCESS_TOKEN] = token;
			account.Properties[ServiceConstants.EXPIRES_IN] = expirationInSeconds.ToString();
			account.Properties[ServiceConstants.REFRESH_TOKEN] = refreshToken;

            DateTime expirationDate = DateTime.Now.AddSeconds(expirationInSeconds);

			// Si se refresco el Token, lo guardo para que lo use la impresion
			DependencyService.Get<IAppGroupSettings>().Set(ServiceConstants.ACCESS_TOKEN,
				token);
			DependencyService.Get<IAppGroupSettings>().Set(ServiceConstants.ACCESS_TOKEN_EXPIRATION,
				expirationDate.ToString());
			DependencyService.Get<IAppGroupSettings>().Set(ServiceConstants.REFRESH_TOKEN,
				refreshToken);

			return await SaveTokenAsync(account).ConfigureAwait(false);

		}


		public static Action SuccessfulLoginAction
		{
			get
			{
				return new Action(() =>
				{
					// reservado para realizar alguna otra accion que no dependa del resultado de SaveToken
				});
			}
		}

		protected override void OnStart()
		{
            MobileCenter.Start(typeof(Analytics),typeof(Crashes));
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
    }
}