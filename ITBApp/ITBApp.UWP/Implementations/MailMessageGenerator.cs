﻿using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(ITBApp.UWP.Implementations.MailMessageGenerator))]
namespace ITBApp.UWP.Implementations
{
    public class MailMessageGenerator : IMailMessageGenerator
    {
        public string CreateMailMessage(string from, string to, byte[] attachment = null, string fileName = null, string mediaType = null, string mediaSubType = null)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("", from));
            message.To.Add(new MailboxAddress("", to));
            message.Subject = String.Format("Se envió a imprimir {0}", fileName);

            var body = new TextPart("plain");

            if (attachment != null)
            {
                using (MemoryStream ms = new MemoryStream(attachment))
                {

                    // create an image attachment!
                    var mailAttachment = new MimePart(mediaType, mediaSubType)
                    {
                        ContentObject = new ContentObject(ms, ContentEncoding.Default),
                        ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                        ContentTransferEncoding = ContentEncoding.Base64,
                        FileName = fileName,
                        IsAttachment = true,
                        ContentId = Guid.NewGuid().ToString()
                    };

                    // now create the multipart/mixed container to hold the message text and the
                    // image attachment
                    var multipart = new Multipart("mixed");
                    multipart.Add(body);
                    multipart.Add(mailAttachment);

                    // now set the multipart/mixed as the message body
                    message.Body = multipart;

                    return message.ToString();
                }
            }

            return message.ToString();
        }
    }
}
