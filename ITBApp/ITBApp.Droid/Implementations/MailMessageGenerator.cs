﻿using System;
using System.IO;
using System.Net.Mail;
using MimeKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(ITBApp.Droid.Implementations.MailMessageGenerator))]
namespace ITBApp.Droid.Implementations
{
	public class MailMessageGenerator : IMailMessageGenerator
	{
        private const string PrintSentMessage = "Se envió a imprimir {0}";
        private const string TextPartFormat = "plain";
        private const string MultiPartFormat = "mixed";

		public MailMessageGenerator()
		{
		}

		public string CreateMailMessage(string from, string to, byte[] attachment = null, string fileName = null, string mediaType = null, string mediaSubType = null)
		{
			var message = new MimeMessage();
			message.From.Add(new MailboxAddress("", from));
			message.To.Add(new MailboxAddress("", to));
			message.Subject = String.Format(PrintSentMessage, fileName);

			var body = new TextPart(TextPartFormat);

			if (attachment != null)
			{
				using (MemoryStream ms = new MemoryStream(attachment))
				{

					// create an image attachment!
					var mailAttachment = new MimePart(mediaType, mediaSubType)
					{
						ContentObject = new ContentObject(ms, ContentEncoding.Default),
						ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
						ContentTransferEncoding = ContentEncoding.Base64,
						FileName = fileName,
						IsAttachment = true,
						ContentId = Guid.NewGuid().ToString()
					};

					// now create the multipart/mixed container to hold the message text and the
					// image attachment
					var multipart = new Multipart(MultiPartFormat);
					multipart.Add(body);
					multipart.Add(mailAttachment);

					// now set the multipart/mixed as the message body
					message.Body = multipart;

					return message.ToString();
				}
			}

			return message.ToString();
		}
	}
}
