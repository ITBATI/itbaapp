﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel
{
	[DataContract]
	[KnownType(typeof(ExamData))]
	public class PendingExamsData
	{
		[DataMember]
		public List<ExamData> Exams { get; set; }

		[DataMember]
		public DateTime Expiration { get; set; }
	}
}
