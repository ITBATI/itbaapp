﻿using System;
using System.Collections.Generic;
using ITBApp.ViewModels;
using Plugin.Connectivity;
using Xamarin.Forms;
using ITBApp.Services;

namespace ITBApp.UI
{
    public partial class EventsPage : ContentPage
    {
        private static bool isTapped = false;
        internal static EventsViewModel ViewModel { get; set; }

        public EventsPage()
        {
            InitializeComponent();

            ((BaseModel)BindingContext).Initialize(this);
            ConfigureEventListeners();
            ViewModel = (EventsViewModel)BindingContext;

            if (DateTime.Today.Month != DateTime.Today.AddDays(ServiceConstants.EVENTS_DAYS_SHOWN-1).Month)
            {
                showSecondMonth = true;
            }
            RefreshMonthName();
		}

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}

		void ConfigureEventListeners()
		{
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
                if (BindingContext != null && App.CurrentPage == Helpers.PageId.EVENTS && args.IsConnected)
				{
                    ((EventsViewModel)BindingContext).LoadDataAsync().ConfigureAwait(false);
				}
			};
		}

        public async static void NavigateToDetail(EventsPage instance, object eventId)
		{
            if (Device.RuntimePlatform == Device.UWP)
			{
                List<EventViewModelData> eventList = ((EventsViewModel)instance.BindingContext).GetEventsFromId(eventId.ToString());
                NavigationPage eventDetailPage = new NavigationPage(new EventDetailPage(eventList));
				eventDetailPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
				eventDetailPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

				App.MasterDetailPage.Detail = eventDetailPage;
			}
			else if (!isTapped)
			{
				isTapped = true;
				List<EventViewModelData> eventList = ((EventsViewModel)instance.BindingContext).GetEventsFromId(eventId.ToString());
				await instance.Navigation.PushAsync(new EventDetailPage(eventList));
				isTapped = false;
			}
		}

        private bool showFirstMonth = true;
        private bool showSecondMonth = false;

        private void RefreshMonthName()
        {
            int nextMonth = DateTime.Today.Month == 12 ? 1 : DateTime.Today.Month;
            lblMonthName.Text = showSecondMonth ? ((EventsViewModel)BindingContext).MonthNames[nextMonth].ToUpper() : string.Empty;
            lblMonthName.Text += showFirstMonth && showSecondMonth ? " / " : string.Empty;
            lblMonthName.Text += showFirstMonth ? ((EventsViewModel)BindingContext).MonthNames[DateTime.Today.Month - 1].ToUpper() : string.Empty;
            int year = DateTime.Today.Year;
            lblMonthName.Text += showFirstMonth ^ showSecondMonth ? " " + year.ToString():string.Empty;
            if (showFirstMonth && !showSecondMonth)
            {
                lblMonthName.Margin = GetMarginForMonth(DateTime.Today.Month);
            }
            else if (!showFirstMonth && showSecondMonth)
            {
                lblMonthName.Margin = GetMarginForMonth(nextMonth + 1);
            }
            else
            {
                lblMonthName.Margin = GetMarginForMonthCombination(nextMonth);
            }
        }

        private Thickness GetMarginForMonth(int month)
		{
			if (month == 1) return new Thickness(-34, 0, 0, 0);
			else if (month == 2) return new Thickness(-44, 0, 0, 0);
			else if (month == 3) return new Thickness(-36, 0, 0, 0);
			else if (month == 4) return new Thickness(-29, 0, 0, 0);
			else if (month == 5) return new Thickness(-30, 0, 0, 0);
			else if (month == 6) return new Thickness(-29, 0, 0, 0);
			else if (month == 7) return new Thickness(-29, 0, 0, 0);
			else if (month == 8) return new Thickness(-40, 0, 0, 0);
			else if (month == 9) return new Thickness(-59, 0, 0, 0);
			else if (month == 10) return new Thickness(-44, 0, 0, 0);
			else if (month == 11) return new Thickness(-55, 0, 0, 0);

			return new Thickness(-50, 0, 0, 0);
		}

		private Thickness GetMarginForMonthCombination(int month)
		{
			if (month == 1) return new Thickness(-56, 0, 0, 0);
			else if (month == 2) return new Thickness(-58, 0, 0, 0);
			else if (month == 3) return new Thickness(-46, 0, 0, 0);
			else if (month == 4) return new Thickness(-40, 0, 0, 0);
			else if (month == 5) return new Thickness(-40, 0, 0, 0);
			else if (month == 6) return new Thickness(-42, 0, 0, 0);
			else if (month == 7) return new Thickness(-49, 0, 0, 0);
			else if (month == 8) return new Thickness(-77, 0, 0, 0);
			else if (month == 9) return new Thickness(-81, 0, 0, 0);
			else if (month == 10) return new Thickness(-80, 0, 0, 0);
			else if (month == 11) return new Thickness(-86, 0, 0, 0);

			return new Thickness(-65, 0, 0, 0);
		}
    }
}
