using ITBApp.Model.DataModel;
using ITBApp.Model.Interfaces;
using ITBApp.Resources.i18n;
using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using ITBApp.ViewModels;
using ITBApp.Services;
using Plugin.Connectivity;

namespace ITBApp
{
	public partial class ClassesByDay : ContentPage
    {

		private int pageCounter;
		private static readonly int PAGE_LIMIT = 6;
		private bool doneLoading;
		private static bool isTapped = false;

        public ClassesByDay()
        {
            InitializeComponent();
            Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();

			((BaseModel)BindingContext).Initialize(this);

			ConfigureEventListeners();

			LoadPage(false);
        }

		void LoadPage(bool forceLoad)
		{
			if (forceLoad)
			{
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
				((ClassesByDayViewModel)BindingContext).LoadDataAsync();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

			}
			else 
			{
				pageCounter = 1;
				doneLoading = false;
			}


		}

		void ConfigureEventListeners()
		{
			cvDays.ItemSelected += (sender, args) =>
				{
					SetIndicator(Color.White);
				};

			cvDays.PropertyChanged += (sender, args) =>
			{
				if (!doneLoading)
				{
					pageCounter++;
					if (pageCounter >= PAGE_LIMIT)
					{
						doneLoading = true;
						SetCurrentPosition();
					}
				}
			};

			btnMonday.Clicked += (sender, args) =>
			{
				cvDays.Position = 0;
			};
			btnTuesday.Clicked += (sender, args) =>
			{
				cvDays.Position = 1;
			};
			btnWednesday.Clicked += (sender, args) =>
			{
				cvDays.Position = 2;
			};
			btnThursday.Clicked += (sender, args) =>
			{
				cvDays.Position = 3;
			};
			btnFriday.Clicked += (sender, args) =>
			{
				cvDays.Position = 4;
			};
			btnSaturday.Clicked += (sender, args) =>
			{
				cvDays.Position = 5;
			};

			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
				if (args.IsConnected)
				{
					if (cvDays.ItemsSource == null || !cvDays.ItemsSource.Cast<object>().Any())
					{
						LoadPage(true);
					}
				}
			};
		}

		private void SetCurrentPosition()
		{
			if (cvDays.ItemsSource != null && cvDays.ItemsSource.Cast<object>().Any())
			{
				cvDays.Position = CurrentPosition;
			}

			SetIndicator(Color.White);
		}

        private void SetIndicator(Color color)
        {
            btnMonday.BackgroundColor = Color.Transparent;
            btnTuesday.BackgroundColor = Color.Transparent;
            btnWednesday.BackgroundColor = Color.Transparent;
            btnThursday.BackgroundColor = Color.Transparent;
            btnFriday.BackgroundColor = Color.Transparent;
            btnSaturday.BackgroundColor = Color.Transparent;

			int position = CurrentPosition;

			if (cvDays.ItemsSource != null && cvDays.ItemsSource.Cast<object>().Any())
			{
				position = cvDays.Position;
			}

            if (position == 0) btnMonday.BackgroundColor = color;
            else if (position == 1) btnTuesday.BackgroundColor = color;
            else if (position == 2) btnWednesday.BackgroundColor = color;
            else if (position == 3) btnThursday.BackgroundColor = color;
            else if (position == 4) btnFriday.BackgroundColor = color;
            else if (position == 5) btnSaturday.BackgroundColor = color;
        }

		int CurrentPosition
		{
			get
			{
				return (int)DateTime.Today.DayOfWeek > 0 ? (int)DateTime.Today.DayOfWeek - 1 : 0;
			}
		}

		public async static void NavigateToCommissions(ClassesByDay instance, object courseData)
		{
			if (!isTapped)
			{
				isTapped = true;
				CourseData course = (CourseData)courseData;
				ClassesByDayService classesService = new ClassesByDayService();

				List<CommissionData> commissions = classesService.getCommissionsForCourse(course.Id);
				await instance.Navigation.PushAsync(new CommissionsPage(course.Name, commissions, course.CommissionId));
				isTapped = false;
			}
		}

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}

    }
}
