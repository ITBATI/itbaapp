
using ITBApp.Util.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;
using ITBApp.Resources.i18n;
using Plugin.Connectivity;
using System.Collections.Generic;
using Xamarin.Utilities;
using System.Text;
using System.Net;
using ITBApp.Extensions;
using Xamarin.Auth;
using ITBApp.Helpers;
using ITBApp.DataModel;
using ITBApp.Interfaces;

namespace ITBApp.Services
{
    public class LoginService
	{
		private async Task<string> loginCallWithToken(string token)
		{
			var uriBuilder = new UriBuilder(AppSettings.LoginServiceURL + token);
			HttpValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
			uriBuilder.Query = query.ToString();

			using (var client = new HttpClient())
			{
				client.MaxResponseContentBufferSize = 256000;
				var content = new StringContent("");

			    HttpResponseMessage response = await client.PostAsync(uriBuilder.Uri, content).ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    return result;
                }
                else
                {
                    throw new AuthenticationError(Translate.login_error_message);
                }
			}
		}

        public async Task<IDictionary<string, string>> getAccessTokenAndExpireTime(string serverCode)
        {
            var queryValues = new Dictionary<string, string> {
                { "grant_type", "authorization_code" },
                { "code", serverCode },
                { "redirect_uri", AppSettings.AuthenticatorRedirectUrl },
                { "client_id", AppSettings.AuthenticatorClientId },
                { "client_secret", AppSettings.AuthenticatorSecret }
            };

            var query = queryValues.FormEncode();

            var req = WebRequest.Create("https://www.googleapis.com/oauth2/v4/token");
            req.Method = "POST";
            var body = Encoding.UTF8.GetBytes(query);
            //req.ContentLength = body.Length;
            req.ContentType = "application/x-www-form-urlencoded";
            using (var s = WebRequestExtensions.GetRequestStream(req))
            {
                s.Write(body, 0, body.Length);
            }

            return await req.GetResponseAsync().ContinueWith(task =>
             {
                 var text = task.Result.GetResponseText();

                // Parse the response
                var data = text.Contains("{") ? WebEx.JsonDecode(text) : WebEx.FormDecode(text);

                 if (data.ContainsKey("error"))
                 {
                     throw new AuthenticationError("Error authenticating: " + data["error"]);
                 }
                 else if (data.ContainsKey("access_token"))
                 {
                     return data;
                 }
                 else
                 {
                     throw new AuthenticationError("Expected access_token in access token response, but did not receive one.");
                 }
             });
        }

		public async Task<UserData> loginWithTokenAsync(string token)
		{
			try
			{
				string resp = await loginCallWithToken(token).ConfigureAwait(false);
				JObject jsonResponse = JObject.Parse(resp);

				JObject user = JObject.Parse(jsonResponse["user"].ToString());

				if (jsonResponse["Resultado"].ToString().Equals("1"))
				{
					throw new AuthenticationError(string.Join(".", JsonHelper.GetAsArray(user["errors"])));
				}

				UserData userData = new UserData()
				{
					CardId = user["cardId"].ToString(),
					LastName = user["last-name"].ToString(),
					FirstName = user["first-name"].ToString(),
					DNI = user["dni"].ToString(),
					Email = user["email"].ToString(),
					Gender = ValidationHelper.GetUserGenderFromString(user["gender"].ToString()),
					Success = true
				};

				await SaveUserDataAsync(userData).ConfigureAwait(false);

				return userData;

			}
			catch (Exception)
			{
				throw new AuthenticationError(Translate.app_genericErrorMessage);
			}
		}

        public async Task<IDictionary<string, string>> RequestRefreshTokenAsync(string refreshToken)
        {
			string clientId = AppSettings.AuthenticatorClientId;
			if (Device.RuntimePlatform == Device.iOS) clientId = AppSettings.AuthenticatorClientIdIos;

			string clientSecret = AppSettings.AuthenticatorSecret;
            if (Device.RuntimePlatform == Device.iOS) clientSecret = String.Empty;

            var queryValues = new Dictionary<string, string>
            {
                { "refresh_token", refreshToken },
                { "client_id", clientId },
                { "grant_type", "refresh_token" },
                { "client_secret", clientSecret }
            };

            var query = queryValues.FormEncode();

            var req = WebRequest.Create("https://accounts.google.com/o/oauth2/token");
            req.Method = "POST";
            var body = Encoding.UTF8.GetBytes(query);
            //req.ContentLength = body.Length;
            req.ContentType = "application/x-www-form-urlencoded";
            using (var s = WebRequestExtensions.GetRequestStream(req))
            {
                s.Write(body, 0, body.Length);
            }

            return await req.GetResponseAsync().ContinueWith(task =>
             {
                 var text = task.Result.GetResponseText();

                 // Parse the response
                 var data = text.Contains("{") ? WebEx.JsonDecode(text) : WebEx.FormDecode(text);

                 if (data.ContainsKey("error"))
                 {
                     throw new AuthenticationError("Error authenticating: " + data["error"]);
                 }
                 else if (data.ContainsKey("access_token"))
                 {
                     return data;
                 }
                 else
                 {
                     throw new AuthenticationError("Expected access_token in access token response, but did not receive one.");
                 }
             });
        }

        public void ClearUserData()
		{
            var firebaseToken = DependencyService.Get<IAppGroupSettings>().Get("firebase_token");

            var notificationLastIdFromSettings = DependencyService.Get<IAppGroupSettings>().Get(ServiceConstants.NOTIFICATIONS_LAST_ID);
            string notificationsLastId = notificationLastIdFromSettings!=null?notificationLastIdFromSettings.ToString():null;

            DependencyService.Get<IAppGroupSettings>().Clear();
            App.Current.Properties.Clear();

            DependencyService.Get<IAppGroupSettings>().Set(ServiceConstants.NOTIFICATION_TOKEN_CHANGED, "true");
            if (notificationsLastId != null)
                DependencyService.Get<IAppGroupSettings>().Set(ServiceConstants.NOTIFICATIONS_LAST_ID, notificationsLastId);

            if (firebaseToken != null)
                DependencyService.Get<IAppGroupSettings>().Set("firebase_token", firebaseToken.ToString());

            App.Current.Properties[ServiceConstants.APP_VERSION] = DependencyService.Get<IAppMethods>().getAppVersion();
		}


		public UserData GetSavedUserData()
		{
			if (Application.Current.Properties.ContainsKey(ServiceConstants.USER_DATA))
			{
				string json = (string)Application.Current.Properties[ServiceConstants.USER_DATA];
				UserData data = (UserData)SerializerHelper.Deserialize(json, (new UserData()).GetType());
				return data;
			}

			return null;
		}



		public async Task<UserData> GetUserDataAsync()
		{
			UserData savedUser = GetSavedUserData();

			if (savedUser == null && CrossConnectivity.Current.IsConnected)
			{
				System.Diagnostics.Debug.WriteLine("No existen datos de usuario guardados - Se llama nuevamente a servicio de login");

				TokenHolder tokenHolder = new TokenHolder();
				string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);

				savedUser = await loginWithTokenAsync(token).ConfigureAwait(false);
				await SaveUserDataAsync(savedUser).ConfigureAwait(false);
			}

			return savedUser;
		}

		public async Task<UserData> SaveUserDataAsync(UserData userData)
		{
			string json = SerializerHelper.Serialize(userData);
			Application.Current.Properties[ServiceConstants.USER_DATA] = json;
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);

			return userData;
		}
	}
}
