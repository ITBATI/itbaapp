﻿using ITBApp;
using ITBApp.Extensions;
using ITBApp.Services;
using ITBApp.UWP.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Navigation;
using Xamarin.Auth;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(GoogleLoginPage), typeof(GoogleLoginPageRenderer))]
namespace ITBApp.UWP.Implementations
{
    public class GoogleLoginPageRenderer : PageRenderer
    {
        private Xamarin.Auth.Authenticator _authenticator;
        private Windows.UI.Xaml.Controls.Frame _frame;

        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (_frame != null)
                    {
                        _frame.NavigationFailed -= Frame_NavigationFailed;
                        _frame.Navigated -= Frame_Navigated;
                        _frame.Navigating -= Frame_Navigating;
                        _frame.NavigationStopped -= Frame_NavigationStopped;
                        _frame = null;
                    }

                    if (_authenticator != null)
                    {
                        _authenticator = null;
                    }

                }
            }
            catch (Exception ex)
            {
                //CommonInfrastructure.InsightsWrapper.SilentlyReportExceptionAsync(ex); // we don't await this one
            }

            base.Dispose(disposing);
        }

        protected override async void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Page> e)
        {
            try
            {
                base.OnElementChanged(e);

                System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: OnElementChanged");

                if (e == null)
                    System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: e = {null}");
                else
                {
                    if (e.NewElement == null)
                        System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: e.NewElement = {null}");
                    if (e.OldElement == null)
                        System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: e.OldElement = {null}");
                }

                if (Element == null)
                    System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: Element is {null}");
                else
                    System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: Element is " + Element);

                if (Control == null)
                    System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: Control is {null}");
                else
                    System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: Control is " + Control);

                if (Control == null)
                {
                    WindowsPage windowsPage = new WindowsPage();

                    _frame = windowsPage.Frame;
                    if (_frame == null)
                    {
                        _frame = new Windows.UI.Xaml.Controls.Frame
                        {
                            Language = global::Windows.Globalization.ApplicationLanguages.Languages[0]
                        };

                        _frame.NavigationFailed += Frame_NavigationFailed;
                        _frame.Navigated += Frame_Navigated;
                        _frame.Navigating += Frame_Navigating;
                        _frame.NavigationStopped += Frame_NavigationStopped;

                        windowsPage.Content = _frame;
                        SetNativeControl(windowsPage);

                        //Window.Current.Content = frame; // This line makes things go pear-shaped
                    }

                    _authenticator = AuthenticatorFactory.GetAuthenticator();
                    
                    Type pageType = _authenticator.GetUI();
                    _frame.Navigate(pageType, _authenticator);
                    Window.Current.Activate();
                }
            }
            catch (Exception ex)
            {
                //await CommonInfrastructure.InsightsWrapper.SilentlyReportExceptionAsync(ex);
            }
        }
        
        private void Frame_NavigationStopped(object sender, Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: In UWP Frame_Stopped");
        }

        private void Frame_Navigating(object sender, Windows.UI.Xaml.Navigation.NavigatingCancelEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: In UWP Frame_Navigating");

            switch (e.NavigationMode)
            {
                case NavigationMode.Back:
                    System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: NavigationMode = Back");
                    break;
                case NavigationMode.Forward:
                    System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: NavigationMode = Forward");
                    break;
                case NavigationMode.New:
                    System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: NavigationMode = New");
                    break;
                case NavigationMode.Refresh:
                    System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: NavigationMode = Refresh");
                    break;
            }
            System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: Cancel = " + e.Cancel);
            System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: SourcePageType = " + e.SourcePageType.ToString());
        }

        private void Frame_Navigated(object sender, Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: In UWP Frame_Navigated");
        }

        private async void Frame_NavigationFailed(object sender, Windows.UI.Xaml.Navigation.NavigationFailedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("AUTHPAGERENDERER: In UWP Frame_NavigationFailed");
            /*
            await CommonInfrastructure.InsightsWrapper.ReportExceptionAsync(
                new Exception(
                    string.Format("Navigation failed. FullName = {0}",
                        e.SourcePageType.FullName)));
                        */
        }
    }
}
