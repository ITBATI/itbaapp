﻿//-----------------------------------------------------------------------
// <copyright file="AcademicHistory.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using Xamarin.Forms;

namespace ITBApp.UI
{
    using System;
    using System.Linq;
    using ITBApp.Helpers;
    using ITBApp.ViewModels;
    using Plugin.Connectivity;

    public partial class AcademicHistory : ContentPage
    {
        private static bool isTapped = false;

        public AcademicHistory()
        {
            InitializeComponent();

            ((BaseModel)BindingContext).Initialize(this);

            if (Device.RuntimePlatform == Device.UWP)
            {
                ToolbarItems.Add(new ToolbarItem("Volver", "pop_arrow_left.png", () =>
                {
                    NavigationPage profilePage = new NavigationPage(new UserProfile());
                    profilePage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                    profilePage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                    App.MasterDetailPage.Detail = profilePage;
                }));
            }

            ConfigureEventListeners();
        }

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}

		void ConfigureEventListeners()
		{

			((AcademicHistoryViewModel)BindingContext).ShowSubjectsCommand = new Command((id) =>
			{
				if (!isTapped)
				{
					isTapped = true;

					QuarterData quarter = ((AcademicHistoryViewModel)BindingContext).Quarters.Where(x => x.Id.Equals(id)).FirstOrDefault();
					quarter.Collapsed = false;

                    ((AcademicHistoryViewModel)BindingContext).RefreshItems();

					isTapped = false;
				}
			});

			((AcademicHistoryViewModel)BindingContext).HideSubjectsCommand = new Command((id) =>
			{
				if (!isTapped)
				{
					isTapped = true;

					QuarterData quarter = ((AcademicHistoryViewModel)BindingContext).Quarters.Where(x => x.Id.Equals(id)).FirstOrDefault();
					quarter.Collapsed = true;

					((AcademicHistoryViewModel)BindingContext).RefreshItems();

					isTapped = false;
				}
			});

			((AcademicHistoryViewModel)BindingContext).DetailCommand = new Command(async (subject) =>
			{
				if (!isTapped)
				{
					isTapped = true;

                    if (Device.RuntimePlatform == Device.UWP)
					{
						NavigationPage detailPage = new NavigationPage(new AcademicHistoryDetail((SubjectData)subject));
						detailPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
						detailPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

						App.MasterDetailPage.Detail = detailPage;
					}
					else
						await Navigation.PushAsync(new AcademicHistoryDetail((SubjectData)subject));

					isTapped = false;
				}
			});

			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
                if (BindingContext != null && App.CurrentPage == PageId.ACADEMIC_HISTORY && args.IsConnected)
				{
					((AcademicHistoryViewModel)BindingContext).LoadDataAsync().ConfigureAwait(false);
                    //lvHistory.ItemsSource = ((AcademicHistoryViewModel)BindingContext).Items;
				}
			};
		}
    }
}
