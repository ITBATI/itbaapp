﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.Content;
using Android.Preferences;
using Android.Provider;
using Android.Widget;
using ITBApp.Interfaces;
using WindowsAzure.Messaging;
using Xamarin.Forms;

[assembly: Dependency(typeof(ITBApp.Droid.Implementations.AppMethods))]
namespace ITBApp.Droid.Implementations
{
	public class AppMethods : IAppMethods
	{
		public void AddPhoneContact(string name, string phoneNumber, string phoneLabel)
		{
			List<ContentProviderOperation> ops = new List<ContentProviderOperation>();
			int rawContactInsertIndex = ops.Count;

			ContentProviderOperation.Builder builder =
				ContentProviderOperation.NewInsert(ContactsContract.RawContacts.ContentUri);
			builder.WithValue(ContactsContract.RawContacts.InterfaceConsts.AccountType, null);
			builder.WithValue(ContactsContract.RawContacts.InterfaceConsts.AccountName, null);
			ops.Add(builder.Build());

			//Name
			builder = ContentProviderOperation.NewInsert(ContactsContract.Data.ContentUri);
			builder.WithValueBackReference(ContactsContract.Data.InterfaceConsts.RawContactId, rawContactInsertIndex);
			builder.WithValue(ContactsContract.Data.InterfaceConsts.Mimetype,
				ContactsContract.CommonDataKinds.StructuredName.ContentItemType);
			builder.WithValue(ContactsContract.CommonDataKinds.StructuredName.DisplayName, name);
			builder.WithValue(ContactsContract.CommonDataKinds.StructuredName.GivenName, name);
			ops.Add(builder.Build());

			//Number
			builder = ContentProviderOperation.NewInsert(ContactsContract.Data.ContentUri);
			builder.WithValueBackReference(ContactsContract.Data.InterfaceConsts.RawContactId, rawContactInsertIndex);
			builder.WithValue(ContactsContract.Data.InterfaceConsts.Mimetype,
				ContactsContract.CommonDataKinds.Phone.ContentItemType);
			builder.WithValue(ContactsContract.CommonDataKinds.Phone.Number, phoneNumber);
			builder.WithValue(ContactsContract.CommonDataKinds.StructuredPostal.InterfaceConsts.Type,
					ContactsContract.CommonDataKinds.StructuredPostal.InterfaceConsts.TypeCustom);
			builder.WithValue(ContactsContract.CommonDataKinds.Phone.InterfaceConsts.Label, phoneLabel);
			ops.Add(builder.Build());

			try
			{
				var res = Forms.Context.ContentResolver.ApplyBatch(ContactsContract.Authority, ops);

				Toast.MakeText(Forms.Context, ITBApp.Helpers.TranslationHelper.GetNewContactsSuccesful(name), ToastLength.Short).Show();
			}
			catch
			{
				Toast.MakeText(Forms.Context, ITBApp.Helpers.TranslationHelper.GetNewContactsError(name), ToastLength.Long).Show();

			}
		}

		public void CloseApp()
		{
			Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
		}

		public string getAppVersion()
		{
			var context = Android.App.Application.Context;
			return context.PackageManager.GetPackageInfo(context.PackageName, 0).VersionName;
		}

		public void showQuickMessage(string message)
		{
			Toast.MakeText(Forms.Context, message, ToastLength.Long).Show();
		}

		public async void RegisterOnNotificationHubAsync(string token, string tag)
		{
			NotificationHub hub = new NotificationHub(AppSettings.NotificationHubName, AppSettings.ListenConnectionString,
													  Android.App.Application.Context);
			await Task.Run(() =>
			{
				try
				{
					hub.UnregisterAll(token);
					string[] tags = new string[] { tag };
					hub.Register(token, tags);
				}
				catch
				{ }
			});
		}

		public void UnregisterFromHub(string token)
		{
			NotificationHub hub = new NotificationHub(AppSettings.NotificationHubName, AppSettings.ListenConnectionString,
													  Android.App.Application.Context);
			hub.UnregisterAll(token);

		}

        public void RestartApp()
        {
            Intent i = Android.App.Application.Context.PackageManager.GetLaunchIntentForPackage(Android.App.Application.Context.PackageName);
			i.AddFlags(ActivityFlags.ClearTop | ActivityFlags.NewTask | ActivityFlags.ClearTask | ActivityFlags.SingleTop);
            Android.App.Application.Context.StartActivity(i);
        }

        public int GetHeight()
        {
            return ConvertPixelsToDp(Android.Content.Res.Resources.System.DisplayMetrics.HeightPixels);
        }

        public int GetWidth()
        {
            return ConvertPixelsToDp(Android.Content.Res.Resources.System.DisplayMetrics.WidthPixels);
        }

        private int ConvertPixelsToDp(float pixelValue)
        {
            var dp = (int)((pixelValue) / Android.Content.Res.Resources.System.DisplayMetrics.Density);
            return dp;
        }

        public int GetHeightInPixels()
        {
            return Android.Content.Res.Resources.System.DisplayMetrics.HeightPixels;
        }
    }
}