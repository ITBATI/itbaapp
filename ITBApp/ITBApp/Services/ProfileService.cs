﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using ITBApp.DataModel;
using ITBApp.Handlers;
using ITBApp.Helpers;
using ITBApp.Model.DataModel;
using ITBApp.Model.Handlers;
using ITBApp.Resources.i18n;
using ITBApp.Util.Helpers;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.Services
{
	public class ProfileService
	{
		private TokenHolder tokenHolder = new TokenHolder();

		public async Task<UserProfileData> getUserProfileData()
		{
			UserProfileData userProfileData = GetSavedUserProfleData();

			if (userProfileData == null)
			{
				if (!CrossConnectivity.Current.IsConnected)
				{
					throw new ServerRequestException(Translate.profile_no_internet_text);
				}

				try
				{
					string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
					userProfileData = await getUserProfileAsync(token).ConfigureAwait(false);

					userProfileData.Expiration = DateTime.Now.AddDays(ServiceConstants.USER_PROFILE_EXPIRATION_IN_DAYS);

					string json = SerializerHelper.Serialize(userProfileData);
					Application.Current.Properties[ServiceConstants.USER_PROFILE_DATA] = json;
					await Application.Current.SavePropertiesAsync().ConfigureAwait(false);

				}
				catch (TokenExpiredException)
				{
					throw new ServerRequestException(Translate.profile_no_internet_text);
				}

			}

			return userProfileData;
		}

		private UserProfileData GetSavedUserProfleData()
		{
			if (Application.Current.Properties.ContainsKey(ServiceConstants.USER_PROFILE_DATA))
			{
				string json = (string)Application.Current.Properties[ServiceConstants.USER_PROFILE_DATA];
				UserProfileData data = (UserProfileData)SerializerHelper.Deserialize(json, (new UserProfileData()).GetType());

				if (data.Expiration > DateTime.Now || !CrossConnectivity.Current.IsConnected)
				{
					return data;
				}
			}

			return null;
		}

		private async Task<UserProfileData> getUserProfileAsync(string token)
		{
			string stringResponse = await makeCallAsync(token).ConfigureAwait(false);
			UserProfileData data = processResponse(stringResponse);

			return data;
		}

		private UserProfileData processResponse(string stringResponse)
		{
			try
			{
				JToken jsonResponse = JToken.Parse(stringResponse);
				JToken studentPerformance = jsonResponse["studentCareerPerformance"];

				UserProfileData userProfileData = new UserProfileData();

				UserData userData = new LoginService().GetSavedUserData();

				userProfileData.Name = userData.FullName;
				userProfileData.CourseOfStudy = studentPerformance["careerName"].ToString();
				userProfileData.CourseProgress = studentPerformance["careerPercentage"] + "%";
				userProfileData.CourseProgressPercentage = double.Parse(studentPerformance["careerPercentage"].ToString()) / 100;
				userProfileData.CurrentCredits = studentPerformance["inCourseCredits"].ToString();
				userProfileData.Status = UserProfileData.getStatusTranslation(studentPerformance["studentState"].ToString());
				if (userData.Gender == UserGender.Female)
                    userProfileData.ImagePath = "https://i.imgur.com/qThz1mv.png";
				else
                    userProfileData.ImagePath = "https://i.imgur.com/yPS1jqY.png";
				userProfileData.ImpressionShare = "?";
				userProfileData.TimeInUniversity = "40";
				userProfileData.Average = ValidationHelper.FormatDoubleNumber(studentPerformance["performance"].ToString(), 2);
				userProfileData.LinealAverage = ValidationHelper.FormatDoubleNumber(studentPerformance["linearPerformance"].ToString(), 2);
				return userProfileData;
			}
			catch (Exception)
			{
				throw new ServerRequestException("Error al procesar la información del perfil de usuario");
			}
		}

		private async Task<string> makeCallAsync(string token)
		{
			var uriBuilder = new UriBuilder(AppSettings.ProfileServiceURL + token);
			HttpValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
			uriBuilder.Query = query.ToString();

			using (var client = new HttpClient())
			{
				client.MaxResponseContentBufferSize = 256000;
				HttpResponseMessage response = await client.GetAsync(uriBuilder.Uri).ConfigureAwait(false);
				if (response.IsSuccessStatusCode)
				{
					string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					return result;

				}
				else
				{
					throw new ServerRequestException("Error de conexión al servicio de perfil de usuario");
				}
			}
		}
	}
}
