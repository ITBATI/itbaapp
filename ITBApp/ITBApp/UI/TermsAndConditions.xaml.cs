﻿
using System.Threading.Tasks;
using ITBApp.Helpers;
using Xamarin.Forms;

namespace ITBApp
{
	public partial class TermsAndConditions : ContentPage
	{
		public TermsAndConditions()
		{
			InitializeComponent();
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			this.BindingContext = null;
			this.Content = null;

            App.HardwareBackPressed = null;
		}

        protected override void OnAppearing()
        {
        	base.OnAppearing();

        	App.HardwareBackPressed = () =>
        	{
        		MainMenuItem item = MainMenuItem.HomeItem;
        		App.CurrentPage = item.Id;
        		item.NavigationPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
        		item.NavigationPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
        		App.MasterDetailPage.Detail = item.NavigationPage;
        		return Task.FromResult<bool?>(false);
        	};
        }
	}
}
