﻿namespace ITBApp.ViewModels
{
    using System.Threading.Tasks;
    using ITBApp.Helpers;
    using ITBApp.Interfaces;
    using ITBApp.Resources.i18n;
    using Xamarin.Forms;

    /// <summary>
    /// Help view model.
    /// </summary>
    internal class HelpViewModel : BaseModel
	{
		/// <summary>
		/// The display ayuda image.
		/// </summary>
		private bool displayAyudaImage;

		/// <summary>
		/// The help text margin.
		/// </summary>
		private string helpTextMargin;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:ITBApp.HelpViewModel"/> class.
		/// </summary>
		public HelpViewModel()
		{
			Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
			var hasHightResoultion = UIHelper.HasMediumResolution() || UIHelper.HasHightResoultion();

			DisplayAyudaImage = hasHightResoultion;
			HelpTextMargin = hasHightResoultion ? "40,0" : "20,0";
		}

		/// <summary>
		/// Gets a value indicating whether this <see cref="T:ITBApp.HelpViewModel"/> display ayuda image.
		/// </summary>
		/// <value><c>true</c> if display ayuda image; otherwise, <c>false</c>.</value>
		public bool DisplayAyudaImage
		{
			get
			{
				return this.displayAyudaImage;
			}

			private set
			{
                this.displayAyudaImage = value;
				this.OnPropertyChanged("DisplayAyudaImage");
			}
		}

		/// <summary>
		/// Gets the help text margin.
		/// </summary>
		/// <value>The help text margin.</value>
		public string HelpTextMargin
		{
			get
			{
				return this.helpTextMargin;
			}

			private set
			{
                this.helpTextMargin = value;
				this.OnPropertyChanged("HelpTextMargin");
			}
		}

		/// <summary>
		/// Loads the data async.
		/// </summary>
		/// <returns>The data async.</returns>
		public override async Task<string> LoadDataAsync()
		{
			return null;
		}

	}
}