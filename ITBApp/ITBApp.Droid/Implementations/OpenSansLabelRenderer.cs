using Xamarin.Forms;
using ITBApp.Droid.Implementations;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using Java.Lang;

[assembly: ExportRenderer(typeof(Xamarin.Forms.Label), typeof(OpenSansLabelRenderer))]
namespace ITBApp.Droid.Implementations
{
    class OpenSansLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Label> e)
        {
            base.OnElementChanged(e);

            if (!string.IsNullOrEmpty(e.NewElement?.FontFamily))
            {
                try
                {
                    var font = Typeface.CreateFromAsset(Forms.Context.ApplicationContext.Assets, e.NewElement.FontFamily + ".ttf");
                    Control.Typeface = font;
                }
                catch (Exception)
                {
                    // An exception means that the custom font wasn't found.
                    // Typeface.CreateFromAsset throws an exception when it didn't find a matching font.
                    // When it isn't found we simply do nothing, meaning it reverts back to default.
                }
            }
        }
    }
}