* Parametros de AppSettings.cs que deben ser cambiados para que funcione:

// Google
- GoogleApisID: se debe dar de alta una cuenta en Google Api Developer Console (https://console.developers.google.com), y copiar en esta variable el ID de Api
- GoogleAppKey: buscar en las Credenciales la Clave
- AuthenticatorSecret: crear un ID de cliente de Web y copiar la clave de secreto
- AuthenticatorClientId: crear un ID de cliente de Android
- AuthenticatorClientIdIos: crear un ID de cliente de iOS

// Visual Studio Mobile Center
- MobileCenterAppID: se debe dar de alta una cuenta en http://mobile.azure.com y copiar el App ID

// Firebase
- FirebaseSenderID: el ID de aplicacion usado en Firebase (en general coincide con GoogleApisID), cuenta en https://console.firebase.google.com

// Azure
- ListenConnectionString: Abrir cuenta en el portal de azure (http://portal.azure.com), configurar el Hub de Notificaciones y copiar desde "Access Policies" la correspondiente para Listen (empieza con Endpoint=)
- NotificationHubName: Abrir cuenta en el portal de azure (http://portal.azure.com) y copiar el name del Hub