﻿using ITBApp.Interfaces;
using ITBApp.Model;
using Xamarin.Forms;

namespace ITBApp.UI
{
	public partial class LoginFail : ContentPage
	{
		public LoginFail()
		{
			InitializeComponent();
		}

		public LoginFail(string message) : this()
		{
			tryAgainMessage.Text = message;
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			DependencyService.Get<IAppMethods>().CloseApp();
		}
	}
}
