﻿using System;
using System.Collections.Generic;
using ITBApp.Model;
using Xamarin.Forms;

namespace ITBApp
{
	public partial class FreeClassroomsDetail : ContentPage
	{
		public FreeClassroomsDetail(FreeClassroomsData fcd)
		{
			InitializeComponent();

			lvClassrooms.ItemsSource = fcd.occupationNow;
			lblTitleCount.Text = fcd.FreeNowCount.ToString();
		}

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}
	}
}
