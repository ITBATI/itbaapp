﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ITBApp.Helpers;
using ITBApp.Model;
using ITBApp.Model.Handlers;
using ITBApp.Resources.i18n;
using ITBApp.Util;
using ITBApp.Util.Helpers;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.Services
{
	public class AvailableClassroomsService
	{
		private TokenHolder tokenHolder = new TokenHolder();

		public bool IsOpen()
		{
			String currentTime = DateTime.Now.ToString("HH:mm");
			return String.Compare(currentTime, ServiceConstants.CLASSROOMS_AVAILABLE_FROM) >= 0 &&
													String.Compare(currentTime, ServiceConstants.CLASSROOMS_AVAILABLE_TO) <= 0;
		}

		public async Task<AvailableClassroomsData> getFreeClassroomsDataAsync(string locationId)
		{
			AvailableClassroomsData freeClassroomData = GetSavedFreeClassroomsData();

			if (freeClassroomData == null)
			{
				if (!CrossConnectivity.Current.IsConnected)
				{
					throw new ServerRequestException(Translate.freeclassrooms_no_internet_text);
				}

				string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);

                freeClassroomData = await getFreeClassroomsAsync(token, locationId).ConfigureAwait(false);

				// Logic to refresh cached data when time is 00, 15, 30 or 45
				freeClassroomData.Expiration = DateTime.Now.AddMinutes(ServiceConstants.FREE_CLASSROOM_EXPIRATION_IN_MINUTES);

				string json = SerializerHelper.Serialize(freeClassroomData);
				Application.Current.Properties[ServiceConstants.FREE_CLASSROOM_DATA] = json;
				await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
			}

			return freeClassroomData;
		}

		private AvailableClassroomsData GetSavedFreeClassroomsData()
		{
			if (Application.Current.Properties.ContainsKey(ServiceConstants.FREE_CLASSROOM_DATA))
			{
				string json = (string)Application.Current.Properties[ServiceConstants.FREE_CLASSROOM_DATA];
				AvailableClassroomsData data = (AvailableClassroomsData)SerializerHelper.Deserialize(json, (new AvailableClassroomsData()).GetType());

				if (!CrossConnectivity.Current.IsConnected)
				{
					return data;
				}
			}

			return null;
		}

		private async Task<AvailableClassroomsData> getFreeClassroomsAsync(string token, string locationId)
		{
			string stringResponse = await makeCallAsync(token,locationId).ConfigureAwait(false);
			AvailableClassroomsData data = processResponse(stringResponse);

			return data;
		}

		private AvailableClassroomsData processResponse(string stringResponse)
		{
			JToken jsonResponse = JToken.Parse(stringResponse);
			JToken classRoomOccupations = jsonResponse["classRoomOccupations"];

			AvailableClassroomsData freeClassroomsData = new AvailableClassroomsData();

			freeClassroomsData.freeNow = new ObservableCollection<OccupationData>();
			freeClassroomsData.freeSoon = new ObservableCollection<OccupationData>();
			Dictionary<String, List<OccupationData>> occupations = new Dictionary<String, List<OccupationData>>();

			HashSet<String> occupationNow = new HashSet<string>();
			HashSet<String> occupationSoon = new HashSet<string>();

			if (IsOpen())
			{
				foreach (JToken occupation in JsonHelper.GetAsArray(classRoomOccupations["classRoomOccupation"]))
				{
					string classRoom = occupation["classRoom"].ToString();
					if (!occupations.ContainsKey(classRoom))
						occupations.Add(classRoom, new List<OccupationData>());

					OccupationData occupationData = new OccupationData();
					occupationData.Classroom = occupation["classRoom"].ToString();
					if (occupation["usedBy"] != null)
					{
						DateTime startTime = Convert.ToDateTime(occupation["hourFrom"].ToString());
						DateTime endTime = Convert.ToDateTime(occupation["hourTo"].ToString());
						occupationData.StartTime = startTime.ToString("HH:mm");
						occupationData.EndTime = endTime.ToString("HH:mm");
						occupationData.Type = occupation["occupationType"].ToString();
						occupationData.UsedBy = occupation["usedBy"].ToString();
                        occupationData.Building = occupation["building"]!=null?occupation["building"].ToString():string.Empty;

						if (startTime.TimeOfDay <= DateTime.Now.TimeOfDay && endTime.TimeOfDay >= DateTime.Now.TimeOfDay)
						{
							occupationNow.Add(occupationData.Classroom);
						}

						if (startTime.TimeOfDay > DateTime.Now.TimeOfDay &&
							startTime.TimeOfDay < DateTime.Now.AddMinutes(ServiceConstants.FREE_CLASSROOM_SOON_RANGE_IN_MINUTES).TimeOfDay)
						{
							occupationSoon.Add(occupationData.Classroom);
						}
						else if (startTime.TimeOfDay < DateTime.Now.TimeOfDay &&
								endTime.TimeOfDay > DateTime.Now.AddMinutes(ServiceConstants.FREE_CLASSROOM_SOON_RANGE_IN_MINUTES).TimeOfDay)
						{
							occupationSoon.Add(occupationData.Classroom);
						}
					}
					occupations[classRoom].Add(occupationData);
				}

				List<String> sortedClassrooms = occupations.Keys.ToList();
				sortedClassrooms = sortedClassrooms.OrderBy(x => ValidationHelper.PadNumbers(x)).ToList();
				foreach (string classroom in sortedClassrooms)
				{
					string currentTime = DateTime.Now.ToString("HH:mm");
					if (!occupationNow.Contains(classroom))
					{
						string occupationTime = ServiceConstants.CLASSROOMS_AVAILABLE_TO;
						foreach (OccupationData od in occupations[classroom])
						{
							if (od.StartTime == null) occupationTime = ServiceConstants.CLASSROOMS_AVAILABLE_TO;
							else if (String.Compare(od.StartTime, currentTime) > 0 &&
								String.Compare(od.StartTime, occupationTime) < 0)
								occupationTime = od.StartTime;
						}

						freeClassroomsData.freeNow.Add(new OccupationData()
						{
							Classroom = classroom,
							StartTime = currentTime,
							EndTime = occupationTime
						});
					}

					if (!occupationSoon.Contains(classroom))
					{
						string freeEndTime = ServiceConstants.CLASSROOMS_AVAILABLE_TO;
						foreach (OccupationData od in occupations[classroom])
						{
							if (od.StartTime == null) freeEndTime = ServiceConstants.CLASSROOMS_AVAILABLE_TO;
							else if (String.Compare(od.StartTime, currentTime) > 0 &&
								String.Compare(od.StartTime, freeEndTime) < 0)
								freeEndTime = od.StartTime;
						}

						string freeStartTime = freeEndTime;
						foreach (OccupationData od in occupations[classroom])
						{
							if (od.EndTime == null) freeStartTime = ServiceConstants.CLASSROOMS_AVAILABLE_FROM;
							else if (String.Compare(od.EndTime, freeStartTime) < 0)
								freeStartTime = od.EndTime;
						}

						if (freeStartTime == freeEndTime || freeStartTime == ServiceConstants.CLASSROOMS_AVAILABLE_FROM) freeStartTime = Translate.freeclassrooms_now;
						else freeStartTime += Translate.hours_abreviation;

						freeClassroomsData.freeSoon.Add(new OccupationData()
						{
							Classroom = classroom,
							StartTime = freeStartTime,
							EndTime = freeEndTime
						});
					}
				}
			}

			return freeClassroomsData;
		}

		private async Task<string> makeCallAsync(string token, string locationId)
		{
            var uriBuilder = new UriBuilder(AppSettings.AvailableClassroomsServiceURL + token + "/" + locationId);
            HttpValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
			uriBuilder.Query = query.ToString();
			using (var client = new HttpClient())
			{
				client.MaxResponseContentBufferSize = 256000;
				HttpResponseMessage response = await client.GetAsync(uriBuilder.Uri).ConfigureAwait(false);
				if (response.IsSuccessStatusCode)
				{
					string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					return result;
				}
				else
				{
					throw new ServerRequestException("Error de conexión al servicio de aulas vacias");
				}
			}
		}
	}
}
