﻿using System;
namespace ITBApp.Handlers
{
	public class TokenExpiredException : Exception
	{
		public TokenExpiredException() { }

		public TokenExpiredException(string msg) : base(msg) { }

		public TokenExpiredException(string msg, Exception cause) : base(msg, cause) { }
	}
}
