using System.Linq;
using System.Threading.Tasks;
using ITBApp.Helpers;
using ITBApp.Interfaces;
using ITBApp.Listeners;
using ITBApp.Model;
using ITBApp.Resources.i18n;
using ITBApp.Services;
using ITBApp.ViewModels;
using Xamarin.Auth;
using Xamarin.Forms;

namespace ITBApp.UI
{
	public partial class MenuPage : ContentPage
	{
		private Color selectionColor = Color.FromHex("#DC1456").MultiplyAlpha(0.5);

		public MenuPage()
		{
			InitializeComponent();

			((BaseModel)BindingContext).Initialize(this);

			MainMenuItem.ClassesByDayItem.ViewItem = slSchedule;
			MainMenuItem.NotificationsItem.ViewItem = slNotifications;
			MainMenuItem.HelpItem.ViewItem = slHelp;
			MainMenuItem.UserProfileItem.ViewItem = slProfile;
			MainMenuItem.FreeClassroomsItem.ViewItem = slFreeClassroom;
			MainMenuItem.ContactsItem.ViewItem = slContacts;
			MainMenuItem.TermsAndConditionsItem.ViewItem = slTermsAndConditions;
			MainMenuItem.SurveyItem.ViewItem = slSurvey;
			MainMenuItem.HomeItem.ViewItem = slHome;
            MainMenuItem.EventsItem.ViewItem = slEvents;
            MainMenuItem.ProceduresItem.ViewItem = slProcedures;
            MainMenuItem.BarMenuItem.ViewItem = slBarMenu;

			// Hago la impresion visible para Windows
            if (Device.RuntimePlatform == Device.UWP)
			{
                cvPrint.IsVisible = true;
				MainMenuItem.PrintItem.ViewItem = slPrint;
			}

			addTapGestureToViewItems();
			addTapGestureToLogout();

			// Guardo el token para ser usado en la impresion
			var account = AccountStore.Create().FindAccountsForService(AppSettings.AppSignInName).FirstOrDefault();
			DependencyService.Get<IAppGroupSettings>().Set(ServiceConstants.ACCESS_TOKEN, account.Properties[ServiceConstants.ACCESS_TOKEN]);

#if false
#else
            var notificationTokenChanged = DependencyService.Get<IAppGroupSettings>().Get(ServiceConstants.NOTIFICATION_TOKEN_CHANGED);

			// Registro la app para notificaciones si hace falta
            if (Device.RuntimePlatform == Device.UWP)
            {
                LoginService loginService = new LoginService();
                string email = loginService.GetUserDataAsync().Result.Email;
                DependencyService.Get<IAppMethods>().RegisterOnNotificationHubAsync(string.Empty, email);
                DependencyService.Get<IAppGroupSettings>().Set(ServiceConstants.NOTIFICATION_TOKEN_CHANGED, "false");
            }
            else if (notificationTokenChanged != null && notificationTokenChanged.ToString() == "true") 
			{
                var firebaseToken = DependencyService.Get<IAppGroupSettings>().Get("firebase_token");
                if (firebaseToken != null)
                {
					LoginService loginService = new LoginService();
                    try
                    {
                        string email = loginService.GetUserDataAsync().Result.Email;
                        DependencyService.Get<IAppMethods>().RegisterOnNotificationHubAsync(firebaseToken.ToString(), email);
                        DependencyService.Get<IAppGroupSettings>().Set(ServiceConstants.NOTIFICATION_TOKEN_CHANGED, "false");
                    }
                    catch {
                        
                    }
                }
			}
#endif

			// TODO: cambiar si cambia la pagina inicial
			SelectObject(slHome);
			App.CurrentPage = MainMenuItem.HomeItem.Id;
		}

		private void addTapGestureToLogout()
		{
			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += async (sender, e) =>
			{
				bool answer = await DisplayAlert(Translate.logout_title, Translate.logout_question, Translate.yes, Translate.no);
				if (answer)
				{
					await Task.Run(() =>
					{
					new LoginService().ClearUserData();
						var account = AccountStore.Create().FindAccountsForService(AppSettings.AppSignInName).FirstOrDefault();
					AccountStore.Create().Delete(account, AppSettings.AppSignInName);

					// Borro el token de impresion
					DependencyService.Get<IAppGroupSettings>().Set(ServiceConstants.ACCESS_TOKEN,
								"");
					});

					DependencyService.Get<ILogoutListener>().DidLogout();

					DependencyService.Get<IAppMethods>().CloseApp();
				}
			};
			slLogout.GestureRecognizers.Add(tapGestureRecognizer);
		}

		private void addTapGestureToViewItems()
		{
			foreach (MainMenuItem item in MainMenuItem.Values())
			{
				if (item.ViewItem != null)
				{

					var tapGestureRecognizer = new TapGestureRecognizer();
					tapGestureRecognizer.Tapped += (sender, e) =>
					{
						StackLayout currentElement = ((StackLayout)sender);
						if (currentElement.BackgroundColor != selectionColor)
						{
							CleanSelections();
							SelectObject(currentElement);

							App.CurrentPage = item.Id;
                            var itemNavigationPage = item.NavigationPage;
                            App.MasterDetailPage.Detail = itemNavigationPage;
							itemNavigationPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
							itemNavigationPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
						}
                        if (Device.RuntimePlatform != Device.UWP)
						    App.MasterDetailPage.IsPresented = false;
					};

					item.ViewItem.GestureRecognizers.Add(tapGestureRecognizer);
				}
			}
		}

		private void CleanSelections()
		{
			foreach (MainMenuItem item in MainMenuItem.Values())
			{
				if (item.ViewItem != null)
				{
					item.ViewItem.BackgroundColor = Color.Transparent;
				}
			}
		}

		private void SelectObject(StackLayout objectSelected)
		{
			if (objectSelected != MainMenuItem.TermsAndConditionsItem.ViewItem)
			{
				objectSelected.BackgroundColor = selectionColor;
			}

		}
	}
}
