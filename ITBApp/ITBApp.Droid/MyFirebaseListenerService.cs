﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Firebase.Messaging;

namespace ITBApp.Droid
{
    [Service, IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
	public class MyFirebaseListenerService : FirebaseMessagingService
	{
        private const string PushNotificationTitle = "ITBA app";
        private const string PushNotificationResponseMessageField = "message";

		public override void OnMessageReceived(RemoteMessage message)
		{
			base.OnMessageReceived(message);
			var context = Android.App.Application.Context;

            var intent =
				context.PackageManager.GetLaunchIntentForPackage(context.PackageName);
			intent.AddFlags(ActivityFlags.ClearTop);

            Bundle fromPushBundle = new Bundle();
            fromPushBundle.PutString("from_push", "True");
            intent.PutExtras(fromPushBundle);

			var pendingIntent = PendingIntent.GetActivity(context, 0, intent,
														  PendingIntentFlags.UpdateCurrent);
            
			// Instantiate the builder and set notification elements:
			Notification.Builder builder = new Notification.Builder(this)
				.SetContentTitle(PushNotificationTitle)
				.SetContentText(message.Data[PushNotificationResponseMessageField])
				.SetContentIntent(pendingIntent)
				.SetSmallIcon(Resource.Drawable.icono_notificacion);

			// Build the notification:
			Notification notification = builder.Build();

			// Get the notification manager:
			NotificationManager notificationManager =
				GetSystemService(NotificationService) as NotificationManager;

			// Publish the notification:
			int notificationId = new Random().Next(0, 10000);
			notificationManager.Notify(notificationId, notification);
		}
	}
}
