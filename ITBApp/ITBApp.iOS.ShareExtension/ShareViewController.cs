﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CoreFoundation;
using CoreGraphics;
using Foundation;
using ITBApp.Helpers;
using ITBApp.Services;
using ITBApp.Util.Helpers;
using MobileCoreServices;
using Plugin.Connectivity;
using Social;
using UIKit;
using Xamarin.Forms;

namespace ITBApp.iOS.ShareExtension
{
	public partial class ShareViewController : SLComposeServiceViewController
	{
		LoadingOverlay loadPop;
		private const string PressPostToPrintMessage = "Presioná Post para Imprimir.";
		private const string PrintMessage = "Imprimir";
		private const string NeedInternetMessage = "Para imprimir necesitás estar conectado a internet.";
		private const string NeedITBAappMessage = "Hubo un error con tu sesión de ITBA app. Probá volver a abrir ITBA app.";
		private const string ReadyToPrintMessage = "¡Listo! Retirá el trabajo en el CIR";
		private const string CouldntPrintMessage = "No se pudo enviar a imprimir. Probá volver a abrir ITBA app.";
		private const long DispatchDeltaNanoseconds = 3000000000;
		private const string ITBAappGroup = "group.ITBAapp";
		private const string FileNotValidMessage = "El tipo de archivo no es válido para imprimir.";
        private const string SendingFileMessage = "Enviando";

		public PrintSettingsViewController settingsVC = new PrintSettingsViewController();

		protected ShareViewController(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.

		}

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Placeholder = PressPostToPrintMessage;
		}

		public override bool IsContentValid()
		{
			// Do validation of contentText and/or NSExtensionContext attachments here
			return true;
		}

        public override void DidSelectPost()
        {
            global::Xamarin.Forms.Forms.Init();

            CheckConnectivity();

            string token = null;

            try {
				var taskToken = Task.Run(async () => {
					// Load all
					token = await LoadAllAsync();
				});

				taskToken.Wait();

				// Run Extension
				RunExtension(token);
            }
            catch {
				Device.BeginInvokeOnMainThread(() =>
				{
					UIAlertController alert = UIAlertController.Create(PrintMessage, NeedITBAappMessage, UIAlertControllerStyle.Alert);

					PresentViewController(alert, true, () =>
					{
						DispatchQueue.MainQueue.DispatchAfter(new DispatchTime(DispatchTime.Now, DispatchDeltaNanoseconds), () =>
						{
							ExtensionContext.CompleteRequest(null, null);
						});
					});
				});
            }
        }

        private void RunExtension(string token)
        {
            NSExtensionItem extensionItem = ExtensionContext.InputItems[0];
            NSItemProvider file = extensionItem.Attachments[0];

            if (file.HasItemConformingTo(UTType.Image) || file.HasItemConformingTo(UTType.PDF) || file.HasItemConformingTo(UTType.URL))
            {

                string fileType = UTType.Image;
                if (file.HasItemConformingTo(UTType.PDF)) fileType = UTType.PDF;
                else if (file.HasItemConformingTo(UTType.URL)) fileType = UTType.URL;

                file.LoadItem(fileType, null, (NSObject item, NSError error) =>
                {
                    var url = (NSUrl)item;
                    var data = NSData.FromUrl(url);
                    var bytes = data.ToArray();

                    string filename = Path.GetFileName(url.ToString());

                    ITBApp.Enums.MediaType mediaType = ITBApp.Enums.MediaType.Application;
                    if (file.HasItemConformingTo(UTType.Image))
                        mediaType = ITBApp.Enums.MediaType.Image;
                    ITBApp.Enums.MediaSubType mediaSubType = ValidationHelper.getMediaSubTypeFromFilename(filename);

                    //bool result = false;
                    nint copies = 0;
                    nint printTypeSelection = 0;

                    Device.BeginInvokeOnMainThread(async() =>
                    {
                        try
                        {
                            copies = this.settingsVC.myPickerView.SelectedRowInComponent(0);
                            printTypeSelection = settingsVC.segmentControl.SelectedSegment;
                        }
                        catch {
                            
                        }

						var bounds = UIScreen.MainScreen.Bounds;
						// show the loading overlay on the UI thread using the correct orientation sizing
						loadPop = new LoadingOverlay(bounds);
						View.Add(loadPop);

						bool result = false;
						EmailPrintService printService = new EmailPrintService();

						for (int copia = 0; copia <= copies; copia++)
						{
							loadPop.SetLoadingMessage(GetLoadingMessage(copia + 1, (int)(copies + 1), SendingFileMessage));
							if (printTypeSelection == 0)
							{
								result = await printService.SendBWEmail("me", bytes, filename,
																		mediaType, mediaSubType, token);
							}
							else
							{
								result = await printService.SendColorEmail("me", bytes, filename,
																			mediaType, mediaSubType, token);
							}
						}

						string message = ReadyToPrintMessage;
						if (!result) message = CouldntPrintMessage;

						loadPop.Hide();
						UIAlertController alert = UIAlertController.Create(PrintMessage, message, UIAlertControllerStyle.Alert);

						PresentViewController(alert, true, () =>
						{
							DispatchQueue.MainQueue.DispatchAfter(new DispatchTime(DispatchTime.Now, DispatchDeltaNanoseconds), () =>
							{
								ExtensionContext.CompleteRequest(null, null);
							});
						});
                    });
                });
            }
            else
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    UIAlertController alert = UIAlertController.Create(PrintMessage, FileNotValidMessage, UIAlertControllerStyle.Alert);

                    PresentViewController(alert, true, () =>
                    {
                        DispatchQueue.MainQueue.DispatchAfter(new DispatchTime(DispatchTime.Now, DispatchDeltaNanoseconds), () =>
                        {
                            ExtensionContext.CompleteRequest(null, null);
                        });
                    });
                });
            }
        }

        private async Task<string> LoadAllAsync()
        {
            string token = null;

            NSUserDefaults shared = new NSUserDefaults(
                    ITBAappGroup,
                    NSUserDefaultsType.SuiteName);
            
            // get values
            token = await new TokenHolder().GetTokenForPrintAsync().ConfigureAwait(false);

            if (String.IsNullOrEmpty(token))
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    UIAlertController alert = UIAlertController.Create(PrintMessage, NeedITBAappMessage, UIAlertControllerStyle.Alert);

                    PresentViewController(alert, true, () =>
                    {
                        DispatchQueue.MainQueue.DispatchAfter(new DispatchTime(DispatchTime.Now, DispatchDeltaNanoseconds), () =>
                        {
                            ExtensionContext.CompleteRequest(null, null);
                        });
                    });
                });
            }

            return token;
        }

        private void CheckConnectivity()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    UIAlertController alert = UIAlertController.Create(PrintMessage, NeedInternetMessage, UIAlertControllerStyle.Alert);

                    PresentViewController(alert, true, () =>
                    {
                        DispatchQueue.MainQueue.DispatchAfter(new DispatchTime(DispatchTime.Now, DispatchDeltaNanoseconds), () =>
                        {
                            ExtensionContext.CompleteRequest(null, null);
                        });
                    });
                });
            }
        }

        public override SLComposeSheetConfigurationItem[] GetConfigurationItems()
		{
			SLComposeSheetConfigurationItem[] configurationArray = new SLComposeSheetConfigurationItem[1];

			SLComposeSheetConfigurationItem itemType = new SLComposeSheetConfigurationItem();
			itemType.Title = "Ajustes de impresión";
			itemType.Title = "Configurar";
			itemType.TapHandler = () =>
			{
				this.PushConfigurationViewController(this.settingsVC);                				
			};

			configurationArray[0] = itemType;

			// To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
			return configurationArray;
		}

        private string GetLoadingMessage(int sendingNumberCopy, int totalCopies, string sendingMessage)
        {            
            return String.Format("{0} {1} de {2} ...", sendingMessage, sendingNumberCopy, totalCopies);
        }

	}
}