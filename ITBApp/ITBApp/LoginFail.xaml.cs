﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITBApp.Model;
using Xamarin.Forms;

namespace ITBApp
{
    public partial class LoginFail : ContentPage
    {
        public LoginFail()
        {
            InitializeComponent();
        }

		public LoginFail(string message) :this()
		{
			tryAgainMessage.Text = message;
		}

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            DependencyService.Get<IAppMethods>().CloseApp();
        }
    }
}
