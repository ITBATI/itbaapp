﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ITBApp.Model.Handlers;
using Plugin.Connectivity;
using Xamarin.Forms;
using ITBApp.Resources.i18n;
using ITBApp.Model.DataModel;
using ITBApp.Util.Helpers;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Text;
using ITBApp.Helpers;
using ITBApp.Handlers;

namespace ITBApp.Services
{
    public class EventsService
    {
        private TokenHolder TokenHolder = new TokenHolder();

		public async Task DeleteCachedDataAsync()
		{
			Application.Current.Properties.Remove(ServiceConstants.ACADEMIC_HISTORY_DATA);
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
		}

        public async Task<List<string>> GetEnrolledEventsIdAsync()
        {
			if (!CrossConnectivity.Current.IsConnected)
			{
				throw new ServerRequestException(Translate.events_no_internet_text);
			}

			try
			{
				string token = await TokenHolder.GetTokenAsync().ConfigureAwait(false);

				try
				{
					var stringResponse = await HttpUtility
						.MakeGetCallAsync(String.Format("{0}{1}", AppSettings.EnrolledEventsURL, token))
						.ConfigureAwait(false);

                    EventsData eventsData = ParseResponse(stringResponse);
                    List<string> result = new List<string>();

                    foreach (EventEntryData ev in eventsData.Events)
                    {
                        result.Add(ev.Id);
                    }

                    return result;
				}
				catch
				{
					throw new ServerRequestException(Translate.events_service_error);
				}

			}
			catch (TokenExpiredException)
			{
				throw new ServerRequestException(Translate.events_no_internet_text);
			}
        }

        public async Task<bool> EnrollAsync(string eventId)
        {
			if (!CrossConnectivity.Current.IsConnected)
			{
				throw new ServerRequestException(Translate.events_no_internet_text);
			}

			try
			{
				string token = await TokenHolder.GetTokenAsync().ConfigureAwait(false);

                string dni = (await new LoginService().GetUserDataAsync()).DNI;

				try
				{
					var stringResponse = await HttpUtility
                        .MakePostCallAsync(String.Format("{0}{1}/{2}/{3}", AppSettings.EventsEnrollURL, token, eventId, dni), string.Empty)
						.ConfigureAwait(false);

                    return true;
				}
				catch
				{
					throw new ServerRequestException(Translate.events_service_error);
				}

			}
			catch (TokenExpiredException)
			{
				throw new ServerRequestException(Translate.events_no_internet_text);
			}
        }

		public async Task<EventsData> GetEventsAsync()
		{
			EventsData eventsData = GetSavedEventsData();

			if (eventsData == null)
			{
				if (!CrossConnectivity.Current.IsConnected)
				{
					throw new ServerRequestException(Translate.events_no_internet_text);
				}

				try
				{
					string token = await TokenHolder.GetTokenAsync().ConfigureAwait(false);

					try
					{
						var stringResponse = await HttpUtility
                            .MakeGetCallAsync(String.Format("{0}{1}", AppSettings.EventsURL, token))
							.ConfigureAwait(false);

						eventsData = ParseResponse(stringResponse);
						eventsData.Expiration = DateTime.Now.AddMinutes(ServiceConstants.EVENTS_EXPIRATION_IN_MINUTES);

						SaveEventsData(eventsData);
					}
					catch
					{
						throw new ServerRequestException(Translate.events_service_error);
					}

				}
				catch (TokenExpiredException)
				{
					throw new ServerRequestException(Translate.events_no_internet_text);
				}

			}

			return eventsData;
		}

        private EventsData ParseResponse(string response)
        {
            var result = new EventsData()
			{
                Events = new List<EventEntryData>()
			};

            try
            {
                //response = response.Replace("“","\"");
                //response = response.Replace("”", "\"");
                //response = response.Replace("@", "");
				JObject jsonResponse = JObject.Parse(response);
                var events = jsonResponse["events"];
                if (events.HasValues)
                {
                    if (events["event"] is JArray)
                    {
                        foreach (var eventEntry in events["event"])
                        {
                            if (eventEntry["time"] != null)
                            {
                                var eventsData = new EventEntryData()
                                {
                                    Id = (string)eventEntry["id"],
                                    Description = (string)eventEntry["description"],
                                    Comments = (string)eventEntry["comments"],
                                    Url = (string)eventEntry["url"],
                                    Date = (string)eventEntry["date"],
                                    DateUntil = (string)eventEntry["dateuntil"],
                                    InscriptionStart = (string)eventEntry["inscriptionstart"],
                                    InscriptionEnd = (string)eventEntry["inscriptionend"],
                                    Quota = (string)eventEntry["quota"],
                                    Enrolled = (string)eventEntry["enrolled"],
                                    Time = new List<EventTimeDetail>()
                                };

                                if (eventEntry["time"] is JArray)
                                {
                                    foreach (var timeDetail in eventEntry["time"])
                                    {
                                        EventTimeDetail eventTimeDetail = new EventTimeDetail()
                                        {
                                            From = (string)timeDetail["from"],
                                            To = (string)timeDetail["to"],
                                            ITBAClassroom = (string)timeDetail["ITBAclassroom"],
                                            Classroom = (string)timeDetail["classroom"],
                                            DayShortName = (string)timeDetail["day"],
                                            Building = (string)timeDetail["building"]
                                        };
                                        eventsData.Time.Add(eventTimeDetail);
                                    }
                                }
                                else
                                {
                                    EventTimeDetail eventTimeDetail = new EventTimeDetail()
                                    {
                                        From = (string)eventEntry["time"]["from"],
                                        To = (string)eventEntry["time"]["to"],
                                        ITBAClassroom = (string)eventEntry["time"]["ITBAclassroom"],
                                        Classroom = (string)eventEntry["time"]["classroom"],
                                        DayShortName = (string)eventEntry["time"]["day"],
                                    };
                                    eventsData.Time.Add(eventTimeDetail);
                                }
                                result.Events.Add(eventsData);
                            }
						}
                    }
                    else
                    {
                        var eventEntry = events["event"];
						var eventsData = new EventEntryData()
						{
							Id = (string)eventEntry["id"],
							Description = (string)eventEntry["description"],
							Comments = (string)eventEntry["comments"],
							Url = (string)eventEntry["url"],
							Date = (string)eventEntry["date"],
							DateUntil = (string)eventEntry["dateuntil"],
							InscriptionStart = (string)eventEntry["inscriptionstart"],
							InscriptionEnd = (string)eventEntry["inscriptionend"],
							Quota = (string)eventEntry["quota"],
							Enrolled = (string)eventEntry["enrolled"],
							Time = new List<EventTimeDetail>()
						};

						if (eventEntry["time"] is JArray)
						{
							foreach (var timeDetail in eventEntry["time"])
							{
								EventTimeDetail eventTimeDetail = new EventTimeDetail()
								{
									From = (string)timeDetail["from"],
									To = (string)timeDetail["to"],
									ITBAClassroom = (string)timeDetail["ITBAclassroom"],
									Classroom = (string)timeDetail["classroom"],
									DayShortName = (string)timeDetail["day"],
									Building = (string)timeDetail["building"]
								};
								eventsData.Time.Add(eventTimeDetail);
							}
						}
						else if (eventEntry["time"] != null)
                        {
                            EventTimeDetail eventTimeDetail = new EventTimeDetail()
                            {
                                From = (string)eventEntry["time"]["from"],
                                To = (string)eventEntry["time"]["to"],
                                ITBAClassroom = (string)eventEntry["time"]["ITBAclassroom"],
                                Classroom = (string)eventEntry["time"]["classroom"],
                                DayShortName = (string)eventEntry["time"]["day"],
                            };
                            eventsData.Time.Add(eventTimeDetail);
                        }

                        if (eventsData.Time.Count > 0)
                            result.Events.Add(eventsData);
                    }
                }
            }
            catch(Exception exc)
			{
				throw new ServerRequestException("Error al procesar los datos de eventos");
			}

			return result;
        }

		private EventsData GetSavedEventsData()
		{
            if (Application.Current.Properties.ContainsKey(ServiceConstants.EVENTS_DATA))
			{
				string json = (string)Application.Current.Properties[ServiceConstants.EVENTS_DATA];
                EventsData data = (EventsData)SerializerHelper.Deserialize(json, (new EventsData()).GetType());

				if (data.Expiration > DateTime.Now || !CrossConnectivity.Current.IsConnected)
				{
					return data;
				}
			}

			return null;
		}

		private async void SaveEventsData(EventsData data)
		{
			string json = SerializerHelper.Serialize(data);
			Application.Current.Properties[ServiceConstants.EVENTS_DATA] = json;
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
		}


        private TokenHolder tokenHolder = new TokenHolder();

		public async Task<string> createCalendar()
		{
			string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
			var uriBuilder = new UriBuilder(AppSettings.GoogleCalendarApiURL);
			HttpValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
			query.Add("access_token", token);
			uriBuilder.Query = query.ToString();
			var content = new StringContent("{\"summary\":\"ITBA Eventos\"}", Encoding.UTF8, "application/json");

			using (var client = new HttpClient())
			{
				client.MaxResponseContentBufferSize = 256000;
				HttpResponseMessage response = await client.PostAsync(uriBuilder.Uri, content).ConfigureAwait(false);

				if (response.IsSuccessStatusCode)
				{
					string stringResponse = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					JToken jTokenResponse = JToken.Parse(stringResponse);
					return jTokenResponse["id"].ToString();
				}
				else
				{
					throw new ServerRequestException("Error al exportar los eventos a Google Calendar.");
				}
			}
		}

		public async Task<string> createEvent(string calendarId, string eventName, string location,
                                              string startTime, string endTime, DateTime eventDate)
		{
			string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
			var uriBuilder = new UriBuilder(AppSettings.GoogleCalendarApiURL + "/" + calendarId + "/events");
			HttpValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
			query.Add("access_token", token);
			uriBuilder.Query = query.ToString();

			string startDate = eventDate.ToString("yyyy-MM-dd");
			string endDate = startDate;

			var content = new StringContent("{\"summary\":\"" + eventName + "\",\"location\":\"" + location + "\",\"start\": {\"dateTime\": \"" + startDate + "T" + startTime + ":00.000-03:00\",\"timeZone\": \"America/Argentina/Buenos_Aires\"},\"end\": {\"dateTime\": \"" + endDate + "T" + endTime + ":00.000-03:00\",\"timeZone\": \"America/Argentina/Buenos_Aires\"}}", Encoding.UTF8, "application/json");

			using (var client = new HttpClient())
			{
				client.MaxResponseContentBufferSize = 256000;
				HttpResponseMessage response = await client.PostAsync(uriBuilder.Uri, content).ConfigureAwait(false);

				if (response.IsSuccessStatusCode)
				{
					return "Tus eventos fueron agregados a Google Calendar";
				}
				else
				{
					throw new ServerRequestException("Error al exportar los eventos a Google Calendar.");
				}
			}
		}


    }
}
