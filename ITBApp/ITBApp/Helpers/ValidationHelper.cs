﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using ITBApp.DataModel;
using ITBApp.Enums;

namespace ITBApp.Helpers
{
    public class ValidationHelper
	{
        // Pad Numbers
        public const int PadNumbersLength = 5;
        public const string PadNumbersRegularExpression = "[0-9]+";

        // Gender
        public const string FemenineGenderString = "FEMININE";

        // Days
        public const string MondayString = "MON";
        public const string TuesdayString = "TUE";
        public const string WednesdayString = "WED";
        public const string ThursdayString = "THU";
        public const string FridayString = "FRI";
        public const string SaturdayString = "SAT";

        // File extensions
        public const string JpgFileExtension = ".jpg";
        public const string PngFileExtension = ".png";
        public const string PdfFileExtension = ".pdf";
        public const string BmpFileExtension = ".bmp";
        public const string GifFileExtension = ".gif";
        public const string DibFileExtension = ".dib";
        public const string TifFileExtension = ".tif";
        private const string TiffFileExtension = ".tiff";

		public static bool IsNumber(String text)
		{
			int numberOut;
			return int.TryParse(text, out numberOut);
		}

		public static string PadNumbers(string input)
		{
			return Regex.Replace(input, PadNumbersRegularExpression, match => match.Value.PadLeft(PadNumbersLength, '0'));
		}

		public static UserGender GetUserGenderFromString(string genderString)
		{
			if (genderString == FemenineGenderString) return UserGender.Female;
			return UserGender.Male;
		}

		public static string FormatDoubleNumber(string inputString, int decimalDigits)
		{
			return Convert.ToDouble(inputString, CultureInfo.InvariantCulture).ToString("##.##").Replace(".", ",");
		}

		public static string FormatBalanceNumber(string inputString)
		{
			var strBuilder = new StringBuilder(inputString);

			var dotIndex = inputString.IndexOf(".");
			var commaIndex = inputString.IndexOf(",");

			if (dotIndex != -1)
				strBuilder[dotIndex] = ',';

			if (commaIndex != -1)
				strBuilder[commaIndex] = '.';

			return strBuilder.ToString();
		}

		public static DayOfWeek GetDayOfWeekFromJsonString(string jsonDayString)
		{
			if (jsonDayString == MondayString) return DayOfWeek.Monday;
            if (jsonDayString == TuesdayString) return DayOfWeek.Tuesday;
            if (jsonDayString == WednesdayString) return DayOfWeek.Wednesday;
            if (jsonDayString == ThursdayString) return DayOfWeek.Thursday;
            if (jsonDayString == FridayString) return DayOfWeek.Friday;
            if (jsonDayString == SaturdayString) return DayOfWeek.Saturday;
			return DayOfWeek.Sunday;
		}

		public static DateTime JavaDateToDateTime(string unixTimeMillis)
		{
			var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			return epoch.AddMilliseconds(long.Parse(unixTimeMillis));
		}

		public static MediaSubType getMediaSubTypeFromFilename(string filename)
		{
			string extension = Path.GetExtension(filename);
            if (extension == JpgFileExtension) return ITBApp.Enums.MediaSubType.JPG;
            else if (extension == PngFileExtension) return ITBApp.Enums.MediaSubType.PNG;
            else if (extension == PdfFileExtension) return ITBApp.Enums.MediaSubType.PDF;
            else if (extension == BmpFileExtension) return ITBApp.Enums.MediaSubType.BMP;
            else if (extension == GifFileExtension) return ITBApp.Enums.MediaSubType.GIF;
            else if (extension == DibFileExtension) return ITBApp.Enums.MediaSubType.DIB;
            else if (extension == TifFileExtension) return ITBApp.Enums.MediaSubType.TIF;
            else if (extension == TiffFileExtension) return ITBApp.Enums.MediaSubType.TIFF;
			return MediaSubType.JPG;
		}
	}
}
