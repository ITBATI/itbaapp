﻿//-----------------------------------------------------------------------
// <copyright file="PendingExamsViewModel.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ITBApp.ViewModels
{
    using System;
    using System.Collections.ObjectModel;
    using System.Threading.Tasks;
    using ITBApp.Helpers;
    using ITBApp.Model.DataModel;
    using ITBApp.Model.Handlers;
    using ITBApp.Interfaces;
    using ITBApp.Resources.i18n;
    using ITBApp.Services;
    using Xamarin.Forms;
    using ITBApp.DataModel;

    /// <summary>
    /// Pending Exams Context View Model
    /// </summary>
    internal class PendingExamsViewModel : BaseModel
    {
        /// <summary>
        /// Exams list Data  
        /// </summary>
        private ObservableCollection<ExamData> exams = new ObservableCollection<ExamData>();

        /// <summary>
        /// Pending exams service instance  
        /// </summary>
        private PendingExamsService service = new PendingExamsService();

        /// <summary>
        /// First time that page appears  
        /// </summary>
        private bool firstAppear = true;

        /// <summary>
        /// Student is male  
        /// </summary>
        private bool isMale;

        /// <summary>
        /// Initializes a new instance of the <see cref="PendingExamsViewModel"/> class 
        /// </summary>
        public PendingExamsViewModel()
        {
            Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
        }

        /// <summary>
        /// Gets the pending exams list  
        /// </summary>
        public ObservableCollection<ExamData> Exams
        {
            get 
            { 
                return this.exams; 
            }

            private set
            {
                this.firstAppear = false;
                this.exams = value;
                this.OnPropertyChanged("Exams");
                this.OnPropertyChanged("EmptyList");
            }
        }

        /// <summary>
        /// Gets a value indicating whether it has no pending exams 
        /// </summary>
        public bool EmptyList
        {
            get 
            {
                if (this.firstAppear)
                {
                    return false;
                }
                else
                {
                    return this.Exams == null || this.Exams.Count == 0;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether student is male 
        /// </summary>
        public bool IsMale
        {
            get
            {
                return this.isMale;
            }

            set
            {
                this.isMale = value;
                this.OnPropertyChanged("IsMale");
            }
        }

        /// <summary>
        /// Fills view model data
        /// </summary>
        /// <returns>
        /// Error message
        /// </returns>
        public override async Task<string> LoadDataAsync()
        {
            try
            {
                this.IsLoading = true;
                this.ErrorMessage = null;

                UserData userData = new LoginService().GetSavedUserData();
                PendingExamsData data = await this.service.GetAsync().ConfigureAwait(false);

                this.Exams = new ObservableCollection<ExamData>(data.Exams);
                this.IsMale = userData.Gender == UserGender.Male;
            }
            catch (ServerRequestException ex)
            {
                this.ErrorMessage = ex.Message;
            }
            catch (Exception exception)
            {
                this.ErrorMessage = "Se produjo un error al recuperar los examenes pendientes";
            }

            this.IsLoading = false;

            return this.ErrorMessage;
        }

        /// <summary>
        /// Deletes previous saved data  
        /// </summary>
        /// <returns>
        /// No value... just task
        /// </returns>
        public override async Task DeleteCache()
        {
            await this.service.DeleteCachedData();
        }

        /// <summary>
        /// Event when Pending Exams page is opened or maximized
        /// </summary>
        /// <param name="sender">
        /// Object that sends the event
        /// </param>
        /// <param name="eventArgs">
        /// Event parameters 
        /// </param>
        protected override void CurrentPageOnDisappearing(object sender, EventArgs eventArgs)
        {
            App.CurrentPage = PageId.USER_PROFILE;
        }

        /// <summary>
        /// Event when Pending Exams page is opened or maximized
        /// </summary>
        /// <param name="sender">
        /// Object that sends the event
        /// </param>
        /// <param name="eventArgs">
        /// Event parameters
        /// </param>
        protected override async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
        {
            App.CurrentPage = PageId.PENDING_EXAMS;

            if (this.exams.Count == 0)
            {
                await this.LoadDataAsync().ConfigureAwait(false);    
            }
        }
    }
}
