﻿using System;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Android;
using Xamarin.UITest.Queries;

namespace ITBApp.UITests.Droid
{
	public class IntegrationDroidTests
	{
        AndroidApp app;

        [SetUp]
        public void SetUp()
        {
            app = ConfigureApp
                .Android
                .StartApp();

            object[] saveTokenParams = new object[3];
            saveTokenParams[0] = "ya29.GltBBGjl1QJpTjoNyvoyIow1LuaVC5JC22vKbU54kXUY8zYs9epDVzCJnOt5KkRzEWZp-DBb75lCj3tCwY6eym9hHj9g7T5CBwY0umjLNzfEX1UGqUqOTtOb5Ooj";
            saveTokenParams[1] = "1/390ZxciNfEkNADGmdzcBkiszSo1syoBLb7cGQPg0pwU";
            saveTokenParams[2] = 3600;


            app.Invoke("SaveTokenForBackdoor", saveTokenParams);

            //Esperamos 1 minuto para asegurar que estemos logeados.
            app.WaitForElement(c => c.Id("action_bar"), timeout: TimeSpan.FromSeconds(60));
        }

		[Test]
		public void TEST001_User_Press_Logout_And_Cancel()
		{
			app.Tap(c => c.Id("action_bar").Child(2));
			app.Tap(c => c.Text("CERRAR SESIÓN"));
			app.Tap(c => c.Text("No"));

			/* Assert */
			AppResult[] result = app.Query(c => c.Class("TextView").Text("Home"));
			Assert.IsTrue(result.Any(), "The error message is not being displayed.");
		}

		[Test]
		public void TEST002_User_EnterProfile()
		{
			app.Tap(c => c.Text("Lautaro Nicolas Gonzalez"));

			/* Assert */
			AppResult[] result = app.Query(c => c.Class("TextView").Text("Mi Perfil"));
			Assert.IsTrue(result.Any(), "The error message is not being displayed.");
		}

		[Test]
		public void TEST003_User_Enter_Schedule()
		{
			app.Tap(c => c.Id("action_bar").Child(2));
			app.Tap(c => c.Text("HORARIOS"));

			/* Assert */
			AppResult[] result = app.Query(c => c.Class("TextView").Text("Horarios"));
			Assert.IsTrue(result.Any(), "The error message is not being displayed.");
		}

		[Test]
		public void TEST004_User_Enter_FreeClassrooms()
		{
			app.Tap(c => c.Id("action_bar").Child(2));
			app.Tap(c => c.Text("AULAS LIBRES"));

			/* Assert */
			AppResult[] result = app.Query(c => c.Class("TextView").Text("Aulas Libres"));
			Assert.IsTrue(result.Any(), "The error message is not being displayed.");
		}

		[Test]
		public void TEST005_User_Enter_Surveys()
		{
			app.Tap(c => c.Id("action_bar").Child(2));
			app.Tap(c => c.Text("OPINÁ"));

			/* Assert */
			AppResult[] result = app.Query(c => c.Class("TextView").Text("Opiná"));
			Assert.IsTrue(result.Any(), "The error message is not being displayed.");
		}

		[Test]
		public void TEST006_User_Enter_Contacts()
		{
			app.Tap(c => c.Id("action_bar").Child(2));
			app.Tap(c => c.Text("CONTACTOS"));

			/* Assert */
			AppResult[] result = app.Query(c => c.Class("TextView").Text("Contactos"));
			Assert.IsTrue(result.Any(), "The error message is not being displayed.");
		}

		[Test]
		public void TEST007_User_Enter_Help()
		{
			app.Tap(c => c.Id("action_bar").Child(2));
			app.Tap(c => c.Text("AYUDA"));

			/* Assert */
			AppResult[] result = app.Query(c => c.Class("TextView").Text("Ayuda"));
			Assert.IsTrue(result.Any(), "The error message is not being displayed.");
		}


	}
}
