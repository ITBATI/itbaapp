﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;

namespace ITBApp.Droid
{
    [Activity(Label = "ITBA app", Icon = "@drawable/icon", 
             ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
             ScreenOrientation = ScreenOrientation.Portrait,
	 		 Theme = "@style/Theme.Splash", //Indicates the theme to use for this activity
			 MainLauncher = true, //Set it as boot activity
			 NoHistory = true)] //Doesn't place it in back stack
	public class SplashActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

            string fromPush = this.Intent.GetStringExtra("from_push");
            var mainActivity = new Intent(this, typeof(MainActivity));
            if (fromPush != null)
                mainActivity.PutExtra ("from_push", fromPush);

            try
            {
                // With this we dismiss all the generated notifications by the app
                NotificationManager notificationManager =
					Android.App.Application.Context.GetSystemService(Android.Content.Context.NotificationService) as NotificationManager;
                notificationManager.CancelAll();
            }
            catch { }

			StartActivity(mainActivity);
		}
	}
}

