﻿using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel.Survey
{
	[DataContract]
	public class SurveyResultData
	{
		[DataMember]
		public string Question { get; set; }
		[DataMember]
		public string QuestionOption { get; set; }
		[DataMember]
		public int Count { get; set; }
		[DataMember]
		public double Percentage { get; set; }
	}
}