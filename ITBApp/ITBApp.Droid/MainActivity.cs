﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using FFImageLoading.Forms.Droid;
using Java.Interop;
using Refractored.XamForms.PullToRefresh.Droid;
using Xamarin.Forms;
using Plugin.Geolocator;
using System.Threading.Tasks;
using ITBApp.Interfaces;

namespace ITBApp.Droid
{
    [Activity(Label = "ITBA app", Icon = "@drawable/icon",
			  ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
			  ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		public event Action<int, Result, Intent> ActivityResult;

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			if (this.ActivityResult != null)
				this.ActivityResult(requestCode, resultCode, data);
		}

        public async override void OnBackPressed()
        {
        	bool? result = await App.CallHardwareBackPressed();
        	if (result == true)
        	{
        		base.OnBackPressed();
        	}
        	else if (result == null)
        	{
                Finish();
            }
        }

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.SetFlags("FastRenderers_Experimental");
            global::Xamarin.Forms.Forms.Init(this, bundle);

			PullToRefreshLayoutRenderer.Init();
            CachedImageRenderer.Init(enableFastRenderer: true);
                               
            // Ask for geolocation before opening
            try {
				Task.Run(async () =>
				{
					await CrossGeolocator.Current.GetPositionAsync(TimeSpan.FromSeconds(1));
				});
            }
            catch (Exception)
            {}
			

            // Push notification settings
			string fromPush = this.Intent.GetStringExtra("from_push");
			if (fromPush != null)
                DependencyService.Get<IAppGroupSettings>().Set("from_push", fromPush);

			//var x = typeof(Xamarin.Forms.Themes.LightThemeResources);
			//var x = typeof(Xamarin.Forms.Themes.Android.UnderlineEffect);

            Microsoft.Azure.Mobile.MobileCenter.Configure(AppSettings.MobileCenterAppID);

			LoadApplication(new App());

			if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
			{
				Window.ClearFlags(WindowManagerFlags.TranslucentStatus);
				Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
				Window.SetStatusBarColor(Android.Graphics.Color.Black);
			}

		}
	}
}

