﻿using System;
namespace ITBApp.Helpers
{
	public class DateHelper
	{
        public const string LastDateFirstSemester = "0630";
        public const string LastDateSecondSemester = "1130";
        public const string DateFormat = "yyyy-MM-dd";

		public static int GetDayOfWeekAsIndex()
		{
			return (int)DateTime.Today.DayOfWeek > 0 ? (int)DateTime.Today.DayOfWeek - 1 : 0;
		}

		public static int GetDayOfWeekAsIndex(DayOfWeek dayOfWeek)
		{
			// DayOfWeek: enumerado de Domingo(=0) a Sabado (=6)
			return (int)dayOfWeek - 1;
		}

		public static string getLastClassDay()
		{
			string lastDate = DateTime.Today.Year.ToString();
			if (DateTime.Today.Month < 7)
				lastDate += LastDateFirstSemester;
			else
				lastDate += LastDateSecondSemester;
			return lastDate;
		}

		public static string getFirstClassDay(int dayOfWeek)
		{
			int year = DateTime.Today.Year;
			int month = DateTime.Today.Month < 7 ? 3 : 8;
			int day = 1;
			DateTime dateTime = new DateTime(year, month, day);

			//Voy al domingo que es el primer día de la semana.
			dateTime = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek);

			return dateTime.AddDays(dayOfWeek).ToString(DateFormat);
		}

        public static DayOfWeek getDayOfWeekFromDayShortName(string dayShortName) 
        {
            if (dayShortName == "MON") return DayOfWeek.Monday;
            else if (dayShortName == "TUE") return DayOfWeek.Tuesday;
            else if (dayShortName == "WED") return DayOfWeek.Wednesday;
            else if (dayShortName == "THU") return DayOfWeek.Thursday;
            else if (dayShortName == "FRI") return DayOfWeek.Friday;
            else if (dayShortName == "SAT") return DayOfWeek.Saturday;
            return DayOfWeek.Sunday;
        }

        public static string getShortName(DateTime date) {
            if (date.DayOfWeek == DayOfWeek.Monday) return "Lun";
            else if (date.DayOfWeek == DayOfWeek.Tuesday) return "Mar";
            else if (date.DayOfWeek == DayOfWeek.Wednesday) return "Mie";
            else if (date.DayOfWeek == DayOfWeek.Thursday) return "Jue";
            else if (date.DayOfWeek == DayOfWeek.Friday) return "Vie";
            else if (date.DayOfWeek == DayOfWeek.Saturday) return "Sab";
			return "Dom";
        }
	}
}
