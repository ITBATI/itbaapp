﻿using Xamarin.Forms;

namespace ITBApp.Model.DataModel
{
	public class ClassDayViewModel
	{
        public const int ShortClassroomMaxLength = 5;

		public string DayName { get; set; }
		public Color DayMainBackgroundColor { get; set; }
		public Color DaySecondaryBackgroundColor { get; set; }

		public string Classroom { get; set; }
		public string Location { get; set; }
		public string StartTime { get; set; }
		public string EndTime { get; set; }

		public bool HasLongClassroom
		{
			get
			{
				return this.Classroom.Length > ShortClassroomMaxLength;
			}
		}
	}
}
