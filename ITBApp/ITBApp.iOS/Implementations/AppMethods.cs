﻿using System;
using System.Diagnostics;
using Contacts;
using Foundation;
using ITBApp.Interfaces;
using UIKit;
using WindowsAzure.Messaging;
using Xamarin.Forms;

[assembly: Dependency(typeof(ITBApp.iOS.Implementations.AppMethods))]
namespace ITBApp.iOS.Implementations
{
    [Preserve(AllMembers = true)]
    public class AppMethods : IAppMethods
    {
        private const string CFBundleShortVersion = "CFBundleShortVersionString";
        private const string ITBAappGroup = "group.ITBAapp";

		public void AddPhoneContact(string name, string phoneNumber, string phoneLabel)
		{
			var contact = new CNMutableContact();
			contact.GivenName = name;

			// Add phone numbers
			var cellPhone = new CNLabeledValue<CNPhoneNumber>(phoneLabel, new CNPhoneNumber(phoneNumber));
			contact.PhoneNumbers = new CNLabeledValue<CNPhoneNumber>[] { cellPhone };

			// Save new contact
			var store = new CNContactStore();
			var saveRequest = new CNSaveRequest();
			saveRequest.AddContact(contact, store.DefaultContainerIdentifier);

			NSError error;
			string errorMsg;

			if (store.ExecuteSaveRequest(saveRequest, out error))
			{
				errorMsg = ITBApp.Helpers.TranslationHelper.GetNewContactsSuccesful(name);
			}
			else if (!UIDevice.CurrentDevice.CheckSystemVersion(9, 0))
			{
				errorMsg = ITBApp.Helpers.TranslationHelper.GetNewContactsVersionErrorIOS(phoneNumber);
			}
			else if (CNContactStore.GetAuthorizationStatus(CNEntityType.Contacts) != CNAuthorizationStatus.Authorized )
			{
				errorMsg = ITBApp.Helpers.TranslationHelper.GetNewContactsAccessErrorIOS(name);
			}
			else {
				errorMsg = ITBApp.Helpers.TranslationHelper.GetNewContactsError(name);
			}

			UIAlertView _error = new UIAlertView(ITBApp.Helpers.TranslationHelper.GetAlertTitle(), errorMsg, null, "Aceptar", null);
			_error.Show();
		}

        public void clearGroupAppSettings()
        {
            throw new NotImplementedException();
        }

        public void CloseApp()
		{
			Process.GetCurrentProcess().CloseMainWindow();
			Process.GetCurrentProcess().Close();
		}

		public string getAppVersion()
		{
			return NSBundle.MainBundle.InfoDictionary[CFBundleShortVersion].ToString();
		}

        public int GetHeight()
        {
			return (int)UIScreen.MainScreen.Bounds.Height * (int)UIScreen.MainScreen.Scale;
        }

        public int GetHeightInPixels()
        {
            return (int)UIScreen.MainScreen.Bounds.Height;
        }

        public int GetWidth()
        {
            return (int)UIScreen.MainScreen.Bounds.Width * (int)UIScreen.MainScreen.Scale;
        }

        public void RegisterOnNotificationHubAsync(string token, string tag)
		{
			NSData tokenData = new NSData(token,NSDataBase64DecodingOptions.None);

			// Register our info with Azure
            var hub = new SBNotificationHub(AppSettings.ListenConnectionString, AppSettings.NotificationHubName);
			NSError error = new NSError();
			hub.UnregisterAll(tokenData, out error);

			string[] tags = new string[] { tag };
			hub.RegisterNativeAsync(tokenData, new NSSet(tags), err =>
			{
				if (err != null)
				{
					//homeViewController.RegisteredForNotifications("Error: " + err.Description);
				}
				else {
					//homeViewController.RegisteredForNotifications("Successfully registered for notifications");
				}
			});
		}

		public void RestartApp()
        {
            throw new NotImplementedException();
        }

		public void showQuickMessage(string message)
		{
			UIAlertView _error = new UIAlertView(ITBApp.Helpers.TranslationHelper.GetAlertTitle(), message, null, "Aceptar", null);
			_error.Show();
		}

		public void UnregisterFromHub(string token)
		{
			NSData tokenData = new NSData(token, NSDataBase64DecodingOptions.None);
			NSError error = new NSError();
            var hub = new SBNotificationHub(AppSettings.ListenConnectionString, 
                                            AppSettings.NotificationHubName);
			
			hub.UnregisterAll(tokenData, out error);
		}
	}
}

