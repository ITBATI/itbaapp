﻿//-----------------------------------------------------------------------
// <copyright file="BaseModel.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ITBApp.ViewModels
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using ITBApp.Helpers;
    using Xamarin.Forms;

    /// <summary>
    /// Base model.
    /// </summary>
    internal abstract class BaseModel : INotifyPropertyChanged
    {
        /// <summary>
        /// The error message.
        /// </summary>
        private string errorMsg;

        /// <summary>
        /// The is busy.
        /// </summary>
        private bool isBusy;

        /// <summary>
        /// The reload command.
        /// </summary>
        private ICommand reloadCommand;

        /// <summary>
        /// Occurs when property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>The error message.</value>
        public string ErrorMessage
        {
            get
            {
                return this.errorMsg;
            }

            set
            {
                this.errorMsg = value;
                this.OnPropertyChanged("ErrorMessage");
                this.OnPropertyChanged("ShowErrorMessage");
                this.OnPropertyChanged("IsLoading");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:ITBApp.ViewModels.BaseModel"/> show error message.
        /// </summary>
        /// <value><c>true</c> if show error message; otherwise, <c>false</c>.</value>
        public bool ShowErrorMessage
        {
            get { return !this.isBusy && this.errorMsg != null; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:ITBApp.ViewModels.BaseModel"/> is loading.
        /// </summary>
        /// <value><c>true</c> if is loading; otherwise, <c>false</c>.</value>
        public bool IsLoading
        {
            get 
            { 
                return this.isBusy;
            }

            set
            {                
                if (this.isBusy != value)
                {
                    this.isBusy = value;
                    this.OnPropertyChanged("ErrorMessage");
                    this.OnPropertyChanged("ShowErrorMessage");
                    this.OnPropertyChanged("IsLoading");
                }
            }
        }

        /// <summary>
        /// Gets the reload command.
        /// </summary>
        /// <value>The reload command.</value>
        public ICommand ReloadCommand
        {
            get { return this.reloadCommand ?? (this.reloadCommand = new Command(async () => await this.ExecuteReloadCommand())); }
        }

        /// <summary>
        /// Gets the current page.
        /// </summary>
        /// <value>The current page.</value>
        protected Page CurrentPage { get; private set; }

        /// <summary>
        /// Initialize the specified page.
        /// </summary>
        /// <param name="page">The current page.</param>
        public void Initialize(Page page)
        {
            this.CurrentPage = page;

            this.CurrentPage.Appearing += this.CurrentPageOnAppearing;
            this.CurrentPage.Disappearing += this.CurrentPageOnDisappearing;
        }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        /// <summary>
        /// Deletes the cache.
        /// Este metodo se PUEDE redefinir para eliminar los datos cacheados por un determinado servicio
        /// y lograr que al recargar la pagina se vuelva a ejecutar la invocacion al servicio.
        /// </summary>
        /// <returns>The cache.</returns>
        public virtual async Task DeleteCache()
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
        }

        /// <summary>
        /// Loads the data async.
        /// Este metodo SE DEBE redefinir para invocar todos los servicios que se requieran para armar la pagina
        /// </summary>
        /// <returns>The data async.</returns>
        public abstract Task<string> LoadDataAsync();

        /// <summary>
        /// You should redifine this method if you want to reload the data of the page when you enter to it.
        /// </summary>
        /// <param name="sender">Sender of the method</param>
        /// <param name="eventArgs">Event arguments.</param>
        protected virtual async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
        {
            await this.LoadDataAsync().ConfigureAwait(false);
        }

        /// <summary>
        /// Currents the page on disappearing.
        /// </summary>
        /// <param name="sender">Sender of the method</param>
        /// <param name="eventArgs">Event arguments.</param>
        protected virtual void CurrentPageOnDisappearing(object sender, EventArgs eventArgs)
        {
            GC.Collect();
        }

        /// <summary>
        /// Ons the property changed.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Executes the reload command.
        /// </summary>
        /// <returns>The reload command.</returns>
        private async Task ExecuteReloadCommand()
        {
            if (this.IsLoading)
            {
                return;
            }

            await this.DeleteCache().ConfigureAwait(false);
            await this.LoadDataAsync().ConfigureAwait(false);
        }
    }
}