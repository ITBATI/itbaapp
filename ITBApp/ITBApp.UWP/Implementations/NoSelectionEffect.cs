﻿using ITBApp.UWP.Implementations;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

[assembly: ResolutionGroupName(ITBApp.Helpers.NoSelectionEffect.EffectNamespace)]
[assembly: ExportEffect(typeof(NoSelectionEffect), nameof(NoSelectionEffect))]
namespace ITBApp.UWP.Implementations
{
    public class NoSelectionEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            // Set selection mode to 'None'     
            (Control as Windows.UI.Xaml.Controls.ListView).SelectionMode = Windows.UI.Xaml.Controls.ListViewSelectionMode.None;
        }

        protected override void OnDetached()
        {
            // Set selection mode back to the default one of Xamarin.Forms (Single)
            (Control as Windows.UI.Xaml.Controls.ListView).SelectionMode = Windows.UI.Xaml.Controls.ListViewSelectionMode.Single;
        }
    }

}
