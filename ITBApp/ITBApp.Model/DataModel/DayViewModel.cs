﻿using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace ITBApp.Model.DataModel
{
	public class DayViewModel
	{
        public string ImageName { get; set; }
		public Color BackgroundColor { get; set; }
		public string FormattedName { get; set; }

		public ObservableCollection<CourseData> Classes { get; set; }
		public bool IsFreeDay
		{
			get
			{
				return this.Classes.Count == 0;
			}
		}

		public bool IsBusyDay
		{
			get
			{
				return this.Classes.Count != 0;
			}
		}

		public bool IsMale { get; set; }
	}
}
