﻿namespace ITBApp.Listeners
{
	public interface ILogoutListener
	{
		void DidLogout();
	}
}
