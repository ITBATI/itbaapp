﻿using System;
using System.Collections.Generic;
using CoreLocation;
using FFImageLoading.Forms.Touch;
using Foundation;
using ITBApp.Services;
using Refractored.XamForms.PullToRefresh.iOS;
using UIKit;
using Xamarin.Forms;
using Plugin.Geolocator;
using ITBApp.Interfaces;
using System.Threading.Tasks;
using ITBApp.Extensions;

namespace ITBApp.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		//
		// This method is invoked when the application has loaded and is ready to run. In this 
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.
		//
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
            /*
			#if ENABLE_TEST_CLOUD
			// requires Xamarin Test Cloud Agent
			Xamarin.Calabash.Start();
			#endif
			*/

			global::Xamarin.Forms.Forms.Init();

			// Presenters Initialization
			global::Xamarin.Auth.Presenters.XamarinIOS.AuthenticationConfiguration.Init();

            Task.Run(async ()=>
            {
                await CrossGeolocator.Current.GetPositionAsync(TimeSpan.FromSeconds(5)).ConfigureAwait(false);
            });

			PullToRefreshLayoutRenderer.Init();
			CachedImageRenderer.Init();

			// Notificaciones
			// --------------
			var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
							   UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
							   new NSSet());

			if (!UIApplication.SharedApplication.IsRegisteredForRemoteNotifications)
			{
				UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
			}
			// --------------

            if (CLLocationManager.Status != CLAuthorizationStatus.AuthorizedWhenInUse)
            {
                var manager = new CLLocationManager();
                manager.RequestWhenInUseAuthorization();
            }

            Microsoft.Azure.Mobile.MobileCenter.Configure(AppSettings.MobileCenterAppID);

			LoadApplication(new App());

			var x = typeof(Xamarin.Forms.Themes.iOS.UnderlineEffect);

			return base.FinishedLaunching(app, options);
		}

		private const string TAG = "PushNotification-APN";

		public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
		{
			UIAlertView avAlert = new UIAlertView("Error", "No se puede conectar correctamente al servicio de notificaciones.", null, "OK", null);
			avAlert.Show();

		}

		public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
		{
#if false
#else
			string encodedString = deviceToken.GetBase64EncodedString(NSDataBase64EncodingOptions.None);

			App.Current.Properties["firebase_token"] = encodedString;

			if (App.IsLoggedIn)
			{
				try
				{
					LoginService loginService = new LoginService();
					string email = loginService.GetUserDataAsync().Result.Email;
					DependencyService.Get<IAppMethods>().RegisterOnNotificationHubAsync(encodedString, email);
					App.Current.Properties[ITBApp.Services.ServiceConstants.NOTIFICATION_TOKEN_CHANGED] = false;
				}
				catch
				{
					//Registrar que hubo un error para registrar el dispositvo en las notificaciones.
				}
			}
			else {
				App.Current.Properties[ITBApp.Services.ServiceConstants.NOTIFICATION_TOKEN_CHANGED] = true;
			}


#endif
		}

		public override void DidRegisterUserNotificationSettings(UIApplication application, UIUserNotificationSettings notificationSettings)
		{
			UIApplication.SharedApplication.RegisterForRemoteNotifications();
		}

		// Uncomment if using remote background notifications. To support this background mode, enable the Remote notifications option from 
		// the Background modes section of iOS project properties. (You can also enable this support by including the UIBackgroundModes key with the remote-notification value in your app�s Info.plist file.)
		public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
		{
            UIApplicationState state = application.ApplicationState;
            if (state == UIApplicationState.Inactive || state == UIApplicationState.Background)
            {
                //write here...Application opened by tapping notification
                DependencyService.Get<IAppGroupSettings>().Set("from_push","True");
            }

            NSDictionary aps = userInfo.ObjectForKey(new NSString("aps")) as NSDictionary;

			string alertBody = string.Empty;
			if (aps.ContainsKey(new NSString("alert")))
				alertBody = (aps[new NSString("alert")] as NSString).ToString();

			// Show alert
			if (!string.IsNullOrEmpty(alertBody))
			{
				// create the notification
				var notification = new UILocalNotification();

				// configure the alert
				notification.AlertAction = "ITBA app";
				notification.AlertBody = alertBody;

				// set the fire date (the date time in which it will fire)
				notification.FireDate = NSDate.FromTimeIntervalSinceNow(1);

				// set the sound to be the default sound
				notification.SoundName = UILocalNotification.DefaultSoundName;

				// schedule it
				UIApplication.SharedApplication.ScheduleLocalNotification(notification);
			}
		}


		public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
		{
			NSDictionary aps = userInfo.ObjectForKey(new NSString("aps")) as NSDictionary;

			string alert = string.Empty;
			if (aps.ContainsKey(new NSString("alert")))
				alert = (aps[new NSString("alert")] as NSString).ToString();

			// Show alert
			if (!string.IsNullOrEmpty(alert))
			{
				UIAlertView avAlert = new UIAlertView("Notification", alert, null, "OK", null);
				avAlert.Show();
			}
		}

		public override bool OpenUrl (UIApplication application, NSUrl url, string sourceApplication, 
              NSObject annotation)
		{
			// Convert iOS NSUrl to C#/netxf/BCL System.Uri - common API
			Uri uri_netfx = new Uri(url.AbsoluteString);

			// load redirect_url Page
			AuthenticationState.Authenticator.OnPageLoading(uri_netfx);

			return true;
		}
	}
}
