﻿namespace ITBApp.Interfaces
{
	public interface ILocalNotificator
	{
		void Notificate(string title, string content);
	}
}
