﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel
{
	[DataContract]
	[KnownType(typeof(NotificationData))]
	public class NotificationListData
	{
		[DataMember]
		public List<NotificationData> notifications { get; set; }

		[DataMember]
		public Int64? LastId { get; set; }

	}
}
