﻿using System;
using Xamarin.Forms;

[assembly: Dependency(typeof(ITBApp.iOS.Implementations.IosLog))]
namespace ITBApp.iOS.Implementations
{
	public class IosLog : ILog
	{
		public IosLog()
		{
		}

		public void WriteLine(string line)
		{
			Console.WriteLine(line);
		}

		public void WriteLine(Exception exception)
		{
			Console.WriteLine(exception);
		}
	}
}
