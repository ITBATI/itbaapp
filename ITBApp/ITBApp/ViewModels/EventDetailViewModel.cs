﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ITBApp.Resources.i18n;
using ITBApp.Selectors;
using ITBApp.Services;

namespace ITBApp.ViewModels
{
    internal class EventDetailViewModel : BaseModel
    {
        private string eventTitle;
        private string eventDescription;
        private string eventUrl;
        private string quota;
        private string enrolled;
        private bool showInformationButton;
        private bool showEventInscription;

        private bool isEnrolled;
		public bool IsEnrolled
		{
			get
			{
				return this.isEnrolled;
			}
			set
			{
				this.isEnrolled = value;
				this.OnPropertyChanged("IsEnrolled");
			}
		}

		public string Quota
		{
			get
			{
                return this.quota;
			}
			set
			{
                this.quota = value;
				this.OnPropertyChanged("Quota");
			}
		}

		public string Enrolled
		{
			get
			{
                return this.enrolled;
			}
			set
			{
				this.enrolled = value;
				this.OnPropertyChanged("Enrolled");
			}
		}

        public string EventTitle
		{
			get
			{
				return this.eventTitle;
			}
			set
			{
				this.eventTitle = value;
				this.OnPropertyChanged("EventTitle");
			}
		}

		public string EventDescription
		{
			get
			{
				return this.eventDescription;
			}
            set
			{
				this.eventDescription = value;
				this.OnPropertyChanged("EventDescription");
			}
		}

		public string EventUrl
		{
			get
			{
				return this.eventUrl;
			}
            set
			{
				this.eventUrl = value;
				this.OnPropertyChanged("EventUrl");
                this.ShowInformationButton = !string.IsNullOrWhiteSpace(this.eventUrl);
			}
		}

        public bool ShowInformationButton
		{
			get
			{
                return this.showInformationButton;
			}
            set
            {
                this.showInformationButton = value;
                this.OnPropertyChanged("ShowInformationButton");
            }
		}

		public bool ShowEventInscription
		{
			get
			{
                return this.showEventInscription;
			}
			set
			{
				this.showEventInscription = value;
				this.OnPropertyChanged("ShowEventInscription");
			}
		}

        public EventDetailViewModel()
        {
        }

        public override async Task<string> LoadDataAsync()
        {
            return string.Empty;
        }

		/// <summary>
		/// Exports the schedule.
		/// </summary>
		/// <returns>The schedule.</returns>
        public async Task<string> ExportSchedule(List<EventViewModelData> events)
		{
			EventsService service = new EventsService();
			string calendarId = await service.createCalendar();

            foreach (EventViewModelData eventVM in events)
            {
				await service.createEvent(calendarId,
                                          eventVM.Title,
                                          Translate.classroom + eventVM.ITBAClassroom,
                                          eventVM.From.Remove(eventVM.From.IndexOf(' ')),
                                          eventVM.To.Remove(eventVM.To.IndexOf(' ')),
                                          eventVM.EventInstanceDate);
            }

			return Translate.added_events_to_calendar;
		}

        public List<EventDetailSelectorModel> GetEventsSelector(List<EventViewModelData> events, string eventDescription)
        {
            var result = new List<EventDetailSelectorModel>();

            //Add Event Description!
            result.Add(new EventDetailSelectorModel()
            {
                IsEventDescription = true,
                IsEndLineTemplate = false,
                EventDescription = eventDescription
            });


            foreach (var item in events)
            {
				result.Add(new EventDetailSelectorModel()
                {
                    IsEventDescription = false,
                    IsEndLineTemplate = false,
                    EventViewModel = item
				});

				result.Add(new EventDetailSelectorModel()
				{
                    IsEndLineTemplate = true,
                    IsEventDescription = false
				});
            }

            return result;
        }
    }
}
