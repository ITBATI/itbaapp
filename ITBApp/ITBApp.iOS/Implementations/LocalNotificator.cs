﻿using System;
using Foundation;
using ITBApp.Interfaces;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(ITBApp.iOS.Implementations.LocalNotificator))]
namespace ITBApp.iOS.Implementations
{
	public class LocalNotificator : ILocalNotificator
	{
		public void Notificate(string title, string content)
		{
			UILocalNotification notification = new UILocalNotification();
			//notification.AlertTitle = "Alert Title"; // required for Apple Watch notifications
			notification.AlertAction = title;
			notification.AlertBody = content;
			notification.FireDate = NSDate.FromTimeIntervalSinceNow(10);
			UIApplication.SharedApplication.ScheduleLocalNotification(notification);
		}
	}
}
