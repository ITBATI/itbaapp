﻿using System;
namespace ITBApp.Services
{
	public class AuthenticationError : Exception
	{
		public AuthenticationError(string message) : base(message){}

		public AuthenticationError(string message, Exception innerException) : base(message, innerException) { }
	}
}

