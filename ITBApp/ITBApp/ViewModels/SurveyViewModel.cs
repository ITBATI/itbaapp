﻿//-----------------------------------------------------------------------
// <copyright file="SurveyViewModel.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ITBApp.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using Helpers;
    using Model.DataModel.Survey;
    using Model.Handlers;
    using ITBApp.Interfaces;
    using Resources.i18n;
    using Selectors;
    using Services;
    using Xamarin.Forms;
    using ITBApp.Model;

    /// <summary>
    /// Survey Context View Model
    /// </summary>
    internal class SurveyViewModel : BaseModel
    {
        /// <summary>
        /// List of survey selection models  
        /// </summary>
        private ObservableCollection<SurveySelectorModel> surveySelectionsModel = new ObservableCollection<SurveySelectorModel>();

        /// <summary>
        /// List of survey selection models   
        /// </summary>
        private ObservableCollection<SurveyResultSelectorModel> surveyResultsModel = new ObservableCollection<SurveyResultSelectorModel>();

        /// <summary>
        /// Survey post data info   
        /// </summary>
        private SurveyPostData surveyPostData = new SurveyPostData();

        /// <summary>
        /// Survey description text  
        /// </summary>
        private string surveyDescription;

        /// <summary>
        /// True if there is a survey available   
        /// </summary>
        private bool isSurveyAvailable;

        /// <summary>
        /// Survey service instance   
        /// </summary>
        private SurveyService service = new SurveyService();

        /// <summary>
        /// True if survey was completed   
        /// </summary>
        private bool isSurveyCompleted;

        /// <summary>
        /// Show survey result 
        /// </summary>
        private bool displaySurveyResult;

        /// <summary>
        /// Show survey questions
        /// </summary>
        private bool displaySurveyQuestions;

        /// <summary>
        /// Question text    
        /// </summary>
        private string surveyQuestion;

        /// <summary>
        /// Text that show the count of people who completed the survey  
        /// </summary>
        private string totalSurveyCompleted;

        /// <summary>
        /// Initializes a new instance of the <see cref="SurveyViewModel"/> class
        /// </summary>
        public SurveyViewModel()
        {
            Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
            this.surveyPostData.Responses = new List<SurveyPostResponseData>();
            this.IsLoading = true;
        }

        /// <summary>
        /// Gets the Survey selection model list
        /// </summary>
        public ObservableCollection<SurveySelectorModel> SurveySelectionsModel
        {
            get 
            { 
                return this.surveySelectionsModel; 
            }

            private set
            {
                this.surveySelectionsModel = value;
                this.OnPropertyChanged("SurveySelectionsModel");
            }
        }

        /// <summary>
        /// Gets the Survey Results Model List
        /// </summary>
        public ObservableCollection<SurveyResultSelectorModel> SurveyResultsModel
        {
            get 
            { 
                return this.surveyResultsModel; 
            }

            private set
            {
                this.surveyResultsModel = value;
                this.OnPropertyChanged("SurveyResultsModel");
            }
        }

        /// <summary>
        /// Gets or sets the survey Description Text
        /// </summary>
        public string SurveyDescription
        {
            get
            {
                return this.surveyDescription;
            }

            set
            {
                this.surveyDescription = value;
                this.OnPropertyChanged("SurveyDescription");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether there is an available survey  
        /// </summary>
        public bool IsSurveyAvailable
        {
            get
            {
                return this.isSurveyAvailable;
            }

            set
            {
                this.isSurveyAvailable = value;
                this.OnPropertyChanged("IsSurveyAvailable");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the survey is completed
        /// </summary>
        public bool IsSurveyCompleted
        {
            get
            {
                return this.isSurveyCompleted;
            }

            set
            {
                this.isSurveyCompleted = value;
                this.OnPropertyChanged("IsSurveyCompleted");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether survey result will be shown
        /// </summary>
        public bool DisplaySurveyResult
        {
            get
            {
                return this.displaySurveyResult;
            }

            set
            {
                this.displaySurveyResult = value;
                this.OnPropertyChanged("DisplaySurveyResult");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether survey questions will be shown
        /// </summary>
        public bool DisplaySurveyQuestions
        {
            get
            {
                return this.displaySurveyQuestions;
            }

            set
            {
                this.displaySurveyQuestions = value;
                this.OnPropertyChanged("DisplaySurveyQuestions");
            }
        }

        /// <summary>
        /// Gets or sets the survey questions
        /// </summary>
        public string SurveyQuestion
        {
            get
            {
                return this.surveyQuestion;
            }

            set
            {
                this.surveyQuestion = value;
                this.OnPropertyChanged("SurveyQuestion");
            }
        }

        /// <summary>
        /// Gets or sets the total survey completed
        /// </summary>
        public string TotalSurveyCompleted
        {
            get
            {
                return this.totalSurveyCompleted;
            }

            set
            {
                this.totalSurveyCompleted = value;
                this.OnPropertyChanged("TotalSurveyCompleted");
            }
        }

        /// <summary>
        /// Posts survey content to service
        /// </summary>
        /// <returns>
        /// True if was successful
        /// </returns>
        public async Task<bool> PostSurveyAsync()
        {
            try
            {
                this.IsLoading = true;
                this.ErrorMessage = null;

                if (this.surveyPostData.Responses.Count > 0)
                {
                    await this.service.PostSurveyResults(this.surveyPostData).ConfigureAwait(false);
                }

                // Obtengo los resultados de la encuesta y lo muestro
                var surveyActives = await this.service.GetActiveSurveysAsync().ConfigureAwait(false);
                if (surveyActives != null)
                {
                    this.SetSurveyResults(surveyActives.First().Id);
                }
                else
                {
                    this.IsSurveyCompleted = true;
                    this.IsSurveyAvailable = false;
                    this.IsLoading = false;
                }

                this.IsLoading = false;

                return true;
            }
            catch (ServerRequestException ex)
            {
                this.ErrorMessage = ex.Message;
            }
            catch (Exception)
            {
                this.ErrorMessage = Translate.surveypage_error_text;
            }

            this.IsLoading = false;
            this.IsSurveyAvailable = true;

            return false;
        }

        /// <summary>
        /// Indicates if survey has responses
        /// </summary>
        /// <returns>
        /// True if survey has responses
        /// </returns>
        public bool HasSurveyResponses()
        {
            return this.surveyPostData.Responses.Count > 0;
        }

        /// <summary>
        /// When user writes a choice 
        /// </summary>
        public void EditorTextWasChanged()
        {
            var stringQuestions = this.surveySelectionsModel.Where(x => !string.IsNullOrWhiteSpace(x.Response)).ToList();
            this.surveyPostData.Responses.RemoveAll(x => !string.IsNullOrWhiteSpace(x.Response));

            // Para el caso de preguntas que el usuario tiene que ingresar la respuesta no se debe enviar el questionOptionId.
            foreach (var item in stringQuestions)
            {
                this.surveyPostData.Responses.Add(new SurveyPostResponseData()
                {
                    QuestionId = item.QuestionId,
                    Response = item.Response
                });
            }
        }

        public async Task<string> LoadDataAsync(int surveyId, bool hasResult)
        {
			try
			{
				this.IsLoading = true;
				this.ErrorMessage = null;
				this.IsSurveyAvailable = false;
				this.DisplaySurveyResult = false;
				this.DisplaySurveyQuestions = false;

                if (hasResult)
                {
                    this.SetSurveyResults(surveyId);
                }
                else
                {					
                    var survey = await this.service.GetUnansweredSurveyByIdAsync(surveyId).ConfigureAwait(false);

					if (survey != null)
					{
						this.SetSurveyQuestions(survey);
						this.IsSurveyAvailable = true;
						this.DisplaySurveyQuestions = true;
					}                    
                }
			}
			catch (ServerRequestException ex)
			{
				this.ErrorMessage = ex.Message;
			}
			catch (Exception)
			{
				this.ErrorMessage = Translate.surveypage_error_text;
				this.IsSurveyAvailable = false;
			}

			this.IsLoading = false;

			return this.ErrorMessage;
            
        }

        /// <summary>
        /// Fills view model data
        /// </summary>
        /// <returns>
        /// Error message
        /// </returns>
        public override async Task<string> LoadDataAsync()
        {
            try
            {
                this.IsLoading = true;
                this.ErrorMessage = null;
                this.IsSurveyAvailable = false;
                this.DisplaySurveyResult = false;
                this.DisplaySurveyQuestions = false;

                var survey = await this.service.GetUnansweredSurveyAsync().ConfigureAwait(false);

                if (survey != null)
                {
                    this.SetSurveyQuestions(survey);
                    this.IsSurveyAvailable = true;
                    this.DisplaySurveyQuestions = true;
                }
                else
                {
                    var surveyActives = await this.service.GetActiveSurveysAsync().ConfigureAwait(false);
                    if (surveyActives != null && surveyActives.Count > 0)
                    {
                        this.SetSurveyResults(surveyActives.First().Id);
                    }
                    else
                    {
                        this.IsSurveyCompleted = true;
                        this.IsSurveyAvailable = false;
                    }
                }
            }
            catch (ServerRequestException ex)
            {
                this.ErrorMessage = ex.Message;
            }
            catch (Exception)
            {
                this.ErrorMessage = Translate.surveypage_error_text;
                this.IsSurveyAvailable = false;
            }

            this.IsLoading = false;

            return this.ErrorMessage;
        }

        /// <summary>
        /// Event when Survey page is opened or maximized
        /// </summary>
        /// <param name="sender">
        /// Object that sends the event
        /// </param>
        /// <param name="eventArgs">
        /// Event parameters
        /// </param>
        protected override async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
        {
            /*
            App.CurrentPage = PageId.SURVEY;

            if (this.SurveySelectionsModel.Count == 0)
            {
                await Task.Run(() => this.LoadDataAsync());
            }
            */
        }

        /// <summary>
        /// Event when Survey page is closed or minimized
        /// </summary>
        /// <param name="sender">
        /// Object that sends the event
        /// </param>
        /// <param name="eventArgs">
        /// Event parameters 
        /// </param>
        protected override void CurrentPageOnDisappearing(object sender, EventArgs eventArgs)
        {
            App.CurrentPage = PageId.SURVEY;
        }

        /// <summary>
        /// Set survey results to model
        /// </summary>
        /// <param name="surverId">
        /// Survey base id 
        /// </param>
        private async void SetSurveyResults(int surverId)
        {
            var surveyResults = await this.service.GetSurveyResultsAsync(surverId).ConfigureAwait(false);

            Device.BeginInvokeOnMainThread(()=>
            {
                int totalWidth = DependencyService.Get<IAppMethods>().GetWidth() / 2;

                var results = new ObservableCollection<SurveyResultSelectorModel>();
                foreach (var item in surveyResults)
                {
                    results.Add(new SurveyResultSelectorModel()
                    {
                        Question = item.QuestionOption,
                        ResultNumberToProgressBar = item.Percentage / 100,
                        PositiveWidth = Convert.ToInt32(totalWidth * item.Percentage / 100),
                        NegativeWidth = Convert.ToInt32(totalWidth * (100 - item.Percentage) / 100),
                        ResultNumberToShow = item.Percentage.Equals(0) ? "0 %" : string.Format("{0} %", item.Percentage.ToString("##.##").Replace(".", ","))
                    });
                }

                var total = surveyResults.Sum(x => x.Count);

                this.TotalSurveyCompleted = string.Format("sobre {0} encuestados", total);
                this.SurveyQuestion = surveyResults.First().Question;
                this.SurveyDescription = "¡Gracias por participar!";

                this.DisplaySurveyResult = true;
                this.DisplaySurveyQuestions = false;
                this.IsSurveyAvailable = true;
                this.SurveyResultsModel = results;
            });
        }

        /// <summary>
        /// Set survey information to model
        /// </summary>
        /// <param name="survey">
        /// Survey base
        /// </param>
        private void SetSurveyQuestions(SurveyData survey)
        {
            ObservableCollection<SurveySelectorModel> surveyModel = new ObservableCollection<SurveySelectorModel>();
            int indexSelector = 0;

            foreach (var question in survey.Questions)
            {
                if (question.Type == Model.QuestionType.Choice)
                {
                    this.SurveyDescription = question.Question;
                    foreach (var choice in question.Choices)
                    {
                    surveyModel.Add(
                        this.GetSurveySelectorModel(
                            indexSelector,
                            choice.IsWritingChoice,
                            choice.Title,
                            choice.Description,
                            choice.Id,
                            question.Id));

                    indexSelector++;
                    }
                }
                else
                {
                    surveyModel.Add(
                        this.GetSurveySelectorModel(
                            indexSelector,
                            true,
                            question.Question,
                            string.Empty,
                            0,
                            question.Id));

                    indexSelector++;
                }
            }

            // Agrego el template del boton de envio de encuestas.
            surveyModel.Add(this.GetSurveyButtonModel());

            this.SurveySelectionsModel = surveyModel;
        }

        /// <summary>
        /// Gets survey selector model (not for send button)
        /// </summary>
        /// <param name="id">
        /// Survey id
        /// </param>
        /// <param name="isWrtingChoice">
        /// If the choice is written ser
        /// </param>
        /// <param name="question">
        /// Question text
        /// </param>
        /// <param name="description">
        /// Survey description
        /// </param>
        /// <param name="choiceId">
        /// Choice id
        /// </param>
        /// <param name="questionId">
        /// Question id
        /// </param>
        /// <returns>
        /// SurveySelectorMode filled
        /// </returns>
        private SurveySelectorModel GetSurveySelectorModel(long id, bool isWrtingChoice, string question, string description, long choiceId, long questionId)
        {
            return new SurveySelectorModel()
            {
                Id = id,
                IsWritingChoice = isWrtingChoice,
                Question = question,
                Description = description,
                IsSelected = false,
                ChoiceId = choiceId,
                QuestionId = questionId,
                IsSendButton = false,
                TapChoiceCommand = new Command(this.OnTappedChoiceCommand)
            };
        }

        /// <summary>
        /// Gets survey selector model for button
        /// </summary>
        /// <returns>
        /// SurveySelectorModel (for button)
        /// </returns>
        private SurveySelectorModel GetSurveyButtonModel()
        {
            return new SurveySelectorModel()
            {
                IsSendButton = true
            };
        }

        /// <summary>
        /// Event when select choice
        /// </summary>
        /// <param name="choiceIdObject">
        /// Choice Id
        /// </param>
        private void OnTappedChoiceCommand(object choiceIdObject)
        {
            long choiceId = (long)choiceIdObject;
            var choice = this.surveySelectionsModel.Where(x => x.Id == choiceId).FirstOrDefault();

            if (choice != null)
            {
                if (choice.IsSelected)
                {
                    choice.IsSelected = false;
                    this.SetSurveyPostData(choice, false);
                    return;
                }
                else
                {
                    choice.IsSelected = true;
                    foreach (var item in this.surveySelectionsModel.Where(x => x.Id != choiceId).ToList())
                    {
                        item.IsSelected = false;
                    }

                    this.SetSurveyPostData(choice, true);

                    return;
                }
            }
        }

        /// <summary>
        /// Event when select choice
        /// </summary>
        /// <param name="choice">
        /// Input choice
        /// </param>
        /// <param name="wasSelected">
        /// True if choice was selected
        /// </param>
        private void SetSurveyPostData(SurveySelectorModel choice, bool wasSelected)
        {
            if (choice != null)
            {
                if (wasSelected)
                {
                    this.surveyPostData.Responses.RemoveAll(x => x.QuestionId == choice.QuestionId &&
                        x.QuestionOptionId != choice.ChoiceId);

                    this.surveyPostData.Responses.Add(new SurveyPostResponseData()
                    {
                        QuestionId = choice.QuestionId,
                        QuestionOptionId = choice.ChoiceId,
                        Response = string.Empty
                    });
                }
                else
                {
                    this.surveyPostData.Responses.RemoveAll(x => x.QuestionId == choice.QuestionId &&
                        x.QuestionOptionId == choice.ChoiceId);
                }
            }
        }
    }
}
