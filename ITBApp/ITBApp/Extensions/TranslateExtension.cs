﻿//-----------------------------------------------------------------------
// <copyright file="TranslateExtension.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ITBApp.Extensions
{
    using System;
    using System.Globalization;
    using System.Reflection;
    using System.Resources;
    using ITBApp.Interfaces;
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [ContentProperty("Text")]

    /// <summary>
    /// Extension for literal translations
    /// </summary>
    public class TranslateExtension : IMarkupExtension
    {
        /// <summary>
        /// Resource Id for Extension
        /// </summary>
        private const string ResourceId = "ITBApp.Resources.i18n.Translate";

        /// <summary>
        /// The Culture Info.
        /// </summary>
        private readonly CultureInfo ci;

        /// <summary>
        /// Initializes a new instance of the <see cref="TranslateExtension"/> class.
        /// </summary>
        public TranslateExtension()
        {
            this.ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
        }

        /// <summary>
        /// Gets or sets the literal text.
        /// </summary>
        /// <value>The literal text.</value>
        public string Text { get; set; }

        /// <summary>
        /// Provides the value.
        /// </summary>
        /// <returns>The value.</returns>
        /// <param name="serviceProvider">Service provider.</param>
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (this.Text == null)
            {
                return string.Empty;
            }

            ResourceManager resmgr = new ResourceManager(
                ResourceId, 
                typeof(TranslateExtension).GetTypeInfo().Assembly);

            var translation = resmgr.GetString(this.Text, this.ci);

            if (translation == null)
            {
#if DEBUG
                throw new ArgumentException(
                    String.Format("Key '{0}' was not found in resources '{1}' for culture '{2}'.", this.Text, ResourceId, ci.Name),
                    "Text");
#else
                translation = this.Text; // HACK: returns the key, which GETS DISPLAYED TO THE USER
#endif
            }

            return translation;
        }
    }
}
