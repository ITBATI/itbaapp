﻿using System;
using Android.Text.Util;
using Android.Util;
using Android.Widget;
using ITBApp;
using ITBApp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(HyperLinkLabel), typeof(HyperLinkLabelRender))]
namespace ITBApp.Droid
{
	public class HyperLinkLabelRender : LabelRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged(e);
			HyperLinkLogic();
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			HyperLinkLogic();
		}

		private void HyperLinkLogic()
		{
			var view = (HyperLinkLabel)Element;
			if (view == null) return;

			TextView textView = new TextView(Forms.Context);
			textView.LayoutParameters = new LayoutParams(LayoutParams.WrapContent, LayoutParams.WrapContent);

			// Setting the auto link mask to capture all types of link-able data
			textView.AutoLinkMask = MatchOptions.All;
			// Make sure to set text after setting the mask
			textView.Text = view.Text;
			textView.SetTextSize(ComplexUnitType.Dip, (float)view.FontSize);
			textView.SetTextColor(view.TextColor.ToAndroid());

			// overriding Xamarin Forms Label and replace with our native control
			SetNativeControl(textView);
		}
	}
}
