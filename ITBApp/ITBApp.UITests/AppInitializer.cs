﻿using Xamarin.UITest;

namespace ITBApp.UITests
{
	public class AppInitializer
	{
		public static IApp StartApp(Platform platform)
		{
			if (platform == Platform.Android)
			{
				return ConfigureApp
					.Android
					.ApkFile("../../../ITBApp.Droid/bin/Debug/ar.edu.itba.itbaapp-Signed.apk")
					.DeviceSerial("169.254.109.177:5555")
					.StartApp();
			}

			return ConfigureApp
				.iOS
				.AppBundle("../../../ITBApp.iOS/bin/iPhoneSimulator/Debug/ITBAppiOS.app")
				.DeviceIdentifier("82205BAE-1A81-462B-B6D7-4656E4220969")
				.StartApp();
		}
	}
}

