﻿using System;
using Xamarin.Forms;

namespace ITBApp.Selectors
{
    public class EventDetailSelector : DataTemplateSelector
    {
        public DataTemplate DayTemplate { get; set; }
        public DataTemplate EventDescriptionTemplate { get; set; }
        public DataTemplate EndLineTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if (item == null) return null;

            if (((EventDetailSelectorModel)item).IsEventDescription)
                return EventDescriptionTemplate;

            if (((EventDetailSelectorModel)item).IsEndLineTemplate)
				return EndLineTemplate;

            return DayTemplate;
        }
    }
}
