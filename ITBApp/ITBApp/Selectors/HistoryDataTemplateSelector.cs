﻿using System;
using ITBApp.ViewModels;
using Xamarin.Forms;

namespace ITBApp.Selectors
{
    public class HistoryDataTemplateSelector : DataTemplateSelector
    {
		public DataTemplate QuarterCollapsedTemplate { get; set; }
        public DataTemplate QuarterNotCollapsedTemplate { get; set; }
        public DataTemplate SubjectTemplate { get; set; }

		protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
		{
			if (item == null) return null;
			HistoryItemViewModel historyItemViewModel = (HistoryItemViewModel)item;
            if (historyItemViewModel.IsQuarterCollapsed) return QuarterCollapsedTemplate;
            if (historyItemViewModel.IsSubject) return SubjectTemplate;
            else return QuarterNotCollapsedTemplate;
		}
    }
}
