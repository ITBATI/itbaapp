﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(ITBApp.UWP.Implementations.Localize))]
namespace ITBApp.UWP.Implementations
{
    public class Localize : ITBApp.Model.Interfaces.ILocalize
    {
        public CultureInfo GetCurrentCultureInfo()
        {
            return System.Globalization.CultureInfo.CurrentCulture;
        }
    }
}
