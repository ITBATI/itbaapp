namespace ITBApp.Services
{
	public abstract class ServiceConstants
	{
		public const string APP_VERSION = "appversion";
		public const string COURSES_DATA = "coursesData";
		public const string USER_DATA = "userData";
		public const string NOTIFICATIONS_DATA = "notificationsData";
		public const string DEVICE_ID_DATA = "deviceId";
		public const string DEVICE_TYPE_DATA = "deviceType";
		public const string ACCOUNT_BALANCE_DATA = "accountBalanceData";
		public const string USER_PROFILE_DATA = "userProfileData";
		public const string FREE_CLASSROOM_DATA = "freeClassroomData";
		public const string PENDING_EXAMS_DATA = "pendingExamsData";
		public const string NOTIFICATION_TOKEN_CHANGED = "notification_token_changed";
        public const string ACCESS_TOKEN = "access_token";
        public const string ACCESS_TOKEN_EXPIRATION = "access_token_expiration";
		public const string EXPIRES_IN = "expires_in";
		public const string REFRESH_TOKEN ="refresh_token";
		public const string FORCED_RELOGIN = "forced_relogin";
		public const string NOTIFICATIONS_LAST_ID = "notificationsLastId";
        public const string ACADEMIC_HISTORY_DATA = "academicHistoryData";
        public const string EVENTS_DATA = "eventsData";
        public const string LOCATIONS_DATA = "locationsData";
        public const string WEATHER_DATA = "weatherData";
        public const string WORK_REQUEST_DATA = "workRequestData";
        public const string BAR_MENU_DATA = "barMenuData";
        public const string WORK_GROUPS_REQUEST_DATA = "workGroupsRequestData";

		public const int CLASES_EXPIRATION_IN_MINUTES = 30;
		public static readonly int NOTIFICATION_LIST_LIMIT = 30;

		public const int ACCOUNT_BALANCE_EXPIRATION_IN_DAYS = 1;
		public const int ACCOUNT_BALANCE_ITEMS_PER_PAGE = 15;

		public const int PENDING_EXAMS_EXPIRATION_IN_DAYS = 1;

		public const int USER_PROFILE_EXPIRATION_IN_DAYS = 1;

		public const int FREE_CLASSROOM_EXPIRATION_IN_MINUTES = 1;
		public const int FREE_CLASSROOM_SOON_RANGE_IN_MINUTES = 30;

		public const string CLASSROOMS_AVAILABLE_FROM = "07:30";
		public const string CLASSROOMS_AVAILABLE_TO = "22:00";
        public const int ACADEMIC_HISTORY_EXPIRATION_IN_MINUTES = 60;

        public const int EVENTS_EXPIRATION_IN_MINUTES = 60;
        public const int EVENTS_DAYS_SHOWN = 14;

        public const int WEATHER_EXPIRATION_IN_MINUTES = 15;
        public const int LOCATIONS_EXPIRATION_IN_DAYS = 1;
        public const int WORK_REQUEST_EXPIRATION_IN_MINUTES = 30;
        public const int GEOLOCATION_EXPIRATION_IN_SECONDS = 60;

        public const int BAR_MENU_EXPIRATION_IN_MINUTES = 15;
	}
}
