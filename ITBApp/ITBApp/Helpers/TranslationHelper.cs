﻿//-----------------------------------------------------------------------
// <copyright file="TranslationHelper.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace ITBApp.Helpers
{
    /// <summary>
    /// Translation helper.
    /// </summary>
    public static class TranslationHelper
    {
        /// <summary>
        /// Gets the alert title.
        /// </summary>
        /// <returns>The alert title.</returns>
        public static string GetAlertTitle()
        {
            return ITBApp.Resources.i18n.Translate.alert_title;
        }

        /// <summary>
        /// Gets the new contacts succesful.
        /// </summary>
        /// <returns>The new contacts succesful.</returns>
        /// <param name="name">User Name.</param>
        public static string GetNewContactsSuccesful(string name)
        {
            return ITBApp.Resources.i18n.Translate.new_contact_succesful.Replace("{0}", name);
        }

        /// <summary>
        /// Gets the new contacts error message.
        /// </summary>
        /// <returns>The new contacts error message.</returns>
        /// <param name="name">User Name.</param>
        public static string GetNewContactsError(string name)
        {
            return ITBApp.Resources.i18n.Translate.new_contact_error.Replace("{0}", name);
        }

        /// <summary>
        /// Gets the new contacts access error message for ios.
        /// </summary>
        /// <returns>The new contacts access error message for ios.</returns>
        /// <param name="name">User Name.</param>
        public static string GetNewContactsAccessErrorIOS(string name)
        {
            return ITBApp.Resources.i18n.Translate.new_contact_access_error_ios.Replace("{0}", name);
        }

        /// <summary>
        /// Gets the new contacts version error message for ios.
        /// </summary>
        /// <returns>The new contacts version error message for ios.</returns>
        /// <param name="phoneNumber">Phone number.</param>
        public static string GetNewContactsVersionErrorIOS(string phoneNumber)
        {
            return ITBApp.Resources.i18n.Translate.new_contact_version_error_ios.Replace("{0}", phoneNumber);
        }
    }
}
