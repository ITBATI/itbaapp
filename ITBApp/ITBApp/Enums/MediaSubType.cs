﻿namespace ITBApp.Enums
{
	public enum MediaSubType
	{
		PDF,
		BMP,
		DIB,
		GIF,
		JFIF,
		JIF,
		JPE,
		JPEG,
		JPG,
		PNG,
		TIF,
		TIFF
	}
}