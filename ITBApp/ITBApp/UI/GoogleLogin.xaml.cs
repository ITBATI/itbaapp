﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ITBApp.Extensions;
using ITBApp.Interfaces;
using ITBApp.Services;
using Xamarin.Auth;
using Xamarin.Auth.XamarinForms;
using Xamarin.Forms;

namespace ITBApp.UI
{
    public partial class GoogleLogin : ContentPage
    {
        public GoogleLogin()
        {
            InitializeComponent();
        }

        Xamarin.Auth.OAuth2Authenticator authenticator = null;

        void DoLogin(object sender, EventArgs args)
        {
            if (App.IsTesting)
            {
                App.SaveTokenAsync("12345", "12345", int.MaxValue);
            }
            else 
            {
                var authService = new GoogleAuthService();
                authenticator = authService.GetAuthenticator();

                authenticator.Completed +=
                    (s, ea) =>
                    {
                        if (ea.Account != null && ea.Account.Properties != null)
                        {
                            App.SaveTokenAsync(ea.Account);
                        }
                        else
                        {
                            throw new AuthException("Not authenticated ");
                        }
                    };

                authenticator.Error +=
                    (s, ea) =>
                    {
                        throw new AuthException(ea.Message);
                    };

                // after initialization (creation and event subscribing) exposing local object 
                AuthenticationState.Authenticator = authenticator;

                authService.PresentUILoginScreen(authenticator);
            }

			return;
        }

		
    }
}
