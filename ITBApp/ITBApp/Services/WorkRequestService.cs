﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using ITBApp.Handlers;
using ITBApp.Helpers;
using ITBApp.Model.Handlers;
using ITBApp.Resources.i18n;
using ITBApp.Util.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.Services
{
    public class WorkRequestService
    {
		private TokenHolder TokenHolder = new TokenHolder();

		public async Task DeleteCachedDataAsync()
		{
            Application.Current.Properties.Remove(ServiceConstants.WORK_REQUEST_DATA);
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
		}

		private WorkRequestData GetSavedWorkRequestData()
		{
            if (Application.Current.Properties.ContainsKey(ServiceConstants.WORK_REQUEST_DATA))
			{
				string json = (string)Application.Current.Properties[ServiceConstants.WORK_REQUEST_DATA];
				WorkRequestData data = (WorkRequestData)SerializerHelper.Deserialize(json, (new WorkRequestData()).GetType());

				if (data.Expiration > DateTime.Now || !CrossConnectivity.Current.IsConnected)
				{
					return data;
				}
			}

			return null;
		}

		private WorkGroupsRequestData GetSavedWorkGroupsRequestData()
		{
            if (Application.Current.Properties.ContainsKey(ServiceConstants.WORK_GROUPS_REQUEST_DATA))
			{
				string json = (string)Application.Current.Properties[ServiceConstants.WORK_GROUPS_REQUEST_DATA];
				WorkGroupsRequestData data = (WorkGroupsRequestData)SerializerHelper.Deserialize(json, (new WorkGroupsRequestData()).GetType());

				if (data.Expiration > DateTime.Now || !CrossConnectivity.Current.IsConnected)
				{
					return data;
				}
			}

			return null;
		}

        private async void SaveWorkRequestData(WorkRequestData data)
		{
			string json = SerializerHelper.Serialize(data);
            Application.Current.Properties[ServiceConstants.WORK_REQUEST_DATA] = json;
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
		}

		private async void SaveWorkGroupsRequestData(WorkGroupsRequestData data)
		{
			string json = SerializerHelper.Serialize(data);
            Application.Current.Properties[ServiceConstants.WORK_GROUPS_REQUEST_DATA] = json;
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
		}

        public async Task<WorkGroupsRequestData> GetWorkGroupsRequestsAsync()
        {
			WorkGroupsRequestData workGroupsRequestData = GetSavedWorkGroupsRequestData();

			if (workGroupsRequestData == null)
			{
				if (!CrossConnectivity.Current.IsConnected)
				{
					throw new ServerRequestException(Translate.work_request_no_internet_text);
				}

				try
				{
					string token = await TokenHolder.GetTokenAsync().ConfigureAwait(false);

					try
					{
						var stringResponse = await HttpUtility
                            .MakeGetCallAsync(String.Format("{0}{1}", AppSettings.WorkGroupsRequestURL, token))
							.ConfigureAwait(false);

						workGroupsRequestData = ParseWorkGroupsResponse(stringResponse);
						workGroupsRequestData.Expiration = DateTime.Now.AddMinutes(ServiceConstants.WORK_REQUEST_EXPIRATION_IN_MINUTES);

						SaveWorkGroupsRequestData(workGroupsRequestData);
					}
					catch
					{
						throw new ServerRequestException(Translate.work_request_service_error);
					}
				}
				catch (TokenExpiredException)
				{
					throw new ServerRequestException(Translate.work_request_no_internet_text);
				}

			}

			return workGroupsRequestData;
            
        }

        public async Task<bool> PostAddNewNewWork(AddWorkRequest addWorkRequest)
		{
			if (!CrossConnectivity.Current.IsConnected)
			{
				throw new ServerRequestException(Translate.work_request_no_internet_text);
			}

			try
			{
				var content = JsonConvert.SerializeObject(addWorkRequest);
				string token = await TokenHolder.GetTokenAsync().ConfigureAwait(false);

				await HttpUtility
                    .MakePostCallAsync(String.Format("{0}{1}", AppSettings.WorkAddRequestURL, token), content)
					.ConfigureAwait(false);


				return true;

			}
			catch (ServerRequestException)
			{
				throw new ServerRequestException("No se pudo enviar tu opinión, volvé a intentar más tarde");
			}
			catch (TokenExpiredException)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}
		}

		public async Task<WorkRequestData> GetWorkRequestsAsync()
		{
			WorkRequestData workRequestData = GetSavedWorkRequestData();

			if (workRequestData == null)
			{
				if (!CrossConnectivity.Current.IsConnected)
				{
					throw new ServerRequestException(Translate.work_request_no_internet_text);
				}

				try
				{
					string token = await TokenHolder.GetTokenAsync().ConfigureAwait(false);

					try
					{
						var stringResponse = await HttpUtility
                            .MakeGetCallAsync(String.Format("{0}{1}", AppSettings.WorkRequestURL, token))
							.ConfigureAwait(false);

						workRequestData = ParseResponse(stringResponse);
                        workRequestData.Expiration = DateTime.Now.AddMinutes(ServiceConstants.WORK_REQUEST_EXPIRATION_IN_MINUTES);

						SaveWorkRequestData(workRequestData);
					}
					catch
					{
						throw new ServerRequestException(Translate.work_request_service_error);
					}
				}
				catch (TokenExpiredException)
				{
					throw new ServerRequestException(Translate.work_request_no_internet_text);
				}

			}

			return workRequestData;
		}

        private WorkGroupsRequestData ParseWorkGroupsResponse(string response)
        {
            var result = new WorkGroupsRequestData()
            {
                Groups = new List<WorkGroupRequestData>()
            };

			try
			{
				JObject jsonResponse = JObject.Parse(response);
				var groups = jsonResponse["groups"];
                if (groups.HasValues)
                {
                    var group = groups["group"];
                    if (group != null)
                    {
                        foreach (var groupItem in group)
                        {
                            result.Groups.Add(new WorkGroupRequestData()
                            {
                                Id = (int)groupItem["@id"],
                                Name = (string)groupItem["@name"]
                            });
                        }
                    }
                }
			}
			catch (Exception exc)
			{
				throw new ServerRequestException("Error al procesar los datos de tus trámites");
			}

			return result;
		}

        private WorkRequestData ParseResponse(string response)
		{			
			var result = new WorkRequestData()
			{
                Changesets = new List<WorkRequestChangesetData>()
			};

			try
			{
				JObject jsonResponse = JObject.Parse(response);
				var changesets = jsonResponse["workRequestChangesets"];
				if (changesets.HasValues)
				{
					if (changesets["workRequestChangeset"] is JArray)
					{
						foreach (var changesetEntry in changesets["workRequestChangeset"])
						{
                            var changesetData = new WorkRequestChangesetData()
							{
								Id = (string)changesetEntry["id"],
								Description = (string)changesetEntry["description"],
                                Title = (string)changesetEntry["title"],
                                Resolution = (string)changesetEntry["resolution"],
                                Assignee = (string)changesetEntry["assignee"],
                                LastResponse = (string)changesetEntry["lastResponse"],
                                CreatedOn = DateTime.Parse((string)changesetEntry["createdOn"],CultureInfo.InvariantCulture)
							};

                            result.Changesets.Add(changesetData);
						}
					}
					else
					{
						var changesetEntry = changesets["workRequestChangeset"];
						var changesetData = new WorkRequestChangesetData()
						{
							Id = (string)changesetEntry["id"],
							Description = (string)changesetEntry["description"],
							Title = (string)changesetEntry["title"],
							Resolution = (string)changesetEntry["resolution"],
							Assignee = (string)changesetEntry["assignee"],
							LastResponse = (string)changesetEntry["lastResponse"],
                            CreatedOn = DateTime.Parse((string)changesetEntry["createdOn"], CultureInfo.InvariantCulture)
						};

                        result.Changesets.Add(changesetData);
					}
				}
			}
			catch (Exception exc)
			{
				throw new ServerRequestException("Error al procesar los datos de tus trámites");
			}

            return result;
		}
    }

	[DataContract]
	[KnownType(typeof(WorkRequestData))]
    public class WorkRequestData
    {
        [DataMember]
        public List<WorkRequestChangesetData> Changesets { get; set; }
        [DataMember]
        public DateTime Expiration { get; set; }
    }

	[DataContract]
	[KnownType(typeof(WorkRequestChangesetData))]
    public class WorkRequestChangesetData
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Resolution { get; set; }
        [DataMember]
        public string Assignee { get; set; }
        [DataMember]
        public string LastResponse { get; set; }
        [DataMember]
        public DateTime CreatedOn { get; set; }
    }

	[DataContract]
	[KnownType(typeof(WorkGroupsRequestData))]
	public class WorkGroupsRequestData
	{
		public List<WorkGroupRequestData> Groups { get; set; }
		public DateTime Expiration { get; set; }
	}

    [DataContract]
    [KnownType(typeof(WorkGroupRequestData))]
    public class WorkGroupRequestData
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    [DataContract]
    [KnownType(typeof(WorkRequestChangeset))]
    public class WorkRequestChangeset
    {
        [DataMember]
        [JsonProperty("title")]
		public string Title { get; set; }
		[DataMember]
        [JsonProperty("description")]
        public string Description { get; set; }
		[DataMember]
        [JsonProperty("group")]
        public string Group { get; set; }
        [DataMember]
        [JsonProperty("creator")]
        public string Creator { get; set; }
	}

	[DataContract]
	[KnownType(typeof(AddWorkRequest))]
	public class AddWorkRequest
	{
		[DataMember]
        [JsonProperty("workRequestChangeset")]
        public WorkRequestChangeset WorkRequestChangeset { get; set; }
	}
}