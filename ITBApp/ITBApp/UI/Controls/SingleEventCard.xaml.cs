﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using ITBApp.Model.DataModel;
using ITBApp.ViewModels;
using Xamarin.Forms;

namespace ITBApp.UI.Controls
{
    public partial class SingleEventCard : ContentView
    {
		public static readonly BindableProperty TitleProperty = BindableProperty.Create("Title", typeof(string), typeof(SingleEventCard), null);
        public static readonly BindableProperty FromProperty = BindableProperty.Create("From", typeof(string), typeof(SingleEventCard), null);
        public static readonly BindableProperty ToProperty = BindableProperty.Create("To", typeof(string), typeof(SingleEventCard), null);
        public static readonly BindableProperty ITBAClassroomProperty = BindableProperty.Create("ITBAClassroom", typeof(string), typeof(SingleEventCard), null);
        public static readonly BindableProperty LocationProperty = BindableProperty.Create("Location", typeof(string), typeof(SingleEventCard), null);
        public static readonly BindableProperty EnrolledProperty = BindableProperty.Create("Enrolled", typeof(bool), typeof(SingleEventCard), false);

        public SingleEventCard()
        {
            InitializeComponent();
            Content.BindingContext = this;
		}

		public string Title
		{
			get { return (string)GetValue(TitleProperty); }
			set { SetValue(TitleProperty, value); }
		}

		public string From
		{
			get { return (string)GetValue(FromProperty); }
			set { SetValue(FromProperty, value); }
		}

		public string To
		{
			get { return (string)GetValue(ToProperty); }
			set { SetValue(ToProperty, value); }
		}

		public string ITBAClassroom
		{
			get { return (string)GetValue(ITBAClassroomProperty); }
			set { SetValue(ITBAClassroomProperty, value); }
		}

		public string Location
		{
			get { return (string)GetValue(LocationProperty); }
			set { SetValue(LocationProperty, value); }
		}

		public bool Enrolled
		{
			get { return (bool)GetValue(EnrolledProperty); }
			set { SetValue(EnrolledProperty, value); }
		}

    }
}
