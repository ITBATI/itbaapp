﻿using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel
{
	[DataContract]
	public class PaymentData
	{
		[DataMember]
		public string Amount { get; set; }

		[DataMember]
		public string Balance { get; set; }

		[DataMember]
		public string DueDate { get; set; }

		[DataMember]
		public string ReceiptDate { get; set; }


		private string receiptDescription;

		[DataMember]
		public string ReceiptDescription
		{
			get
			{
				return receiptDescription != null ? receiptDescription.ToUpper() : null;
			}
			set
			{
				receiptDescription = value;
			}
		}

		[DataMember]
		public string ReceiptId { get; set; }
	}
}
