﻿//-----------------------------------------------------------------------
// <copyright file="OAuth2AuthenticatorExtension.cs" company="ITBA">
//     Copyright (c) Xamarin Inc. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
//
//  Copyright 2012-2014, Xamarin Inc.
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

namespace ITBApp.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Xamarin.Auth;
    using Xamarin.Utilities;

    /// <summary>
    /// Custom Authenticator Extension
    /// </summary>
    public class OAuth2AuthenticatorExtension : OAuth2Authenticator
    {
        /// <summary>
        /// Google client id
        /// </summary>
        private string clientId;

        /// <summary>
        /// Google client secret
        /// </summary>
        private string clientSecret;

        /// <summary>
        /// Google scopes required
        /// </summary>
        private string scope;

        /// <summary>
        /// Authorization Url
        /// </summary>
        private Uri authorizeUrl;

        /// <summary>
        /// Redirect Url if something weird happens in login
        /// </summary>
        private Uri redirectUrlCopy;

        /// <summary>
        /// Google Api URL
        /// </summary>
        private Uri accessTokenUrl;

        /// <summary>
        /// Function to get username
        /// </summary>
        private GetUsernameAsyncFunc getUsernameAsync;

        /// <summary>
        /// state of the request
        /// </summary>
        private string requestState;

        /// <summary>
        /// Reported Forgery setting
        /// </summary>
        private bool reportedForgery = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="OAuth2AuthenticatorExtension"/> class.
        /// that authenticates using authorization codes (code).
        /// </summary>
        /// <param name='clientId'>
        /// Client identifier.
        /// </param>
        /// <param name='clientSecret'>
        /// Client secret.
        /// </param>
        /// <param name='scope'>
        /// Authorization scope.
        /// </param>
        /// <param name='authorizeUrl'>
        /// Authorize URL.
        /// </param>
        /// <param name='redirectUrl'>
        /// Redirect URL.
        /// </param>
        /// <param name='accessTokenUrl'>
        /// URL used to request access tokens after an authorization code was received.
        /// </param>
        /// <param name='getUsernameAsync'>
        /// Method used to fetch the username of an account
        /// after it has been successfully authenticated.
        /// </param>
        public OAuth2AuthenticatorExtension(string clientId, string clientSecret, string scope, Uri authorizeUrl, Uri redirectUrl, Uri accessTokenUrl, GetUsernameAsyncFunc getUsernameAsync = null)
            : base(clientId, clientSecret, scope, authorizeUrl, redirectUrl, accessTokenUrl)
        {
            if (string.IsNullOrEmpty(clientId))
            {
                throw new ArgumentException("clientId must be provided", "clientId");
            }

            this.clientId = clientId;

            if (string.IsNullOrEmpty(clientSecret))
            {
                throw new ArgumentException("clientSecret must be provided", "clientSecret");
            }

            this.clientSecret = clientSecret;

            this.redirectUrlCopy = redirectUrl;

            this.scope = scope ?? string.Empty;

            if (authorizeUrl == null)
            {
                throw new ArgumentNullException("authorizeUrl");
            }

            this.authorizeUrl = authorizeUrl;

            if (accessTokenUrl == null)
            {
                throw new ArgumentNullException("accessTokenUrl");
            }

            this.accessTokenUrl = accessTokenUrl;

            this.getUsernameAsync = getUsernameAsync;

            // Generate a unique state string to check for forgeries
            var chars = new char[16];
            var rand = new Random();
            for (var i = 0; i < chars.Length; i++)
            {
                chars[i] = (char)rand.Next((int)'a', (int)'z' + 1);
            }

            this.requestState = new string(chars);
        }

        /// <summary>
        /// Gets the client identifier.
        /// </summary>
        /// <value>The client identifier.</value>
        public string ClientId
        {
            get { return this.clientId; }
        }

        /// <summary>
        /// Gets the client secret.
        /// </summary>
        /// <value>The client secret.</value>
        public string ClientSecret
        {
            get { return this.clientSecret; }
        }

        /// <summary>
        /// Gets the authorization scope.
        /// </summary>
        /// <value>The authorization scope.</value>
        public string Scope
        {
            get { return this.scope; }
        }

        /// <summary>
        /// Gets the authorize URL.
        /// </summary>
        /// <value>The authorize URL.</value>
        public Uri AuthorizeUrl
        {
            get { return this.authorizeUrl; }
        }

        /// <summary>
        /// Gets the access token URL.
        /// </summary>
        /// <value>The URL used to request access tokens after an authorization code was received.</value>
        public Uri AccessTokenUrl
        {
            get { return this.accessTokenUrl; }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:ITBApp.Extensions.OAuth2AuthenticatorExtension"/> is implicit.
        /// </summary>
        /// <value><c>true</c> if is implicit; otherwise, <c>false</c>.</value>
        private bool IsImplicit
        {
            get
            {
                return this.accessTokenUrl == null;
            }
        }

        /// <summary>
        /// Method that returns the initial URL to be displayed in the web browser.
        /// </summary>
        /// <returns>
        /// A task that will return the initial URL.
        /// </returns>
        public override Task<Uri> GetInitialUrlAsync()
        {
            var url = new Uri(string.Format(
                "{0}?client_id={1}&redirect_uri={2}&response_type={3}&scope={4}&state={5}&hd=itba.edu.ar&access_type=offline&approval_prompt=force",
                this.authorizeUrl.AbsoluteUri,
                Uri.EscapeDataString(this.clientId),
                Uri.EscapeDataString(this.redirectUrlCopy.AbsoluteUri),
                this.IsImplicit ? "token" : "code",
                Uri.EscapeDataString(this.scope),
                Uri.EscapeDataString(this.requestState)));

            var tcs = new TaskCompletionSource<Uri>();
            tcs.SetResult(url);
            return tcs.Task;
        }

        /// <summary>
        /// Requests for a new token
        /// </summary>
        /// <param name='refreshToken'>
        /// Refresh token provided by Google Api
        /// </param>
        /// <returns>
        /// Gets the Google Api results as a Dictionary
        /// </returns>
        public virtual Task<IDictionary<string, string>> RequestRefreshTokenAsync(string refreshToken)
        {
            var queryValues = new Dictionary<string, string>
            {
                { "refresh_token", refreshToken },
                { "client_id", this.ClientId },
                { "grant_type", "refresh_token" }
            };

            if (!string.IsNullOrEmpty(this.ClientSecret))
            {
                queryValues["client_secret"] = this.ClientSecret;
            }

            return this.RequestAccessTokenAsync(queryValues).ContinueWith(result =>
            {
                var accountProperties = result.Result;

                this.OnRetrievedAccountProperties(accountProperties);

                return accountProperties;
            });
        }

        /// <summary>
        /// Raised when a new page has been loaded.
        /// </summary>
        /// <param name='url'>
        /// URL of the page.
        /// </param>
        /// <param name='query'>
        /// The parsed query of the URL.
        /// </param>
        /// <param name='fragment'>
        /// The parsed fragment of the URL.
        /// </param>
        protected override void OnPageEncountered(Uri url, IDictionary<string, string> query, IDictionary<string, string> fragment)
        {
            var all = new Dictionary<string, string>(query);
            foreach (var kv in fragment)
            {
                all[kv.Key] = kv.Value;
            }

            // Check for forgeries
            if (all.ContainsKey("state"))
            {
                if (all["state"] != this.requestState && !this.reportedForgery)
                {
                    this.reportedForgery = true;
                    this.OnError("Invalid state from server. Possible forgery!");
                    return;
                }
            }

            // Continue processing
            base.OnPageEncountered(url, query, fragment);
        }

        /// <summary>
        /// Raised when a new page has been loaded.
        /// </summary>
        /// <param name='url'>
        /// URL of the page.
        /// </param>
        /// <param name='query'>
        /// The parsed query string of the URL.
        /// </param>
        /// <param name='fragment'>
        /// The parsed fragment of the URL.
        /// </param>
        protected override void OnRedirectPageLoaded(Uri url, IDictionary<string, string> query, IDictionary<string, string> fragment)
        {
            // Look for the access_token
            if (fragment.ContainsKey("access_token"))
            {
                // We found an access_token
                this.OnRetrievedAccountProperties(fragment);
            }
            else if (!this.IsImplicit)
            {
                // Look for the code
                if (query.ContainsKey("code"))
                {
                    var code = query["code"];
                    this.RequestAccessTokenAsync(code).ContinueWith(
                    task =>
                    {
                        if (task.IsFaulted)
                        {
                            this.OnError(task.Exception);
                        }
                        else
                        {
                            this.OnRetrievedAccountProperties(task.Result);
                        }
                    }, 
                    TaskScheduler.FromCurrentSynchronizationContext());
                }
                else
                {
                    this.OnError("Expected code in response, but did not receive one.");
                    return;
                }
            }
            else
            {
                this.OnError("Expected access_token in response, but did not receive one.");
                return;
            }
        }

        /// <summary>
        /// Asynchronously makes a request to the access token URL with the given parameters.
        /// </summary>
        /// <param name="queryValues">The parameters to make the request with.</param>
        /// <returns>The data provided in the response to the access token request.</returns>
        protected Task<IDictionary<string, string>> RequestAccessTokenAsync(IDictionary<string, string> queryValues)
        {
            var query = queryValues.FormEncode();

            var req = WebRequest.Create(this.accessTokenUrl);
            req.Method = "POST";
            var body = Encoding.UTF8.GetBytes(query);
            req.ContentType = "application/x-www-form-urlencoded";
            using (var s = WebRequestExtensions.GetRequestStream(req))
            {
                s.Write(body, 0, body.Length);
            }

            return req.GetResponseAsync().ContinueWith(task =>
            {
                var text = task.Result.GetResponseText();

                // Parse the response
                var data = text.Contains("{") ? WebEx.JsonDecode(text) : WebEx.FormDecode(text);

                if (data.ContainsKey("error"))
                {
                    throw new AuthException("Error authenticating: " + data["error"]);
                }
                else if (data.ContainsKey("access_token"))
                {
                    return data;
                }
                else
                {
                    throw new AuthException("Expected access_token in access token response, but did not receive one.");
                }
            });
        }

        /// <summary>
        /// Event handler that is fired when an access token has been retreived.
        /// </summary>
        /// <param name='accountProperties'>
        /// The retrieved account properties
        /// </param>
        protected virtual void OnRetrievedAccountProperties(IDictionary<string, string> accountProperties)
        {
            // Now we just need a username for the account
            if (this.getUsernameAsync != null)
            {
                this.getUsernameAsync(accountProperties).ContinueWith(
                    task =>
                    {
                        if (task.IsFaulted)
                        {
                            this.OnError(task.Exception);
                        }
                        else
                        {
                            this.OnSucceeded(task.Result, accountProperties);
                        }
                    },
                    TaskScheduler.FromCurrentSynchronizationContext());
            }
            else
            {
                this.OnSucceeded(string.Empty, accountProperties);
            }
        }

        /// <summary>
        /// Asynchronously requests an access token with an authorization <paramref name="code"/>.
        /// </summary>
        /// <returns>
        /// A dictionary of data returned from the authorization request.
        /// </returns>
        /// <param name='code'>The authorization code.</param>
        /// <remarks>Implements: http://tools.ietf.org/html/rfc6749#section-4.1</remarks>
        private Task<IDictionary<string, string>> RequestAccessTokenAsync(string code)
        {
            var queryValues = new Dictionary<string, string> 
            {
                { "grant_type", "authorization_code" },
                { "code", code },
                { "redirect_uri", this.redirectUrlCopy.AbsoluteUri },
                { "client_id", this.clientId },
            };
            if (!string.IsNullOrEmpty(this.clientSecret))
            {
                queryValues["client_secret"] = this.clientSecret;
            }

            return this.RequestAccessTokenAsync(queryValues);
        }
    }
}