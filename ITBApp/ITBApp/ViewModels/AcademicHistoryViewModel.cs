﻿//-----------------------------------------------------------------------
// <copyright file="AcademicHistoryViewModel.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ITBApp.ViewModels
{
    using System;
    using ITBApp.Model.DataModel;
    using System.Threading.Tasks;
    using System.Collections.ObjectModel;
    using ITBApp.Resources.i18n;
    using Xamarin.Forms;
    using ITBApp.Interfaces;
    using ITBApp.Helpers;
    using ITBApp.Services;
    using System.Linq;
    using System.Windows.Input;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Text.RegularExpressions;
    using ITBApp.Model.Handlers;

    internal class AcademicHistoryViewModel : BaseModel
    {
        private ObservableCollection<QuarterData> quarters = new ObservableCollection<QuarterData>();

        public ObservableCollection<HistoryItemViewModel> Items { get; set; }

        public ICommand DetailCommand { get; set; }
        public ICommand ShowSubjectsCommand { get; set; }
        public ICommand HideSubjectsCommand { get; set; }

        public ObservableCollection<QuarterData> Quarters
        {
            get
            {
                return this.quarters;
            }

            private set
            {
                this.quarters = value;
                this.OnPropertyChanged("Quarters");
            }
        }

        public AcademicHistoryViewModel()
        {
            Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
        }

        public override async Task<string> LoadDataAsync()
        {
            try
            {
                this.IsLoading = true;
                this.ErrorMessage = null;

                Quarters = new ObservableCollection<QuarterData>();
                AcademicHistoryData data = await new AcademicHistoryService().GetAcademicHistoryAsync();

                foreach (CourseEntryData course in data.Courses)
                {
                    int period = course.Period == "FirstSemester" ? 1 : 2;
                    int year = int.Parse(course.Year);
                    QuarterData quarter = Quarters.Where(x => x.Year == year && x.Quarter == period).FirstOrDefault();
                    if (quarter == null)
                    {
                        quarter = new QuarterData()
                        {
                            Year = year,
                            Quarter = period
                        };
                        Quarters.Add(quarter);
                    }

                    // Add course to period
                    SubjectData subject = quarter.Subjects.Where(x => x.CourseId == course.CourseId).FirstOrDefault();
                    if (subject == null)
                    {
                        string icon = string.Empty;
                        string translatedStatus = string.Empty;
                        Color statusColor = Color.FromHex("#00ADEE");
                        Color finalMarkColor = Color.FromHex("#00ADEE");

                        if (course.Status == "Approved")
                        {
                            icon = "historial_aprobada.png";
                            if (course.Mark == "Approved")
                            {
                                translatedStatus = "Aprobado";
                            }
                            else
                            {
                                translatedStatus = course.Mark;
                            }
                            statusColor = Color.FromHex("#77D5CD");
                        }
                        else if (course.Status == "Failed")
                        {
                            icon = "historial_desaprobada.png";
                            if (course.Mark == "Absent")
                            {
                                translatedStatus = "Ausente";
                                statusColor = Color.FromHex("#D11554");
                            }
                            else if (course.Mark == "Approved")
                            {
                                translatedStatus = "Aprobado";
                                statusColor = Color.FromHex("#77D5CD");
                            }
                            else
                            {
                                translatedStatus = course.Mark;
                                statusColor = Color.FromHex("#D11554");
                            }
                        }
                        else
                        {
                            // Enrolled
                            if (course.Mark != "Enrolled")
                            {
                                icon = "historial_warning.png";
                                translatedStatus = course.Mark;
                                statusColor = getColorForMark(translatedStatus,statusColor);
                            }
                            else
                            {
                                icon = "historial_cursando.png";
                                translatedStatus = "CURSANDO";
                            }
                        }

                        ObservableCollection<MarkData> marks = new ObservableCollection<MarkData>();

                        if (course.ExamResults != null)
                        {
                            foreach (ExamenResultCourseEntryData exams in course.ExamResults)
                            {
                                marks.Add(new MarkData()
                                {
                                    HasFinalMark = exams.FinalMark != null,
                                    FinalMark = exams.FinalMark!=null?exams.FinalMark:"---",
                                    FinalMarkColor = getColorForMark(exams.FinalMark,statusColor),
                                    FinalMarkDate = exams.Date,
                                    ActNumber = exams.ActNumber!= null ? exams.ActNumber : "---"
                                });
                            }

                            // Reverse list because its order is inverse
                            marks = new ObservableCollection<MarkData>(marks.Reverse());

                            // Add class mark to the first mark data
                            marks.First().ClassMark = translatedStatus != null ? translatedStatus : "---";
                            marks.First().ClassMarkColor = statusColor;
                        }
                        string finalMark = marks.Count() > 0 ? marks.First().FinalMark : "---";
                        finalMarkColor = getColorForMark(finalMark, statusColor);

						var hasHightResolution = UIHelper.HasMediumResolution() || UIHelper.HasHightResoultion();
						
						subject = new SubjectData()
                        {
                            Title = course.Name,
                            Credits = course.Credits,
                            Commission = course.CommissionName,
                            Code = course.Code,
                            Icon = icon,
                            ClassMark = translatedStatus!=null?translatedStatus:"---",
                            StatusColor = statusColor,
                            FontSize = hasHightResolution ? 14 : 12,
                            FinalMarkColor = finalMarkColor,
                            FinalMark = finalMark,
                            Marks = marks
                        };
                        quarter.Subjects.Add(subject);
                    }

                    // Show the first quarter
                    Quarters[0].Collapsed = false;

                    RefreshItems();
                }

            }
			catch (ServerRequestException ex)
			{
				this.ErrorMessage = ex.Message;
			}
			catch (Exception exception)
			{
				this.ErrorMessage = Translate.accountbalancepage_error_text;
			}

            this.IsLoading = false;

            return this.ErrorMessage;
        }

        private Color getColorForMark(string mark, Color defaultColor) {
            double result = 0;
            if (! double.TryParse(mark, out result)) return defaultColor;
            else if (string.IsNullOrEmpty(mark) || double.Parse(mark) < 4) {
                return Color.FromHex("#D11554");
            }
            else if (Regex.IsMatch(mark, @"\d"))
                return Color.FromHex("#77D5CD");
            
            return Color.FromHex("#00ADEE");
        }

		/// <summary>
		/// Cleans view model cache
		/// </summary>
		/// <returns>
		/// No value is returned
		/// </returns>
		public override async Task DeleteCache()
		{
            AcademicHistoryService service = new AcademicHistoryService();
			await service.DeleteCachedDataAsync();
		}

        public void RefreshItems() {
			Items = new ObservableCollection<HistoryItemViewModel>();

			foreach (QuarterData quarter in Quarters)
			{
				if (quarter.Collapsed)
				{
					Items.Add(new HistoryItemViewModel()
					{
                        Id = quarter.Id,
						Title = quarter.Title,
                        IsSubject = false,
						IsQuarterCollapsed = true
					});
				}
                else {
					Items.Add(new HistoryItemViewModel()
                    {
                        Id = quarter.Id,
                        Title = quarter.Title,
                        IsSubject = false,
						IsQuarterCollapsed = false
					});

                    foreach (SubjectData subject in quarter.Subjects)
                    {
						Items.Add(new HistoryItemViewModel()
						{
                            
                            Title = subject.Title,
                            IsSubject = true,
                            IsQuarterCollapsed = false,
                            Subject = subject
						});

                    }
                }
			}

			this.OnPropertyChanged("Items");
        }

        protected override async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
        {
            App.CurrentPage = PageId.ACADEMIC_HISTORY;

            if (this.Quarters.Count == 0)
            {
                await this.LoadDataAsync().ConfigureAwait(false);
            }
        }

        protected override void CurrentPageOnDisappearing(object sender, EventArgs eventArgs)
        {
            App.CurrentPage = PageId.USER_PROFILE;
        }
    }

    public class SubjectData
    {
        public string Title { get; set; }
        public string CourseId { get; set; }
        public string Credits { get; set; }
        public string Commission { get; set; }
        public string Code { get; set; }
        public string Icon { get; set; }
        public Color StatusColor { get; set; }
        public string Status { get; set; }
        public int FontSize { get; set; }
        public Color FinalMarkColor { get; set; }
        public string ClassMark { get; set; }
        public string FinalMark { get; set; }
        public ObservableCollection<MarkData> Marks { get; set; }
    }

    public class MarkData
    {
        public bool HasFinalMark { get; set; }
        public string FinalMark { get; set; }
        public string FinalMarkDate { get; set; }
        public Color ClassMarkColor { get; set; }
        public Color FinalMarkColor { get; set; }
        public string ClassMark { get; set; }
        public string ClassMarkDate { get; set; }
        public string ActNumber { get; set; }
    }

    public class HistoryItemViewModel {

            public string Id { get; set; }
	        public string Title { get; set; }
            public SubjectData Subject { get; set; }

		#region boolean selectors
		public bool IsQuarterCollapsed { get; set; }
        public bool IsSubject { get; set; }
		#endregion
	}

    public class QuarterData: INotifyPropertyChanged {

        public int Year { get; set; }
        public int Quarter { get; set; }
        public ObservableCollection<AcademicHistoryData> History { get; set; }
        public ObservableCollection<SubjectData> Subjects { get; set; }
        public bool Collapsed { get; set; }
        public ICommand ToggleCollapsed { get; set; }

        private int listViewHeight;
        public int ListViewHeight { 
            get {
                return this.listViewHeight;
            }
            set {
                this.listViewHeight = value;
                this.OnPropertyChanged("ListViewHeight");
            }
        }

        public string Id {
            get {
                return Year.ToString() + Quarter.ToString();
            }
        }

        public QuarterData() {
            this.History = new ObservableCollection<AcademicHistoryData>();
            this.Subjects = new ObservableCollection<SubjectData>();
            Collapsed = true;
            ListViewHeight = 0;
            ToggleCollapsed = new Command(() => { 
                this.Collapsed = !this.Collapsed;
                this.OnPropertyChanged("Collapsed");
            });
        }

        public string Title {
            get {
                string title = string.Empty;
                if (Quarter == 1) title += "1er ";
                else title += "2do ";

                title += "Cuatrimestre " + Year;

                return title;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

		public void OnPropertyChanged([CallerMemberName]string propertyName = null)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
    }
}
