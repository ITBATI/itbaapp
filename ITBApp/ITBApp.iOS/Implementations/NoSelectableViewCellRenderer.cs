﻿using System;
using Foundation;
using ITBApp.iOS.Implementations;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ViewCell), typeof(NoSelectableViewCellRenderer))]
namespace ITBApp.iOS.Implementations
{
    [Preserve(AllMembers = true)]
	public class NoSelectableViewCellRenderer: ViewCellRenderer
	{
		public override UITableViewCell GetCell(Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var cell = base.GetCell(item, reusableCell, tv);
			if (cell != null)
			{
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				item.Tapped += (a, b) =>
				{
					UpdateBackground(cell, item);
				};
			}
			return cell;
		}
	}
}
