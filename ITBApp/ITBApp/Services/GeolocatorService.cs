﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using ITBApp.Model.Handlers;
using ITBApp.Interfaces;
using ITBApp.Util.Helpers;
using Plugin.Connectivity;
using Plugin.Geolocator;
using Xamarin.Forms;
using ITBApp.Resources.i18n;
using Newtonsoft.Json.Linq;
using ITBApp.Helpers;
using ITBApp.Handlers;

namespace ITBApp.Services
{
    public class GeolocatorService
    {
        LocationsData Locations;
        private static string LastLocationId;
        private static DateTime geolocationExpiration = DateTime.MinValue;

        public async Task<string> GetLocationIdAsync()
        {
            var locationId = "0";

            if (geolocationExpiration > DateTime.Now)
            {
                return LastLocationId;
            }
            else
            {
                ITBALocation location = await new GeolocatorService().GetCurrentITBALocation().ConfigureAwait(false);
                if (location != null)
                {
                    locationId = location.Id;
                    LastLocationId = locationId;
                    geolocationExpiration = DateTime.Now.AddSeconds(ServiceConstants.GEOLOCATION_EXPIRATION_IN_SECONDS);
                }
            }
            if (locationId != "0") App.lastLocationId = locationId;
            return locationId;
        }

        public async Task<ITBALocation> GetCurrentITBALocation()
        {
            try {
                // Get ITBA Locations
                if (Locations == null) Locations = await GetLocationsAsync().ConfigureAwait(false);
            }
            catch
            {
                return null;
            }

            // First check for position
            ITBALocation resultLocation = null;

            try
            {
				var locator = CrossGeolocator.Current;
                var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10)).ConfigureAwait(false);
				if (position == null) return null;
				PositionPoint currentPoint = new PositionPoint()
				{
					Lat = position.Latitude,
					Long = position.Longitude
				};

				resultLocation = GetITBALocationForPosition(currentPoint);
            }
            catch (Exception exc)
            {
				// then check for wifi
				var ips = DependencyService.Get<IIPAddressManager>().GetIPAddress();
				foreach (string ip in ips)
				{
					if (resultLocation == null)
						resultLocation = GetITBALocationForIP(ip);
				}
            }

            return resultLocation;
        }

        public ITBALocation GetITBALocationForPosition(PositionPoint point)
        {
            foreach (ITBALocation location in Locations.ITBALocations)
			{
                if (DistanceBetweenPlaces(point, location.Point) <= location.Distance)
                    return location;
			}
			return null;
        }

        public ITBALocation GetITBALocationForIP(string ip)
        {
            ITBALocation locationFound = null;
            foreach (ITBALocation location in Locations.ITBALocations)
            {
                if (locationFound == null)
                {
                    foreach (IPRange range in location.IPRanges)
                    {
                        if (locationFound == null && range.IsInRange(ip.Split('.'))) 
                            locationFound = location;
                    }
                }
            }
            return locationFound;
        }

		// cos(d) = sin(φА)·sin(φB) + cos(φА)·cos(φB)·cos(λА − λB),
		//  where φА, φB are latitudes and λА, λB are longitudes
		// Distance = d * R
        public double DistanceBetweenPlaces(PositionPoint point1, PositionPoint point2)
		{
			double R = 6371000; // meters

            double sLat1 = Math.Sin(GetRadians(point1.Lat));
            double sLat2 = Math.Sin(GetRadians(point2.Lat));
            double cLat1 = Math.Cos(GetRadians(point1.Lat));
            double cLat2 = Math.Cos(GetRadians(point2.Lat));
            double cLon = Math.Cos(GetRadians(point1.Long) - GetRadians(point2.Long));

			double cosD = sLat1 * sLat2 + cLat1 * cLat2 * cLon;

			double d = Math.Acos(cosD);

			double dist = R * d;

			return dist;
		}

		/// <summary>
		/// Convert degrees to Radians
		/// </summary>
		/// <param name="x">Degrees</param>
		/// <returns>The equivalent in radians</returns>
		public double GetRadians(double x)
		{
            return x * Math.PI / 180;
		}

        private TokenHolder TokenHolder = new TokenHolder();

		public async Task DeleteCachedDataAsync()
		{
            Application.Current.Properties.Remove(ServiceConstants.LOCATIONS_DATA);
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
		}

        private LocationsData GetSavedLocationsData()
		{
            if (Application.Current.Properties.ContainsKey(ServiceConstants.LOCATIONS_DATA))
			{
                string json = (string)Application.Current.Properties[ServiceConstants.LOCATIONS_DATA];
                LocationsData data = (LocationsData)SerializerHelper.Deserialize(json, (new LocationsData()).GetType());

				if (data.Expiration > DateTime.Now || !CrossConnectivity.Current.IsConnected)
				{
					return data;
				}
			}

			return null;
		}

		private async void SaveLocationsData(LocationsData data)
		{
			string json = SerializerHelper.Serialize(data);
            Application.Current.Properties[ServiceConstants.LOCATIONS_DATA] = json;
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
		}

		public async Task<LocationsData> GetLocationsAsync()
		{
			LocationsData locationsData = GetSavedLocationsData();

			if (locationsData == null)
			{
				if (!CrossConnectivity.Current.IsConnected)
				{
					throw new ServerRequestException(Translate.locations_no_internet_text);
				}

				try
				{
					string token = await TokenHolder.GetTokenAsync().ConfigureAwait(false);

					try
					{
						var stringResponse = await HttpUtility
                            .MakeGetCallAsync(String.Format("{0}{1}", AppSettings.LocationsURL, token))
							.ConfigureAwait(false);

						locationsData = ParseLocationsResponse(stringResponse);
                        locationsData.Expiration = DateTime.Now.AddDays(ServiceConstants.LOCATIONS_EXPIRATION_IN_DAYS);

						SaveLocationsData(locationsData);
					}
					catch
					{
						throw new ServerRequestException(Translate.locations_service_error);
					}
				}
				catch (TokenExpiredException)
				{
					throw new ServerRequestException(Translate.locations_no_internet_text);
				}

			}

			return locationsData;
		}

        private LocationsData ParseLocationsResponse(string response)
		{
            var result = new LocationsData()
			{
                ITBALocations = new List<ITBALocation>()
			};

			try
			{
				JObject jsonResponse = JObject.Parse(response);
				var locations = jsonResponse["campi"];
				if (locations.HasValues)
				{
					if (locations["campus"] is JArray)
					{
						foreach (var locationEntry in locations["campus"])
						{
                            var ITBAlocationData = new ITBALocation()
                            {
                                Id = (string)locationEntry["id"],
                                Name = (string)locationEntry["name"],
                                Point = new PositionPoint()
                                {
                                    Lat = (double)locationEntry["latitude"],
                                    Long = (double)locationEntry["longitude"]
                                },
                                Distance = (int)locationEntry["ratioMeters"],
                                IPRanges = new List<IPRange>()
							};

							if (locationEntry["ipRanges"]["ipRange"] is JArray)
							{
								foreach (var ipRangeDetail in locationEntry["ipRanges"]["ipRange"])
								{
                                    string[] ips = ipRangeDetail.ToString().Split('-');
                                    IPRange ipRange = new IPRange()
                                    {
                                        IPFrom = ips[0].Split('.'),
                                        IPTo = ips[1].Split('.')
									};
                                    ITBAlocationData.IPRanges.Add(ipRange);
								}
							}
							else
							{
                                string[] ips = locationEntry["ipRanges"]["ipRange"].ToString().Split('-');
								IPRange ipRange = new IPRange()
								{
									IPFrom = ips[0].Split('.'),
									IPTo = ips[1].Split('.')
								};
								ITBAlocationData.IPRanges.Add(ipRange);
							}

                            result.ITBALocations.Add(ITBAlocationData);
						}
					}
					else
					{
						var locationEntry = locations["campus"];
                        var ITBAlocationData = new ITBALocation()
						{
							Id = (string)locationEntry["id"],
							Name = (string)locationEntry["name"],
							Point = new PositionPoint()
							{
								Lat = (double)locationEntry["latitude"],
								Long = (double)locationEntry["longitude"]
							},
							Distance = (int)locationEntry["ratioMeters"],
							IPRanges = new List<IPRange>()
						};

						if (locationEntry["ipRanges"]["ipRange"] is JArray)
						{
							foreach (var ipRangeDetail in locationEntry["ipRanges"]["ipRange"])
							{
                                string[] ips = ipRangeDetail.ToString().Split('-');
								IPRange ipRange = new IPRange()
								{
									IPFrom = ips[0].Split('.'),
									IPTo = ips[1].Split('.')
								};
								ITBAlocationData.IPRanges.Add(ipRange);
							}
						}
						else
						{
                            string[] ips = locationEntry["ipRanges"]["ipRange"].ToString().Split('-');
							IPRange ipRange = new IPRange()
							{
								IPFrom = ips[0].Split('.'),
								IPTo = ips[1].Split('.')
							};
							ITBAlocationData.IPRanges.Add(ipRange);
						}

						result.ITBALocations.Add(ITBAlocationData);
					}
				}
			}
			catch (Exception exc)
			{
				throw new ServerRequestException("Error al procesar los datos de localización de sedes");
			}

			return result;
		}
    }

	[DataContract]
	[KnownType(typeof(LocationsData))]
	public class LocationsData
	{
        [DataMember]
		public List<ITBALocation> ITBALocations { get; set; }

        [DataMember]
		public DateTime Expiration { get; set; }
	}

	[DataContract]
	[KnownType(typeof(ITBALocation))]
	public class ITBALocation
	{
        [DataMember]
		public string Id { get; set; }
        [DataMember]
		public string Name { get; set; }
        [DataMember]
		public PositionPoint Point { get; set; }
        [DataMember]
		public int Distance { get; set; }
        [DataMember]
		public List<IPRange> IPRanges { get; set; }
	}

	public class PositionPoint
	{
		public double Lat { get; set; }
		public double Long { get; set; }
	}

	public class IPRange
	{
        [DataMember]
		public string[] IPFrom { get; set; }
        [DataMember]
		public string[] IPTo { get; set; }

		public bool IsInRange(string[] ip)
		{
            for (int position = 0; position < 4; position++)
            {
                int ipFrom = int.Parse(IPFrom[position]);
                int ipTo = int.Parse(IPTo[position]);
                int currentIp = int.Parse(ip[position]);
                if (ipFrom > currentIp || ipTo < currentIp) return false;
            }
            return true;
		}
	}
}
