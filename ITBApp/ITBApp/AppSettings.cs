﻿//-----------------------------------------------------------------------
// <copyright file="AppSettings.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace ITBApp
{
    using Xamarin.Forms;

    /// <summary>
    /// Some constants used by the Application.
    /// </summary>
    public class AppSettings
    {
        public static readonly string MondayImageName = "bg_lunes_cursos.jpg";
        public static readonly string TuesdayImageName = "bg_martes_cursos.jpg";
        public static readonly string WednesdayImageName = "bg_miercoles_cursos.jpg";
        public static readonly string ThursdayImageName = "bg_jueves_cursos.jpg";
        public static readonly string FridayImageName = "bg_viernes_cursos.jpg";
        public static readonly string SaturdayImageName = "bg_sabado_cursos.jpg";
        public static readonly string BBPlaystoreURL = "https://play.google.com/store/apps/details?id=com.blackboard.android.bbstudent&hl=es_419";
        public static readonly string BBApplestoreURL = "https://itunes.apple.com/us/app/bb-student-by-blackboard/id950424861?mt=8";
        public static readonly string BBWindowsStoreURL = "https://www.microsoft.com/es-ar/store/p/bb-student/9nblggh08j8w";

        /// <summary>
        /// Google API ID provided by Google API Console
        /// </summary>
        public static readonly string GoogleApisID = "xxx";

        public static readonly string MobileCenterAppID = "xxx";
        public static readonly string FirebaseSenderID = "xxx";
        public static readonly string ListenConnectionString = "xxx";
        public static readonly string NotificationHubName = "xxx";

        /// <summary>
        /// Google Calendar API ID provided by Google API Console
        /// </summary>
        public static readonly string GoogleCalendarApiURL = "https://www.googleapis.com/calendar/v3/calendars";

        /// <summary>
        /// Gmail API Send Email Rest Service URL
        /// </summary>
        public static readonly string GoogleGmailApiURL = "https://www.googleapis.com/gmail/v1/users/{0}/messages/send";

        /// <summary>
        /// Gmail API Attach File Rest Service URL
        /// </summary>
        public static readonly string GoogleGmailAttachmentApiURL = "https://www.googleapis.com/upload/gmail/v1/users/{0}/messages/send";

        /// <summary>
        /// Google App Key provided by Google API Console
        /// </summary>
        public static readonly string GoogleAppKey = "xxx";

        /// <summary>
        /// Email address used for printing files on black and white
        /// </summary>
        public static readonly string BwPrinterEmail = "imprimir.bn@itba.edu.ar";

		/// <summary>
		/// Email address used for printing files on color
		/// </summary>
		public static readonly string ColorPrinterEmail = "imprimir.color@itba.edu.ar";

        /// <summary>
        /// ID to identify our app for Google Login
        /// </summary>
        public static readonly string AppSignInName = "ITBApp-Google-sign-in";

        /// <summary>
        /// AppStyle attribute name
        /// </summary>
        public static readonly string AppStyleAttribute = "AppStyle";

        /// <summary>
        /// Navigation Bar Background Color for the whole app
        /// </summary>
        public static readonly Color NavigationBarBackgroundColor = Color.FromHex("#002b4e");

        /// <summary>
        /// Icon showed at the top of the Master Detail Menu
        /// </summary>
        public static readonly string MasterDetailImageName = "hamburger.png";

        /// <summary>
        /// ID required for the Master Detail Menu
        /// </summary>
        public static readonly string MasterDetailAutomationId = "ActionMenu";

        /// <summary>
        /// Google Authenticator Secret
        /// </summary>
        public static readonly string AuthenticatorSecret = "xxx";

        /// <summary>
        /// Google Authenticator Client Id
        /// </summary>
        public static readonly string AuthenticatorClientId = "xxx";

        /// <summary>
        /// Google Authenticator Client Id for iOS
        /// </summary>
        public static readonly string AuthenticatorClientIdIos = "xxx";

        /// <summary>
        /// Webpage that is showed if authentication service fails
        /// </summary>
        public static readonly string AuthenticatorRedirectUrl = "https://www.google.com.ar/";
#if DEBUG
        public static readonly string ClassesServiceURL = "xxx";
        public static readonly string LoginServiceURL = "xxx";
        public static readonly string NotificationsURL = "xxx";
        public static readonly string RegistrationServiceURL = "xxx";
        public static readonly string ProfileServiceURL = "xxx";
        public static readonly string AccountBalanceServiceURL = "xxx";
        public static readonly string AvailableClassroomsServiceURL = "xxx";
        public static readonly string PendingExamsServiceURL = "xxx";
        public static readonly string SurveyActiveURL = "xxx";
        public static readonly string SurveyActiveQuestionsURL = "xxx";
        public static readonly string SurveyPostResponseURL = "xxx";
        public static readonly string SurveyUnansweredURL = "xxx";
        public static readonly string SurveyResultsURL = "xxx";
        public static readonly string PrintQuotaURL = "xxx";
        public static readonly string HdyftOptionsURL = "xxx";
        public static readonly string HistoricAcademicURL = "xxx";
        public static readonly string DailyBarMenuURL = "xxx";
        public static readonly string WeatherURL = "xxx";
        public static readonly string WorkGroupsRequestURL = "xxx";
        public static readonly string WorkAddRequestURL = "xxx";
        public static readonly string WorkRequestURL = "xxx";
        public static readonly string EnrolledEventsURL = "xxx";
        public static readonly string EventsEnrollURL = "xxx";
        public static readonly string LocationsURL = "xxx";
        public static readonly string EventsURL = "xxx";
#else
        /// <summary>
        /// URL for the Classes Rest Service
        /// </summary>
        public static readonly string ClassesServiceURL = "xxx";

        /// <summary>
        /// URL for the Login Rest Service
        /// </summary>
        public static readonly string LoginServiceURL = "xxx";

        /// <summary>
        /// URL for the Notifications Rest Service
        /// </summary>
        public static readonly string NotificationsURL = "xxx";

        /// <summary>
        /// URL for the Device Registration Rest Service
        /// </summary>
        public static readonly string RegistrationServiceURL = "xxx";

        /// <summary>
        /// URL for the User Profile Rest Service
        /// </summary>
        public static readonly string ProfileServiceURL = "xxx";

        /// <summary>
        /// URL for the Account Balance Rest Service
        /// </summary>
        public static readonly string AccountBalanceServiceURL = "xxx";

        /// <summary>
        /// URL for the Available Classrooms Rest Service
        /// </summary>
        public static readonly string AvailableClassroomsServiceURL = "xxx";

        /// <summary>
        /// URL for the Pending Exams Rest Service
        /// </summary>
        public static readonly string PendingExamsServiceURL = "xxx";

        /// <summary>
        /// URL for the Survey State Rest Service
        /// </summary>
        public static readonly string SurveyActiveURL = "xxx";

        /// <summary>
        /// URL for the Survey Active Questions Rest Service
        /// </summary>
        public static readonly string SurveyActiveQuestionsURL = "xxx";

        /// <summary>
        /// URL for the Survey Post Response Rest Service
        /// </summary>
        public static readonly string SurveyPostResponseURL = "xxx";

        /// <summary>
        /// URL for the Survey Unanswered Rest Service
        /// </summary>
        public static readonly string SurveyUnansweredURL = "xxx";

        /// <summary>
        /// URL for the Survey Results URL Rest Service
        /// </summary>
        public static readonly string SurveyResultsURL = "xxx";

        /// <summary>
        /// URL for the Print Quota Rest Service
        /// </summary>
        public static readonly string PrintQuotaURL = "xxx";

        /// <summary>
        /// URL for the HDYFT Rest Service
        /// </summary>
        public static readonly string HdyftOptionsURL = "xxx";

        public static readonly string HistoricAcademicURL = "xxx";
        public static readonly string DailyBarMenuURL = "xxx";
        public static readonly string WeatherURL = "xxx";
        public static readonly string WorkGroupsRequestURL = "xxx";
        public static readonly string WorkAddRequestURL = "xxx";
        public static readonly string WorkRequestURL = "xxx";
        public static readonly string EnrolledEventsURL = "xxx";
        public static readonly string EventsEnrollURL = "xxx";
        public static readonly string LocationsURL = "xxx";
        public static readonly string EventsURL = "xxx";
#endif
		/// <summary>
		/// Removes the previus account so it starts with login page
		/// </summary>
		private static bool removePreviousAccount = false;

        /// <summary>
        /// Gets or sets a value indicating whether the previous account should be removed before running
        /// </summary>
        public static bool RemovePreviousAccount
        {
            get { return removePreviousAccount; }
            set { removePreviousAccount = value; }
        }
    }
}