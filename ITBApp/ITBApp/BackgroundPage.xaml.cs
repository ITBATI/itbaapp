﻿using System;
using System.Collections.Generic;
using ITBApp.Model.Interfaces;
using ITBApp.Resources.i18n;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp
{
	public partial class BackgroundPage : ContentPage
	{
		public BackgroundPage()
		{
			InitializeComponent();
			Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();

			if (!CrossConnectivity.Current.IsConnected)
			{
				Navigation.PushModalAsync(new LoginFail(Translate.loginpage_noInternet_message));

			}
			else 
			{ 
				if (!App.IsLoggedIn)
				{
					Navigation.PushModalAsync(new GoogleLoginPage());
				}
			}


		}
	}
}

