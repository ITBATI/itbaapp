﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel.Survey
{
	[DataContract]
	[KnownType(typeof(SurveyQuestionData))]
	public class SurveyData
	{
		[DataMember]
		public int Id { get; set;}

		[DataMember]
		public DateTime From { get; set; }

		[DataMember]
		public DateTime To { get; set; }

		[DataMember]
		public String Comments { get; set; }

		[DataMember]
		public String Description { get; set; }

		public List<SurveyQuestionData> Questions { get; set;}
	}
}