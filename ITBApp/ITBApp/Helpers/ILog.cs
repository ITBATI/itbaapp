﻿using System;
namespace ITBApp
{
    public interface ILog
    {
    	void WriteLine(string line);
    	void WriteLine(Exception exception);
    }
}
