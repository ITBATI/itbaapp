﻿using System;
using System.ComponentModel;
using System.Windows.Input;

namespace ITBApp.Selectors
{
	public class SurveySelectorModel : INotifyPropertyChanged
	{
		private bool _isSelected;
		private String _response;

		public event PropertyChangedEventHandler PropertyChanged;

		public long Id { get; set; }
		public String Question { get; set; }
		public String Description { get; set; }
		public long ChoiceId { get; set;}
		public long QuestionId { get; set;}

		public bool IsSendButton { get; set; }
		public bool IsWritingChoice { get; set;}
		public ICommand TapChoiceCommand { get; set; }

		public bool IsSelected
		{
			get { return _isSelected; }
			set
			{
				_isSelected = value;
				NotifyPropertyChanged("IsSelected");
			}
		}

		public String Response
		{
			get { return _response; }
			set
			{
				_response = value;
				NotifyPropertyChanged("Response");
			}
		}

		private void NotifyPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}