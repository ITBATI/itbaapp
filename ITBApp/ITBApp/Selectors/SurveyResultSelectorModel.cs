﻿using System.ComponentModel;
using Xamarin.Forms;

namespace ITBApp.Selectors
{
	public class SurveyResultSelectorModel : INotifyPropertyChanged
    {
		private string _question;
		private string _resultNumberToShow;
		private double _resultNumberToProgressBar;

		public SurveyResultSelectorModel()
		{
		}

		public string Question
		{
			get
			{
				return _question;
			}
			set
			{
				_question = value;
                this.NotifyPropertyChanged("Question");
            }
		}

		public string ResultNumberToShow
		{
			get
			{
				return _resultNumberToShow;
			}
			set
			{
				_resultNumberToShow = value;
                this.NotifyPropertyChanged("ResultNumberToShow");
            }
		}

		public double ResultNumberToProgressBar
		{
			get
			{
				return _resultNumberToProgressBar;
			}
			set
			{
				_resultNumberToProgressBar = value;
                this.NotifyPropertyChanged("ResultNumberToProgressBar");
            }
		}

        public bool IsWindows
        {
            get
            {
                return Device.RuntimePlatform == Device.UWP;
            }
        }

        private int positiveWidth;
        public int PositiveWidth
        {
            get
            {
                return positiveWidth;
            }
            set
            {
                positiveWidth = value;
                this.NotifyPropertyChanged("PositiveWidth");
            }
        }

        private int negativeWidth;
        public int NegativeWidth
        {
            get
            {
                return negativeWidth;
            }
            set
            {
                negativeWidth = value;
                this.NotifyPropertyChanged("NegativeWidth");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}