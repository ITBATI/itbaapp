﻿using System;
using System.Collections.Generic;
using ITBApp.ViewModels;
using Xamarin.Forms;

namespace ITBApp.UI.Controls
{
    public partial class SingleDayCard : ContentView
    {
        public static readonly BindableProperty EventDayProperty = BindableProperty.Create("EventDay", typeof(EventDayData), typeof(SingleDayCard), null);
        bool isTapped = false;

        public SingleDayCard()
        {
            InitializeComponent();
            Content.BindingContext = this;
        }

		public EventDayData EventDay
		{
			get { return (EventDayData)GetValue(EventDayProperty); }
			set { SetValue(EventDayProperty, value); }
		}
    }
}
