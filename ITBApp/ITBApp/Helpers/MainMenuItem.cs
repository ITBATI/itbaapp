namespace ITBApp.Helpers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using ITBApp.Model.DataModel.Survey;
    using ITBApp.Services;
    using Model.Handlers;
    using UI;
    using Xamarin.Forms;

	public class MainMenuItem : Enumeration<MainMenuItem>
	{
		public static readonly MainMenuItem ClassesByDayItem = new MainMenuItem(PageId.CLASSES_BY_DAY);
		public static readonly MainMenuItem NotificationsItem = new MainMenuItem(PageId.NOTIFICATIONS);
		public static readonly MainMenuItem HelpItem = new MainMenuItem(PageId.HELP);
		public static readonly MainMenuItem UserProfileItem = new MainMenuItem(PageId.USER_PROFILE);
		public static readonly MainMenuItem ContactsItem = new MainMenuItem(PageId.CONTACTS);
        public static readonly MainMenuItem FreeClassroomsItem = new MainMenuItem(PageId.AVAILABLE_CLASSROOMS);
		public static readonly MainMenuItem TermsAndConditionsItem = new MainMenuItem(PageId.TERMS_AND_CONDITIONS);
		public static readonly MainMenuItem SurveyItem = new MainMenuItem(PageId.SURVEY);
		public static readonly MainMenuItem HomeItem = new MainMenuItem(PageId.HOME);
		public static readonly MainMenuItem PrintItem = new MainMenuItem(PageId.PRINT);
        public static readonly MainMenuItem EventsItem = new MainMenuItem(PageId.EVENTS);
        public static readonly MainMenuItem ProceduresItem = new MainMenuItem(PageId.PROCEDURES);
        public static readonly MainMenuItem BarMenuItem = new MainMenuItem(PageId.BAR_MENU);

		private readonly PageId id;
		private ContentPage page;
		private StackLayout viewItem;

		private MainMenuItem(PageId pageId) : base(pageId.ToString())
		{
			this.id = pageId;
		}

		public ContentPage Page
		{
			get { return page; }
		}

		public NavigationPage NavigationPage
		{
			get
			{
                if (id == PageId.CLASSES_BY_DAY) page = new Schedule();
                else if (id == PageId.NOTIFICATIONS) page = new NotificationsPage();
                else if (id == PageId.HELP) page = new HelpPage();
                else if (id == PageId.USER_PROFILE) page = new UserProfile();
                else if (id == PageId.CONTACTS) page = new ContactsPage();
                else if (id == PageId.AVAILABLE_CLASSROOMS) page = new AvailableClassrooms();
                else if (id == PageId.TERMS_AND_CONDITIONS) page = new TermsAndConditions();
                else if (id == PageId.SURVEY) page = new SurveyEntryPage();
				else if (id == PageId.HOME) page = new HomePage();
				else if (id == PageId.PRINT) page = new PrintPage();
                else if (id == PageId.EVENTS) page = new EventsPage();
                else if (id == PageId.PROCEDURES) page = new ProceduresPage();
                else if (id == PageId.BAR_MENU) page = new BarMenuPage();
				NavigationPage nav = new NavigationPage(this.page);
				nav.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
				nav.SetValue(NavigationPage.BarTextColorProperty, Color.White);
				return nav;
			}
		}

		public StackLayout ViewItem
		{
			get { return viewItem; }
			set { viewItem = value; }
		}

		public PageId Id
		{
			get { return id; }
		}
	}
}
