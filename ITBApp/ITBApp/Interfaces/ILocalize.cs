﻿using System.Globalization;

namespace ITBApp.Interfaces
{
	public interface ILocalize
	{
		CultureInfo GetCurrentCultureInfo();
	}
}
