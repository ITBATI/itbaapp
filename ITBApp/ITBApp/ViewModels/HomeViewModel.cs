﻿//-----------------------------------------------------------------------
// <copyright file="HomeViewModel.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ITBApp.ViewModels
{
	using System;
	using System.Collections.Generic;
	using System.Threading.Tasks;
	using ITBApp.Model.DataModel;
	using ITBApp.Model.Handlers;
	using ITBApp.Interfaces;
	using ITBApp.Resources.i18n;
	using ITBApp.Services;
	using ITBApp.UI;
	using Xamarin.Forms;
    using ITBApp.Handlers;
    using ITBApp.DataModel;

    /// <summary>
    /// Home view model.
    /// </summary>
    internal class HomeViewModel : BaseModel
    {
		private int errorCount = 0;
		private bool needToBeRelogin = false;

        /// <summary>
        /// The notification service.
        /// </summary>
        private readonly NotificationsService notificationService = new NotificationsService();

        /// <summary>
        /// The free class room service.
        /// </summary>
        private readonly AvailableClassroomsService freeClassRoomService = new AvailableClassroomsService();

        /// <summary>
        /// The survey service.
        /// </summary>
        private readonly SurveyService surveyService = new SurveyService();

        /// <summary>
        /// The email service.
        /// </summary>
        private readonly EmailPrintService emailService = new EmailPrintService();

        /// <summary>
        /// The classes service.
        /// </summary>
        private readonly ClassesByDayService classesService = new ClassesByDayService();

        /// <summary>
        /// The notification count.
        /// </summary>
        private string notificationCount = "-";

        /// <summary>
        /// The has notifications.
        /// </summary>
        private bool hasNotifications = false;

        /// <summary>
        /// The free class room count.
        /// </summary>
        private string freeClassRoomCount = "-";

        /// <summary>
        /// The has free class room.
        /// </summary>
        private bool hasFreeClassRoom = false;

        /// <summary>
        /// The survey count.
        /// </summary>
        private string surveyCount = "-";

        /// <summary>
        /// The has survey.
        /// </summary>
        private bool hasSurvey = false;

        /// <summary>
        /// The print quota count.
        /// </summary>
        private string printQuotaCount = "-";

        /// <summary>
        /// The has print quota.
        /// </summary>
        private bool hasPrintQuota = false;

        /// <summary>
        /// The is male.
        /// </summary>
        private bool isMale = true;

        /// <summary>
        /// The has course.
        /// </summary>
        private bool hasCourse = false;

		/// <summary>
		/// The has no course.
		/// </summary>
		private bool hasNoCourse = false;

        /// <summary>
        /// The course title.
        /// </summary>
        private string courseTitle = string.Empty;

        /// <summary>
        /// The course start time.
        /// </summary>
        private string courseStartTime = string.Empty;

        /// <summary>
        /// The course end time.
        /// </summary>
        private string courseEndTime = string.Empty;

        /// <summary>
        /// The course class room.
        /// </summary>
        private string courseClassRoom = string.Empty;

        /// <summary>
        /// The course location.
        /// </summary>
        private string courseLocation = string.Empty;

        /// <summary>
        /// Initializes a new instance of the HomeViewModel class.
        /// </summary>
        public HomeViewModel()
        {
            Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
        }

        #region Properties

        /// <summary>
        /// Gets the notification count.
        /// </summary>
        /// <value>The notification count.</value>
        public string NotificationCount
        {
            get 
            { 
                return this.notificationCount; 
            }

            private set
            {
                this.notificationCount = value;
                this.OnPropertyChanged("NotificationCount");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this HomeViewModel has notifications.
        /// </summary>
        /// <value><c>true</c> if has notifications; otherwise, <c>false</c>.</value>
        public bool HasNotifications
        {
            get 
            { 
                return this.hasNotifications; 
            }

            private set
            {
                this.hasNotifications = value;                
                this.OnPropertyChanged("HasNotifications");
            }
        }

        /// <summary>
        /// Gets the free class room count.
        /// </summary>
        /// <value>The free class room count.</value>
        public string FreeClassRoomCount
        {
            get 
            { 
                return this.freeClassRoomCount; 
            }

            private set
            {
                this.freeClassRoomCount = value;
                this.OnPropertyChanged("FreeClassRoomCount");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this HomeViewModel has free class room.
        /// </summary>
        /// <value><c>true</c> if has free class room; otherwise, <c>false</c>.</value>
        public bool HasFreeClassRoom
        {
            get 
            { 
                return this.hasFreeClassRoom; 
            }

            private set
            {
                this.hasFreeClassRoom = value;
                this.OnPropertyChanged("HasFreeClassRoom");
            }
        }

        /// <summary>
        /// Gets the survey count.
        /// </summary>
        /// <value>The survey count.</value>
        public string SurveyCount
        {
            get 
            { 
                return this.surveyCount; 
            }

            private set
            {
                this.surveyCount = value;
                this.OnPropertyChanged("SurveyCount");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this HomeViewModel has survey.
        /// </summary>
        /// <value><c>true</c> if has survey; otherwise, <c>false</c>.</value>
        public bool HasSurvey
        {
            get 
            { 
                return this.hasSurvey; 
            }

            private set
            {
                this.hasSurvey = value;         
                this.OnPropertyChanged("HasSurvey");
            }
        }

        /// <summary>
        /// Gets the print quota count.
        /// </summary>
        /// <value>The print quota count.</value>
        public string PrintQuotaCount
        {
            get 
            { 
                return this.printQuotaCount; 
            }

            private set
            {
                this.printQuotaCount = value;
                this.OnPropertyChanged("PrintQuotaCount");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this HomeViewModel has print quota.
        /// </summary>
        /// <value><c>true</c> if has print quota; otherwise, <c>false</c>.</value>
        public bool HasPrintQuota
        {
            get 
            { 
                return this.hasPrintQuota;
            }

            private set
            {
                this.hasPrintQuota = value;
                this.OnPropertyChanged("HasPrintQuota");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this HomeViewModel is male.
        /// </summary>
        /// <value><c>true</c> if is male; otherwise, <c>false</c>.</value>
        public bool IsMale
        {
            get 
            { 
                return this.isMale; 
            }

            private set
            {
                this.isMale = value;
                this.OnPropertyChanged("IsMale");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this HomeViewModel has course.
        /// </summary>
        /// <value><c>true</c> if has course; otherwise, <c>false</c>.</value>
        public bool HasCourse
        {
            get 
            { 
                return this.hasCourse; 
            }

            private set
            {
                this.hasCourse = value;
				HasNoCourse = !value;
                this.OnPropertyChanged("HasCourse");
            }
        }

		/// <summary>
		/// Gets a value indicating whether this HomeViewModel has no course.
		/// </summary>
		/// <value><c>true</c> if has course; otherwise, <c>false</c>.</value>
		public bool HasNoCourse
		{
			get
			{
				return this.hasNoCourse;
			}

			private set
			{
				this.hasNoCourse = value;
				this.OnPropertyChanged("HasNoCourse");
			}
		}

        /// <summary>
        /// Gets the course title.
        /// </summary>
        /// <value>The course title.</value>
        public string CourseTitle
        {
            get 
            { 
                return this.courseTitle; 
            }

            private set
            {
                this.courseTitle = value;
                this.OnPropertyChanged("CourseTitle");
            }
        }

        /// <summary>
        /// Gets the course start time.
        /// </summary>
        /// <value>The course start time.</value>
        public string CourseStartTime
        {
            get 
            { 
                return this.courseStartTime; 
            }

            private set
            {
                this.courseStartTime = value;
                this.OnPropertyChanged("CourseStartTime");
            }
        }

        /// <summary>
        /// Gets the course end time.
        /// </summary>
        /// <value>The course end time.</value>
        public string CourseEndTime
        {
            get 
            { 
                return this.courseEndTime; 
            }

            private set
            {
                this.courseEndTime = value;
                this.OnPropertyChanged("CourseEndTime");
            }
        }

        /// <summary>
        /// Gets the course class room.
        /// </summary>
        /// <value>The course class room.</value>
        public string CourseClassRoom
        {
            get 
            { 
                return this.courseClassRoom; 
            }

            private set
            {
                this.courseClassRoom = value;
                this.OnPropertyChanged("CourseClassRoom");
            }
        }

        /// <summary>
        /// Gets or sets the course location.
        /// </summary>
        /// <value>The course location.</value>
        public string CourseLocation
        {
            get
            {
                return this.courseLocation;
            }

            set
            {
                this.courseLocation = value;
                this.OnPropertyChanged("CourseLocation");
            }
        }

		#endregion

		private async Task<string> GetCurrentCourseTask()
		{
			try
			{
				// Get Course
				var courseData = await this.classesService.GetCurrentCourseAsync().ConfigureAwait(false);
				if (courseData == null)
				{
					this.HasCourse = false;
				}
				else
				{
					this.HasCourse = true;
                    this.CourseTitle = courseData.Name.ToUpper();
					this.CourseStartTime = courseData.CurrentTimetable.StartTime;
					this.CourseEndTime = courseData.CurrentTimetable.EndTime;
					this.CourseClassRoom = courseData.FormattedClassroom;
                    this.CourseLocation = courseData.CurrentTimetable.Location;
				}
			}
			catch (TokenExpiredException)
			{
				needToBeRelogin = true;
			}
			catch
			{
				this.errorCount++;
			}

			return string.Empty;
		}

		private async Task<string> GetNotificationsTask()
		{
			try
            {
                UserData userData = new LoginService().GetSavedUserData();
                this.IsMale = userData.Gender == UserGender.Male;

				// Get Notifications!
				int notifications = await this.notificationService.getUnreadNotificationsCount();
                this.NotificationCount = notifications.ToString();
                this.HasNotifications = notifications > 0;

			}
            catch (TokenExpiredException)
            {
                needToBeRelogin = true;
            }
			catch
			{
                this.errorCount++;
			}

			return string.Empty;
		}

		private async Task<string> GetFreeClassRoomTask()
		{
			var LocationId = "0";

			try
			{
                LocationId = await new GeolocatorService().GetLocationIdAsync().ConfigureAwait(false);
			}
			catch
			{
                LocationId = "0";
			}

			try
            {
				if (LocationId == "0") LocationId = App.lastLocationId;
			
                // Get Free ClassRoom
                var classRoomData = await this.freeClassRoomService.getFreeClassroomsDataAsync(LocationId).ConfigureAwait(false);

				this.FreeClassRoomCount = classRoomData.freeNow.Count.ToString();
                this.HasFreeClassRoom = classRoomData.freeNow.Count > 0;

			}
            catch (TokenExpiredException)
            {
                needToBeRelogin = true;
            }
			catch
			{
                this.errorCount++;
			}

			return string.Empty;
		}

		private async Task<string> GetSurveyTask()
		{
			try
            {
                // Get Survey
                var surveyData = await this.surveyService.GetUnansweredSurveysAsync().ConfigureAwait(false);

				this.SurveyCount = surveyData.Count.ToString();
                this.HasSurvey = surveyData.Count > 0;

			}
            catch (TokenExpiredException)
            {
                needToBeRelogin = true;
            }
			catch
			{
                this.errorCount++;
			}

			return string.Empty;
		}

		private async Task<string> GetPrintQuotaTask()
		{
			try
            {
                // Get PrintQuota
                var printQuota = await this.emailService.GetPrintQuotaAsync().ConfigureAwait(false);

				this.PrintQuotaCount = printQuota.ToString();
                this.HasPrintQuota = printQuota > 0;

			}
            catch (TokenExpiredException)
            {
                needToBeRelogin = true;
            }
			catch
			{
                this.errorCount++;
			}

			return string.Empty;
		}


        /// <summary>
        /// Loads the data async.
        /// </summary>
        /// <returns>The data async.</returns>
        public override async Task<string> LoadDataAsync()
        {
            try
            {
                this.IsLoading = true;
                this.ErrorMessage = null;
                this.errorCount = 0;
                this.needToBeRelogin = false;


                /* Se resolvió hacer las llamadas a los 5 servicios de manera independiente
                 para que, en caso de que haya un error con alguno, se pueda seguir viendo la home. */

                //Fuerzo un refresh del token y espero a que lo devuelva
                string token = await new TokenHolder().GetTokenAsync().ConfigureAwait(false);

                var tasks = new List<Task>();

                if (!string.IsNullOrEmpty(token))
                {
                    tasks.Add(Task.Factory
                              .StartNew(async () => await GetCurrentCourseTask(), TaskCreationOptions.None));

                    tasks.Add(Task.Factory
                              .StartNew(async () => await GetNotificationsTask(), TaskCreationOptions.None));

                    tasks.Add(Task.Factory
                              .StartNew(async () => await GetFreeClassRoomTask(), TaskCreationOptions.None));

                    tasks.Add(Task.Factory
                              .StartNew(async () => await GetSurveyTask(), TaskCreationOptions.None));

                    tasks.Add(Task.Factory
                              .StartNew(async () => await GetPrintQuotaTask(), TaskCreationOptions.None));
                }

                await Task.WhenAll(tasks).ContinueWith((arg) =>
                {
                    this.IsLoading = false;
                    MessagingCenter.Send<HomeViewModel>(this, "LoadComplete");
                });

                if (needToBeRelogin)
                {
                    await App.LogoutAndRelogin();
                }

                if (errorCount == 5)
                {
                    this.ErrorMessage = Translate.home_error_message;
                }
            }
            catch (ServerRequestException ex)
            {
                this.ErrorMessage = ex.Message;
            }
            catch (Exception exception)
            {
                this.ErrorMessage = Translate.home_error_message;
            }

            return this.ErrorMessage;
        }

        /// <summary>
        /// Currents the page on appearing.
        /// </summary>
        /// <param name="sender">Sender of the metod</param>
        /// <param name="eventArgs">Event arguments.</param>
        protected override async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
        {
            await this.LoadDataAsync().ConfigureAwait(false);
            ((HomePage)CurrentPage).InitConnectivityMessage();
        }

        /// <summary>
        /// Gets the name of the user first.
        /// </summary>
        /// <returns>The user first name.</returns>
        /// <param name="userData">User data.</param>
        private string GetUserFirstName(UserData userData)
        {
            if (userData.FirstName.IndexOf(' ') == -1)
            {
                // No tiene segundo nombre!
                return userData.FirstName;
            }
            else
            {
                return userData.FirstName.Substring(0, userData.FirstName.IndexOf(' '));
            }
        }
    }
}
