﻿using System;
using ITBApp.Resources.i18n;
using Plugin.FilePicker;
using Xamarin.Forms;

namespace ITBApp.UI
{
	public partial class PrintFilePick : ContentPage
	{
		public PrintFilePick()
		{
			InitializeComponent();

			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += async (sender, e) =>
			{
				try
				{
					await CrossFilePicker.Current.PickFile();
				}
				catch (Exception)
				{
					await DisplayAlert(Translate.print_title, Translate.print_authorization, Translate.Ok);
				}
			};

			cvPickFile.GestureRecognizers.Add(tapGestureRecognizer);
		}
	}
}
