﻿using System.Collections.Generic;
using System.Net;
using Android.Content;
using Android.Net;
using Android.Net.Wifi;
using ITBApp.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(ITBApp.Droid.Implementations.IPAddressManager))]

namespace ITBApp.Droid.Implementations
{
    public class IPAddressManager : IIPAddressManager
    {
		public List<string> GetIPAddress()
		{
            List<string> result = new List<string>();

            ConnectivityManager connectivityManager = (ConnectivityManager)Forms.Context.GetSystemService(Android.Content.Context.ConnectivityService);
            NetworkInfo networkInfo = connectivityManager.ActiveNetworkInfo;
            bool isWifi = networkInfo.Type == ConnectivityType.Wifi;

            // if not connected to WIFI no IPs
            if (!isWifi) return result;

            /*
            string wifiName = GetWifiId();
            if (wifiName == null || !wifiName.Contains("ITBA")) return result;
            */

            IPAddress[] addresses = Dns.GetHostAddresses(Dns.GetHostName());
			if (addresses != null)
			{
                foreach(IPAddress address in addresses)
                {
                    result.Add(address.ToString());
                }
			}
            return result;
		}

		/// <summary>
		/// Constant returned from ConnectionInfo.SSID if there is no network currently connected
		/// Taken from the Android source code: WifiSsid.java.
		/// </summary>
		private const string SsidNone = "<unknown ssid>";

		/// <summary>
		/// Returns an identifier (the SSID or BSSID) of the Wifi the device is corrently connected to.
		/// If the current SSID is not available, the BSSID is used as identifier.
		/// </summary>
		/// <returns>identifier (SSID or BSSID) for the corrently connected Wifi</returns>
		private string GetWifiId()
		{
			var wifiManager = (WifiManager)Forms.Context.GetSystemService(Context.WifiService);

			var ssid = wifiManager.ConnectionInfo.SSID;      //The SSID may be <unknown ssid> if there is no network currently connected.
			var bssid = wifiManager.ConnectionInfo.BSSID;    //The BSSID may be null if there is no network currently connected.

			if (! string.IsNullOrEmpty(ssid) && ssid != SsidNone)
			{
				return ssid.Trim(' ', '"');
			}

			if (! string.IsNullOrEmpty(bssid))
			{
				return bssid.Trim(' ', '"');
			}

			return null;
		}
    }
}
