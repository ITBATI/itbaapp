﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace ITBApp.Model.DataModel.Survey
{
	public class SurveyPostData
	{
		[DataMember]
		[JsonProperty(PropertyName = "responses")]
		public List<SurveyPostResponseData> Responses { get; set; }

	}

	public class SurveyPostResponseData
	{
		[DataMember]
		[JsonProperty(PropertyName = "questionId")]
		public long QuestionId { get; set; }

		[DataMember]
		[JsonProperty(PropertyName = "questionOptionId")]
		public long? QuestionOptionId { get; set; }

		[DataMember]
		[JsonProperty(PropertyName = "response")]
		public string Response { get; set; }
	}
}