//-----------------------------------------------------------------------
// <copyright file="UserProfileViewModel.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ITBApp.ViewModels
{
	using System;
	using System.Threading.Tasks;
	using ITBApp.Model.DataModel;
	using ITBApp.Model.Handlers;
	using ITBApp.Interfaces;
	using ITBApp.Resources.i18n;
	using ITBApp.Services;
	using Xamarin.Forms;
    using ITBApp.Helpers;

    /// <summary>
    /// User Profile Context View Model
    /// </summary>
    internal class UserProfileViewModel : BaseModel
    {
        /// <summary>
        /// User Profile Data 
        /// </summary>
        private UserProfileData currentUserProfileData;

		/// <summary>
		/// The display current profile image.
		/// </summary>
		private bool displayCurrentProfileImage;

		/// <summary>
		/// The height of the profile container.
		/// </summary>
		private int profileContainerHeight;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserProfileViewModel"/> class
        /// </summary>
        public UserProfileViewModel()
        {
            Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
			var hasHightResolution = UIHelper.HasMediumResolution() || UIHelper.HasHightResoultion();

			DisplayCurrentProfileImage = hasHightResolution;
			ProfileContainerHeight = hasHightResolution ? 160 : 80;
        }

        public void HideProfileImage(double spaceAvailable)
        {
            DisplayCurrentProfileImage = false;
            ProfileContainerHeight = Convert.ToInt32(spaceAvailable);
        }

        /// <summary>
        /// Gets the current user profile data
        /// </summary>
        public UserProfileData CurrentUserProfileData
        {
            get
            {
                return this.currentUserProfileData;
            }

            private set
            {
                this.currentUserProfileData = value;
                this.OnPropertyChanged("CurrentUserProfileData");
            }
        }

		/// <summary>
		/// Gets a value indicating whether DisplayCurrentProfileImage
		/// profile image.
		/// </summary>
		/// <value><c>true</c> if display current profile image; otherwise, <c>false</c>.</value>
		public bool DisplayCurrentProfileImage
		{
			get
			{
				return this.displayCurrentProfileImage;
			}

			private set
			{
                this.displayCurrentProfileImage = value;
				this.OnPropertyChanged("DisplayCurrentProfileImage");
			}
		}


		/// <summary>
		/// Gets a value indicating profile container height.
		/// </summary>
		/// <value><c>true</c> if profile container height; otherwise, <c>false</c>.</value>
		public int ProfileContainerHeight
		{
			get
			{
				return this.profileContainerHeight;
			}

			private set
			{
                this.profileContainerHeight = value;
				this.OnPropertyChanged("ProfileContainerHeight");
			}
		}

        /// <summary>
        /// Fills view model data
        /// </summary>
        /// <returns>
        /// Error message
        /// </returns>
        public override async Task<string> LoadDataAsync()
        {
            try
            {
                this.IsLoading = true;
                this.ErrorMessage = null;

                ProfileService service = new ProfileService();
                UserProfileData data = await service.getUserProfileData().ConfigureAwait(false);

                this.CurrentUserProfileData = data;
            }
            catch (ServerRequestException ex)
            {
                this.ErrorMessage = ex.Message;
            }
            catch (Exception exception)
            {
                this.ErrorMessage = Translate.surveypage_error_text;
            }

            this.IsLoading = false;

            return this.ErrorMessage;
        }

        /// <summary>
        /// Event when User Profile page is opened or maximized
        /// </summary>
        /// <param name="sender">
        /// Object that sends the event
        /// </param>
        /// <param name="eventArgs">
        /// Event parameters
        /// </param>
        protected override async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
        {
            if (this.CurrentUserProfileData == null)
            {
                await this.LoadDataAsync().ConfigureAwait(false);
            }
        }
    }
}
