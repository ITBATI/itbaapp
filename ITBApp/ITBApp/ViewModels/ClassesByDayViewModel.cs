﻿//-----------------------------------------------------------------------
// <copyright file="ClassesByDayViewModel.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ITBApp.ViewModels
{
	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Threading.Tasks;
    using ITBApp.DataModel;
    using ITBApp.Helpers;
    using ITBApp.Model.DataModel;
	using ITBApp.Model.Handlers;
	using ITBApp.Resources.i18n;
	using ITBApp.Services;
	using ITBApp.UI;
	using Xamarin.Forms;

	/// <summary>
	/// Classes by day view model.
	/// </summary>
	internal class ClassesByDayViewModel : BaseModel
    {
        /// <summary>
        /// The days.
        /// </summary>
        private ObservableCollection<DayViewModel> days = new ObservableCollection<DayViewModel>();

        /// <summary>
        /// The position.
        /// </summary>
        private int position = 0;

		/// <summary>
		/// The size of the font.
		/// </summary>
		private double fontSize = 0;

        /// <summary>
        /// The font size location.
        /// </summary>
        private int fontSizeLocation = 0;

        /// <summary>
        /// Initializes a new instance of the ClassesByDayViewModel class.
        /// </summary>
        public ClassesByDayViewModel()
        {
            //Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
			var hasHightResolution = UIHelper.HasMediumResolution() || UIHelper.HasHightResoultion();

			FontSize = hasHightResolution ? 
				Device.GetNamedSize(NamedSize.Medium, typeof(Label)) : 
			          Device.GetNamedSize(NamedSize.Small, typeof(Label));

            FontSizeLocation = hasHightResolution ? 16 : 13;
        }

        /// <summary>
        /// Gets the bindable days.
        /// </summary>
        /// <value>The bindable days.</value>
        public ObservableCollection<DayViewModel> BindableDays
        {
            get 
            { 
                return this.days; 
            }

            private set
            {
                this.days = value;
                this.OnPropertyChanged("BindableDays");
            }
        }

        /// <summary>
        /// Gets or sets the days position.
        /// </summary>
        /// <value>The days position.</value>
        public int DaysPosition
        {
            get
            {
                return this.position;
            }

            set
            {
                this.position = value;
                this.OnPropertyChanged("DaysPosition");
            }
        }

        /// <summary>
        /// Gets or sets the size of the font.
        /// </summary>
        /// <value>The size of the font.</value>
		public double FontSize
		{
			get
			{
				return this.fontSize;
			}

			set
			{
				this.fontSize = value;
				this.OnPropertyChanged("FontSize");
			}
		}

        /// <summary>
        /// Gets or sets the font size location.
        /// </summary>
        /// <value>The font size location.</value>
        public int FontSizeLocation
        {
        	get
        	{
                return this.fontSizeLocation;
        	}

        	set
            {
                this.fontSizeLocation = value;
                this.OnPropertyChanged("FontSizeLocation");
            }
        }
        
        /// <summary>
        /// Loads the data async.
        /// </summary>
        /// <returns>The data async.</returns>
        public override async Task<string> LoadDataAsync()
        {
            ObservableCollection<DayViewModel> temp = new ObservableCollection<DayViewModel>();

            try
            {
                this.IsLoading = true;
                this.ErrorMessage = null;

                ClassesByDayService service = new ClassesByDayService();
                UserCoursesData data = await service.getClassesByDayAsync().ConfigureAwait(false);

                for (int dayNumber = 1; dayNumber <= 6; dayNumber++)
                {
                    temp.Add(this.CreateDayViewModel((DayOfWeek)dayNumber, data));
                }

                this.BindableDays = temp;
            }
            catch (ServerRequestException ex)
            {
                this.ErrorMessage = ex.Message;
            }
            catch (Exception exception)
            {
                this.ErrorMessage = Translate.classesByDay_error_message;
            }

            this.IsLoading = false;

            return this.ErrorMessage;
        }

        /// <summary>
        /// Exports the schedule.
        /// </summary>
        /// <returns>The schedule.</returns>
        public async Task<string> ExportSchedule()
        {
            ClassesByDayService service = new ClassesByDayService();
            string calendarId = await service.createCalendar();
            UserCoursesData data = await service.getClassesByDayAsync().ConfigureAwait(false);

            for (int numberOfDay = 0; numberOfDay < 7; numberOfDay++)
            {
                foreach (CourseData currentClass in data.CoursesByDay.GetCoursesOfDay(numberOfDay))
                {
                    await service.createEvent(calendarId,
                                              currentClass.Name,
                                              Translate.classroom + currentClass.FormattedClassroom,
                                              currentClass.CurrentTimetable.StartTime,
                                              currentClass.CurrentTimetable.EndTime,
                                              numberOfDay);
                }
            }

            return Translate.added_to_calendar;
        }

        /// <summary>
        /// Currents the page on appearing.
        /// </summary>
        /// <param name="sender">Sender of the method</param>
        /// <param name="eventArgs">Event arguments.</param>
        protected override async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
        {
            if (this.days.Count == 0)
            {
                string errorMessage = await this.LoadDataAsync().ConfigureAwait(false);
                if (errorMessage == null)
                {
                    // This delay do carousel works
                    await Task.Delay(100);
                    this.DaysPosition = App.DaysPosition;
                }
            }
        }

        /// <summary>
        /// Creates the day view model.
        /// </summary>
        /// <returns>The day view model.</returns>
        /// <param name="day">Day of the week</param>
        /// <param name="coursesData">Courses data.</param>
        private DayViewModel CreateDayViewModel(DayOfWeek day, UserCoursesData coursesData)
        {
            if (day == DayOfWeek.Monday)
            {
                return new DayViewModel()
                {
                    ImageName = AppSettings.MondayImageName,
                    BackgroundColor = ViewModelConstants.MondayBackgroundColor,
                    FormattedName = Translate.Monday.ToUpper(),
                    Classes = this.GetObservableCollectionFromList(coursesData.CoursesByDay.GetCoursesOfDay(day)),
                    IsMale = coursesData.IsMale
                };
            }
            else if (day == DayOfWeek.Tuesday)
            {
                return new DayViewModel()
                {
                    ImageName = AppSettings.TuesdayImageName,
                    BackgroundColor = ViewModelConstants.TuesdayBackgroundColor,
                    FormattedName = Translate.Tuesday.ToUpper(),
                    Classes = this.GetObservableCollectionFromList(coursesData.CoursesByDay.GetCoursesOfDay(day)),
                    IsMale = coursesData.IsMale
                };
            }
            else if (day == DayOfWeek.Wednesday)
            {
                return new DayViewModel()
                {
                    ImageName = AppSettings.WednesdayImageName,
                    BackgroundColor = ViewModelConstants.WednesdayBackgroundColor,
                    FormattedName = Translate.Wednesday.ToUpper(),
                    Classes = this.GetObservableCollectionFromList(coursesData.CoursesByDay.GetCoursesOfDay(day)),
                    IsMale = coursesData.IsMale
                };
            }
            else if (day == DayOfWeek.Thursday)
            {
                return new DayViewModel()
                {
                    ImageName = AppSettings.ThursdayImageName,
                    BackgroundColor = Color.FromHex("#A977D6"),
                    FormattedName = Translate.Thursday.ToUpper(),
                    Classes = this.GetObservableCollectionFromList(coursesData.CoursesByDay.GetCoursesOfDay(day)),
                    IsMale = coursesData.IsMale
                };
            }
            else if (day == DayOfWeek.Friday)
            {
                return new DayViewModel()
                {
                    ImageName = AppSettings.FridayImageName,
                    BackgroundColor = Color.FromHex("#FF806F"),
                    FormattedName = Translate.Friday.ToUpper(),
                    Classes = this.GetObservableCollectionFromList(coursesData.CoursesByDay.GetCoursesOfDay(day)),
                    IsMale = coursesData.IsMale
                };
            }
            else
            {
                return new DayViewModel()
                {
                    ImageName = AppSettings.SaturdayImageName,
                    BackgroundColor = ViewModelConstants.SaturdayBackgroundColor,
                    FormattedName = Translate.Saturday.ToUpper(),
                    Classes = this.GetObservableCollectionFromList(coursesData.CoursesByDay.GetCoursesOfDay(day)),
                    IsMale = coursesData.IsMale
                };
            }
        }

        /// <summary>
        /// Gets the observable collection from list.
        /// </summary>
        /// <returns>The observable collection from list.</returns>
        /// <param name="courseList">Course list.</param>
        private ObservableCollection<CourseData> GetObservableCollectionFromList(List<CourseData> courseList)
        {
            foreach (CourseData course in courseList)
            {
                course.TapCommand = new Command(this.NavigateToCommissions);
                course.Name = course.Name.ToUpper();
				course.FontSize = FontSize;
                course.CurrentTimetable.FontSizeLocation = FontSizeLocation;
            }

            return new ObservableCollection<CourseData>(courseList);
        }

        /// <summary>
        /// Navigates to commissions.
        /// </summary>
        /// <param name="courseData">Course data.</param>
        private void NavigateToCommissions(object courseData)
        {
            Schedule.NavigateToCommissions((Schedule)CurrentPage, courseData);
        }
    }
}