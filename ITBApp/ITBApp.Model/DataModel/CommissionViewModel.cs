﻿namespace ITBApp.Model.DataModel
{
	public class CommissionViewModel
    {
		#region commission Title Data
        public string CommissionName { get; set; }
		#endregion

		#region Class Day Data
		public ClassDayViewModel ClassDay { get; set; }
		#endregion

		#region Teacher Data
		public string TeacherName { get; set; }
		#endregion

		#region boolean selectors
		public bool IsCommissionTitle { get; set; }
		public bool IsClassDay { get; set; }
		public bool IsTeachersTitle { get; set; }
		public bool IsTeacherData { get; set; }
		public bool IsEndLine { get; set; }
		public bool IsMoreCommissionsButton { get; set; }
		#endregion
    }
}
