﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ITBApp.Model.Handlers;
using ITBApp.Interfaces;
using ITBApp.Resources.i18n;
using ITBApp.Selectors;
using Xamarin.Forms;
using System.Linq;
using ITBApp.Services;
using ITBApp.Helpers;

namespace ITBApp.ViewModels
{
    internal class NewProcedureViewModel : BaseModel
    {
        private ObservableCollection<NewProcedureSelectorModel> newProcedures = new ObservableCollection<NewProcedureSelectorModel>();
        private WorkRequestService service = new WorkRequestService();
        private bool newProcedureCompleted = false;

		public ObservableCollection<NewProcedureSelectorModel> NewProcedures
		{
			get
			{
				return this.newProcedures;
			}

			private set
			{
				this.newProcedures = value;
				this.OnPropertyChanged("NewProcedures");
			}
		}

        public bool NewProcedureCompleted
		{
			get
			{
				return this.newProcedureCompleted;
			}

			private set
			{
				this.newProcedureCompleted = value;
				this.OnPropertyChanged("NewProcedureCompleted");
			}
		}

        public NewProcedureViewModel()
        {
            Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
        }

        public ValidateFormResponse IsReadyToSend()
        {
            var result = new ValidateFormResponse()
            {
                IsValid = true
            };

            if (this.newProcedures.Where(x => x.IsSelected).Count() != 1)
            {
                result.IsValid = false;
                result.Error = "Debe seleccionar un tipo de trámite.";

                return result;
            }

            var form = this.newProcedures.FirstOrDefault(x => x.IsFormTemplate);

            if (string.IsNullOrWhiteSpace(form.ProcedureTitle) || string.IsNullOrWhiteSpace(form.ProcedureDescription))
            {
				result.IsValid = false;
				result.Error = "Debe ingresar un título y descripción";

				return result;
            }

            return result;
        }

        public async Task<string> SendNewProcedure()
        {
			try
			{
				this.IsLoading = true;				
				this.NewProcedureCompleted = false;

                //TODO: Send procedure!
                var addWorkRequestToPost = this.GetAddWorkRequest();
                await service.PostAddNewNewWork(addWorkRequestToPost);

                this.NewProcedureCompleted = true;
				
			}
			catch (ServerRequestException ex)
			{
				this.ErrorMessage = ex.Message;
			}
			catch (Exception exception)
			{
				this.ErrorMessage = Translate.work_request_service_error;
			}

			this.IsLoading = false;

			return this.ErrorMessage;
            
        }

        public override async Task<string> LoadDataAsync()
        {
            try
            {
                this.IsLoading = true;
                this.ErrorMessage = null;
                this.NewProcedureCompleted = false;

                var res = await service.GetWorkGroupsRequestsAsync().ConfigureAwait(false);

                if (res.Groups.Count() > 0)
                {
                    ProcessGroupData(res);
                }
            }
			catch (ServerRequestException ex)
			{
				this.ErrorMessage = ex.Message;
			}
			catch (Exception exception)
			{
				this.ErrorMessage = Translate.work_request_service_error;
			}

			this.IsLoading = false;			
			return this.ErrorMessage;
        }

        private AddWorkRequest GetAddWorkRequest()
        {
            var loginService = new LoginService();
            var form = this.newProcedures.FirstOrDefault(x => x.IsFormTemplate);
            var group = this.newProcedures.FirstOrDefault(x => x.IsSelected);

            var res = new AddWorkRequest();

            //newProcedures
            var workRequestChangeset = new WorkRequestChangeset
            {
                Creator = loginService.GetSavedUserData().DNI,
                Description = form.ProcedureDescription,
                Title = form.ProcedureTitle,
                Group = GetGroupNameForPost(group.OptionDisplayName)
            };

            res.WorkRequestChangeset = workRequestChangeset;

            return res;
        }

        private void ProcessGroupData(WorkGroupsRequestData serverData)
        {
            var res = new ObservableCollection<NewProcedureSelectorModel>();

            foreach (var item in serverData.Groups)
            {
				res.Add(new NewProcedureSelectorModel()
				{
					IsAnOptionTemplate = true,
					IsFormTemplate = false,
					IsSelected = false,
                    OptionId = item.Id,
                    OptionDisplayName = GetGroupNameForDisplay(item.Name),
					TapOptionCommand = new Command(this.OnTappedOptionCommand)
				});
            }

			res.Add(new NewProcedureSelectorModel()
			{
				IsAnOptionTemplate = false,
				IsFormTemplate = true
			});

            NewProcedures = res;

        }

        private string GetGroupNameForDisplay(string name)
        {
            if (name == "ACADEMICS")
                return "Académicos";

            if (name == "ADMINISTRATIVES")
                return "Administrativos";

            return name;
        }

        private string GetGroupNameForPost(string name)
        {
            if (name == "Académicos")
                return "ACADEMICS";

            if (name == "Administrativos")
                return "ADMINISTRATIVES";

            return name;
        }


		protected override async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
		{
			App.CurrentPage = PageId.PROCEDURES;

            if (NewProcedures.Count() == 0)
			{
				await this.LoadDataAsync().ConfigureAwait(false);
			}
		}

		private void OnTappedOptionCommand(object optionIdObject)
		{
			int optionId = (int)optionIdObject;
            var option = this.NewProcedures.FirstOrDefault(x => x.OptionId == optionId);

			if (option != null)
			{
				if (option.IsSelected)
				{
					option.IsSelected = false;					
					return;
				}
				else
				{
					option.IsSelected = true;
					foreach (var item in this.NewProcedures.Where(x => x.OptionId != optionId).ToList())
					{
						item.IsSelected = false;
					}

					return;
				}
			}
		}
    }

    public class ValidateFormResponse
    {
        public bool IsValid
        {
            get;
            set;    
        }

        public string Error
        {
            get;
            set;
        }
    }
}