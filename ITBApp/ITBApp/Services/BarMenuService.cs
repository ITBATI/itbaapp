﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using ITBApp.Handlers;
using ITBApp.Helpers;
using ITBApp.Model.Handlers;
using ITBApp.Resources.i18n;
using ITBApp.Util.Helpers;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.Services
{
    public class BarMenuService
    {
        private TokenHolder TokenHolder = new TokenHolder();

		public async Task DeleteCachedDataAsync()
		{
			Application.Current.Properties.Remove(ServiceConstants.BAR_MENU_DATA);
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
		}

		private BarMenuData GetSavedMenuData()
		{
			if (Application.Current.Properties.ContainsKey(ServiceConstants.BAR_MENU_DATA))
			{
				string json = (string)Application.Current.Properties[ServiceConstants.BAR_MENU_DATA];
				BarMenuData data = (BarMenuData)SerializerHelper.Deserialize(json, (new BarMenuData()).GetType());

				if (data.Expiration > DateTime.Now || !CrossConnectivity.Current.IsConnected)
				{
					return data;
				}
			}

			return null;
		}

		private async void SaveMenuData(BarMenuData data)
		{
			string json = SerializerHelper.Serialize(data);
            Application.Current.Properties[ServiceConstants.BAR_MENU_DATA] = json;
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
		}

		public async Task<BarMenuData> GetMenuAsync()
		{
			BarMenuData menuData = GetSavedMenuData();

            if (menuData == null)
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    throw new ServerRequestException(Translate.weather_no_internet_text);
                }

                try
                {
                    string token = await TokenHolder.GetTokenAsync().ConfigureAwait(false);

                    var stringResponse = await HttpUtility
                        .MakeGetCallAsync(String.Format("{0}{1}", AppSettings.DailyBarMenuURL, token))
                        .ConfigureAwait(false);

                    menuData = ParseResponse(stringResponse);
                    menuData.Expiration = DateTime.Now.AddMinutes(ServiceConstants.BAR_MENU_EXPIRATION_IN_MINUTES);

                    SaveMenuData(menuData);
                }
                catch (TokenExpiredException)
                {
                    throw new ServerRequestException(Translate.bar_menu_no_internet_text);
                }
                catch
                {
                    throw new ServerRequestException(Translate.bar_menu_service_error);
                }
            }

			return menuData;
		}

        private BarMenuData ParseResponse(string response)
		{
			var result = new BarMenuData()
			{
                Menues = new List<SingleMenuData>()
			};

			try
			{
                JArray jsonResponse = JArray.Parse(response);
			
				foreach (var menuEntry in jsonResponse)
				{
                    var menuData = new SingleMenuData()
					{
						Title = (string)menuEntry["description"],
						Description = (string)menuEntry["comments"],
						Price = (string)menuEntry["price"],
						Date = (string)menuEntry["menuDate"],
						Location = (string)menuEntry["CampusID"]
					};

                    result.Menues.Add(menuData);
				}
			}
			catch (Exception exc)
			{
				throw new ServerRequestException("Error al procesar los datos de información del menu del día");
			}

			return result;
		}

		[DataContract]
		[KnownType(typeof(BarMenuData))]
		public class BarMenuData
		{
            [DataMember]
			public List<SingleMenuData> Menues { get; set; }
            [DataMember]
            public DateTime Expiration { get; set; }
		}

		[DataContract]
		[KnownType(typeof(SingleMenuData))]
		public class SingleMenuData
		{
            [DataMember]
			public string Title { get; set; }
            [DataMember]
			public string Description { get; set; }
            [DataMember]
			public string Price { get; set; }
			[DataMember]
			public string Date { get; set; }
			[DataMember]
			public string Location { get; set; }
		}
    }
}
