﻿//-----------------------------------------------------------------------
// <copyright file="ViewModelConstants.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ITBApp
{
    using Xamarin.Forms;

    /// <summary>
    /// Constants used in ViewModels
    /// </summary>
    public static class ViewModelConstants
    {
        /// <summary>
        /// Schedule Monday Background Color  
        /// </summary>
        public static readonly Color MondayBackgroundColor = Color.FromHex("#8CCEC2");

        /// <summary>
        /// Schedule Tuesday Background Color   
        /// </summary>
        public static readonly Color TuesdayBackgroundColor = Color.FromHex("#FF5353");

        /// <summary>
        /// Schedule Wednesday Background Color    
        /// </summary>
        public static readonly Color WednesdayBackgroundColor = Color.FromHex("#3DBFE6");

		/// <summary>
		/// Schedule Thursday Background Color    
		/// </summary>
		public static readonly Color ThursdayBackgroundColor = Color.FromHex("#A977D6");

		/// <summary>
		/// Schedule Friday Background Color    
		/// </summary>
		public static readonly Color FridayBackgroundColor = Color.FromHex("#FF806F");

		/// <summary>
		/// Schedule Saturday Background Color    
		/// </summary>
		public static readonly Color SaturdayBackgroundColor = Color.FromHex("#FFDF80");

		/// <summary>
		/// Schedule Saturday Background Color    
		/// </summary>
		public static readonly Color SundayBackgroundColor = Color.FromHex("#769CD3");

    }
}
