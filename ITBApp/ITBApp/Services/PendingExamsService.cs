﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ITBApp.Handlers;
using ITBApp.Helpers;
using ITBApp.Model.DataModel;
using ITBApp.Model.Handlers;
using ITBApp.Resources.i18n;
using ITBApp.Util;
using ITBApp.Util.Helpers;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.Services
{
	public class PendingExamsService
	{
		private TokenHolder tokenHolder = new TokenHolder();

		public async Task<PendingExamsData> GetAsync()
		{
			PendingExamsData data = GetSavedPendingExams();

			if (data == null)
			{
				if (!CrossConnectivity.Current.IsConnected)
				{
					throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
				}


				try
				{
					string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
					data = await getAsync(token).ConfigureAwait(false);

					data.Expiration = DateTime.Now.AddDays(ServiceConstants.PENDING_EXAMS_EXPIRATION_IN_DAYS);

					string json = SerializerHelper.Serialize(data);
					Application.Current.Properties[ServiceConstants.PENDING_EXAMS_DATA] = json;
					await Application.Current.SavePropertiesAsync().ConfigureAwait(false);

				}
				catch (TokenExpiredException)
				{
					throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
				}
			}

			return data;
		}


		private PendingExamsData GetSavedPendingExams()
		{
			if (Application.Current.Properties.ContainsKey(ServiceConstants.PENDING_EXAMS_DATA))
			{
				string json = (string)Application.Current.Properties[ServiceConstants.PENDING_EXAMS_DATA];
				PendingExamsData data = (PendingExamsData)SerializerHelper.Deserialize(json, (new PendingExamsData()).GetType());

				if (data.Expiration > DateTime.Now || !CrossConnectivity.Current.IsConnected)
				{
					return data;
				}
			}

			return null;
		}


		private async Task<PendingExamsData> getAsync(string token)
		{
			string stringResponse = await makeCallAsync(token).ConfigureAwait(false);

			List<ExamData> exams = processResponse(stringResponse);

			PendingExamsData data = new PendingExamsData();
			data.Exams = exams;

			return data;
		}

		private List<ExamData> processResponse(string stringResponse)
		{
			JToken jsonResponse = JToken.Parse(stringResponse);

			List<ExamData> exams = new List<ExamData>();

			//No tiene examenes pendientes!
			if (!jsonResponse["examPermits"].HasValues)
				return exams;

			JObject examsPermits = JObject.Parse(jsonResponse["examPermits"].ToString());

			foreach (JToken item in JsonHelper.GetAsArray(examsPermits["examPermit"]))
			{
				ExamData data = new ExamData();
				data.Allowed = item["allowedExams"].ToString();
				data.Date = item["expirationDate"].ToString();
				data.Description = item["subjectDescription"].ToString();
				data.Taken = item["takenExams"].ToString();

				exams.Add(data);
			}

			return exams;
		}


		private async Task<string> makeCallAsync(string token)
		{
			var uriBuilder = new UriBuilder(AppSettings.PendingExamsServiceURL + token);
			HttpValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
			uriBuilder.Query = query.ToString();

			using (var client = new HttpClient())
			{
				client.MaxResponseContentBufferSize = 256000;
				HttpResponseMessage response = await client.GetAsync(uriBuilder.Uri).ConfigureAwait(false);
				if (response.IsSuccessStatusCode)
				{
					string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					return result;

				}
				else
				{
					throw new ServerRequestException("Error de conexión al servicio de finales pendientes");
				}
			}
		}

		public async Task DeleteCachedData()
		{
			Application.Current.Properties.Remove(ServiceConstants.PENDING_EXAMS_DATA);
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
		}
	}
}
