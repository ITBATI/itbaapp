﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ITBApp.Handlers;
using ITBApp.Helpers;
using ITBApp.Model.DataModel;
using ITBApp.Model.Handlers;
using ITBApp.Resources.i18n;
using ITBApp.Util;
using ITBApp.Util.Helpers;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.Services
{
	public class AccountBalanceService
	{
		private TokenHolder tokenHolder = new TokenHolder();

		public async Task<AccountBalanceData> GetAccountBalanceAsync()
		{
			AccountBalanceData accountData = GetSavedAccountData();

			if (accountData == null)
			{
				if (!CrossConnectivity.Current.IsConnected)
				{
					throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
				}


				try
				{
					string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
					accountData = await getAccountBalanceAsync(token).ConfigureAwait(false);

					accountData.Expiration = DateTime.Now.AddDays(ServiceConstants.ACCOUNT_BALANCE_EXPIRATION_IN_DAYS);

					string json = SerializerHelper.Serialize(accountData);
					Application.Current.Properties[ServiceConstants.ACCOUNT_BALANCE_DATA] = json;
					await Application.Current.SavePropertiesAsync().ConfigureAwait(false);

				}
				catch (TokenExpiredException)
				{
					throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
				}

			}

			return accountData;
		}


		private AccountBalanceData GetSavedAccountData()
		{
			if (Application.Current.Properties.ContainsKey(ServiceConstants.ACCOUNT_BALANCE_DATA))
			{
				string json = (string)Application.Current.Properties[ServiceConstants.ACCOUNT_BALANCE_DATA];
				AccountBalanceData data = (AccountBalanceData)SerializerHelper.Deserialize(json, (new AccountBalanceData()).GetType());

				if (data.Expiration > DateTime.Now || !CrossConnectivity.Current.IsConnected)
				{
					return data;
				}
			}

			return null;
		}


		private async Task<AccountBalanceData> getAccountBalanceAsync(string token)
		{
			string stringResponse = await makeCallAsync(token).ConfigureAwait(false);

			List<PaymentData> payments = processResponse(stringResponse);

			AccountBalanceData data = new AccountBalanceData();
			data.Payments = payments;

			return data;
		}

		private List<PaymentData> processResponse(string stringResponse)
		{
			JToken jsonResponse = JToken.Parse(stringResponse);

			/*
			JObject jsonResponse = JObject.Parse(stringResponse);

			JObject financialState = JObject.Parse(jsonResponse["financialState"].ToString());

			if (!jsonResponse["Resultado"].ToString().Equals("0"))
			{
				throw new ServerRequestException(string.Join(".", JsonHelper.GetAsArray(financialState["errors"])));
			}
			*/

			List<PaymentData> payments = new List<PaymentData>();

			//foreach (JToken item in JsonHelper.GetAsArray(financialState["group"]))
			foreach (JToken item in JsonHelper.GetAsArray(jsonResponse))
			{
				PaymentData paymentData = new PaymentData();
				paymentData.Amount = ValidationHelper.FormatBalanceNumber(item["amount"].ToString());
				paymentData.Balance = ValidationHelper.FormatBalanceNumber(item["balance"].ToString());
				paymentData.DueDate = item["dueDate"].ToString();
				paymentData.ReceiptDate = item["receiptDate"].ToString();
				paymentData.ReceiptDescription = item["receiptDescription"].ToString();
				paymentData.ReceiptId = item["receipt"].ToString();

				payments.Add(paymentData);
			}

			return payments;
		}


		private async Task<string> makeCallAsync(string token)
		{
			var uriBuilder = new UriBuilder(AppSettings.AccountBalanceServiceURL + token + "/top/" + ServiceConstants.ACCOUNT_BALANCE_ITEMS_PER_PAGE);
			HttpValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
			uriBuilder.Query = query.ToString();

			using (var client = new HttpClient())
			{
				client.MaxResponseContentBufferSize = 256000;
				HttpResponseMessage response = await client.GetAsync(uriBuilder.Uri).ConfigureAwait(false);
				if (response.IsSuccessStatusCode)
				{
					string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					return result;

				}
				else
				{
					throw new ServerRequestException("Error de conexión al servicio de estado de cuenta");
				}
			}
		}

		public async Task DeleteCachedData()
		{
			Application.Current.Properties.Remove(ServiceConstants.ACCOUNT_BALANCE_DATA);
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
		}

	}
}
