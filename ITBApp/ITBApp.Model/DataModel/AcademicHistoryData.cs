﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel
{
	[DataContract]
	[KnownType(typeof(CourseEntryData))]
    public class AcademicHistoryData
    {
        [DataMember]
        public List<CourseEntryData> Courses { get; set; }
        [DataMember]
        public DateTime Expiration { get; set; }
    }
}
