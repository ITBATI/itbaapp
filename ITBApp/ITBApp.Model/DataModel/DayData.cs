﻿using Xamarin.Forms;

namespace ITBApp.Model.DataModel
{
	public class DayData
	{
		public static Color GetMainBackgroundColorForDay(int day)
		{
			Color[] MainBackgroundColors = { 
                Color.FromHex("#FFFFFF"), // Sunday
                Color.FromHex("#95D8B0"), // Monday
                Color.FromHex("#FF5353"), // Tuesday
			    Color.FromHex("#3DBFE6"), // Wednesday
                Color.FromHex("#A977D6"), // Thursday
                Color.FromHex("#FF806F"), // Friday
                Color.FromHex("#FFDF80")  // Saturday
            };
			return MainBackgroundColors[day];
		}

		public static Color GetSecondaryBackgroundColorForDay(int day)
		{
			Color[] SecondaryBackgroundColors = { 
                Color.FromHex("#FFFFFF"), // Sunday
                Color.FromHex("#E6FFEF"), // Monday
                Color.FromHex("#FFBFBC"), // Tuesday
			    Color.FromHex("#C5EBF7"), // Wednesday
                Color.FromHex("#ECD0F2"), // Thursday
                Color.FromHex("#FFD4CA"), // Friday
                Color.FromHex("#FFF5D2")  // Saturday
            };
			return SecondaryBackgroundColors[day];
		}

	}
}

