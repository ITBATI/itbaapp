﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ITBApp.Model
{
	[DataContract]
	[KnownType(typeof(OccupationData))]
	public class AvailableClassroomsData
	{
		[DataMember]
		public DateTime Expiration { get; set; }
		[DataMember]
		public ObservableCollection<OccupationData> freeNow { get; set; }
		[DataMember]
		public ObservableCollection<OccupationData> freeSoon { get; set; }
	}

	[DataContract]
	public class OccupationData
	{
        // Font Sizes
        public const int LargeFontSize = 30;
        public const int MediumFontSize = 18;
        public const int SmallFontSize = 15;

        // Classroom Lengths
        public const int ClassroomSmallMaxLength = 4;
        public const int ClassroomMediumMaxLength = 4;


		[DataMember]
		public string Classroom { get; set; }
		[DataMember]
		public string Type { get; set; }
		[DataMember]
		public string Building { get; set; }
		[DataMember]
		public string StartTime { get; set; }
		[DataMember]
		public string EndTime { get; set; }
		[DataMember]
		public string UsedBy { get; set; }

		public int ClassroomFontSize
		{
			get
			{
				if (this.Classroom.Length <= ClassroomSmallMaxLength)
				{
					return LargeFontSize;
				}
				else if (this.Classroom.Length <= ClassroomMediumMaxLength)
				{
					return MediumFontSize;
				}
				else
				{
					return SmallFontSize;
				}
			}
		}
	}
}
