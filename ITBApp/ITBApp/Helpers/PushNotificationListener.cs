using System.Diagnostics;
using Newtonsoft.Json.Linq;
using ITBApp.Services;
using Xamarin.Forms;

namespace ITBApp.Helpers
{
	//Class to handle push notifications listens to events such as registration, unregistration, message arrival and errors.
	public class PushNotificationListener : IPushNotificationListener
	{

		public void OnMessage(JObject values, DeviceType deviceType)
		{
			Debug.WriteLine("Message Arrived " + values);
		}

		public void OnRegistered(string deviceId, DeviceType deviceType)
		{
			Debug.WriteLine(string.Format("Push Notification - Device Registered - Token : {0}", deviceId));

			RegistrationService service = new RegistrationService();

			if (App.IsLoggedIn)
			{
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
				service.RegisterDeviceAsync(deviceId, deviceType);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

			}
			else {
				DependencyService.Get<ILog>().WriteLine("No se puede registrar dispositivo aun. Guardando informacion de dispositivo.");
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
				service.SaveDeviceInfoAsync(deviceId, deviceType);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
			}

		}

		public void OnUnregistered(DeviceType deviceType)
		{
			Debug.WriteLine("Push Notification - Device Unnregistered");

		}

		public void OnError(string message, DeviceType deviceType)
		{
			Debug.WriteLine(string.Format("Push notification error - {0}", message));
		}

		public bool ShouldShowNotification()
		{
			return true;
		}
	}
}
