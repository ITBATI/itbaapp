﻿using System.Threading.Tasks;
using ITBApp.Helpers;
using ITBApp.Model.Interfaces;
using ITBApp.Resources.i18n;
using ITBApp.ViewModels;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.UI
{
	public partial class FreeClassrooms : ContentPage
	{
		private static bool isTapped = false;

		public FreeClassrooms()
		{
			InitializeComponent();
			Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();

			((BaseModel)BindingContext).Initialize(this);
			ConfigureEventListeners();
		}

		void LoadPage()
		{
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
			((AvailableClassroomsViewModel)BindingContext).LoadDataAsync();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
		}

		void ConfigureEventListeners()
		{

			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
				if (BindingContext != null && App.CurrentPage == PageId.FREE_CLASSROOMS && args.IsConnected)
				{
					LoadPage();
				}
			};

			((AvailableClassroomsViewModel)BindingContext).ClassroomsNowTapCommand = new Command(async() =>
			{
				if (((AvailableClassroomsViewModel)BindingContext).LoadingIsDone && !isTapped
				    && ((AvailableClassroomsViewModel)BindingContext).BindableAvailableClassroomsData.freeNow.Count > 0)
				{
					isTapped = true;

                    if (Device.RuntimePlatform == "Windows")
                    {
                        NavigationPage availableclassroomsDetailPage = new NavigationPage(new FreeClassroomsNow(((AvailableClassroomsViewModel)BindingContext).BindableAvailableClassroomsData));
                        availableclassroomsDetailPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                        availableclassroomsDetailPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

                        App.MasterDetailPage.Detail = availableclassroomsDetailPage;
                    }
                    else
                        await Navigation.PushAsync(new FreeClassroomsNow(((AvailableClassroomsViewModel)BindingContext).BindableAvailableClassroomsData));


                    isTapped = false;
				}
			});

			((AvailableClassroomsViewModel)BindingContext).ClassroomsSoonTapCommand = new Command(async () =>
			{
				if (((AvailableClassroomsViewModel)BindingContext).LoadingIsDone && !isTapped
					&& ((AvailableClassroomsViewModel)BindingContext).BindableAvailableClassroomsData.freeSoon.Count > 0)
				{
					isTapped = true;

                    if (Device.RuntimePlatform == "Windows")
                    {
                        NavigationPage availableclassroomsDetailPage = new NavigationPage(new FreeClassroomsSoon(((AvailableClassroomsViewModel)BindingContext).BindableAvailableClassroomsData));
                        availableclassroomsDetailPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                        availableclassroomsDetailPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

                        App.MasterDetailPage.Detail = availableclassroomsDetailPage;
                    }
                    else
                        await Navigation.PushAsync(new FreeClassroomsSoon(((AvailableClassroomsViewModel)BindingContext).BindableAvailableClassroomsData));

					isTapped = false;
				}
			});
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			if (!isTapped)
			{
				this.BindingContext = null;
				this.Content = null;
			}
            App.HardwareBackPressed = null;
		}

        protected override void OnAppearing()
        {
        	base.OnAppearing();

        	App.HardwareBackPressed = () =>
        	{
        		MainMenuItem item = MainMenuItem.HomeItem;
        		App.CurrentPage = item.Id;
        		item.NavigationPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
        		item.NavigationPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
        		App.MasterDetailPage.Detail = item.NavigationPage;
        		return Task.FromResult<bool?>(false);
        	};
        }

	}
}
