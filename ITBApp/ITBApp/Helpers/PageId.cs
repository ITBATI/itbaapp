﻿//-----------------------------------------------------------------------
// <copyright file="Pageid.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace ITBApp.Helpers
{
    public enum PageId
    {
        CLASSES_BY_DAY,
        NOTIFICATIONS,
        HELP,
        USER_PROFILE,
        CONTACTS,
        AVAILABLE_CLASSROOMS,
        LOGOUT,
        ACCOUNT_BALANCE,
        PENDING_EXAMS,
        TERMS_AND_CONDITIONS,
        SURVEY,
        PRINT,
        HOME,
        ACADEMIC_HISTORY,
        EVENTS,
        PROCEDURES,
        BAR_MENU
	}
}
