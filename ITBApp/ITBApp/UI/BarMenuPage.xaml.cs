﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ITBApp.Helpers;
using ITBApp.Interfaces;
using ITBApp.Resources.i18n;
using ITBApp.Services;
using ITBApp.ViewModels;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.UI
{
    public partial class BarMenuPage : ContentPage
    {
        public BarMenuPage()
        {
            InitializeComponent();
			Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
			((BaseModel)BindingContext).Initialize(this);
			ConfigureEventListeners();

			TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer
			{
				Command = new Command(async () =>
				{
					((BarMenuViewModel)BindingContext).LocationId = "1";
					App.lastLocationId = "1";
                    await ((BarMenuViewModel)BindingContext).LoadDataAsync();
					RefreshLocations();
				})
			};
			slSede1.GestureRecognizers.Add(tapGestureRecognizer);

			tapGestureRecognizer = new TapGestureRecognizer
			{
				Command = new Command(async () =>
				{
					((BarMenuViewModel)BindingContext).LocationId = "2";
					App.lastLocationId = "2";
					await ((BarMenuViewModel)BindingContext).LoadDataAsync();
					RefreshLocations();
				})
			};
			slSede2.GestureRecognizers.Add(tapGestureRecognizer);

			tapGestureRecognizer = new TapGestureRecognizer
			{
				Command = new Command(async () =>
				{
					((BarMenuViewModel)BindingContext).LocationId = "3";
					App.lastLocationId = "3";
					await ((BarMenuViewModel)BindingContext).LoadDataAsync();
					RefreshLocations();
				})
			};
			slSede3.GestureRecognizers.Add(tapGestureRecognizer);
        }

		void ConfigureEventListeners()
		{
			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
                if (BindingContext != null && App.CurrentPage == PageId.BAR_MENU && args.IsConnected)
				{
					((BarMenuViewModel)BindingContext).LoadDataAsync().ConfigureAwait(false);
				}
			};
		}

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}

		private void RefreshLocations()
		{
            if (((BarMenuViewModel)BindingContext).LocationId == "1")
			{
				slSede1.BackgroundColor = Color.FromHex("#0E2B4D");
				lblSede1Enabled.IsVisible = true;
				lblSede1Disabled.IsVisible = false;
				imgSede1.Source = "sede_1_enabled.png";
			}
			else
			{
				slSede1.BackgroundColor = Color.FromHex("#002539");
				lblSede1Enabled.IsVisible = false;
				lblSede1Disabled.IsVisible = true;
				imgSede1.Source = "sede_1_disabled.png";
			}

			if (((BarMenuViewModel)BindingContext).LocationId == "2")
			{
				slSede2.BackgroundColor = Color.FromHex("#0E2B4D");
				lblSede2Enabled.IsVisible = true;
				lblSede2Disabled.IsVisible = false;
				imgSede2.Source = "sede_2_enabled.png";
			}
			else
			{
				slSede2.BackgroundColor = Color.FromHex("#002539");
				lblSede2Enabled.IsVisible = false;
				lblSede2Disabled.IsVisible = true;
				imgSede2.Source = "sede_2_disabled.png";
			}

			if (((BarMenuViewModel)BindingContext).LocationId == "3")
			{
				slSede3.BackgroundColor = Color.FromHex("#0E2B4D");
				lblSede3Enabled.IsVisible = true;
				lblSede3Disabled.IsVisible = false;
				imgSede3.Source = "sede_3_enabled.png";
			}
			else
			{
				slSede3.BackgroundColor = Color.FromHex("#002539");
				lblSede3Enabled.IsVisible = false;
				lblSede3Disabled.IsVisible = true;
				imgSede3.Source = "sede_3_disabled.png";
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			App.HardwareBackPressed = () =>
			{
				MainMenuItem item = MainMenuItem.HomeItem;
				App.CurrentPage = item.Id;
				item.NavigationPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
				item.NavigationPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
				App.MasterDetailPage.Detail = item.NavigationPage;
				return Task.FromResult<bool?>(false);
			};

			// Image by location
			Device.BeginInvokeOnMainThread(async () =>
			{
				try
				{
                    if (Device.RuntimePlatform == Device.UWP)
					{
						await Task.Delay(300);
					}
                    var locations = await new GeolocatorService().GetLocationsAsync().ConfigureAwait(false);

                    if (((BarMenuViewModel)BindingContext).LocationId == string.Empty)
					{
                        ((BarMenuViewModel)BindingContext).LocationId = await new GeolocatorService().GetLocationIdAsync().ConfigureAwait(false);

						if (((BarMenuViewModel)BindingContext).LocationId == "0") ((BarMenuViewModel)BindingContext).LocationId = App.lastLocationId;
					}

                    Device.BeginInvokeOnMainThread(()=>{
						lblSede1Enabled.Text = locations.ITBALocations.Where(x => x.Id == "1").First().Name;
						lblSede2Enabled.Text = locations.ITBALocations.Where(x => x.Id == "2").First().Name;
						lblSede3Enabled.Text = locations.ITBALocations.Where(x => x.Id == "3").First().Name;
						lblSede1Disabled.Text = locations.ITBALocations.Where(x => x.Id == "1").First().Name;
						lblSede2Disabled.Text = locations.ITBALocations.Where(x => x.Id == "2").First().Name;
						lblSede3Disabled.Text = locations.ITBALocations.Where(x => x.Id == "3").First().Name;

                        RefreshLocations();
                    });
				}
				catch (Exception exc)
				{
					//
				}
			});
		}
    }
}
