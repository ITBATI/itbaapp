﻿using System;
using ITBApp.ViewModels;
using Xamarin.Forms;

namespace ITBApp.Selectors
{
    public class EventDaySelector: DataTemplateSelector
    {
		public DataTemplate NoEventsDayTemplate { get; set; }
		public DataTemplate OneEventDayTemplate { get; set; }
		public DataTemplate FiveEventsDayTemplate { get; set; }
        public DataTemplate TenEventsDayTemplate { get; set; }
        public DataTemplate MoreThanTenEventsDayTemplate { get; set; }

		protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
		{
			if (item == null) return null;
            EventDayData dayViewModel = (EventDayData)item;

            if (dayViewModel.Event1 == null) return NoEventsDayTemplate;
            if (dayViewModel.Event2 == null) return OneEventDayTemplate;
            if (dayViewModel.Event6 == null) return FiveEventsDayTemplate;
            if (dayViewModel.Event11 == null) return TenEventsDayTemplate;
			return MoreThanTenEventsDayTemplate;
		}
    }
}
