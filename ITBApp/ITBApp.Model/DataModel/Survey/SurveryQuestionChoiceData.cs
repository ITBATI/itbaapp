﻿using System;
using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel.Survey
{
	public class SurveryQuestionChoiceData
	{
		[DataMember]
		public long Id { get; set; }
		
		[DataMember]
		public bool IsSelected { get; set; }

		[DataMember]
		public bool IsWritingChoice { get; set; }

		[DataMember]
		public String Description { get; set; }

		[DataMember]
		public String Title { get; set; }
	}
}