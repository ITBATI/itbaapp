﻿using System;
using ITBApp.Helpers;
using ITBApp.Interfaces;
using ITBApp.Resources.i18n;
using ITBApp.Services;
using ITBApp.ViewModels;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.UI
{
    public partial class NewProcedurePage : ContentPage
    {
        public NewProcedurePage()
        {
            InitializeComponent();
            ((BaseModel)BindingContext).Initialize(this);
            ((NewProcedureViewModel)BindingContext).IsLoading = true;

            if (Device.RuntimePlatform == Device.UWP)
            {
                ToolbarItems.Add(new ToolbarItem("Volver", "pop_arrow_left.png", () =>
                {
                    NavigationPage procedurePage = new NavigationPage(new ProceduresPage());
                    procedurePage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                    procedurePage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                    App.MasterDetailPage.Detail = procedurePage;
                }));
            }

            ConfigureEventListeners();
        }

		void ConfigureEventListeners()
		{
			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
				if (BindingContext != null && App.CurrentPage == PageId.PROCEDURES && args.IsConnected)
				{
					((NewProcedureViewModel)BindingContext).LoadDataAsync().ConfigureAwait(false);
				}
			};
		}

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}

        async void BackToProceduresPage_Tapped(object sender, System.EventArgs e)
        {
            // Clean saved data to refresh list
            await new WorkRequestService().DeleteCachedDataAsync();
            ActionUIHelper.OpenPage(MainMenuItem.ProceduresItem);
        }

        async void NewProcedure_Tapped(object sender, System.EventArgs e)
        {
            var isFromValid = ((NewProcedureViewModel)BindingContext).IsReadyToSend();

            if (!isFromValid.IsValid)
            {
                await DisplayAlert("Trámites", isFromValid.Error, "Aceptar");
            }
            else
            {
                var answer = await DisplayAlert("Trámites", "Estás por solicitar un nuevo trámite. ¿Estás seguro?", "SI", "NO");
                if (answer)
                {
                    await ((NewProcedureViewModel)BindingContext).SendNewProcedure().ConfigureAwait(false);
                }
            }
        }

    }
}
