﻿using ITBApp.Listeners;

[assembly: Xamarin.Forms.Dependency(typeof(ITBApp.UWP.Implementations.LogoutListener))]
namespace ITBApp.UWP.Implementations
{
    public class LogoutListener : ILogoutListener
    {
        public LogoutListener()
        {
        }

        void ILogoutListener.DidLogout()
        {
            // nada
            // Este metodo se incluye por compatibilidad con IOS
        }
    }
}

