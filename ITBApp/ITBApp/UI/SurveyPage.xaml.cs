﻿using System;
using System.Threading.Tasks;
using ITBApp.Helpers;
using ITBApp.Interfaces;
using ITBApp.Resources.i18n;
using ITBApp.ViewModels;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp
{
	public partial class SurveyPage : ContentPage
	{
        private int SurveyId = 0;
        private bool HasResult = false;

		public SurveyPage()
		{
			InitializeComponent();
			Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
			((BaseModel)BindingContext).Initialize(this);
            //ConfigureEventListeners();

            if(string.IsNullOrWhiteSpace(this.Title))
            {
                this.Title = "Opiná";

                if (Device.RuntimePlatform == Device.UWP)
                {
                    ToolbarItems.Add(new ToolbarItem("Volver", "pop_arrow_left.png", () =>
                    {
                        NavigationPage surveyEntryPage = new NavigationPage(new SurveyEntryPage());
                        surveyEntryPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                        surveyEntryPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                        App.MasterDetailPage.Detail = surveyEntryPage;
                    }));
                }
            }

            ((SurveyViewModel)BindingContext).LoadDataAsync().ConfigureAwait(false);
		}

        public SurveyPage(int surveyId, bool hasResult)
		{
            InitializeComponent();

            SurveyId = surveyId;
            HasResult = hasResult;

            if (Device.RuntimePlatform == Device.UWP)
            {
                ToolbarItems.Add(new ToolbarItem("Volver", "pop_arrow_left.png", () =>
                {
                    NavigationPage surveyEntryPage = new NavigationPage(new SurveyEntryPage());
                    surveyEntryPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                    surveyEntryPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                    App.MasterDetailPage.Detail = surveyEntryPage;
                }));
            }

            Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
			((BaseModel)BindingContext).Initialize(this);			

            ((SurveyViewModel)BindingContext).LoadDataAsync(SurveyId, HasResult).ConfigureAwait(false);
		}

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}

		void ConfigureEventListeners()
		{

			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
				if (BindingContext != null && App.CurrentPage == PageId.SURVEY && args.IsConnected)
				{
                    if (SurveyId == 0)
			            ((SurveyViewModel)BindingContext).LoadDataAsync().ConfigureAwait(false);

					((SurveyViewModel)BindingContext).LoadDataAsync(SurveyId, HasResult).ConfigureAwait(false);
				}
			};
		}

		void Handle_Completed(object sender, System.EventArgs e)
		{
			if (!String.IsNullOrWhiteSpace(((Editor)sender).Text))
			{
				((SurveyViewModel)BindingContext).EditorTextWasChanged();
				return;
			}
		}

		void SurveyCompletedButtomTapped(Object sender, EventArgs args)
		{
			if (((SurveyViewModel)BindingContext).HasSurveyResponses())
			{
				((SurveyViewModel)BindingContext).PostSurveyAsync().ConfigureAwait(false);
			}
			else
			{
                this.DisplayAlert(Translate.itba_ask_survey, Translate.itba_must_complete, Translate.Ok);
			}
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			this.BindingContext = null;
			this.Content = null;

            App.HardwareBackPressed = null;
		}

        protected override void OnAppearing()
        {
        	base.OnAppearing();

        	App.HardwareBackPressed = () =>
        	{
        		MainMenuItem item = MainMenuItem.HomeItem;
        		App.CurrentPage = item.Id;
        		item.NavigationPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
        		item.NavigationPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
        		App.MasterDetailPage.Detail = item.NavigationPage;
        		return Task.FromResult<bool?>(false);
        	};
        }
	}
}
