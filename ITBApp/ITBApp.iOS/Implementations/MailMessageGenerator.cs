﻿using System;
using System.IO;
using System.Net.Mail;
using Foundation;
using MimeKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(ITBApp.iOS.Implementations.MailMessageGenerator))]
namespace ITBApp.iOS.Implementations
{
    [Preserve(AllMembers = true)]
	public class MailMessageGenerator : IMailMessageGenerator
	{
        private const string MailSentMessage = "Se envió a imprimir {0}";
        private const string TextPartType = "plain";
        private const string MultiPartType = "mixed";

		public MailMessageGenerator()
		{
		}

		public string CreateMailMessage(string from, string to, byte[] attachment = null, string fileName = null, string mediaType = null, string mediaSubType = null)
		{
			var message = new MimeMessage();
			message.From.Add(new MailboxAddress("", from));
			message.To.Add(new MailboxAddress("", to));
			message.Subject = String.Format(MailSentMessage, fileName);

			var body = new TextPart(TextPartType);

			if (attachment != null)
			{
				using (MemoryStream ms = new MemoryStream(attachment))
				{

					// create an image attachment!
					var mailAttachment = new MimePart(mediaType, mediaSubType)
					{
						ContentObject = new ContentObject(ms, ContentEncoding.Default),
						ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
						ContentTransferEncoding = ContentEncoding.Base64,
						FileName = fileName,
						IsAttachment = true,
						ContentId = Guid.NewGuid().ToString()
					};

					// now create the multipart/mixed container to hold the message text and the
					// image attachment
					var multipart = new Multipart(MultiPartType);
					multipart.Add(body);
					multipart.Add(mailAttachment);

					// now set the multipart/mixed as the message body
					message.Body = multipart;

					return message.ToString();
				}
			}

			return message.ToString();
		}
	}
}