﻿namespace ITBApp.Interfaces
{
	public interface IAppMethods
	{
		void CloseApp();

        void RestartApp();

		void AddPhoneContact(string name, string phoneNumber, string phoneLabel);

		void showQuickMessage(string message);

		string getAppVersion();

		void RegisterOnNotificationHubAsync(string token, string tag);

		void UnregisterFromHub(string token);

        int GetHeightInPixels();

        int GetHeight();

        int GetWidth();
	}
}

