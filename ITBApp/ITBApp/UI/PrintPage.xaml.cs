﻿using ITBApp.Resources.i18n;
using ITBApp.Enums;
using Xamarin.Forms;

namespace ITBApp.UI
{
    public partial class PrintPage : ContentPage
    {
        public MediaType SelectedFileMediaType { get; set; }
        public MediaSubType SelectedFileMediaSubType { get; set; }
        public byte[] SelectedFileBytes { get; set; }
        public string SelectedFileFilename { get; set; }
        private int Copies = 1;
        private bool ColorPrint = false;

        public PrintPage()
        {
            InitializeComponent();
        }

        public void EnablePrintControls()
        {
            gridFilePicker.IsVisible = false;
            slSelectedFile.IsVisible = true;

            lblImprimir.TextColor = Color.FromHex("#77D5CD");
            imgPrintButton.Source = "print_button_enabled.png";
            lblCopias.TextColor = Color.FromHex("#77D5CD");
            imgMoreCopies.Source = "print_more_copies_enabled.png";
            lblSettings.TextColor = Color.FromHex("#77D5CD");
            imgCountCircle.Source = "print_empty_count.png";
            lblCopies.Text = Copies.ToString();

            imgBW.Source = "print_bw_enabled.png";

            // Print
            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += async(sender, e) =>
            {
                string printType = ColorPrint? "color":"blanco y negro";
                string plural = Copies > 1 ? "s" : string.Empty;
                if (await DisplayAlert("Imprimir", "¿ Deseas imprimir " + lblCopies.Text + " copia" + plural + " en " + printType + " ?", "Aceptar", "Cancelar"))
                {
                    slPrinting.IsVisible = true;
                    lblSending.Text = Translate.print_sending_message.Replace("{0}", "1").Replace("{1}", Copies.ToString());

                    EmailPrintService printService = new EmailPrintService();
                    bool resultado = false;
                    if (ColorPrint)
                    {
                        for (int iteration = 0; iteration < Copies; iteration++)
                        {
                            lblSending.Text = Translate.print_sending_message.Replace("{0}", (iteration+1).ToString()).Replace("{1}", Copies.ToString());
                            resultado = await printService.SendColorEmail("me", SelectedFileBytes, SelectedFileFilename, SelectedFileMediaType, SelectedFileMediaSubType, null);
                        }
                    }
                    else
                    {
                        for (int iteration = 0; iteration < Copies; iteration++)
                        {
                            lblSending.Text = Translate.print_sending_message.Replace("{0}", (iteration + 1).ToString()).Replace("{1}", Copies.ToString());
                            resultado = await printService.SendBWEmail("me", SelectedFileBytes, SelectedFileFilename, SelectedFileMediaType, SelectedFileMediaSubType, null);
                        }
                    }
                    slPrinting.IsVisible = false;
                    if (resultado)
                    {
                        await DisplayAlert("Imprimir", "¡Listo! Retirá el trabajo en el CIR", "Aceptar");
                        var localNavPage = new NavigationPage(new PrintPage());
                        localNavPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                        localNavPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                        App.MasterDetailPage.Detail = localNavPage;
                    }
                    else await DisplayAlert("Imprimir", "No se pudo enviar el trabajo a imprimir", "Aceptar");
                }
            };
            imgPrintButton.GestureRecognizers.Add(tapGestureRecognizer);

            // Select Color Print
            tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += (sender, e) =>
            {
                if (!ColorPrint)
                {
                    imgBW.Source = "print_type_disabled.png";
                    imgColor.Source = "print_color_enabled.png";
                    ColorPrint = true;
                }
            };
            imgColor.GestureRecognizers.Add(tapGestureRecognizer);

            // Select BW Print
            tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += (sender, e) =>
            {
                if (ColorPrint)
                {
                    imgBW.Source = "print_bw_enabled.png";
                    imgColor.Source = "print_type_disabled.png";
                    ColorPrint = false;
                }
            };
            imgBW.GestureRecognizers.Add(tapGestureRecognizer);

            // Remove file
            tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += (sender, e) =>
            {
                var localNavPage = new NavigationPage(new PrintPage());
                localNavPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                localNavPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                App.MasterDetailPage.Detail = localNavPage;
            };
            imgRemoveFile.GestureRecognizers.Add(tapGestureRecognizer);
            
            // Select more copies
            tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += (sender, e) =>
            {
                if (Copies < 10)
                {
                    Copies++;
                    imgLessCopies.Source = "print_less_copies_enabled.png";
                    lblCopies.Text = Copies.ToString();
                    if (Copies == 10) imgMoreCopies.Source = "print_more_copies_disabled.png";
                }
            };
            imgMoreCopies.GestureRecognizers.Add(tapGestureRecognizer);

            // Select less copies
            tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += (sender, e) =>
            {
                if (Copies > 1)
                {
                    Copies--;
                    imgMoreCopies.Source = "print_more_copies_enabled.png";
                    lblCopies.Text = Copies.ToString();
                    if (Copies == 1) imgLessCopies.Source = "print_less_copies_disabled.png";
                }
            };
            imgLessCopies.GestureRecognizers.Add(tapGestureRecognizer);

            entryFilename.Text = SelectedFileFilename;
        }
    }
}
