using System;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using Xamarin.Forms;
using ITBApp.Droid.Implementations;

[assembly: ExportRenderer(typeof(Entry), typeof(OpenSansEntryRenderer))]
namespace ITBApp.Droid.Implementations
{
    class OpenSansEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            Control.Background = new Android.Graphics.Drawables.ColorDrawable(Android.Graphics.Color.Transparent);
			// Con esto quito la linea que sale en la parte de abajo
			Control?.SetBackgroundColor(Android.Graphics.Color.Transparent);

            if (!string.IsNullOrEmpty(e.NewElement?.FontFamily))
            {
                try
                {
                    var font = Typeface.CreateFromAsset(Forms.Context.ApplicationContext.Assets, e.NewElement.FontFamily + ".ttf");
                    Control.Typeface = font;
                }
                catch (Exception)
                {
                    // An exception means that the custom font wasn't found.
                    // Typeface.CreateFromAsset throws an exception when it didn't find a matching font.
                    // When it isn't found we simply do nothing, meaning it reverts back to default.
                }
            }
        }
    }
}