﻿using System;
using System.Threading.Tasks;
using ITBApp.Helpers;
using ITBApp.Resources.i18n;
using Xamarin.Forms;

namespace ITBApp.UI
{
	public partial class ContactsPage : ContentPage
	{
		public ContactsPage()
		{
			InitializeComponent();

			AddPhoneLinkToView(imgITBAPhone, "1121504800");
			AddPhoneLinkToView(imgAlumnosPhone, "1121504880");
			AddPhoneLinkToView(imgInformesPhone, "1121504800");
			AddPhoneLinkToView(lblAyudaPhone, "1121504800");
			AddPhoneLinkToView(imgPosgradoPhone, "1121504840");
			AddPhoneLinkToView(imgGuardiaCentralPhone, "1121504802");
			AddPhoneLinkToView(imgGuardiaPosgradoPhone, "1121504803");
			AddPhoneLinkToView(imgSDTPhone, "1121504804");
			AddPhoneLinkToView(imgAGITBAPhone, "1121504892");
			AddPhoneLinkToView(lblFacturacionPhone, "1121504800");
			AddPhoneLinkToView(lblBibliotecaPhone, "1121504800");
			AddMailLinkToView(imgAGITBAMail, "agitba@itba.edu.ar");
			AddMailLinkToView(imgAyudaMail, "ayuda@itba.edu.ar");
			AddMailLinkToView(imgAlumnosMail, "atenciondealumnos@itba.edu.ar");
			AddMailLinkToView(imgInformesMail, "informes@itba.edu.ar");
			AddMailLinkToView(imgPosgradoMail, "postgrado@itba.edu.ar");
			AddMailLinkToView(imgFacturacionMail, "facturacion@itba.edu.ar");
			AddMailLinkToView(imgBibliotecaMail, "biblio@itba.edu.ar");
		}

		public void AddPhoneLinkToView(View view, string phoneNumber)
		{
			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += async (s, e) =>
			{
				if (await this.DisplayAlert(
                     Translate.itba_contacts,
					 phoneNumber,
					 Translate.call,
                     Translate.Cancel))
				{
					Device.OpenUri(new Uri("tel:" + phoneNumber));
				}

			};
			view.GestureRecognizers.Add(tapGestureRecognizer);
		}

		public void AddMailLinkToView(View view, string emailAddress)
		{
			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (s, e) =>
			{
				Device.OpenUri(new Uri("mailto:" + emailAddress));
			};
			view.GestureRecognizers.Add(tapGestureRecognizer);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			this.BindingContext = null;
			this.Content = null;

            App.HardwareBackPressed = null;
		}

        protected override void OnAppearing()
        {
        	base.OnAppearing();

        	App.HardwareBackPressed = () =>
        	{
        		MainMenuItem item = MainMenuItem.HomeItem;
        		App.CurrentPage = item.Id;
        		item.NavigationPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
        		item.NavigationPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
        		App.MasterDetailPage.Detail = item.NavigationPage;
        		return Task.FromResult<bool?>(false);
        	};
        }
	}
}
