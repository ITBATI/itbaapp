using System;
using System.Linq;
using System.Threading.Tasks;
using ITBApp.Helpers;
using ITBApp.Model.DataModel;
using ITBApp.Interfaces;
using ITBApp.Resources.i18n;
using ITBApp.ViewModels;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.UI
{
	public partial class NotificationsPage : ContentPage
	{
		private static bool isTapped = false;

		public NotificationsPage()
		{
			InitializeComponent();
			Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();

			((BaseModel)BindingContext).Initialize(this);

			ConfigureEventListeners();
		}

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}

		public async static void NavigateToNotificationDetail(NotificationsPage instance, object notificationData)
		{
            if (Device.RuntimePlatform == Device.UWP)
            {
                NotificationData notification = (NotificationData)notificationData;
                NavigationPage detailPage = new NavigationPage(new NotificationDetail(notification));
                detailPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                detailPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

                App.MasterDetailPage.Detail = detailPage;
            }
            else if(!isTapped)
			{
				isTapped = true;
				NotificationData notification = (NotificationData)notificationData;

				await instance.Navigation.PushAsync(new NotificationDetail(notification));
				isTapped = false;
			}
		}


		void ConfigureEventListeners()
		{

			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
				if (BindingContext != null && App.CurrentPage == PageId.NOTIFICATIONS)
				{
					cvWarningUpdate.IsVisible = !args.IsConnected;
					if (args.IsConnected)
					{
						if (lvNotifications.ItemsSource == null || !lvNotifications.ItemsSource.Cast<object>().Any())
						{
							((NotificationsViewModel)BindingContext).LoadDataAsync().ConfigureAwait(false);
						}
					}
				}
			};
		}

		public void InitConnectivityMessage()
		{

			Device.BeginInvokeOnMainThread(() =>
			{
				cvWarningUpdate.IsVisible = !CrossConnectivity.Current.IsConnected;
			});

		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			if (!isTapped)
			{
				this.BindingContext = null;
				this.Content = null;
			}
            App.HardwareBackPressed = null;
		}

        protected override void OnAppearing()
        {
        	base.OnAppearing();

        	App.HardwareBackPressed = () =>
        	{
        		MainMenuItem item = MainMenuItem.HomeItem;
        		App.CurrentPage = item.Id;
        		item.NavigationPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
        		item.NavigationPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
        		App.MasterDetailPage.Detail = item.NavigationPage;
        		return Task.FromResult<bool?>(false);
        	};
        }

	}
}