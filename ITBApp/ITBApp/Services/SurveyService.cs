﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ITBApp.Model.DataModel.Survey;
using ITBApp.Model.Handlers;
using ITBApp.Resources.i18n;
using ITBApp.Util.Helpers;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using System.Linq;
using Newtonsoft.Json;
using ITBApp.Helpers;
using ITBApp.Handlers;

namespace ITBApp.Services
{
	public class SurveyService
	{
		private TokenHolder tokenHolder = new TokenHolder();

        public async Task<List<SurveyData>> GetUnansweredSurveysAsync()
        {
			if (!CrossConnectivity.Current.IsConnected)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}

			try
			{
				string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
                //Filtrar encuestas distinta a "Como te sentis hoy"
                return GetSurveyUnansweredAsync(token).Result.OrderBy(x => x.From).ToList();
			}
			catch (TokenExpiredException)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}
        }

		public async Task<SurveyData> GetUnansweredSurveyAsync()
		{
			if (!CrossConnectivity.Current.IsConnected)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}

			try
			{
				string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
				//Filtrar encuestas distinta a "Como te sentis hoy"
				var surveyData = GetSurveyUnansweredAsync(token).Result.FirstOrDefault();

				//No hay encuestas activas o fue completada.
				if (surveyData == null)
					return null;


				surveyData.Questions = GetActiveSurveyQuestionsAsync(token, surveyData.Id).Result;

				return surveyData;

			}
			catch (TokenExpiredException)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}
		}

		public async Task<SurveyData> GetUnansweredSurveyByIdAsync(int id)
		{
			if (!CrossConnectivity.Current.IsConnected)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}

			try
			{
				string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
                //Filtrar encuestas distinta a "Como te sentis hoy"
                var surveyData = GetSurveyUnansweredAsync(token).Result.FirstOrDefault(x => x.Id == id);

				//No hay encuestas activas o fue completada.
				if (surveyData == null)
					return null;


				surveyData.Questions = GetActiveSurveyQuestionsAsync(token, surveyData.Id).Result;

				return surveyData;

			}
			catch (TokenExpiredException)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}
		}

		public async Task<List<SurveyData>> GetListUnansweredSurveyAsync()
		{
			if (!CrossConnectivity.Current.IsConnected)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}

			try
			{
				string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
				List<SurveyData> surveyData = GetSurveyUnansweredAsync(token).Result;
                return surveyData.OrderBy(x => x.From).ToList();

			}
			catch (TokenExpiredException)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}
		}

		public async Task<List<SurveyData>> GetActiveSurveysAsync()
		{
			if (!CrossConnectivity.Current.IsConnected)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}

			try
			{
				string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
				List<SurveyData> surveyData = GetSurveyActiveAsync(token).Result;
				return surveyData;

			}
			catch (TokenExpiredException)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}
		}

        /*
		public async Task<string> GetHowDoYouFillTodaySurveyOptionsAsync()
		{
			if (!CrossConnectivity.Current.IsConnected)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}

			try
			{
				string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
				return GetHowDoYouFillTodaySurveyOptions(token).Result;
			}
			catch (TokenExpiredException)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}
		}
		*/

		public async Task<List<SurveyResultData>> GetSurveyResultsAsync(int surveyId)
		{
			if (!CrossConnectivity.Current.IsConnected)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}

			try
			{
				string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
				return GetSurveyResultAsync(token, surveyId).Result;
			}
			catch (TokenExpiredException)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}
		}

		public async Task<bool> PostSurveyResults(SurveyPostData surveyData)
		{
			if (!CrossConnectivity.Current.IsConnected)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}

			try
			{
				var content = JsonConvert.SerializeObject(surveyData);
				var token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);

				await HttpUtility
					.MakePostCallAsync(String.Format("{0}{1}", AppSettings.SurveyPostResponseURL, token), content)
					.ConfigureAwait(false);


				return true;

			}
			catch (ServerRequestException)
			{
				throw new ServerRequestException("No se pudo enviar tu opinión, volvé a intentar más tarde");
			}
			catch (TokenExpiredException)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}
		}

		private async Task<List<SurveyData>> GetSurveyActiveAsync(string token)
		{
			string stringResponse;

			try
			{
				stringResponse = await HttpUtility
					.MakeGetCallAsync(String.Format("{0}{1}", AppSettings.SurveyActiveURL, token))
					.ConfigureAwait(false);
			}
			catch
			{
				throw new ServerRequestException("Error de conexión al servicio de encuesta.");
			}

			try
			{
				var result = new List<SurveyData>();
				JToken jsonResponse = JToken.Parse(stringResponse);

				if (jsonResponse == null || stringResponse == "[]")
					return result;

				foreach (var item in jsonResponse)
				{
					var surveyItem = new SurveyData();

					surveyItem.Id = int.Parse(item["id"].ToString());
					surveyItem.From = DateTime.Parse(item["dateFrom"].ToString());
					surveyItem.To = DateTime.Parse(item["dateTo"].ToString());
					surveyItem.Comments = item["comments"].ToString();
					surveyItem.Description = item["description"].ToString();

					result.Add(surveyItem);
				}

				return result;
			}
			catch
			{
				throw new ServerRequestException("Error al procesar los datos de las encuestas activas.");
			}
		}


		private async Task<List<SurveyData>> GetSurveyUnansweredAsync(string token)
		{
			string stringResponse;

			try
			{
				stringResponse = await HttpUtility
					.MakeGetCallAsync(String.Format("{0}{1}", AppSettings.SurveyUnansweredURL, token))
					.ConfigureAwait(false);
			}
			catch
			{
				throw new ServerRequestException("Error de conexión al servicio de encuesta.");
			}

			try
			{
				var result = new List<SurveyData>();
				JToken jsonResponse = JToken.Parse(stringResponse);

				if (jsonResponse == null || stringResponse == "[]")
					return result;

				foreach (var item in jsonResponse)
				{
					var surveyItem = new SurveyData();

					surveyItem.Id = int.Parse(item["id"].ToString());
					surveyItem.From = DateTime.Parse(item["dateFrom"].ToString());
					surveyItem.To = DateTime.Parse(item["dateTo"].ToString());
					surveyItem.Comments = item["comments"].ToString();
					surveyItem.Description = item["description"].ToString();

					result.Add(surveyItem);
				}

				return result;
			}
			catch
			{
				throw new ServerRequestException("Error al procesar los datos de las encuestas activas no respondidas por el usuario.");
			}
		}

        /*
		private async Task<string> GetHowDoYouFillTodaySurveyOptions(string token)
		{
			string stringResponse;

			try
			{
				stringResponse = await HttpUtility
					.MakeGetCallAsync(String.Format("{0}{1}", AppSettings.HdyftOptionsURL, token))
					.ConfigureAwait(false);
			}
			catch
			{
				throw new ServerRequestException("Error de conexión al servicio de HDYFT.");
			}

			try
			{
				JToken options = JToken.Parse(stringResponse);
				return stringResponse;
			}
			catch
			{
				throw new ServerRequestException("Error al procesar los datos de como te sentis hoy.");
			}
		}
		*/

		private async Task<List<SurveyResultData>> GetSurveyResultAsync(string token, long surveyId)
		{
			string stringResponse;

			try
			{
				stringResponse = await HttpUtility
					.MakeGetCallAsync(String.Format("{0}{1}/{2}", AppSettings.SurveyResultsURL, token, surveyId))
					.ConfigureAwait(false);
			}
			catch
			{
				throw new ServerRequestException("Error de conexión al servicio de encuesta.");
			}


			try
			{
				var surveyResults = new List<SurveyResultData>();

				JToken resutls = JToken.Parse(stringResponse);
				foreach (var item in resutls)
				{
					var surveyResult = new SurveyResultData();
					surveyResult.Count = int.Parse(item["result"].ToString());
					surveyResult.QuestionOption = item["questionOption"].ToString();
					surveyResult.Question = item["question"].ToString();

					surveyResults.Add(surveyResult);
				}

				double total = Convert.ToDouble(surveyResults.Sum(x => x.Count));

				foreach (var item in surveyResults)
				{
					item.Percentage = (( Convert.ToDouble(item.Count) * 100) / total);
				}

				surveyResults = surveyResults.OrderByDescending(x => x.Percentage).ToList();

				return surveyResults;

			}
			catch
			{
				throw new ServerRequestException("Error al procesar los resultados de la encuesta.");
			}
		}

		private async Task<List<SurveyQuestionData>> GetActiveSurveyQuestionsAsync(string token, long surveyId)
		{
			string stringResponse;

			try
			{
				stringResponse = await HttpUtility
					.MakeGetCallAsync(String.Format("{0}{1}/{2}", AppSettings.SurveyActiveQuestionsURL, token, surveyId))
					.ConfigureAwait(false);
			}
			catch
			{
				throw new ServerRequestException("Error de conexión al servicio de encuesta.");
			}

			try
			{
				var surveyQuestions = new List<SurveyQuestionData>();
				JToken questions = JToken.Parse(stringResponse);

				foreach (var question in questions)
				{
					var surveyQuestion = new SurveyQuestionData()
					{
						Id = long.Parse(question["id"].ToString()),
						Question = question["description"].ToString(),
						Type = question["type"].ToString() == "String" ? Model.QuestionType.String : Model.QuestionType.Choice
					};

					var surveyQuestionChoices = new List<SurveryQuestionChoiceData>();
					surveyQuestion.Choices = surveyQuestionChoices;
					surveyQuestions.Add(surveyQuestion);

					int choiceIndex = 1;

					foreach (var item in question["choices"])
					{
						surveyQuestionChoices.Add(new SurveryQuestionChoiceData()
						{
							Id = long.Parse(item["id"].ToString()),
							Title = String.Format("OPCION {0}", choiceIndex),
							Description = item["stringValue"].ToString(),
							IsWritingChoice = surveyQuestion.Type == Model.QuestionType.String
						});

						choiceIndex++;
					}

					surveyQuestion.Choices = surveyQuestion.Choices.OrderBy(x => x.Id).ToList();
				}

				surveyQuestions = surveyQuestions.OrderBy(x => x.Id).ToList();

				return surveyQuestions;
			}
			catch
			{
				throw new ServerRequestException("Error al procesar los datos de las encuestas activas.");
			}
		}
	}
}
