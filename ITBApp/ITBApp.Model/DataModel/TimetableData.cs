﻿using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel
{
	[DataContract]
	public class TimetableData
	{
		[DataMember]
		public string StartTime { get; set; }

		[DataMember]
		public string EndTime { get; set; }

		[DataMember]
		public int Day { get; set; }

		[DataMember]
		public string Classroom { get; set; }

		[DataMember]
		public string Location { get; set; }

        public int FontSizeLocation { get; set; }

		public TimetableData Clone()
		{
			return new TimetableData()
			{
				StartTime = this.StartTime,
				EndTime = this.EndTime,
				Day = this.Day,
				Classroom = this.Classroom,
				Location = this.Location
			};
		}

	}
}
