﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ITBApp.Handlers;
using ITBApp.Helpers;
using ITBApp.Model.DataModel;
using ITBApp.Model.Handlers;
using ITBApp.Resources.i18n;
using ITBApp.Util.Helpers;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.Services
{
    public class AcademicHistoryService
    {
        private TokenHolder TokenHolder = new TokenHolder();

        public AcademicHistoryService()
        {
        }

        public async Task DeleteCachedDataAsync()
        {
            Application.Current.Properties.Remove(ServiceConstants.ACADEMIC_HISTORY_DATA);
            await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
        }

        public async Task<AcademicHistoryData> GetAcademicHistoryAsync()
        {
            AcademicHistoryData academicData = GetSavedAcademicHistoryData();

            if (academicData == null)
            {
				if (!CrossConnectivity.Current.IsConnected)
				{
					throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
				}

				try
				{
					string token = await TokenHolder.GetTokenAsync().ConfigureAwait(false);

					try
					{
						var stringResponse = await HttpUtility
							.MakeGetCallAsync(String.Format("{0}{1}", AppSettings.HistoricAcademicURL, token))
							.ConfigureAwait(false);

                        academicData = ParseResponse(stringResponse);
                        academicData.Expiration = DateTime.Now.AddMinutes(ServiceConstants.ACADEMIC_HISTORY_EXPIRATION_IN_MINUTES);

                        SaveAcademicHistoryData(academicData);
					}
					catch
					{
						throw new ServerRequestException("Error de conexión al servicio de historial academico.");
					}

				}
				catch (TokenExpiredException)
				{
					throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
				}
                                
            }

            return academicData;
        }

        private AcademicHistoryData ParseResponse(string response)
        {
            var result = new AcademicHistoryData()
            {
                Courses = new List<CourseEntryData>()
            };

            try
            {
                JObject jsonResponse = JObject.Parse(response);
                var courses = jsonResponse["courseEntries"];

                foreach (var course in courses["courseEntry"])
                {
                    var courseEntryData = new CourseEntryData()
                    {
                        Year = (string)course["year"],
                        Period = (string)course["period"],
                        CourseId = (string)course["courseId"],
                        Code = (string)course["code"],
                        Name = (string)course["name"],
                        CommissionId = (string)course["commissionId"],
                        CommissionName = (string)course["commissionName"],
                        Mark = (string)course["mark"],
                        Status = (string)course["status"],
                        Credits = (string)course["credits"]
                    };

                    if (course["examResults"] != null)
                    {
                        var exams = new List<ExamenResultCourseEntryData>();

                        if (course["examResults"].Type == JTokenType.Object)
                        {
							var exam = new ExamenResultCourseEntryData()
							{
                                Date = course["examResults"]["date"] != null ? 
                                    String.Format("{0:dd-MM-yyyy}", DateTime.Parse((string)course["examResults"]["date"])) : string.Empty,
								FinalMark = (string)course["examResults"]["finalMark"],
                                ActNumber = (string)course["examResults"]["actNumber"]
							};

                            exams.Add(exam);
                            
                        }
                        else
                        {
							foreach (var item in course["examResults"])
							{
								var exam = new ExamenResultCourseEntryData()
								{
                                    Date = item["date"] != null ? 
                                        String.Format("{0:dd-MM-yyyy}", DateTime.Parse((string)item["date"])) : string.Empty,
									FinalMark = (string)item["finalMark"]
								};

								exams.Add(exam);
							}
                        }

                        courseEntryData.ExamResults = exams;
                    }

                    result.Courses.Add(courseEntryData);
                }

            }
            catch
            {
                throw new ServerRequestException("Error al procesar los datos de historial academico");
            }

            return result;
        }


		private AcademicHistoryData GetSavedAcademicHistoryData()
		{
            if (Application.Current.Properties.ContainsKey(ServiceConstants.ACADEMIC_HISTORY_DATA))
			{
				string json = (string)Application.Current.Properties[ServiceConstants.ACADEMIC_HISTORY_DATA];
                AcademicHistoryData data = (AcademicHistoryData)SerializerHelper.Deserialize(json, (new AcademicHistoryData()).GetType());

				if (data.Expiration > DateTime.Now || !CrossConnectivity.Current.IsConnected)
				{
					return data;
				}
			}

			return null;
		}

        private async void SaveAcademicHistoryData(AcademicHistoryData data)
        {
			string json = SerializerHelper.Serialize(data);
            Application.Current.Properties[ServiceConstants.ACADEMIC_HISTORY_DATA] = json;
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
        }
    }
}
