﻿using ITBApp.Interfaces;
using Xamarin.Forms;

namespace ITBApp.Helpers
{
    public class UIHelper
	{
		public static bool HasSmallResolution()
		{
			var width = DependencyService.Get<IAppMethods>().GetWidth();
			var height = DependencyService.Get<IAppMethods>().GetHeight();

            if (Xamarin.Forms.Device.RuntimePlatform == Device.Android)
            {
               return width < 360 && height < 640; 
            }

			return width < 750 && height < 1334;
		}

		public static bool HasMediumResolution()
		{
			var width = DependencyService.Get<IAppMethods>().GetWidth();
			var height = DependencyService.Get<IAppMethods>().GetHeight();

            if (Xamarin.Forms.Device.RuntimePlatform == Device.Android)
            {
                return (width >= 360 && height >= 640) && (width < 800 && height < 1232); 
            }

            return (width >= 750 && height >= 1334) && (width < 1536 && height < 2048);
		}

		public static bool HasHightResoultion()
		{
			return !HasSmallResolution() && !HasMediumResolution();
		}
	}
}
