﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ITBApp.Model.DataModel.Survey;

namespace ITBApp.Model.DataModel
{
	[DataContract]
	[KnownType(typeof(ExamenResultCourseEntryData))]
    public class CourseEntryData
    {
		/// <summary>
		/// Gets or sets the year.
		/// </summary>
		/// <value>The year.</value>
		[DataMember]
		public string Year { get; set; }

        /// <summary>
        /// Gets or sets the period.
        /// </summary>
        /// <value>The period.</value>
        [DataMember] 
        public string Period { get; set; }

        /// <summary>
        /// Gets or sets the course identifier.
        /// </summary>
        /// <value>The course identifier.</value>
        [DataMember] 
        public string CourseId { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        [DataMember] 
        public string Code { get; set; }

		/// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
		[DataMember] 
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the commission identifier.
        /// </summary>
        /// <value>The commission identifier.</value>
        [DataMember] 
        public string CommissionId { get; set; }

        /// <summary>
        /// Gets or sets the name of the commission.
        /// </summary>
        /// <value>The name of the commission.</value>
        [DataMember] 
        public string CommissionName { get; set; }

        /// <summary>
        /// Gets or sets the mark.
        /// </summary>
        /// <value>The mark.</value>
        [DataMember] 
        public string Mark { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        [DataMember] 
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the credits.
        /// </summary>
        /// <value>The credits.</value>
        [DataMember] 
        public string Credits { get; set; }

        /// <summary>
        /// Gets or sets the exam results.
        /// </summary>
        /// <value>The exam results.</value>
        [DataMember] 
        public List<ExamenResultCourseEntryData> ExamResults { get; set; }
    }
}
