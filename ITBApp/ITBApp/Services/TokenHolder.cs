﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Auth;
using Xamarin.Forms;
using System.Collections.Generic;
using ITBApp.Model;
using ITBApp.Interfaces;
using Plugin.Connectivity;
using ITBApp.Handlers;

namespace ITBApp.Services
{

	public class TokenHolder
	{
        private Object thisLock = new Object();

        public async Task<string> GetTokenForPrintAsync()
        {
			if (IsTokenExpired())
			{
                string token = DependencyService.Get<IAppGroupSettings>().Get(ServiceConstants.REFRESH_TOKEN).ToString();

				IDictionary<string, string> newAccountProperties = await RefreshTokenAsync(token).ConfigureAwait(false);

                return newAccountProperties[ServiceConstants.REFRESH_TOKEN];
			}

            return DependencyService.Get<IAppGroupSettings>().Get(ServiceConstants.ACCESS_TOKEN).ToString();
        }


		/// <exception cref="TokenExpiredException">Si no es posible renovar el token por falta de conectividad u otros motivos</exception>
		public async Task<string> GetTokenAsync()
		{
            Account account = AccountStore.Create().FindAccountsForService(AppSettings.AppSignInName).FirstOrDefault();

            if (account != null)
            {
                if (IsTokenExpired())
                {
                    string token = account.Properties[ServiceConstants.REFRESH_TOKEN];

                    IDictionary<string, string> newAccountProperties = await RefreshTokenAsync(token).ConfigureAwait(false);

                    SetExpirationInSeconds(int.Parse(newAccountProperties[ServiceConstants.EXPIRES_IN]));
                    account.Properties[ServiceConstants.ACCESS_TOKEN] = newAccountProperties[ServiceConstants.ACCESS_TOKEN];

                    if (newAccountProperties.ContainsKey(ServiceConstants.REFRESH_TOKEN))
                    {
                        account.Properties[ServiceConstants.REFRESH_TOKEN] = newAccountProperties[ServiceConstants.REFRESH_TOKEN];
                    }

                    AccountStore.Create().Save(account, AppSettings.AppSignInName);

                    // Si se refresco el Token, lo guardo para que lo use la impresion
                    DependencyService.Get<IAppGroupSettings>().Set(ServiceConstants.ACCESS_TOKEN,
                        account.Properties[ServiceConstants.ACCESS_TOKEN]);
                    DependencyService.Get<IAppGroupSettings>().Set(ServiceConstants.ACCESS_TOKEN_EXPIRATION,
                       DependencyService.Get<IAppGroupSettings>().Get("tokenExpiration").ToString());
                    DependencyService.Get<IAppGroupSettings>().Set(ServiceConstants.REFRESH_TOKEN,
                        account.Properties[ServiceConstants.REFRESH_TOKEN]);
                }

                return account.Properties[ServiceConstants.ACCESS_TOKEN];
            }

			return null;
		}


		public static void SetExpiration(DateTime expirationDate)
		{
            DependencyService.Get<IAppGroupSettings>().Set("tokenExpiration", expirationDate.ToString());
		}


		public static void SetExpirationInSeconds(int seconds)
		{
			SetExpiration(DateTime.Now.AddSeconds(seconds));
		}

		public static bool IsTokenExpired()
		{
            var tokenExpiration = DependencyService.Get<IAppGroupSettings>().Get("tokenExpiration");

			if (tokenExpiration != null)
			{
                DateTime expiration = DateTime.Parse(tokenExpiration.ToString());
                return expiration.Subtract(DateTime.Now) <= new TimeSpan(0);
			}

            return true;
		}

        private const string TokenExpiredMessage = "No se puede refrescar el token porque no hay conectividad";

		private async Task<IDictionary<string, string>> RefreshTokenAsync(string refreshToken)
		{
			if (!CrossConnectivity.Current.IsConnected)
			{
				throw new TokenExpiredException(TokenExpiredMessage);
			}

			LoginService service = new LoginService();
			var newAccountProperties = await service.RequestRefreshTokenAsync(refreshToken)
																 .ConfigureAwait(false);

			return newAccountProperties;
		}

	}

}
