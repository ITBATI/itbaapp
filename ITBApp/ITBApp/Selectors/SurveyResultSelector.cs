﻿using Xamarin.Forms;

namespace ITBApp.Selectors
{
	public class SurveyResultSelector : DataTemplateSelector
	{
		public DataTemplate ResultTemplate { get; set; }

		protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
		{
			return ResultTemplate;
		}
	}
}
