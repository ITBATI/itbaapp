﻿//-----------------------------------------------------------------------
// <copyright file="AccountBalanceViewModel.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace ITBApp.ViewModels
{
    using System;
    using System.Collections.ObjectModel;
    using System.Threading.Tasks;
    using ITBApp.Helpers;
    using ITBApp.Model.DataModel;
    using ITBApp.Model.Handlers;
    using ITBApp.Interfaces;
    using ITBApp.Resources.i18n;
    using ITBApp.Services;
    using Xamarin.Forms;

    /// <summary>
    /// Account Balance Context View Model
    /// </summary>
    internal class AccountBalanceViewModel : BaseModel
    {
        /// <summary>
        /// Payment list
        /// </summary>
        private ObservableCollection<PaymentData> payments = new ObservableCollection<PaymentData>();

        /// <summary>
        /// Account Balance Service instance
        /// </summary>
        private AccountBalanceService service = new AccountBalanceService();

        /// <summary>
        /// Current balance
        /// </summary>
        private string currentBalance;

        /// <summary>
        /// The show balance.
        /// </summary>
        private bool showBalance;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountBalanceViewModel"/> class
        /// </summary>
        public AccountBalanceViewModel()
        {
            Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
        }

        /// <summary>
        /// Gets the payments
        /// </summary>
        public ObservableCollection<PaymentData> Payments
        {
            get 
            { 
                return this.payments; 
            }

            private set
            {
                this.payments = value;
                this.OnPropertyChanged("Payments");
            }
        }

        /// <summary>
        /// Gets or sets the current balance
        /// </summary>
        public string CurrentBalance
        {
            get
            {
                return this.currentBalance;
            }

            set
            {
                this.currentBalance = value;
                this.OnPropertyChanged("CurrentBalance");
            }
        }

        /// <summary>
        /// Gets or sets the show balance.
        /// </summary>
        /// <value>The show balance.</value>
        public bool ShowBalance
		{
			get
			{
                return this.showBalance;
			}

			set
			{
				this.showBalance = value;
				this.OnPropertyChanged("ShowBalance");
			}
		}

        /// <summary>
        /// Fills view model data
        /// </summary>
        /// <returns>
        /// Error message
        /// </returns>
        public override async Task<string> LoadDataAsync()
        {
            try
            {
                this.IsLoading = true;
                this.ErrorMessage = null;
                this.ShowBalance = false;

                AccountBalanceData data = await this.service.GetAccountBalanceAsync().ConfigureAwait(false);

                this.Payments = new ObservableCollection<PaymentData>(data.Payments);

                // Agrego el signo $
                foreach (PaymentData pay in this.Payments)
                {
                    pay.Amount = "$ " + pay.Amount;
                }

                if (data.Payments.Count > 0)
                {
                    this.CurrentBalance = "$ " + data.Payments[0].Balance;
                }
            }
            catch (ServerRequestException ex)
            {
                this.ErrorMessage = ex.Message;
            }
            catch (Exception exception)
            {
                this.ErrorMessage = Translate.accountbalancepage_error_text;
            }

            this.IsLoading = false;
            this.ShowBalance = true;

            return this.ErrorMessage;
        }

        /// <summary>
        /// Cleans view model cache
        /// </summary>
        /// <returns>
        /// No value is returned
        /// </returns>
        public override async Task DeleteCache()
        {
            await this.service.DeleteCachedData();
        }

        /// <summary>
        /// Event when Account Balance page is open or maximized
        /// </summary>
        /// <param name="sender">
        /// Object that sends the event
        /// </param>
        /// <param name="eventArgs">
        /// Event parameters
        /// </param>
        protected override async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
        {
            App.CurrentPage = PageId.ACCOUNT_BALANCE;

            if (this.payments.Count == 0)
            {
                await this.LoadDataAsync().ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Event when Account Balance page is closed or minimized
        /// </summary>
        /// <param name="sender">
        /// Object that sends the event
        /// </param>
        /// <param name="eventArgs">
        /// Event parameters
        /// </param>
        protected override void CurrentPageOnDisappearing(object sender, EventArgs eventArgs)
        {
            App.CurrentPage = PageId.USER_PROFILE;
        }
    }
}
