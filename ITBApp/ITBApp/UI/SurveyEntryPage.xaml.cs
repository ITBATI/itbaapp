﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ITBApp.Helpers;
using ITBApp.Interfaces;
using ITBApp.Resources.i18n;
using ITBApp.ViewModels;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp
{
    public partial class SurveyEntryPage : ContentPage
    {
        private static bool isTapped = false;

        public SurveyEntryPage()
        {
			InitializeComponent();
			Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
			((BaseModel)BindingContext).Initialize(this);
			ConfigureEventListeners();

            Task.Run(async() => 
            {
                await ((SurveyEntryViewModel)BindingContext).LoadDataAsync();
            });

            this.Appearing += Handle_Appearing;

        }

        void Handle_Appearing(object sender, EventArgs e)
        {
			Task.Run(async () =>
			{
                ((SurveyEntryViewModel)BindingContext).IsLoading = false;
				await ((SurveyEntryViewModel)BindingContext).LoadDataAsync();
			});

		}

        void ConfigureEventListeners()
		{
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
                if (BindingContext != null && App.CurrentPage == PageId.SURVEY && args.IsConnected)
				{
                    ((SurveyEntryViewModel)BindingContext).LoadDataAsync().ConfigureAwait(false);
				}
			};
		}

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}        	

        async void Handle_Tapped(object sender, System.EventArgs e)
        {
            if (!isTapped)
            {
                ((SurveyEntryViewModel)BindingContext).IsLoading = true;

                isTapped = true;
				int id = (int)((TappedEventArgs)e).Parameter;
				bool hasResult = ((SurveyEntryViewModel)BindingContext).HasResultSurvey(id);
                if (Device.RuntimePlatform == Device.UWP)
                {
					NavigationPage surveyPage = new NavigationPage(new SurveyPage(id, hasResult));
					surveyPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
					surveyPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

					App.MasterDetailPage.Detail = surveyPage;
                }
                else
                {
                    ((SurveyEntryViewModel)BindingContext).IsLoading = true;
					await Navigation.PushAsync(new SurveyPage(id, hasResult));
                }
                isTapped = false;
            }
        }

        public async static void NavigateToSurvey(SurveyEntryPage instance)
		{
            Device.BeginInvokeOnMainThread(()=>{
				if (Device.RuntimePlatform == Device.UWP)
				{
					NavigationPage eventDetailPage = new NavigationPage(new SurveyPage());
                    eventDetailPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
					eventDetailPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

					App.MasterDetailPage.Detail = eventDetailPage;
				}
				else if (!isTapped)
				{
					isTapped = true;
					NavigationPage eventDetailPage = new NavigationPage(new SurveyPage());
					eventDetailPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
					eventDetailPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

					App.MasterDetailPage.Detail = eventDetailPage;
					isTapped = false;
				}
            });
			
		}

    }
}
