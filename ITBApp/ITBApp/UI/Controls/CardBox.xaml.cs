﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ITBApp.UI.Controls
{
	public partial class CardBox : ContentView
	{
		public static readonly BindableProperty TitleProperty = BindableProperty.Create("Title", typeof(string), typeof(CardBox), null);
		public static readonly BindableProperty StartTimeProperty = BindableProperty.Create("StartTime", typeof(string), typeof(CardBox), null);
		public static readonly BindableProperty EndTimeProperty = BindableProperty.Create("EndTime", typeof(string), typeof(CardBox), null);
		public static readonly BindableProperty ClassRoomProperty = BindableProperty.Create("ClassRoom", typeof(string), typeof(CardBox), null);
		public static readonly BindableProperty IsMaleProperty = BindableProperty.Create("IsMale", typeof(bool), typeof(CardBox), false);
		public static readonly BindableProperty HasActivitiesProperty = BindableProperty.Create("HasActivities", typeof(bool), typeof(CardBox), false);
		public static readonly BindableProperty HasNoActivitiesProperty = BindableProperty.Create("HasNoActivities", typeof(bool), typeof(CardBox), false);
        public static readonly BindableProperty LocationProperty = BindableProperty.Create("Location", typeof(string), typeof(CardBox), null);


		public CardBox()
		{
			InitializeComponent();
			Content.BindingContext = this;

		}

		public string Title
		{
			get { return (string)GetValue(TitleProperty); }
			set
			{
				SetValue(TitleProperty, value);
			}
		}

		public string StartTime
		{
			get { return (string)GetValue(StartTimeProperty); }
			set
			{
				SetValue(StartTimeProperty, value);
			}
		}

		public string EndTime
		{
			get { return (string)GetValue(EndTimeProperty); }
			set
			{
				SetValue(EndTimeProperty, value);
			}
		}

		public string ClassRoom
		{
			get { return (string)GetValue(ClassRoomProperty); }
			set
			{
				SetValue(ClassRoomProperty, value);
			}
		}

		public bool IsMale
		{
			get { return (bool)GetValue(IsMaleProperty); }
			set
			{
				SetValue(IsMaleProperty, value);
			}
		}

		public bool HasActivities
		{
			get { return (bool)GetValue(HasActivitiesProperty); }
			set
			{
				SetValue(HasActivitiesProperty, value);
			}
		}

		public bool HasNoActivities
		{
			get { return (bool)GetValue(HasNoActivitiesProperty); }
			set
			{
				SetValue(HasNoActivitiesProperty, value);
			}
		}

        public string Location
        {
            get
            {
                return (string)GetValue(LocationProperty);
            }
            set
            {
                SetValue(LocationProperty, value);
            }
        }

		private Command _tappedEvent;
		public Command TappedEvent
		{
			get { return _tappedEvent; }
			set
			{
				_tappedEvent = value;

				TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer
				{
					Command = _tappedEvent
				};
				slFree.GestureRecognizers.Add(tapGestureRecognizer);
				slSchedule.GestureRecognizers.Add(tapGestureRecognizer);
				lblText1.GestureRecognizers.Add(tapGestureRecognizer);
				lblText2.GestureRecognizers.Add(tapGestureRecognizer);
				lblText3.GestureRecognizers.Add(tapGestureRecognizer);
				lblText4.GestureRecognizers.Add(tapGestureRecognizer);
				lblText5.GestureRecognizers.Add(tapGestureRecognizer);
				lblText6.GestureRecognizers.Add(tapGestureRecognizer);
				lblText7.GestureRecognizers.Add(tapGestureRecognizer);
				lblText8.GestureRecognizers.Add(tapGestureRecognizer);
				lblText9.GestureRecognizers.Add(tapGestureRecognizer);
				imgNextClass.GestureRecognizers.Add(tapGestureRecognizer);
			}
		}
	}
}
