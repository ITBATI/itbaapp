﻿using ITBApp.UI;
using ITBApp.Util.Enums;
using ITBApp.UWP.Implementations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(Grid), typeof(DroppableGridRenderer))]
namespace ITBApp.UWP.Implementations
{
    class DroppableGridRenderer : ViewRenderer<Grid, Windows.UI.Xaml.FrameworkElement>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Grid> e)
        {
            base.OnElementChanged(e);

            if (this == null)
                return;
            
            if (ITBApp.App.CurrentPage == Helpers.PageId.PRINT && this.AllowDrop == false)
            {
                this.AllowDrop = true;
                this.Drop += OnDropAsync;
                this.DragOver += OnDragOver;
                this.DragLeave += OnDragLeave;
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (ITBApp.App.CurrentPage == Helpers.PageId.PRINT && this.AllowDrop == false)
            {
                this.AllowDrop = true;
                this.Drop += OnDropAsync;
                this.DragOver += OnDragOver;
                this.DragLeave += OnDragLeave;
            }
        }

        private void OnDropAsync(object sender, DragEventArgs e)
        {
            string result = string.Empty;
            Device.BeginInvokeOnMainThread(async() => {
                var items = await e.DataView.GetStorageItemsAsync();
                if (items.Count == 1)
                {
                    List<string> printableFormats = new List<string>();
                    foreach (var mediaSubType in Enum.GetValues(typeof(MediaSubType)))
                    {
                        printableFormats.Add("." + mediaSubType.ToString());
                    }

                    var item = items[0] as StorageFile;

                    if (printableFormats.Contains(item.FileType.ToUpper()))
                    {
                        ITBApp.Util.Enums.MediaType mediaType;
                        ITBApp.Util.Enums.MediaSubType mediaSubType;
                        if (item.ContentType.Contains("image"))
                        {
                            mediaType = Util.Enums.MediaType.Image;
                            mediaSubType = Util.Enums.MediaSubType.JPG;
                        }
                        else
                        {
                            mediaType = Util.Enums.MediaType.Application;
                            mediaSubType = Util.Enums.MediaSubType.PDF;
                        }

                        byte[] fileBytes = null;

                        using (IRandomAccessStreamWithContentType stream = await item.OpenReadAsync())
                        {
                            fileBytes = new byte[stream.Size];
                            using (DataReader reader = new DataReader(stream))
                            {
                                await reader.LoadAsync((uint)stream.Size);
                                reader.ReadBytes(fileBytes);
                            }
                        }

                        PrintPage currentPage = ((PrintPage)((NavigationPage)ITBApp.App.MasterDetailPage.Detail).CurrentPage);
                        currentPage.SelectedFileBytes = fileBytes;
                        currentPage.SelectedFileFilename = item.DisplayName + item.FileType;
                        currentPage.SelectedFileMediaType = mediaType;
                        currentPage.SelectedFileMediaSubType = mediaSubType;
                        currentPage.EnablePrintControls();
                    }
                    else
                    {
                        result = "El formato del archivo no es correcto.";
                    }
                }
                else
                {
                    result = "No se puede imprimir más de un archivo.";
                }
                if (result != string.Empty)
                {
                    var dialog = new Windows.UI.Popups.MessageDialog(result);
                    await dialog.ShowAsync();
                }
            });
        }

        private void OnDragLeave(object sender, DragEventArgs e)
        {
            if (this.Background != null && ((SolidColorBrush)this.Background).Color == Windows.UI.Color.FromArgb(255, 119, 138, 143))
                this.Background = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 239, 239, 230));
        }


        private void OnDragOver(object sender, DragEventArgs e)
        {
            if (this.Background != null && ((SolidColorBrush)this.Background).Color == Windows.UI.Color.FromArgb(255, 239, 239, 230))
                this.Background = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 119, 138, 143));
            if (e.DataView.Contains(StandardDataFormats.StorageItems) || e.DataView.Contains(StandardDataFormats.Text))
            {
                e.AcceptedOperation = DataPackageOperation.Copy;

                if (e.DragUIOverride != null)
                {
                    e.DragUIOverride.Caption = "Imprimir";
                    e.DragUIOverride.IsContentVisible = true;
                }
            }
        }
    }
}
