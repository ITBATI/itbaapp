﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel
{
	[DataContract]
	[KnownType(typeof(TimetableData))]
	public class CommissionData
	{
		[DataMember]
		public string Id { get; set; }

		[DataMember]
		public string Name { get; set; }

		[DataMember]
		public List<string> Teachers { get; set; }

		[DataMember]
		public List<TimetableData> Timetables { get; set; }
	}
}

