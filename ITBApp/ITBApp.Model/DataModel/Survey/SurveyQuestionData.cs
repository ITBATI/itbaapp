﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel.Survey
{
	[DataContract]
	public class SurveyQuestionData
	{
		[DataMember]
		public long Id { get; set; }

		[DataMember]
		public String Question { get; set; }

		[DataMember]
		public QuestionType Type { get; set;}

		public List<SurveryQuestionChoiceData> Choices { get; set;}

		[DataMember]
		public String Response { get; set;}
	}
}
