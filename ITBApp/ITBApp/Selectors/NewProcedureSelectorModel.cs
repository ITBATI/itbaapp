﻿using System;
using System.ComponentModel;
using System.Windows.Input;

namespace ITBApp.Selectors
{
    public class NewProcedureSelectorModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool isAnOptionTemplate;
        private bool isFormTemplate;

        private string procedureTitle;
        private string procedureDescription;

        public int OptionId { get; set; }
        public string OptionDisplayName { get; set; }
        private bool isSelected;

        public ICommand TapOptionCommand { get; set; }

		public bool IsAnOptionTemplate
		{
			get { return isAnOptionTemplate; }
			set
			{
				isAnOptionTemplate = value;
				NotifyPropertyChanged("IsAnOptionTemplate");
			}
		}

		public bool IsFormTemplate
		{
			get { return isFormTemplate; }
			set
			{
				isFormTemplate = value;
				NotifyPropertyChanged("IsFormTemplate");
			}
		}

        public string ProcedureTitle
		{
			get { return procedureTitle; }
			set
			{
				procedureTitle = value;
				NotifyPropertyChanged("ProcedureTitle");
			}
		}

		public string ProcedureDescription
		{
			get { return procedureDescription; }
			set
			{
				procedureDescription = value;
				NotifyPropertyChanged("ProcedureDescription");
			}
		}

		public bool IsSelected
		{
			get { return isSelected; }
			set
			{
				isSelected = value;
				NotifyPropertyChanged("IsSelected");
			}
		}

		private void NotifyPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
    }
}
