﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ITBApp.Helpers;
using ITBApp.Interfaces;
using ITBApp.Resources.i18n;
using ITBApp.Services;
using ITBApp.ViewModels;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.UI
{
	public partial class AvailableClassrooms : ContentPage
	{
		private static bool isTapped = false;

		public AvailableClassrooms()
		{
			InitializeComponent();
			Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();

			((BaseModel)BindingContext).Initialize(this);
			ConfigureEventListeners();

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = new Command(async() =>
                {
                    if (CrossConnectivity.Current.IsConnected)
                    {
						((AvailableClassroomsViewModel)BindingContext).LocationId = "1";
						App.lastLocationId = "1";
						await ((AvailableClassroomsViewModel)BindingContext).LoadDataAsync();

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            RefreshLocations();
                        });
                    }
                })
			};
            slSede1.GestureRecognizers.Add(tapGestureRecognizer);

			tapGestureRecognizer = new TapGestureRecognizer
			{
				Command = new Command(async () =>
				{
                    if (CrossConnectivity.Current.IsConnected)
                    {
                        ((AvailableClassroomsViewModel)BindingContext).LocationId = "2";
                        App.lastLocationId = "2";
                        await ((AvailableClassroomsViewModel)BindingContext).LoadDataAsync();
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            RefreshLocations();
                        });
                    }
				})
			};
			slSede2.GestureRecognizers.Add(tapGestureRecognizer);

			tapGestureRecognizer = new TapGestureRecognizer
			{
				Command = new Command(async () =>
				{
                    if (CrossConnectivity.Current.IsConnected)
                    {
                        ((AvailableClassroomsViewModel)BindingContext).LocationId = "3";
                        App.lastLocationId = "3";
                        await ((AvailableClassroomsViewModel)BindingContext).LoadDataAsync();
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            RefreshLocations();
                        });
                    }
				})
			};
			slSede3.GestureRecognizers.Add(tapGestureRecognizer);
		}

		void LoadPage()
		{
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
			((AvailableClassroomsViewModel)BindingContext).LoadDataAsync();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
		}

		void ConfigureEventListeners()
		{

			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
                if (BindingContext != null && App.CurrentPage == PageId.AVAILABLE_CLASSROOMS && args.IsConnected)
				{
					LoadPage();
				}
			};

			((AvailableClassroomsViewModel)BindingContext).ClassroomsNowTapCommand = new Command(async() =>
			{
                if (!((AvailableClassroomsViewModel)BindingContext).IsLoading && !isTapped
				    && ((AvailableClassroomsViewModel)BindingContext).BindableAvailableClassroomsData.freeNow.Count > 0)
				{
					isTapped = true;

                    string locationName = string.Empty;
                    if (((AvailableClassroomsViewModel)BindingContext).LocationId == "1") locationName = "En " + lblSede1Enabled.Text;
                    else if (((AvailableClassroomsViewModel)BindingContext).LocationId == "2") locationName = "En " + lblSede2Enabled.Text;
                    else if (((AvailableClassroomsViewModel)BindingContext).LocationId == "3") locationName = "En " + lblSede3Enabled.Text;

                    if (Device.RuntimePlatform == Device.UWP)
                    {
                        NavigationPage availableclassroomsDetailPage = new NavigationPage(new AvailableClassroomsDetail(Translate.freeclassrooms_title_now,locationName,((AvailableClassroomsViewModel)BindingContext).BindableAvailableClassroomsData.freeNow));
                        availableclassroomsDetailPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                        availableclassroomsDetailPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);



                        App.MasterDetailPage.Detail = availableclassroomsDetailPage;
                    }
                    else
                        await Navigation.PushAsync(new AvailableClassroomsDetail(Translate.freeclassrooms_title_now, locationName,((AvailableClassroomsViewModel)BindingContext).BindableAvailableClassroomsData.freeNow));


                    isTapped = false;
				}
			});

			((AvailableClassroomsViewModel)BindingContext).ClassroomsSoonTapCommand = new Command(async () =>
			{
                if (!((AvailableClassroomsViewModel)BindingContext).IsLoading && !isTapped
					&& ((AvailableClassroomsViewModel)BindingContext).BindableAvailableClassroomsData.freeSoon.Count > 0)
				{
					isTapped = true;

					string locationName = string.Empty;
					if (((AvailableClassroomsViewModel)BindingContext).LocationId == "1") locationName = "En " + lblSede1Enabled.Text;
					else if (((AvailableClassroomsViewModel)BindingContext).LocationId == "2") locationName = "En " + lblSede2Enabled.Text;
					else if (((AvailableClassroomsViewModel)BindingContext).LocationId == "3") locationName = "En " + lblSede3Enabled.Text;

                    if (Device.RuntimePlatform == Device.UWP)
                    {
                        NavigationPage availableclassroomsDetailPage = new NavigationPage(new AvailableClassroomsDetail(Translate.freeclassrooms_title_soon,locationName,((AvailableClassroomsViewModel)BindingContext).BindableAvailableClassroomsData.freeSoon));
                        availableclassroomsDetailPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                        availableclassroomsDetailPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

                        App.MasterDetailPage.Detail = availableclassroomsDetailPage;
                    }
                    else
                        await Navigation.PushAsync(new AvailableClassroomsDetail(Translate.freeclassrooms_title_soon, locationName, ((AvailableClassroomsViewModel)BindingContext).BindableAvailableClassroomsData.freeSoon));

					isTapped = false;
				}
			});
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			if (!isTapped)
			{
				this.BindingContext = null;
				this.Content = null;
			}
            App.HardwareBackPressed = null;
		}

        private void RefreshLocations()
        {
			if (((AvailableClassroomsViewModel)BindingContext).LocationId == "1")
			{
				slSede1.BackgroundColor = Color.FromHex("#0E2B4D");
				lblSede1Enabled.IsVisible = true;
				lblSede1Disabled.IsVisible = false;
				imgSede1.Source = "sede_1_enabled.png";
			}
			else
			{
				slSede1.BackgroundColor = Color.FromHex("#002539");
				lblSede1Enabled.IsVisible = false;
				lblSede1Disabled.IsVisible = true;
				imgSede1.Source = "sede_1_disabled.png";
			}

			if (((AvailableClassroomsViewModel)BindingContext).LocationId == "2")
			{
				slSede2.BackgroundColor = Color.FromHex("#0E2B4D");
				lblSede2Enabled.IsVisible = true;
				lblSede2Disabled.IsVisible = false;
				imgSede2.Source = "sede_2_enabled.png";
			}
			else
			{
				slSede2.BackgroundColor = Color.FromHex("#002539");
				lblSede2Enabled.IsVisible = false;
				lblSede2Disabled.IsVisible = true;
				imgSede2.Source = "sede_2_disabled.png";
			}

			if (((AvailableClassroomsViewModel)BindingContext).LocationId == "3")
			{
				slSede3.BackgroundColor = Color.FromHex("#0E2B4D");
				lblSede3Enabled.IsVisible = true;
				lblSede3Disabled.IsVisible = false;
				imgSede3.Source = "sede_3_enabled.png";
			}
			else
			{
				slSede3.BackgroundColor = Color.FromHex("#002539");
				lblSede3Enabled.IsVisible = false;
				lblSede3Disabled.IsVisible = true;
				imgSede3.Source = "sede_3_disabled.png";
			}
        }

        protected override void OnAppearing()
        {
        	base.OnAppearing();

        	App.HardwareBackPressed = () =>
        	{
        		MainMenuItem item = MainMenuItem.HomeItem;
        		App.CurrentPage = item.Id;
        		item.NavigationPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
        		item.NavigationPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
        		App.MasterDetailPage.Detail = item.NavigationPage;
        		return Task.FromResult<bool?>(false);
        	};

			// Image by location
			Device.BeginInvokeOnMainThread(async () =>
			{
				try
				{
                    if (Device.RuntimePlatform == Device.UWP) 
                    {
                        await Task.Delay(300);
                    }
                    var locations = await new GeolocatorService().GetLocationsAsync().ConfigureAwait(false);

                    if (((AvailableClassroomsViewModel)BindingContext).LocationId == string.Empty) {
                        ((AvailableClassroomsViewModel)BindingContext).LocationId = await new GeolocatorService().GetLocationIdAsync().ConfigureAwait(false);
						
						if (((AvailableClassroomsViewModel)BindingContext).LocationId == "0") ((AvailableClassroomsViewModel)BindingContext).LocationId = App.lastLocationId;
                    }

                    Device.BeginInvokeOnMainThread(()=>{
						lblSede1Enabled.Text = locations.ITBALocations.Where(x => x.Id == "1").First().Name;
						lblSede2Enabled.Text = locations.ITBALocations.Where(x => x.Id == "2").First().Name;
						lblSede3Enabled.Text = locations.ITBALocations.Where(x => x.Id == "3").First().Name;
						lblSede1Disabled.Text = locations.ITBALocations.Where(x => x.Id == "1").First().Name;
						lblSede2Disabled.Text = locations.ITBALocations.Where(x => x.Id == "2").First().Name;
						lblSede3Disabled.Text = locations.ITBALocations.Where(x => x.Id == "3").First().Name;

						RefreshLocations();
                    });
				}
				catch (Exception exc)
				{
					//
				}
			});
        }

	}
}
