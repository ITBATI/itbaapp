﻿using ITBApp.Model.DataModel;
using Xamarin.Forms;

namespace ITBApp.Helpers
{
	public class CommissionsDataTemplateSelector: DataTemplateSelector
	{
		public DataTemplate CommissionTitleTemplate { get; set; }
		public DataTemplate ClassDayTemplate { get; set; }
		public DataTemplate TeachersTitleTemplate { get; set; }
		public DataTemplate TeachersDataTemplate { get; set; }
		public DataTemplate EndLineTemplate { get; set; }
		public DataTemplate MoreCommisionsButtonTemplate { get; set; }

		protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
		{
            if (item == null) return null;
			CommissionViewModel commissionViewModel = (CommissionViewModel)item;
			if (commissionViewModel.IsCommissionTitle) return CommissionTitleTemplate;
			if (commissionViewModel.IsClassDay) return ClassDayTemplate;
			if (commissionViewModel.IsTeachersTitle) return TeachersTitleTemplate;
			if (commissionViewModel.IsTeacherData) return TeachersDataTemplate;
			if (commissionViewModel.IsEndLine) return EndLineTemplate;
			if (commissionViewModel.IsMoreCommissionsButton) return MoreCommisionsButtonTemplate;
			return null;
		}
	}
}

