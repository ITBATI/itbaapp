﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ITBApp.Helpers
{
    public class NoSelectionEffect : RoutingEffect
    {
        public const string EffectNamespace = "ITBAapp";

        public NoSelectionEffect() : base($"{EffectNamespace}.{nameof(NoSelectionEffect)}")
        {

        }
    }
}
