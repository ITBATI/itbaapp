﻿using System;
using System.Collections.Generic;
using ITBApp.ViewModels;
using Xamarin.Forms;

namespace ITBApp.UI.Controls
{
    public partial class MonthEventsList : ContentView
    {
        public static readonly BindableProperty MonthProperty = BindableProperty.Create("Month", typeof(MonthsData), typeof(MonthEventsList), null);
        public static readonly BindableProperty IsIosPropery = BindableProperty.Create("IsIos", typeof(bool), typeof(MonthEventsList), false);

        public MonthEventsList()
        {
			InitializeComponent();
			Content.BindingContext = this;
        }

		public MonthsData Month
		{
			get { return (MonthsData)GetValue(MonthProperty); }
			set { SetValue(MonthProperty, value); }
		}

        public bool IsIos
        {
            get { return (Device.RuntimePlatform == Device.iOS); }
        }

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}
    }
}
