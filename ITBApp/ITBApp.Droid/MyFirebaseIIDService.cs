﻿using Android.App;
using Firebase.Iid;
using ITBApp.DataModel;
using ITBApp.Interfaces;
using ITBApp.Services;
using WindowsAzure.Messaging;
using Xamarin.Forms;

namespace ITBApp.Droid
{
    [Service]
	[IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
	public class MyFirebaseIIDService : FirebaseInstanceIdService
	{
		public override void OnTokenRefresh()
		{
			var refreshedToken = FirebaseInstanceId.Instance.Token;
			SendRegistrationToServer(refreshedToken);
		}

		public void SendRegistrationToServer(string token)
		{
#if false
#else
			if (App.IsLoggedIn)
			{
				try
				{
					LoginService loginService = new LoginService();
					UserData userData = loginService.GetSavedUserData();
					NotificationHub hub = new NotificationHub(AppSettings.NotificationHubName, AppSettings.ListenConnectionString,
                                                            Forms.Context);
					hub.UnregisterAll(token);
					string[] tags = new string[] { userData.Email };

					hub.Register(token, tags);

                    DependencyService.Get<IAppGroupSettings>().Set(ServiceConstants.NOTIFICATION_TOKEN_CHANGED, "false");
				}
				catch { }
			}
			else {
				DependencyService.Get<IAppGroupSettings>().Set("firebase_token", token);
                DependencyService.Get<IAppGroupSettings>().Set(ServiceConstants.NOTIFICATION_TOKEN_CHANGED, "true");
			}
#endif
		}
	}
}
