﻿using System;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.iOS;
using Xamarin.UITest.Queries;

namespace ITBApp.UITests.IOS
{
    [TestFixture]
    public class IntegratinIOSTests
	{
		iOSApp app;

        [SetUp]
		public void BeforeEachTest()
		{
			//app = ConfigureApp.iOS.StartApp();
		}

        [TearDown]
		public void Logout()
		{ 
			//app.Tap(c => c.Id("hamburger.png"));
			//app.Tap(c => c.Id("lblLogout"));
			//app.Tap(c => c.Text("Sí"));
		}

		[Test]
		public void TEST001_User_Put_Data_And_Access_To_App()
		{
            app = ConfigureApp.iOS.StartApp();
            // Solo si es en simulador
            //Thread.Sleep(1000);
            //app.Tap(c => c.Text("OK"));
            Thread.Sleep(1000);
            app.Tap(c => c.Text("Ingresar"));
            Thread.Sleep(5000);
            /* Assert */
            AppResult[] result = app.Query(c => c.Id("Home"));
            Assert.GreaterOrEqual(result.Count(), 1);
		}

		[Test]
		public void TEST002_User_Press_Logout_And_Cancel()
		{
            PressHamburgerMenu();
			app.Tap(c => c.Id("lblLogout"));
			app.Tap(c => c.Text("No"));

			/* Assert */
			AppResult[] result = app.Query(c => c.Id("lblStudentName"));
			Assert.GreaterOrEqual(result.Count(), 1);
		}

		[Test]
		public void TEST003_User_EnterProfile()
		{
			app.Tap(c => c.Id("lblStudentName"));

			/* Assert */
			AppResult[] result = app.Query(c => c.Text("Mi Perfil"));
			Assert.GreaterOrEqual(result.Count(), 1);
		}

		[Test]
		public void TEST004_User_Enter_Schedule()
		{
            PressHamburgerMenu();
			app.Tap(c => c.Id("lblSchedule"));

			/* Assert */
			AppResult[] result = app.Query(c => c.Text("Horarios"));
			Assert.GreaterOrEqual(result.Count(), 1);
		}

		[Test]
		public void TEST005_User_Enter_FreeClassrooms()
		{
            PressHamburgerMenu();
			app.Tap(c => c.Id("lblFreeClassrooms"));

			/* Assert */
			AppResult[] result = app.Query(c => c.Text("Aulas Libres"));
			Assert.GreaterOrEqual(result.Count(), 1);
		}

		[Test]
		public void TEST006_User_Enter_Surveys()
		{
            PressHamburgerMenu();
			app.Tap(c => c.Id("lblSurvey"));

			/* Assert */
			AppResult[] result = app.Query(c => c.Text("Opiná"));
			Assert.GreaterOrEqual(result.Count(), 1);
		}

		[Test]
		public void TEST007_User_Enter_Contacts()
		{
            PressHamburgerMenu();
			app.Tap(c => c.Id("lblContacts"));

			/* Assert */
			AppResult[] result = app.Query(c => c.Text("Contactos"));
			Assert.GreaterOrEqual(result.Count(), 1);
		}

		[Test]
		public void TEST008_User_Enter_Help()
		{
            PressHamburgerMenu();
			app.Tap(c => c.Id("lblHelp"));

			/* Assert */
			AppResult[] result = app.Query(c => c.Text("Ayuda"));
			Assert.GreaterOrEqual(result.Count(), 1);
		}

        private void PressHamburgerMenu()
        {
            app.TapCoordinates(50, 50);
        }

	}
}


