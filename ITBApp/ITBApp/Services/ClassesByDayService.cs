using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ITBApp.DataModel;
using ITBApp.Handlers;
using ITBApp.Helpers;
using ITBApp.Model.DataModel;
using ITBApp.Model.Handlers;
using ITBApp.Resources.i18n;
using ITBApp.Util;
using ITBApp.Util.Helpers;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.Services
{
	public class ClassesByDayService
	{
		private TokenHolder tokenHolder = new TokenHolder();

		public async Task<UserCoursesData> getClassesByDayAsync()
		{
			UserCoursesData coursesData = GetSavedUserCoursesData();
			UserData userData = new LoginService().GetSavedUserData();

			if (coursesData == null)
			{
				if (!CrossConnectivity.Current.IsConnected)
				{
					throw new ServerRequestException(Translate.loginpage_noInternet_message);
				}

				try
				{
					string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
					coursesData = await getClassesByDayAsync(token).ConfigureAwait(false);

					coursesData.Expiration = DateTime.Now.AddMinutes(ServiceConstants.CLASES_EXPIRATION_IN_MINUTES);

					string json = SerializerHelper.Serialize(coursesData);
					Application.Current.Properties[ServiceConstants.COURSES_DATA] = json;
					await Application.Current.SavePropertiesAsync().ConfigureAwait(false);

				}
				catch (TokenExpiredException)
				{
					throw new ServerRequestException(Translate.loginpage_noInternet_message);
				}

			}

			coursesData.IsMale = (userData.Gender == UserGender.Male);

			return coursesData;
		}

		public async Task<CourseData> GetCurrentCourseAsync()
		{
			var cursesData = await getClassesByDayAsync().ConfigureAwait(false);

			if (cursesData.CoursesByDay != null &&
				cursesData.CoursesByDay.HasCourses(DateTime.Now.DayOfWeek))
			{
				var coursesForToday = cursesData.CoursesByDay.GetCoursesOfDay(DateTime.Now.DayOfWeek);
				foreach (var course in coursesForToday)
				{
					if (DateTime.Now.ToLocalTime() <= DateTime.Parse(course.CurrentTimetable.StartTime) ||
						(DateTime.Now.ToLocalTime() > DateTime.Parse(course.CurrentTimetable.StartTime) &&
						 DateTime.Now.ToLocalTime() <= DateTime.Parse(course.CurrentTimetable.EndTime)))
					{
						return course;
					}
				}
			}

			return null;
		}

		private UserCoursesData GetSavedUserCoursesData()
		{
			if (Application.Current.Properties.ContainsKey(ServiceConstants.COURSES_DATA))
			{
				UserCoursesData data = DeserializeUserCoursesData();

				if (data.Expiration > DateTime.Now || !CrossConnectivity.Current.IsConnected)
				{
					return data;
				}
			}

			return null;
		}


		private async Task<UserCoursesData> getClassesByDayAsync(string token)
		{
			string stringResponse = await coursesCallAsync(token).ConfigureAwait(false);

			List<CourseData> coursesData = processCoursesCallResponse(stringResponse);

			return searchCurrentTimetables(coursesData);
		}


		private UserCoursesData searchCurrentTimetables(List<CourseData> coursesData)
		{

			Dictionary<string, List<CommissionData>> commissionsByCourse = new Dictionary<string, List<CommissionData>>();

			CoursesByDayData coursesByday = new CoursesByDayData();

			foreach (CourseData data in coursesData)
			{
				foreach (CommissionData commission in data.Commissions)
				{
					if (commission.Id.Equals(data.CommissionId))
					{
						foreach (TimetableData timetable in commission.Timetables)
						{
							CourseData courseData = new CourseData()
							{
								CommissionId = data.CommissionId,
								Code = data.Code,
								Id = data.Id,
								Name = data.Name,
								CurrentTimetable = timetable.Clone()
							};

							coursesByday.AddCourseToDay(courseData);
						}
					}
				}

				commissionsByCourse.Add(data.Id, data.Commissions);
			}

			coursesByday.sort();

			UserCoursesData userCourses = new UserCoursesData();
			userCourses.CoursesByDay = coursesByday;
			userCourses.Commissions = commissionsByCourse;
			return userCourses;
		}


		private List<CourseData> processCoursesCallResponse(string stringResponse)
		{
			try
			{
				JObject jsonResponse = JObject.Parse(stringResponse);

				if (!jsonResponse["Resultado"].ToString().Equals("0"))
				{
					throw new ServerRequestException(string.Join(".", JsonHelper.GetAsArray(jsonResponse["errors"])));
				}

				Dictionary<string, string> commissionByCourse = BuildCommissionsByCourse(jsonResponse);

				List<CourseData> coursesData = new List<CourseData>();
				JObject userCourses = JObject.Parse(jsonResponse["userCourses"].ToString());

				foreach (JToken data in userCourses["userCourse"])
				{
					JToken course = data["course"];
					CourseData courseData = BuildCourseData(commissionByCourse, course);

					JToken commissionsJson = course["commissions"];

					foreach (JToken commission in JsonHelper.GetAsArray(commissionsJson["commission"]))
					{
						JToken timetableJson = commission["timetable"];

						foreach (JToken timeGroup in JsonHelper.GetAsArray(timetableJson["group"]))
						{
							CommissionData commissionData = BuildCommissionData(timeGroup);

							if (timeGroup["period"] != null)
							{
								foreach (JToken period in JsonHelper.GetAsArray(timeGroup["period"]))
								{
									TimetableData timetable = BuildTimetableData(period);
									commissionData.Timetables.Add(timetable);
								}
							}

							courseData.Commissions.Add(commissionData);
						}
					}


					if (courseData.Commissions.Any())
					{
						coursesData.Add(courseData);
					}

				}

				return coursesData;

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}


		private TimetableData BuildTimetableData(JToken period)
		{
			return new TimetableData()
			{
				Day = (int)ValidationHelper.GetDayOfWeekFromJsonString(period["@day"].ToString()),
				StartTime = period["@from"].ToString(),
				EndTime = period["@to"].ToString(),
				Classroom = period["@on"].ToString(),
				Location = period["@building"].ToString()
			};
		}

		private CommissionData BuildCommissionData(JToken timeGroup)
		{
			CommissionData commissionData = new CommissionData()
			{
				Name = timeGroup["@name"].ToString(),
				Id = timeGroup["@id"].ToString(),
				Timetables = new List<TimetableData>(),
				// TODO: IMPLEMENTAR!!!
				Teachers = new List<string>() { }
			};
			return commissionData;
		}

		private CourseData BuildCourseData(Dictionary<string, string> commissionByCourse, JToken course)
		{
			return new CourseData()
			{
				Id = course["id"].ToString(),
				Code = course["code"].ToString(),
				Name = course["name"].ToString(),
				CommissionId = commissionByCourse[course["id"].ToString()],
				Commissions = new List<CommissionData>()
			};
		}

		private Dictionary<string, string> BuildCommissionsByCourse(JObject jsonResponse)
		{
			// Mapa que asocia el commissionId de cada curso del usuario
			Dictionary<string, string> commissionByCourse = new Dictionary<string, string>();
			foreach (JToken course in JsonHelper.GetAsArray(jsonResponse["userCourseProfile"]))
			{
				string key = course["@id"].ToString();
				string value = course["@commissionId"].ToString();
				commissionByCourse.Add(key, value);
			}

			return commissionByCourse;
		}

		private async Task<string> coursesCallAsync(string token)
		{
			var uriBuilder = new UriBuilder(AppSettings.ClassesServiceURL + token);
			HttpValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
			uriBuilder.Query = query.ToString();

			using (var client = new HttpClient())
			{
				client.MaxResponseContentBufferSize = 256000;
				HttpResponseMessage response = await client.GetAsync(uriBuilder.Uri).ConfigureAwait(false);
				if (response.IsSuccessStatusCode)
				{
					string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					return result;

				}
				else
				{
					throw new ServerRequestException("Error de conexión al servicio de cursos");
				}
			}
		}

		internal List<CommissionData> getCommissionsForCourse(string courseId)
		{
			if (Application.Current.Properties.ContainsKey(ServiceConstants.COURSES_DATA))
			{
				UserCoursesData data = DeserializeUserCoursesData();

				List<CommissionData> commissions = data.Commissions[courseId];

				if (commissions != null)
				{
					return commissions;
				}
			}

			throw new ServerRequestException("No se pudo encontrar información de las comisiones");
		}

		private UserCoursesData DeserializeUserCoursesData()
		{
			string json = (string)Application.Current.Properties[ServiceConstants.COURSES_DATA];
			UserCoursesData data = (UserCoursesData)SerializerHelper.Deserialize(json, (new UserCoursesData()).GetType());
			return data;
		}

		public async Task<string> createCalendar()
		{
			string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
			var uriBuilder = new UriBuilder(AppSettings.GoogleCalendarApiURL);
			HttpValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
			query.Add("access_token", token);
			uriBuilder.Query = query.ToString();
			var content = new StringContent("{\"summary\":\"ITBA Horarios\"}", Encoding.UTF8, "application/json");

			using (var client = new HttpClient())
			{
				client.MaxResponseContentBufferSize = 256000;
				HttpResponseMessage response = await client.PostAsync(uriBuilder.Uri, content).ConfigureAwait(false);

				if (response.IsSuccessStatusCode)
				{
					string stringResponse = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					JToken jTokenResponse = JToken.Parse(stringResponse);
					return jTokenResponse["id"].ToString();
				}
				else
				{
					throw new ServerRequestException("Error al exportar los horarios a Google Calendar.");
				}
			}
		}

		public async Task<string> createEvent(string calendarId, string courseName, string location,
											 string startTime, string endTime, int dayOfWeek)
		{
			string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
			var uriBuilder = new UriBuilder(AppSettings.GoogleCalendarApiURL + "/" + calendarId + "/events");
			HttpValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
			query.Add("access_token", token);
			uriBuilder.Query = query.ToString();

			string lastDate = DateHelper.getLastClassDay();
			string startDate = DateHelper.getFirstClassDay(dayOfWeek);
			string endDate = startDate;

			var content = new StringContent("{\"summary\":\"" + courseName + "\",\"location\":\"" + location + "\",\"start\": {\"dateTime\": \"" + startDate + "T" + startTime + ":00.000-03:00\",\"timeZone\": \"America/Argentina/Buenos_Aires\"},\"end\": {\"dateTime\": \"" + endDate + "T" + endTime + ":00.000-03:00\",\"timeZone\": \"America/Argentina/Buenos_Aires\"},\"recurrence\": [\"RRULE:FREQ=WEEKLY;UNTIL=" + lastDate + "T235900Z\"]}", Encoding.UTF8, "application/json");

			using (var client = new HttpClient())
			{
				client.MaxResponseContentBufferSize = 256000;
				HttpResponseMessage response = await client.PostAsync(uriBuilder.Uri, content).ConfigureAwait(false);

				if (response.IsSuccessStatusCode)
				{
					return "Tus horarios fueron agregados a Google Calendar";
				}
				else
				{
					throw new ServerRequestException("Error al exportar los horarios a Google Calendar.");
				}
			}
		}
	}
}

