﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ITBApp.Extensions;

namespace ITBApp.Droid
{
	[Activity(Label = "ActivityCustomUrlSchemeInterceptor", NoHistory = true, LaunchMode = LaunchMode.SingleTop)]
	[
	IntentFilter
	(
		actions: new[] { Intent.ActionView },
		Categories = new[]
				{
					Intent.CategoryDefault,
					Intent.CategoryBrowsable
				},
		DataSchemes = new[]
				{
					"com.googleusercontent.apps.843843481588-0bm5i908b7sng9bu63il6agcukggeftb",
                },
		DataPath = "/oauth2redirect"
	)
]
    public class ActivityCustomUrlSchemeInterceptor : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

			global::Android.Net.Uri uri_android = Intent.Data;

			// Convert Android.Net.Url to C#/netxf/BCL System.Uri - common API
			Uri uri_netfx = new Uri(uri_android.ToString());

			// load redirect_url Page for parsing
			AuthenticationState.Authenticator.OnPageLoading(uri_netfx);

			this.Finish();

			return;
        }
    }
}
