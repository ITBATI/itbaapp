﻿//-----------------------------------------------------------------------
// <copyright file="AvailableClassroomsViewModel.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace ITBApp.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using ITBApp.Model;
    using ITBApp.Model.Handlers;
    using ITBApp.Interfaces;
    using ITBApp.Resources.i18n;
    using ITBApp.Services;
    using Xamarin.Forms;

    /// <summary>
    /// Available Classrooms Context View Model
    /// </summary>
    internal class AvailableClassroomsViewModel : BaseModel
    {
        /// <summary>
        /// Command related to available classrooms now
        /// </summary>
        private ICommand classroomsNowTapCommand;

        /// <summary>
        /// Command related to available classrooms soon
        /// </summary>
        private ICommand classroomsSoonTapCommand;

        /// <summary>
        /// Lists of available classrooms
        /// </summary>
        private AvailableClassroomsData availableClassroomsData;

        public string LocationId = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="AvailableClassroomsViewModel"/> class
        /// </summary>
        public AvailableClassroomsViewModel()
        {
            Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
        }

        /// <summary>
        /// Gets a value indicating whether page is open
        /// </summary>
        public bool IsOpen
        {
            get
            {
                return new AvailableClassroomsService().IsOpen();
            }
        }

        /// <summary>
        /// Gets or sets the available classrooms now tap command
        /// </summary>
        public ICommand ClassroomsNowTapCommand
        {
            get
            {
                return this.classroomsNowTapCommand;
            }

            set
            {
                this.classroomsNowTapCommand = value;
                this.OnPropertyChanged("ClassroomsNowTapCommand");
            }
        }

        /// <summary>
        /// Gets or sets the available classrooms soon tap command
        /// </summary>
        public ICommand ClassroomsSoonTapCommand 
        { 
            get 
            { 
                return this.classroomsSoonTapCommand; 
            }

            set
            {
                this.classroomsSoonTapCommand = value;
                this.OnPropertyChanged("ClassroomsSoonTapCommand");
            }
        }

        /// <summary>
        /// Gets the available classrooms data
        /// </summary>
        public AvailableClassroomsData BindableAvailableClassroomsData
        {
            get 
            { 
                return this.availableClassroomsData; 
            }

            private set
            {
                this.availableClassroomsData = value;
                this.OnPropertyChanged("BindableAvailableClassroomsData");
            }
        }

        /// <summary>
        /// Load data from services
        /// </summary>
        /// <returns>
        /// Error message
        /// </returns>
        public override async Task<string> LoadDataAsync()
        {
            try
            {
                this.IsLoading = true;
                this.ErrorMessage = null;

				try
				{
                    if (LocationId == string.Empty)
                    {
						LocationId = await new GeolocatorService().GetLocationIdAsync();
						if (LocationId == "0") LocationId = App.lastLocationId;
                    }
				}
				catch (Exception)
				{
					// If location fails, show default
					if (LocationId == string.Empty)
					{
					    LocationId = App.lastLocationId;
                    }
				}

                AvailableClassroomsService service = new AvailableClassroomsService();
                this.BindableAvailableClassroomsData = await service.getFreeClassroomsDataAsync(LocationId).ConfigureAwait(false);
            }
            catch (ServerRequestException ex)
            {
                this.ErrorMessage = ex.Message;
            }
            catch (Exception exception)
            {
                this.ErrorMessage = Translate.free_classrooms_error_message;
            }

            this.IsLoading = false;

            return this.ErrorMessage;
        }

        /// <summary>
        /// Event when page is opened or maximized
        /// </summary>
        /// <param name="sender">
        /// Object that sends the event
        /// </param>
        /// <param name="eventArgs">
        /// Event parameters
        /// </param>
        protected override async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
        {
            if (this.BindableAvailableClassroomsData == null || this.BindableAvailableClassroomsData.Expiration < DateTime.Now)
            {
                await this.LoadDataAsync().ConfigureAwait(false);
            }
        }
    }
}
