﻿using System;
using ITBApp.Helpers;
using ITBApp.Interfaces;
using ITBApp.Resources.i18n;
using ITBApp.ViewModels;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.UI
{
    public partial class ProceduresPage : ContentPage
    {
        private static bool isTapped = false;

        public ProceduresPage()
        {
            InitializeComponent();
            Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
			((BaseModel)BindingContext).Initialize(this);
            ConfigureEventListeners();
        }

		void ConfigureEventListeners()
		{
			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
                if (BindingContext != null && App.CurrentPage == PageId.PROCEDURES && args.IsConnected)
				{
                    ((ProceduresViewModel)BindingContext).LoadDataAsync().ConfigureAwait(false);
				}
			};
		}

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}

        public async void NewProcedure_Tapped(object sender, System.EventArgs e)
        {
            if (Device.RuntimePlatform == Device.UWP)
            {
                NavigationPage newProcedurePage = new NavigationPage(new NewProcedurePage());
                newProcedurePage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                newProcedurePage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

                App.MasterDetailPage.Detail = newProcedurePage;
            }
            else if (!isTapped)
            {
                isTapped = true;
                await Navigation.PushAsync(new NewProcedurePage());
                isTapped = false;
            }
        }
    }
}
