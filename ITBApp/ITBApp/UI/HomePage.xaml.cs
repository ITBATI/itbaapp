﻿using ITBApp.Helpers;
using ITBApp.Interfaces;
using ITBApp.Resources.i18n;
using ITBApp.Services;
using ITBApp.ViewModels;
using Plugin.Connectivity;
using Xamarin.Forms;
using System.Threading.Tasks;
using System;

namespace ITBApp.UI
{
    public partial class HomePage : ContentPage
	{
		private static bool isTapped = false;
		private Color SelectionColor = Color.FromHex("#DC1456").MultiplyAlpha(0.5);
        private string locationId = "0";

		public HomePage()
		{
			InitializeComponent();
			Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();

			((BaseModel)BindingContext).Initialize(this);
            ((HomeViewModel)BindingContext).IsLoading = true;

            App.Current.Properties[ServiceConstants.APP_VERSION] = DependencyService.Get<IAppMethods>().getAppVersion();

			cardBoxSchedule.TappedEvent = new Command(CardTapped);
			circleBoxClassrooms.TappedEvent = new Command(FreeClassRoomTapped);
			circleBoxSmallSurvey.TappedEvent = new Command(SurveyTapped);
			circleBoxNotifications.TappedEvent = new Command(NotificationTapped);

			ConfigureEventListeners();
		}

		void ConfigureEventListeners()
		{
			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
				if (BindingContext != null && App.CurrentPage == PageId.HOME)
				{
					cvWarningUpdate.IsVisible = !args.IsConnected;
					if (args.IsConnected)
					{
						((HomeViewModel)BindingContext).LoadDataAsync().ConfigureAwait(false);
					}
				}
			};
		}

		public void InitConnectivityMessage()
		{
			Device.BeginInvokeOnMainThread(() =>
			{
				cvWarningUpdate.IsVisible = !CrossConnectivity.Current.IsConnected;
			});
		}

		public void FreeClassRoomTapped()
		{
			OpenPage(MainMenuItem.FreeClassroomsItem);
		}

		public void NotificationTapped()
		{
			OpenPage(MainMenuItem.NotificationsItem);
		}

		public void SurveyTapped()
		{
            OpenPage(MainMenuItem.SurveyItem);
		}

		public void CardTapped()
		{
			OpenPage(MainMenuItem.ClassesByDayItem);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			if (!isTapped)
			{
				this.BindingContext = null;
				this.Content = null;
			}
		}

		private void OpenPage(MainMenuItem menuItem)
		{
			CleanSelections();
			menuItem.ViewItem.BackgroundColor = SelectionColor;

			App.CurrentPage = menuItem.Id;
			App.MasterDetailPage.Detail = menuItem.NavigationPage;
			MainMenuItem.FreeClassroomsItem.NavigationPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
			MainMenuItem.FreeClassroomsItem.NavigationPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

            if (Device.RuntimePlatform != Device.UWP)
                App.MasterDetailPage.IsPresented = false;
		}

		private void CleanSelections()
		{
			foreach (MainMenuItem item in MainMenuItem.Values())
			{
				if (item.ViewItem != null)
				{
					item.ViewItem.BackgroundColor = Color.Transparent;
				}
			}
		}

        bool gridHeightWasCalculated = false;

        void Handle_SizeChanged(object sender, System.EventArgs e)
        {
            if (!gridHeightWasCalculated)
            {
                gridHeightWasCalculated = true;
                int minImageLocationHeight = 170;
                double totalHeight = Device.RuntimePlatform == Device.iOS ?
                    DependencyService.Get<IAppMethods>().GetHeightInPixels() :
                    DependencyService.Get<IAppMethods>().GetHeight();

                double firstControlHeight = cardBoxSchedule.Height;
                double boxesHeight = gridBoxes.Height;
                int margins = Device.RuntimePlatform == Device.Android ? 80 : 60; // Status bar included in Android
                double maxImageRatio = 0.75;

                var availableSpace = totalHeight - firstControlHeight - boxesHeight - margins;

                if (availableSpace > (minImageLocationHeight * maxImageRatio))
                {
                    // Image by location
                    if (availableSpace < minImageLocationHeight)
                    {
                        gridLocationImage.HeightRequest = availableSpace - 25;
                    }

                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        locationId = await new GeolocatorService().GetLocationIdAsync();
                        string imageSource = await new WeatherService().getWeatherImageAsync(locationId).ConfigureAwait(false);

                        await imgITBALocation.FadeTo(0, 400);
                        imgITBALocation.Source = imageSource;
                        await imgITBALocation.FadeTo(1, 400);
                    });

                    imgITBALocation.IsVisible = true;
                }
                else
                {
                    gridLocationImage.IsVisible = false;
                    gridLocationImage.Margin = 0;
                }
            }
        }
    }
}
