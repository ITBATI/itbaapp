﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Database;
using Android.OS;
using Android.Provider;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using ITBApp.Services;
using ITBApp.Helpers;
using Plugin.Connectivity;
using Xamarin.Forms;
using ITBApp.Interfaces;

namespace ITBApp.Droid
{
    [Activity(Label = "Imprimir", Icon = "@drawable/icon",
             ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
             ScreenOrientation = ScreenOrientation.Portrait,
              Theme = "@style/Theme.Splash", //Indicates the theme to use for this activity
             NoHistory = true)] //Doesn't place it in back stack
    [IntentFilter(new string[] { Intent.ActionSend },
        Categories = new string[] { Intent.CategoryDefault, Intent.CategoryBrowsable },
        DataMimeType = "image/jpeg")]
    [IntentFilter(new string[] { Intent.ActionSend },
        Categories = new string[] { Intent.CategoryDefault, Intent.CategoryBrowsable },
        DataMimeType = "image/png")]
    [IntentFilter(new string[] { Intent.ActionSend },
        Categories = new string[] { Intent.CategoryDefault, Intent.CategoryBrowsable },
        DataMimeType = "image/bmp")]
    [IntentFilter(new string[] { Intent.ActionSend },
        Categories = new string[] { Intent.CategoryDefault, Intent.CategoryBrowsable },
        DataMimeType = "image/gif")]
    [IntentFilter(new string[] { Intent.ActionSend },
        Categories = new string[] { Intent.CategoryDefault, Intent.CategoryBrowsable },
        DataMimeType = "image/tiff")]
    [IntentFilter(new string[] { Intent.ActionSend },
        Categories = new string[] { Intent.CategoryDefault, Intent.CategoryBrowsable },
        DataMimeType = "application/pdf")]
    public class PrintActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        private const string MessageBoxTitle = "Imprimir";
        private const string InternetConnectionMessage = "Para imprimir necesitás estar conectado a internet.";
        private const string AcceptMessage = "Aceptar";
        private const string CancelMessage = "Cancelar";
        private const string PrintReadyMessage = "¡Listo! Retirá el trabajo en el CIR";
        private const string AppOpenMessage = "Para imprimir necesitás tener abierta ITBAapp.";
        private const string SessionOpenMessage = "Para imprimir necesitás iniciar sesión en ITBAapp.";
        private const string SendFileMessage = "¿Querés enviar tu archivo a imprimir?";
        private const string SendingFileMessage = "Enviando";
        private const string DateFormat = "yyyyMMddHHmmss";

		protected override async void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
			{
				Window.ClearFlags(WindowManagerFlags.TranslucentStatus);
				Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
				Window.SetStatusBarColor(Android.Graphics.Color.Black);
			}

            if (!Xamarin.Forms.Forms.IsInitialized)
            {
                global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            }

			if (!CrossConnectivity.Current.IsConnected)
			{
				AlertDialog.Builder alert = new AlertDialog.Builder(this);
				alert.SetTitle(MessageBoxTitle);
				alert.SetMessage(InternetConnectionMessage);
				alert.SetCancelable(false);
				alert.SetPositiveButton(AcceptMessage, (senderAlert, args) =>
				{
					Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
				});

				alert.Show();
			}
			else {
                var token = await new TokenHolder().GetTokenAsync().ConfigureAwait(false);

                if (token == null) 
                {
					AlertDialog.Builder alert = new AlertDialog.Builder(this);
					alert.SetTitle(MessageBoxTitle);
					alert.SetMessage(SessionOpenMessage);
					alert.SetCancelable(false);
					alert.SetPositiveButton(AcceptMessage, (senderAlert, args) =>
					{
						Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
					});

					alert.Show();
                }
                else 
                {
					var transaction = FragmentManager.BeginTransaction();
					var inflater = (LayoutInflater)this.GetSystemService(Context.LayoutInflaterService);
					var view = inflater.Inflate(Resource.Layout.NumberPickerDialog, null);
					var numberPicker = view.FindViewById<NumberPicker>(Resource.Id.numberPicker);
					numberPicker.MaxValue = 10;
					numberPicker.MinValue = 1;
					numberPicker.Value = 1;

					Spinner spinner = view.FindViewById<Spinner>(Resource.Id.spinnerType);
					var adapter = ArrayAdapter.CreateFromResource(
						this, Resource.Array.tipos_array, Android.Resource.Layout.SimpleSpinnerItem);

					adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
					spinner.Adapter = adapter;

					var dialog = new AlertDialog.Builder(this);
					dialog.SetTitle("Imprimir");
					dialog.SetView(view);
					dialog.SetNegativeButton("Cancelar", (s, a) =>
					{
						this.Finish();
					});
					dialog.SetPositiveButton("Aceptar", async (s, a) =>
					{
						if (Intent.Action == Intent.ActionSend && Intent.Extras.ContainsKey(Intent.ExtraStream))
						{
							try
							{
								ProgressDialog loadingDialog = ProgressDialog.Show(dialog.Context, "", SendingFileMessage + " 1 de " + numberPicker.Value, false, false);

								var fileUri = (Android.Net.Uri)Intent.Extras.GetParcelable(Intent.ExtraStream);

								byte[] bytes;

								var fileType = Intent.Type;
								ITBApp.Enums.MediaType mediaType;
								if (fileType.Contains("image"))
									mediaType = ITBApp.Enums.MediaType.Image;
								else
									mediaType = ITBApp.Enums.MediaType.Application;
								ITBApp.Enums.MediaSubType mediaSubType;
								if (fileType.Contains("image"))
									mediaSubType = ITBApp.Enums.MediaSubType.JPG;
								else
									mediaSubType = ITBApp.Enums.MediaSubType.PDF;

								string filename = DateTime.Now.ToString(DateFormat) + "." + mediaSubType.ToString();

								if (fileUri.ToString().Contains("com.google"))
								{
									// es Google Drive
									ICursor cursor = ContentResolver.Query(fileUri, null, null, null, null);
									cursor.MoveToFirst();
									filename = cursor.GetString(cursor.GetColumnIndex("_display_name"));
									mediaSubType = ValidationHelper.getMediaSubTypeFromFilename(filename);

									using (var streamReader = new StreamReader(ContentResolver.OpenInputStream(fileUri)))
									{
										bytes = default(byte[]);
										using (var memstream = new MemoryStream())
										{
											streamReader.BaseStream.CopyTo(memstream);
											bytes = memstream.ToArray();
										}
									}
								}
								else if (fileUri.ToString().Contains("file:///") || fileUri.ToString().Contains("content://com."))
								{
									{
										int REQUEST_EXTERNAL_STORAGE = 1;
										String[] PERMISSIONS_STORAGE = {
										Manifest.Permission.ReadExternalStorage
									};
										// Check if we have write permission
										var permission = ActivityCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage);

										if (permission != Permission.Granted)
										{
											// We don't have permission so prompt the user
											ActivityCompat.RequestPermissions(
													this,
													PERMISSIONS_STORAGE,
													REQUEST_EXTERNAL_STORAGE
											);
										}

										// Viene de whatsapp
										filename = Path.GetFileName(fileUri.ToString());
										mediaSubType = ValidationHelper.getMediaSubTypeFromFilename(filename);

										using (var streamReader = new StreamReader(ContentResolver.OpenInputStream(fileUri)))
										{
											bytes = default(byte[]);
											using (var memstream = new MemoryStream())
											{
												streamReader.BaseStream.CopyTo(memstream);
												bytes = memstream.ToArray();
											}
										}
									}
								}
								else
								{
									// Es Storage

									int REQUEST_EXTERNAL_STORAGE = 1;
									String[] PERMISSIONS_STORAGE = {
										Manifest.Permission.ReadExternalStorage
									};

									// Check if we have write permission
									var permission = ActivityCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage);

									if (permission != Permission.Granted)
									{
										// We don't have permission so prompt the user
										ActivityCompat.RequestPermissions(
												this,
												PERMISSIONS_STORAGE,
												REQUEST_EXTERNAL_STORAGE
										);
									}


                                    try {
                                        string filePath = GetRealPathFromURI(fileUri);
                                        filename = Path.GetFileName(filePath);
                                        mediaSubType = ValidationHelper.getMediaSubTypeFromFilename(filename);

                                        var webClient = new WebClient();
                                        webClient.Encoding = Encoding.UTF8;
                                        bytes = webClient.DownloadData(filePath);
                                    }
                                    catch {
                                        // Get the info from ClipData 
                                        var pdf = Intent.ClipData.GetItemAt(0);

                                        // Open a stream from the URI 
                                        var pdfStream = ContentResolver.OpenInputStream(pdf.Uri);

                                        // Copy to bytes
                                        var memOfPdf = new System.IO.MemoryStream(); 
                                        pdfStream.CopyTo(memOfPdf);
                                        bytes = memOfPdf.ToArray();
                                    }
									
								}

                                // check if file has extension and if no... add it
                                if (!Path.HasExtension(filename))
                                {
                                    if (fileType.Contains("image"))
                                        filename += ".jpg";
                                    else
                                        filename += ".pdf";
                                }

								EmailPrintService printService = new EmailPrintService();

								if (spinner.SelectedItemPosition == 0)
								{
									for (int iteration = 1; iteration <= numberPicker.Value; iteration++)
									{
										loadingDialog.SetMessage(SendingFileMessage + " " + iteration + " de " + numberPicker.Value + "...");
										await printService.SendBWEmail("me", bytes, filename,
											 mediaType, mediaSubType, token);
									}
								}
								else
								{
									for (int iteration = 1; iteration <= numberPicker.Value; iteration++)
									{
										loadingDialog.SetMessage(SendingFileMessage + " " + iteration + " de " + numberPicker.Value + "...");
										await printService.SendColorEmail("me", bytes, filename,
											  mediaType, mediaSubType, token);
									}
								}

								loadingDialog.Hide();

								Toast.MakeText(this, PrintReadyMessage, ToastLength.Long).Show();

								this.Finish();
							}
							catch (Exception exc)
							{
								Toast.MakeText(this, exc.Message, ToastLength.Long).Show();
								this.Finish();
							}
						}
					});

					dialog.Create().Show();
                }
			}
		}

		private string GetRealPathFromURI(Android.Net.Uri contentURI)
		{
			ICursor cursor = ContentResolver.Query(contentURI, null, null, null, null);
			cursor.MoveToFirst();
			string documentId = cursor.GetString(0);
			if (documentId.Contains(':'))
				documentId = documentId.Split(':')[1];
			cursor.Close();

			cursor = ContentResolver.Query(
			Android.Provider.MediaStore.Images.Media.ExternalContentUri,
			null, MediaStore.Images.Media.InterfaceConsts.Id + " = ? ", new[] { documentId }, null);
			cursor.MoveToFirst();
			string path = cursor.GetString(cursor.GetColumnIndex(MediaStore.Images.Media.InterfaceConsts.Data));
			cursor.Close();

			return path;
		}
	}
}
