﻿using System;
using Xamarin.Forms;

namespace ITBApp.Selectors
{
    public class NewProcedureTemplateSelector : DataTemplateSelector
    {
		public DataTemplate OptionTemplate { get; set; }
		public DataTemplate FormTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            NewProcedureSelectorModel selectorModel = (NewProcedureSelectorModel)item;

            if (selectorModel.IsAnOptionTemplate)
                return OptionTemplate;

            return FormTemplate;
        }
    }
}
