﻿using System;
using System.ComponentModel;
using ITBApp.ViewModels;

namespace ITBApp.Selectors
{
    public class EventDetailSelectorModel : INotifyPropertyChanged
    {
        private bool isEventDescription;
        private EventViewModelData eventViewModel;
        private string eventDescription;
        private bool isEndLineTemplate;

        public event PropertyChangedEventHandler PropertyChanged;

		public bool IsEventDescription
		{
			get { return isEventDescription; }
			set
			{
				isEventDescription = value;
				NotifyPropertyChanged("IsEventDescription");
			}
		}

		public bool IsEndLineTemplate
		{
			get { return isEndLineTemplate; }
			set
			{
				isEndLineTemplate = value;
				NotifyPropertyChanged("IsEndLineTemplate");
			}
		}

        public string EventDescription
		{
			get { return eventDescription; }
			set
			{
				eventDescription = value;
				NotifyPropertyChanged("EventDescription");
			}
		}

		public EventViewModelData EventViewModel
		{
			get { return eventViewModel; }
			set
			{
				eventViewModel = value;
				NotifyPropertyChanged("EventViewModel");
			}
		}

		private void NotifyPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
    }
}
