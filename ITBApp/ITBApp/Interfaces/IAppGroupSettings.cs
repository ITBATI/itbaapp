﻿namespace ITBApp.Interfaces
{
    public interface IAppGroupSettings
    {
        object Get(string key);

		void Set(string key, string value);

		void Clear();
    }
}
