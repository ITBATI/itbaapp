﻿using ITBApp.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Networking.Connectivity;
using Xamarin.Forms;

[assembly: Dependency(typeof(ITBApp.UWP.Implementations.IPAddressManager))]

namespace ITBApp.UWP.Implementations
{
    public class IPAddressManager : IIPAddressManager
    {
        public List<string> GetIPAddress()
        {
            try
            {
                List<string> IpAddress = new List<string>();

                /*
				string wifiName = GetWifiId();
				if (wifiName == null || !wifiName.Contains("ITBA")) return IpAddress;
				*/

                var Hosts = Windows.Networking.Connectivity.NetworkInformation.GetHostNames().ToList();
                foreach (var Host in Hosts)
                {
                    string IP = Host.DisplayName;
                    IpAddress.Add(IP);
                }

                return IpAddress;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

		/// <summary>
		/// Returns an identifier (the SSID or BSSID) of the Wifi the device is corrently connected to.
		/// If the current SSID is not available, the BSSID is used as identifier.
		/// </summary>
		/// <returns>identifier (SSID or BSSID) for the corrently connected Wifi</returns>
		public string GetWifiId()
		{
			var ssid = GetSsid();
			var bssid = GetBssid();

			if (!string.IsNullOrEmpty(ssid))
			{
				return ssid;
			}

			if (!string.IsNullOrEmpty(bssid))
			{
				return bssid;
			}

			return null;
		}

		private String GetBssid()
		{
			String bssid = null;

			try
			{
				var connectedProfile = NetworkInformation.GetInternetConnectionProfile();

				if (connectedProfile != null)
				{
					var adapterId = connectedProfile.NetworkAdapter.NetworkAdapterId;

					if (adapterId != null && !adapterId.Equals(Guid.Empty))
					{
						var lanIdentifier = NetworkInformation.GetLanIdentifiers().FirstOrDefault(l => l.NetworkAdapterId == adapterId);

						if (lanIdentifier != null)
						{
							byte[] b1 = lanIdentifier.InfrastructureId.Value.ToArray();
							bssid = BitConverter.ToString(b1);
						}
					}
				}
			}
			catch (Exception)
			{
				//ignore
			}

			return bssid;
		}

		private String GetSsid()
		{
			String ssid = null;

			try
			{
				var connectedProfile = NetworkInformation.GetInternetConnectionProfile();

				if (connectedProfile != null &&
					connectedProfile.IsWlanConnectionProfile &&
					connectedProfile.WlanConnectionProfileDetails != null)
				{
					ssid = connectedProfile.WlanConnectionProfileDetails.GetConnectedSsid();
				}
			}
			catch (Exception)
			{
				//ignore
			}

			return ssid;
		}

	}
}
