﻿using System;
using System.Collections.Generic;
using ITBApp.Helpers;
using Xamarin.Forms;

namespace ITBApp.UI
{
    public partial class EventInscriptionResponsePage : ContentPage
    {
        private bool ShowError;

        public EventInscriptionResponsePage(bool showError)
        {
            InitializeComponent();
            ShowError = showError;

            if (!ShowError)
            {
                DisplayMessage.Text = "¡Gracias por inscribirte!";
            }
            {
                //TODO agregar mensaje y lógica para mostrar otra image.

            }
        }

        void BackToEventsButtom_Tapped(object sender, System.EventArgs e)
        {
            ActionUIHelper.OpenPage(MainMenuItem.EventsItem);
        }
    }
}
