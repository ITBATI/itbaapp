﻿using System;
using CoreGraphics;
using Foundation;
using ITBApp;
using ITBApp.iOS.Implementations;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


[assembly: ExportRenderer(typeof(HyperLinkLabel), typeof(HyperLinkLabelRender))]
namespace ITBApp.iOS.Implementations
{
    [Preserve(AllMembers = true)]
	public class HyperLinkLabelRender : ViewRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged(e);
			HyperLinkLogic();
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			HyperLinkLogic();
		}

		private void HyperLinkLogic()
		{
			var view = (HyperLinkLabel)Element;
			if (view == null) return;

			UITextView uilabelleftside = new UITextView(new CGRect(0, 0, view.Width, view.Height));
			uilabelleftside.Text = view.Text;
			uilabelleftside.Font = UIFont.SystemFontOfSize((float)view.FontSize);
			uilabelleftside.Editable = false;

			// Setting the data detector types mask to capture all types of link-able data
			uilabelleftside.DataDetectorTypes = UIDataDetectorType.All;
			uilabelleftside.BackgroundColor = UIColor.Clear;
			uilabelleftside.TextColor = view.TextColor.ToUIColor();

			// overriding Xamarin Forms Label and replace with our native control
			SetNativeControl(uilabelleftside);
		}
	}
}