﻿using System.Threading.Tasks;
using ITBApp.Helpers;
using ITBApp.Interfaces;
using ITBApp.Resources.i18n;
using ITBApp.ViewModels;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.UI
{
	public partial class UserProfile : ContentPage
	{
		private static bool isTapped = false;

        protected override void OnAppearing()
        {
        	base.OnAppearing();

        	App.HardwareBackPressed = () =>
        	{
        		MainMenuItem item = MainMenuItem.HomeItem;
        		App.CurrentPage = item.Id;
        		item.NavigationPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
        		item.NavigationPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
        		App.MasterDetailPage.Detail = item.NavigationPage;
        		return Task.FromResult<bool?>(false);
        	};
        }

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			if (!isTapped)
			{
				this.BindingContext = null;
				this.Content = null;
			}
            App.HardwareBackPressed = null;
		}

		public UserProfile()
		{
			InitializeComponent();
			Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();

			((BaseModel)BindingContext).Initialize(this);

			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += async (sender, e) =>
            {
				if (!isTapped)
				{
					isTapped = true;

                    if (Device.RuntimePlatform == Device.UWP)
                    {
                        NavigationPage accountPage = new NavigationPage(new AccountBalancePage());
                        accountPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                        accountPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

                        App.MasterDetailPage.Detail = accountPage;
                    }
                    else
                        await Navigation.PushAsync(new AccountBalancePage());

					isTapped = false;
				}
			};
			slStatus.GestureRecognizers.Add(tapGestureRecognizer);

			tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += async (sender, e) =>
			{
				if (!isTapped)
				{
					isTapped = true;

                    if (Device.RuntimePlatform == Device.UWP)
                    {
                        NavigationPage examsPage = new NavigationPage(new PendingExams());
                        examsPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                        examsPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

                        App.MasterDetailPage.Detail = examsPage;
                    }
                    else
                        await Navigation.PushAsync(new PendingExams());
					isTapped = false;
				}
			};
			slExams.GestureRecognizers.Add(tapGestureRecognizer);

			tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += async (sender, e) =>
			{
				if (!isTapped)
				{
					isTapped = true;

					if (Device.RuntimePlatform == Device.UWP)
					{
						NavigationPage academicHistoryPage = new NavigationPage(new AcademicHistory());
						academicHistoryPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
						academicHistoryPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

						App.MasterDetailPage.Detail = academicHistoryPage;
					}
					else
                        await Navigation.PushAsync(new AcademicHistory());
					isTapped = false;
				}
			};
            slHistory.GestureRecognizers.Add(tapGestureRecognizer);


			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
				if (App.CurrentPage == PageId.USER_PROFILE && args.IsConnected)
				{
					if (((UserProfileViewModel)BindingContext).CurrentUserProfileData == null)
					{
						((UserProfileViewModel)BindingContext).LoadDataAsync().ConfigureAwait(false);
					}

				}
			};
		}

        bool layoutHeightWasCalculated = false;
        void Handle_SizeChanged(object sender, System.EventArgs e)
        {
            if (!layoutHeightWasCalculated){
                layoutHeightWasCalculated = true;

				int minImageProfileHeight = 100;
				int titleBarHeight = 60;
				int deviceHeight = DependencyService.Get<IAppMethods>().GetHeight() - titleBarHeight;
                double spaceAvailable = deviceHeight - slProfile.Height;
                if (spaceAvailable <= minImageProfileHeight)
                {
                    ((UserProfileViewModel)BindingContext).HideProfileImage(spaceAvailable);
                }
            }
        }
    }
}
