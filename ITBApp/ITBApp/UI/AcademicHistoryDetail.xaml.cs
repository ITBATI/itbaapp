﻿using System;
using System.Collections.Generic;
using ITBApp.ViewModels;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace ITBApp.UI
{
    public partial class AcademicHistoryDetail : ContentPage
    {
        public AcademicHistoryDetail(SubjectData subject)
        {
            InitializeComponent();

            if (Device.RuntimePlatform == Device.UWP)
            {
                ToolbarItems.Add(new ToolbarItem("Volver", "pop_arrow_left.png", () =>
                {
                    NavigationPage historyPage = new NavigationPage(new AcademicHistory());
                    historyPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                    historyPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                    App.MasterDetailPage.Detail = historyPage;
                }));
            }

            lblSubjectName.Text = subject.Title;
            lblSubjectCredits.Text = subject.Credits;
            lblSubjectCommission.Text = subject.Commission;
            lblSubjectCode.Text = subject.Code;
            imgIcon.Source = subject.Icon;

            if (subject.Marks.Count > 0)
                lvMarks.ItemsSource = subject.Marks;
            else {
                var marks = new ObservableCollection<MarkData>();
                marks.Add(new MarkData() {
                    HasFinalMark = false,
                    ClassMark = subject.ClassMark,
                    ClassMarkColor = subject.StatusColor,
                    FinalMark = subject.FinalMark,
                    FinalMarkColor = subject.FinalMarkColor,
                    ActNumber = "---"
                });
                lvMarks.ItemsSource = marks;
            }
                

		}

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}
    }
}
