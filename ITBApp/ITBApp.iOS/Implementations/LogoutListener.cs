﻿using Foundation;
using ITBApp.Listeners;

[assembly: Xamarin.Forms.Dependency(typeof(ITBApp.iOS.Implementations.LogoutListener))]
namespace ITBApp.iOS.Implementations
{
    [Preserve(AllMembers = true)]
    public class LogoutListener : ILogoutListener
	{
		void ILogoutListener.DidLogout()
		{
			//SignIn.SharedInstance.SignOutUser();
		}
	}
}
