﻿using System;

[assembly: Xamarin.Forms.Dependency(typeof(ITBApp.Droid.Implementations.AndroidLog))]
namespace ITBApp.Droid.Implementations
{
	public class AndroidLog : ILog
	{
		public AndroidLog()
		{
		}

		public void WriteLine(string line)
		{
			Console.WriteLine(line);
		}

		public void WriteLine(Exception exception)
		{
			Console.WriteLine(exception);
		}
	}
}
