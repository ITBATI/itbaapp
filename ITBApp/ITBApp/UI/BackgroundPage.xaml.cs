﻿using System.Threading.Tasks;
using ITBApp.Interfaces;
using ITBApp.Resources.i18n;
using Plugin.Connectivity;
using Xamarin.Auth;
using Xamarin.Forms;

namespace ITBApp.UI
{
    public partial class BackgroundPage : ContentPage
	{
		public BackgroundPage()
		{
			InitializeComponent();
			Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();

			if (!CrossConnectivity.Current.IsConnected)
			{
				Navigation.PushModalAsync(new LoginFail(Translate.loginpage_noInternet_message));
			}
			else
			{
                if (App.IsTesting)
                {
                    Task.Run(async()=>{
                        await App.SaveTokenAsync("12345","12345",int.MaxValue);
                    });
                }
                else {
                    if (!App.IsLoggedIn)
                    {
                        Page localNavPage = new GoogleLoginPage();
                        localNavPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                        localNavPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                        Navigation.PushModalAsync(localNavPage);
                    }
                }
			}


		}
	}
}

