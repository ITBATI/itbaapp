﻿//-----------------------------------------------------------------------
// <copyright file="NotificationsViewModel.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ITBApp.ViewModels
{
    using System;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Reflection;
    using System.Threading.Tasks;
    using Model.DataModel;
    using Model.Handlers;
    using ITBApp.Interfaces;
    using Resources.i18n;
    using Services;
    using UI;
    using Xamarin.Forms;
    using ITBApp.Handlers;
    using ITBApp.DataModel;

    /// <summary>
    /// Notifications Context View Model
    /// </summary>
    internal class NotificationsViewModel : BaseModel
    {
        /// <summary>
        /// Notification list   
        /// </summary>
        private ObservableCollection<NotificationData> notifications = new ObservableCollection<NotificationData>();

        /// <summary>
        /// Indicates if notification list is empty
        /// </summary>
        private bool notificationListIsEmpty = false;

        /// <summary>
        /// Indicates if student is male   
        /// </summary>
        private bool isMale;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationsViewModel"/> class
        /// </summary>
        public NotificationsViewModel()
        {
            Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
        }

        /// <summary>
        /// Gets notification list  
        /// </summary>
        public ObservableCollection<NotificationData> BindableNotifications
        {
            get 
            { 
                return this.notifications; 
            }

            private set
            {
                this.notifications = value;
                this.OnPropertyChanged("BindableNotifications");
            }
        }

        /// <summary>
        /// Gets a value indicating whether notification list is empty
        /// </summary>
        public bool NotificationListIsEmpty
        {
            get 
            { 
                return this.notificationListIsEmpty; 
            }

            private set
            {
                this.notificationListIsEmpty = value;
                this.OnPropertyChanged("NotificationListIsEmpty");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether student is male 
        /// </summary>
        public bool IsMale
        {
            get
            {
                return this.isMale;
            }

            set
            {
                this.isMale = value;
                this.OnPropertyChanged("IsMale");
            }
        }

        /// <summary>
        /// Load data from services
        /// </summary>
        /// <returns>
        /// Error message
        /// </returns>
        public override async Task<string> LoadDataAsync()
        {
            ObservableCollection<NotificationData> temp = new ObservableCollection<NotificationData>();

            try
            {
                this.IsLoading = true;
                this.ErrorMessage = null;

                NotificationsService service = new NotificationsService();
                NotificationListData data = await service.getNotificationsAsync().ConfigureAwait(false);
                UserData userData = new LoginService().GetSavedUserData();

                foreach (NotificationData notification in data.notifications)
                {
                    notification.TapCommand = new Command(this.NavigateToNotificationDetail);
                    notification.Title = notification.Title.ToUpper();
                    temp.Add(notification);
                }

                this.BindableNotifications = temp;
                this.IsMale = userData.Gender == UserGender.Male;
            }
            catch (ServerRequestException ex)
            {
                this.ErrorMessage = ex.Message;
            }
            catch (TokenExpiredException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                this.ErrorMessage = Translate.notifications_error_message;
            }

            this.IsLoading = false;
            this.NotificationListIsEmpty = this.BindableNotifications.Count == 0;

            return this.ErrorMessage;
        }

        /// <summary>
        /// Event when page is opened or maximized
        /// </summary>
        /// <param name="sender">
        /// Object that sends the event
        /// </param>
        /// <param name="eventArgs">
        /// Event parameters
        /// </param>
        protected override async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
        {
            await this.LoadDataAsync().ConfigureAwait(false);
            ((NotificationsPage)CurrentPage).InitConnectivityMessage();
        }

        /// <summary>
        /// Navigate to notification detail    
        /// </summary>
        /// <param name="notificationData">
        /// Notification data that will be shown in details
        /// </param>
        private void NavigateToNotificationDetail(object notificationData)
        {
            NotificationsPage.NavigateToNotificationDetail((NotificationsPage)CurrentPage, notificationData);
        }
    }
}