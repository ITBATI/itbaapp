﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ITBApp.Model.Handlers;
using ITBApp.Resources.i18n;
using ITBApp.Selectors;
using ITBApp.Services;
using System.Linq;
using Xamarin.Forms;
using ITBApp.Helpers;

namespace ITBApp.ViewModels
{
    internal class SurveyEntryViewModel : BaseModel
    {
        private ObservableCollection<SurveyEntrySelector> surveyEntrySelectionsModel = new ObservableCollection<SurveyEntrySelector>();
        private SurveyService surveyService = new SurveyService();
        private bool showSurveys = false;

        public SurveyEntryViewModel()
        {            
            SurveyEntrySelectionsModel.Clear();
        }
        		
		public ObservableCollection<SurveyEntrySelector> SurveyEntrySelectionsModel
		{
			get
			{
				return this.surveyEntrySelectionsModel;
			}

			private set
			{
				this.surveyEntrySelectionsModel = value;
				this.OnPropertyChanged("SurveyEntrySelectionsModel");
			}
		}

        public bool ShowSurveys
		{
			get
			{
				return this.showSurveys;
			}

			private set
			{
				this.showSurveys = value;
				this.OnPropertyChanged("ShowSurveys");
			}
		}
		protected override async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
		{			
            App.CurrentPage = PageId.SURVEY;            		
		}

        public override async Task<string> LoadDataAsync()
        {
            if (!IsLoading)
            {
                try
                {
                    IsLoading = true;
                    ShowSurveys = false;
                    SurveyEntrySelectionsModel.Clear();
                    this.ErrorMessage = null;

                    var activeSurveys = await surveyService.GetActiveSurveysAsync().ConfigureAwait(false);
                    if (activeSurveys == null || activeSurveys.Count <= 1)
                    {
                        SurveyEntryPage.NavigateToSurvey((SurveyEntryPage)CurrentPage);
                    }

                    var surveys = new ObservableCollection<SurveyEntrySelector>();

                    var unanswerdedSurveys = await surveyService.GetListUnansweredSurveyAsync().ConfigureAwait(false);
                    //var activeSurveys = await surveyService.GetActiveSurveysAsync().ConfigureAwait(false);

                    //Cargo las encuestas que no fueron contestadas!
                    foreach (var survey in unanswerdedSurveys)
                    {
                        surveys.Add(new SurveyEntrySelector()
                        {
                            Id = survey.Id,
                            Question = survey.Description,
                            HasResult = false,
                            QuestionTextColor = Color.FromHex("#77D5CD"),
                            QuestionBackgroundColor = Color.FromHex("#002b4e")
                        });
                    }

                    //Obtengo las encuestas activas y contestadas!
                    foreach (var survey in activeSurveys)
                    {
                        var searchedSurvey = surveys.Where(x => x.Id == survey.Id).FirstOrDefault();

                        if (searchedSurvey == null)
                        {
                            surveys.Add(new SurveyEntrySelector()
                            {
                                Id = survey.Id,
                                Question = survey.Description,
                                HasResult = true,
                                QuestionTextColor = Color.White,
                                QuestionBackgroundColor = Color.FromHex("#002539")
                            });
                        }
                    }

                    SurveyEntrySelectionsModel = surveys;

                    ShowSurveys = true;
                }
			    catch (ServerRequestException ex)
			    {
				    this.ErrorMessage = ex.Message;
			    }
			    catch (Exception exc)
			    {
				    this.ErrorMessage = Translate.surveypage_error_text;				
			    }

                await Task.Delay(100);
			    this.IsLoading = false;
            }

            return this.ErrorMessage;            
        }

        public bool HasResultSurvey(int id)
        {
            return this.SurveyEntrySelectionsModel.Where(x => x.Id == id).First().HasResult;
        }

        private void GenerateFakeSurveys()
        {
			var fakeData = new ObservableCollection<SurveyEntrySelector>();

			fakeData.Add(new SurveyEntrySelector()
			{
				Id = 1,
				HasResult = false,
				Question = "Que te parece la nueva wifi del ITBA en la Sede Central?"
			});

			fakeData.Add(new SurveyEntrySelector()
			{
				Id = 2,
				HasResult = false,
				Question = "Qué funcionalidad te gustaría que agreguemos al ITBA app en nuestro próxumo release?"
			});

			fakeData.Add(new SurveyEntrySelector()
			{
				Id = 3,
				HasResult = false,
				Question = "Cómo es tu experiencia usando los CIR?"
			});

			fakeData.Add(new SurveyEntrySelector()
			{
				Id = 4,
				HasResult = true,
				Question = "Asistirías a algunas clases a través de video chat?"
			});

			fakeData.Add(new SurveyEntrySelector()
			{
				Id = 5,
				HasResult = true,
				Question = "Cómo venis con los finales?"
			});

			fakeData.Add(new SurveyEntrySelector()
			{
				Id = 6,
				HasResult = true,
				Question = "Cómo venis con los finales?"
			});

			this.SurveyEntrySelectionsModel = fakeData;
            
        }
    }
}
