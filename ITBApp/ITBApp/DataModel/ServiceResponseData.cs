﻿using System.Runtime.Serialization;

namespace ITBApp.DataModel
{
	[DataContract]
    public class ServiceResponseData
    {
		[DataMember]
        public bool Success { get; set; }

		[DataMember]
        public string Message { get; set; }
    }
}
