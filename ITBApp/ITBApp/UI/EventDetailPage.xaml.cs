﻿using System;
using System.Collections.Generic;
using ITBApp.ViewModels;
using Xamarin.Forms;
using System.Linq;
using Plugin.Connectivity;
using ITBApp.Resources.i18n;
using ITBApp.Services;
using ITBApp.Interfaces;

namespace ITBApp.UI
{
    public partial class EventDetailPage : ContentPage
    {
        private List<EventViewModelData> eventList;
        private string EventId;

        public EventDetailPage(List<EventViewModelData> events)
        {
			InitializeComponent();

            if (Device.RuntimePlatform == Device.UWP)
            {
                ToolbarItems.Add(new ToolbarItem("Volver", "pop_arrow_left.png", () =>
                {
                    NavigationPage eventsPage = new NavigationPage(new EventsPage());
                    eventsPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                    eventsPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                    App.MasterDetailPage.Detail = eventsPage;
                }));
            }

            ((BaseModel)BindingContext).Initialize(this);

            eventList = events;

            if (events != null && events.Count > 0)
            {
                ((EventDetailViewModel)BindingContext).EventTitle = events.First().Title;
                ((EventDetailViewModel)BindingContext).EventDescription = events.First().Comments;

                var eventUrl = events.First().Url;

                ((EventDetailViewModel)BindingContext).EventUrl = eventUrl;
                if (!string.IsNullOrEmpty(eventUrl) && ! ((EventDetailViewModel)BindingContext).EventUrl.StartsWith("http", StringComparison.CurrentCulture))
                    ((EventDetailViewModel)BindingContext).EventUrl = "http://" + ((EventDetailViewModel)BindingContext).EventUrl;

                EventId = events.First().Id;

                ((EventDetailViewModel)BindingContext).Quota = events.First().Quota;
                ((EventDetailViewModel)BindingContext).Enrolled = events.First().Enrolled;
                ((EventDetailViewModel)BindingContext).IsEnrolled = events.First().IsEnrolled;

                ((EventDetailViewModel)BindingContext).ShowInformationButton = ! string.IsNullOrEmpty(((EventDetailViewModel)BindingContext).EventUrl);
                ((EventDetailViewModel)BindingContext).ShowEventInscription = !events.First().IsEnrolled && events.First().ShowInscription;                                                      
			}

            lvDays.ItemsSource = ((EventDetailViewModel)BindingContext).GetEventsSelector(events, ((EventDetailViewModel)BindingContext).EventDescription);
        }

        public void MoreInfoButtomTapped(Object sender, EventArgs args)
        {
            if (((EventDetailViewModel)BindingContext).ShowInformationButton && !isTapped)
            {
                isTapped = true;
                var eventUrl = ((EventDetailViewModel)BindingContext).EventUrl;

                if (!string.IsNullOrWhiteSpace(eventUrl))
                {
                    Device.OpenUri(new Uri(eventUrl));
                }
                isTapped = false;
            }
        }

        public async void InscriptionButtomTapped(Object sender, EventArgs args)
        {
            if (((EventDetailViewModel)BindingContext).ShowEventInscription && !isTapped)
			{
                isTapped = true;
				var answer = await DisplayAlert("Eventos", string.Format("Estás por inscribirte a \n\n {0}.\n\n ¿Estás seguro?", ((EventDetailViewModel)BindingContext).EventTitle), "SI", "NO");
				if (answer)
				{
					await new EventsService().EnrollAsync(EventId);
					await Navigation.PushAsync(new EventInscriptionResponsePage(false));
				}
                isTapped = false;
            }
        }

        private bool isTapped = false;

		public async void ExportCalendarTapped(Object sender, EventArgs args)
		{
			if (await this.DisplayAlert(
					Translate.events_title,
					Translate.events_export_question,
					Translate.yes,
					Translate.no))
			{
				if (!CrossConnectivity.Current.IsConnected)
				{
					await DisplayAlert(Translate.classes_by_day_title, Translate.notifications_not_connected, Translate.Ok);
				}
				else
				{
					try
					{
                        if (!isTapped)
                        {
                            isTapped = true;
                            acitivyIndicator.IsVisible = true;
                            acitivyIndicator.IsRunning = true;
                            DependencyService.Get<IAppMethods>().showQuickMessage(
                                await((EventDetailViewModel)BindingContext).ExportSchedule(eventList));
                            acitivyIndicator.IsVisible = false;
							acitivyIndicator.IsRunning = false;
                        }
					}
					catch (Exception)
					{
						await DisplayAlert(Translate.events_title, Translate.schedule_calendar_authorization, Translate.Ok);
					}
					isTapped = false;
				}
			}
		}

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}
    }
}
