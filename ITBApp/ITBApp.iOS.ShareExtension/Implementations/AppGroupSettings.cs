﻿using System;
using Foundation;
using ITBApp.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(ITBApp.iOS.Implementations.AppGroupSettings))]
namespace ITBApp.iOS.Implementations
{
    public class AppGroupSettings : IAppGroupSettings
    {
        private const string ITBAappGroup = "group.ITBAapp";

        public void Clear()
        {
			// Load all
			NSUserDefaults shared = new NSUserDefaults(
			ITBAappGroup,
			NSUserDefaultsType.SuiteName);
            
            shared.RemovePersistentDomain(NSBundle.MainBundle.BundleIdentifier);
            shared.Synchronize();
        }

        public object Get(string key)
		{
			// Load all
			NSUserDefaults shared = new NSUserDefaults(
			ITBAappGroup,
			NSUserDefaultsType.SuiteName);
            
            return shared.StringForKey(key);
		}

		public void Set(string key, string value)
		{
			NSUserDefaults shared = new NSUserDefaults(
			ITBAappGroup,
			NSUserDefaultsType.SuiteName);
            
            // set values
            shared.SetString(value, key);
            shared.Synchronize();
		}
    }
}
