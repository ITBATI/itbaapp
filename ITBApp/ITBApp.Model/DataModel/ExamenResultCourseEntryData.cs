﻿﻿using System;
using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel
{
	[DataContract]
	[KnownType(typeof(ExamenResultCourseEntryData))]
    public class ExamenResultCourseEntryData
    {
        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <value>The date.</value>
        [DataMember] 
        public string Date { get; set; }
        /// <summary>
        /// Gets or sets the final mark.
        /// </summary>
        /// <value>The final mark.</value>
        [DataMember] 
        public string FinalMark { get; set; }
        /// <summary>
        /// Gets or sets the final mark.
        /// </summary>
        /// <value>The final mark.</value>
        [DataMember] 
        public string ActNumber { get; set; }
    }
}
