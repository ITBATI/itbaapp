﻿using System;
using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel
{
	[DataContract]
	public class UserProfileData
	{
        // Status
        public const string RegularStatus = "REGULAR";
        public const string StandByStatus = "STAND_BY";
        public const string ExchangeInStatus = "EXCHANGE_IN";
        public const string ExchangeOutStatus = "EXCHANGE_OUT";
        public const string ExchangeEndStatus = "EXCHANGE_END";
        public const string QuitStatus = "QUIT";
        public const string ExpelledStatus = "EXPELLED";
        public const string GraduatedStatus = "GRADUATED";

        // Values
        public const string RegularValue = "Regular";
        public const string StandByValue = "No regular - Latente";
        public const string ExchangeInValue = "Regular - Intercambio IN";
        public const string ExchangeOutValue = "Regular - Intercambio OUT";
        public const string ExchangeEndValue = "Baja - Intercambio IN";
        public const string QuitValue = "Baja - Abandono";
        public const string ExpelledValue = "Baja - Separación";
        public const string GraduatedValue = "Egresado";
        public const string NotRegularValue = "No regular";

		public static string getStatusTranslation(string statusInput)
		{
			if (statusInput == RegularStatus) return RegularValue;
			else if (statusInput == StandByStatus) return StandByValue;
			else if (statusInput == ExchangeInStatus) return ExchangeInValue;
			else if (statusInput == ExchangeOutStatus) return ExchangeOutValue;
			else if (statusInput == ExchangeEndStatus) return ExchangeEndValue;
			else if (statusInput == QuitStatus) return QuitValue;
			else if (statusInput == ExpelledStatus) return ExpelledValue;
			else if (statusInput == GraduatedStatus) return GraduatedValue;
			else return NotRegularValue;
		}

		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public string ImagePath { get; set; }
		[DataMember]
		public string CourseOfStudy { get; set; }
		[DataMember]
		public string Status { get; set; }
		[DataMember]
		public string ImpressionShare { get; set; }
		[DataMember]
		public string TimeInUniversity { get; set; }
		[DataMember]
		public string CourseProgress { get; set; }
		[DataMember]
		public double CourseProgressPercentage { get; set; }
		[DataMember]
		public string Average { get; set; }
		[DataMember]
		public string LinealAverage { get; set; }
		[DataMember]
		public DateTime Expiration { get; set; }
		[DataMember]
		public string CurrentCredits { get; set; }
	}
}
