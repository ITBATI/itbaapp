﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Input;

namespace ITBApp.Model.DataModel
{
	[DataContract]
	[KnownType(typeof(TimetableData))]
	[KnownType(typeof(CommissionData))]
    public class CourseData
    {
		[DataMember]
        public string Id { get; set; }

		[DataMember]
		public string Code { get; set; }

		[DataMember]
        public string Name { get; set; }

		[DataMember]
		public TimetableData CurrentTimetable { get; set; }

		[DataMember]
		public string CommissionId { get; set; }

        public ICommand TapCommand { get; set; }

		[DataMember]
		public List<CommissionData> Commissions { get; set; }

        public int FontSizeClassroom { 
            get
            {
				if (FormattedClassroom.Length > 5) return 16;
				if (FormattedClassroom.Length > 10) return 14;
				if (FormattedClassroom.Length > 15) return 12;
                return 18;
            }
        }

        public string FormattedClassroom
        {
            get
            {
                // TODO: revisar si sigue haciendo falta esto
                return this.CurrentTimetable.Classroom.Substring(this.CurrentTimetable.Classroom.IndexOf(":") + 1);
            }
        }
        public bool HasLongClassroom
        {
            get
            {
                return this.FormattedClassroom.Length > 5;
            }
        }

		public double FontSize { get; set; }
    }
}
