﻿using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel
{
	[DataContract]
	public class ExamData
	{
		[DataMember]
		public string Allowed { get; set; }

		[DataMember]
		public string Taken { get; set; }

		[DataMember]
		public string Description { get; set; }

		[DataMember]
		public string Date { get; set; }
	}
}
