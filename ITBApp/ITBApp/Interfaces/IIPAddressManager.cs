﻿using System;
using System.Collections.Generic;

namespace ITBApp.Interfaces
{
    public interface IIPAddressManager
    {
        List<string> GetIPAddress();
    }
}
