﻿using System;
using ITBApp.Listeners;

[assembly: Xamarin.Forms.Dependency(typeof(ITBApp.Droid.Implementations.LogoutListener))]
namespace ITBApp.Droid.Implementations
{
	public class LogoutListener : ILogoutListener
	{
		public LogoutListener()
		{
		}

		void ILogoutListener.DidLogout()
		{
			// nada
			// Este metodo se incluye por compatibilidad con IOS
		}
	}
}
