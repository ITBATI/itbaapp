﻿using Xamarin.Forms;

namespace ITBApp.Selectors
{
	public class SurveyDataTemplateSelector : DataTemplateSelector
	{
		public DataTemplate ChoiceTemplate { get; set; }
		public DataTemplate WritingChoiceTemplate { get; set; }
		public DataTemplate SendSurveyTemplate { get; set; }

		protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
		{
			SurveySelectorModel selectorModel = (SurveySelectorModel)item;


			if (selectorModel.IsSendButton)
				return SendSurveyTemplate;

			if (!selectorModel.IsWritingChoice)
			{
				return ChoiceTemplate;
			}
			else
			{
				return WritingChoiceTemplate;
			}
		}
	}
}
