//-----------------------------------------------------------------------
// <copyright file="EventsViewModel.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using ITBApp.Helpers;
using ITBApp.Model.DataModel;
using ITBApp.Model.Handlers;
using ITBApp.Interfaces;
using ITBApp.Resources.i18n;
using ITBApp.Services;
using ITBApp.UI;
using ITBApp.Util.Helpers;
using Xamarin.Forms;

namespace ITBApp.ViewModels
{
	/// <summary>
	/// Events Context View Model
	/// </summary>
	internal class EventsViewModel : BaseModel
    {
		public string[] MonthNames = { Translate.January, Translate.February, Translate.March, Translate.April,
			Translate.May, Translate.June, Translate.July, Translate.August, Translate.September, Translate.October,
			Translate.November, Translate.December };

        public bool IsIos {
            get {
                return Device.RuntimePlatform == Device.iOS;
            }
        }

        private MonthsData month;
		public MonthsData Month
		{
			get
			{
				return this.month;
			}

			private set
			{
				this.month = value;
				this.OnPropertyChanged("Month");
			}
		}

        private bool displayMonthName = false;
        public bool DisplayMonthName
		{
			get
			{
				return this.displayMonthName;
			}

			private set
			{
				this.displayMonthName = value;
				this.OnPropertyChanged("DisplayMonthName");
			}
		}


        private Dictionary<string, List<EventViewModelData>> EventsById = new Dictionary<string, List<EventViewModelData>>();

        public EventsViewModel()
        {
            Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
        }

        public List<EventViewModelData> GetEventsFromId(string id)
        {
            if (EventsById.ContainsKey(id))
                return EventsById[id];
            
            return new List<EventViewModelData>();
        }

		private void NavigateToDetail(object eventId)
		{
            EventsPage.NavigateToDetail((EventsPage)CurrentPage, eventId);
		}

        public override async Task<string> LoadDataAsync()
        {
            try
            {
                this.IsLoading = true;
                this.ErrorMessage = null;
                this.DisplayMonthName = false;

                var tempMonth = new MonthsData();

                EventsService service = new EventsService();

                List<string> enrolledEventsList = await service.GetEnrolledEventsIdAsync().ConfigureAwait(false);

                DateTime iterationDay = DateTime.Today;
				string[] daysNames = { Translate.Sunday, Translate.Monday, Translate.Tuesday, Translate.Wednesday,
											Translate.Thursday, Translate.Friday, Translate.Saturday };

                EventsData data = await service.GetEventsAsync().ConfigureAwait(false);

				for (int iteration = 1; iteration <= ServiceConstants.EVENTS_DAYS_SHOWN; iteration++)
				{
					Color backgroundColor = ViewModelConstants.MondayBackgroundColor;
					if (iterationDay.DayOfWeek == DayOfWeek.Tuesday) backgroundColor = ViewModelConstants.TuesdayBackgroundColor;
					else if (iterationDay.DayOfWeek == DayOfWeek.Wednesday) backgroundColor = ViewModelConstants.WednesdayBackgroundColor;
					else if (iterationDay.DayOfWeek == DayOfWeek.Thursday) backgroundColor = ViewModelConstants.ThursdayBackgroundColor;
					else if (iterationDay.DayOfWeek == DayOfWeek.Friday) backgroundColor = ViewModelConstants.FridayBackgroundColor;
					else if (iterationDay.DayOfWeek == DayOfWeek.Saturday) backgroundColor = ViewModelConstants.SaturdayBackgroundColor;
					else if (iterationDay.DayOfWeek == DayOfWeek.Sunday) backgroundColor = ViewModelConstants.SundayBackgroundColor;

                    int leftMargin = -10;
                    if (iterationDay.DayOfWeek == DayOfWeek.Sunday) leftMargin -= 4;
                    if (iterationDay.Day.ToString().Length == 1) leftMargin += 4;

					EventDayData dayData = new EventDayData()
					{
						Date = iterationDay,
						Name = DateHelper.getShortName(iterationDay),
						Margin = new Thickness(leftMargin, 0, 0, 0),
						Number = " " + iterationDay.Day,
						DayColor = backgroundColor,
                        LastDay = iterationDay.Day == DateTime.DaysInMonth(iterationDay.Year,iterationDay.Month),
						DayTextColor = iterationDay.DayOfWeek == DayOfWeek.Saturday ? Color.Black : Color.White
					};

					if (tempMonth.Days == null) 
                        tempMonth = new MonthsData()
					    {
                            IsNewMonth = iterationDay.Day == 1,
                            Days = new ObservableCollection<EventDayData>()
					};

					var bindableMonth = tempMonth;

					addEventDayDataToWeek(bindableMonth, dayData);
					iterationDay = iterationDay.AddDays(1);
				}

                foreach (EventEntryData eventData in data.Events)
                {
                    DateTime eventDateTime = DateTime.ParseExact(eventData.Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    var eDayData = findEventDayData(tempMonth, eventDateTime);

                    if (eDayData == null)
                        eDayData = findEventDayData(tempMonth, eventDateTime);

                    bool showInscription = false;

                    if (eventData.InscriptionStart != null && eventData.InscriptionEnd != null)
                    {
                        DateTime startDate = DateTime.ParseExact(eventData.InscriptionStart, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        DateTime endDate = DateTime.ParseExact(eventData.InscriptionEnd, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        if (DateTime.Now >= startDate && DateTime.Now <= endDate)
                        {
                            if (int.Parse(eventData.Quota) == 0 || int.Parse(eventData.Quota) > int.Parse(eventData.Enrolled))
                            {
                                showInscription = true;
                            }
                        }
                    }

                    if (eventData.DateUntil == null)
					{
                        if (eDayData != null && eventData.Time[0].ITBAClassroom != null)
                        {
                            eDayData.Event1 = new EventViewModelData()
                            {
                                ShowInscription = showInscription,
                                Id = eventData.Id,
                                From = eventData.Time[0].From + " hs",
                                To = eventData.Time[0].To + " hs",
                                ITBAClassroom = eventData.Time[0].ITBAClassroom,
                                Classroom = eventData.Time[0].Classroom,
                                Title = eventData.Description.ToUpper(),
                                Comments = eventData.Comments,
                                Url = eventData.Url,
                                Day = eventDateTime.DayOfWeek,
                                DayColor = eDayData.DayColor,
                                DayTextColor = eDayData.DayTextColor,
                                DayName = daysNames[(int)DateTime.ParseExact(eventData.Date, "dd/MM/yyyy", CultureInfo.InvariantCulture).DayOfWeek],
                                EventInstanceDate = eventDateTime,
                                DateDescription = eventDateTime.Day + " de " + MonthNames[eventDateTime.Month - 1].Substring(0,3) + ".",
                                Location = eventData.Time[0].Building,
                                Enrolled = eventData.Enrolled,
                                Quota = eventData.Quota,
                                IsEnrolled = enrolledEventsList.Contains(eventData.Id),
                                TapCommand = new Command(() => NavigateToDetail(eventData.Id))
                            };

                            if (!EventsById.ContainsKey(eventData.Id)) EventsById[eventData.Id] = new List<EventViewModelData>();
							//if (EventsById[eventData.Id].Where(x => x.Day == dayData.Event1.Day).FirstOrDefault() == null)
								EventsById[eventData.Id].Add(eDayData.Event1);
                        }
					}
                    else
                    {
                        if (eventDateTime < DateTime.Today)
                            eDayData = tempMonth.Days.First();
                        
                        DateTime dateUntil = DateTime.ParseExact(eventData.DateUntil, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                        while (eDayData != null && 
                               eDayData.Date < dateUntil) 
                        {
							foreach (EventTimeDetail timeDetail in eventData.Time) 
                            {
                                if (timeDetail.ITBAClassroom != null && DateHelper.getDayOfWeekFromDayShortName(timeDetail.DayShortName) ==  
                                    eDayData.Date.DayOfWeek)
                                {
                                    var eventVMData = new EventViewModelData()
                                    {
                                        ShowInscription = showInscription,
                                        Id = eventData.Id,
										From = timeDetail.From + " hs",
										To = timeDetail.To + " hs",
										ITBAClassroom = timeDetail.ITBAClassroom,
										Classroom = timeDetail.Classroom,
										Title = eventData.Description.ToUpper(),
                                        Location = timeDetail.Building,
                                        Comments = eventData.Comments,
                                        Url = eventData.Url,
                                        Day = eDayData.Date.DayOfWeek,
										DayColor = eDayData.DayColor,
                                        DayTextColor = eDayData.DayTextColor,
										DayName = daysNames[(int)eDayData.Date.DayOfWeek],
                                        EventInstanceDate = eDayData.Date,
										DateDescription = eDayData.Date.Day + " de " + MonthNames[eDayData.Date.Month - 1].Substring(0, 3) + ".",
                                        Enrolled = eventData.Enrolled,
                                        Quota = eventData.Quota,
                                        IsEnrolled = enrolledEventsList.Contains(eventData.Id),
                                        TapCommand = new Command(() => NavigateToDetail(eventData.Id))
									};

                                    if (eDayData.Event1 == null) eDayData.Event1 = eventVMData;
                                    else if (eDayData.Event2 == null) eDayData.Event2 = eventVMData;
                                    else if (eDayData.Event3 == null) eDayData.Event3 = eventVMData;
									else if (eDayData.Event4 == null) eDayData.Event4 = eventVMData;
									else if (eDayData.Event5 == null) eDayData.Event5 = eventVMData;
									else if (eDayData.Event6 == null) eDayData.Event6 = eventVMData;
									else if (eDayData.Event7 == null) eDayData.Event7 = eventVMData;
									else if (eDayData.Event8 == null) eDayData.Event8 = eventVMData;
									else if (eDayData.Event9 == null) eDayData.Event9 = eventVMData;
									else if (eDayData.Event10 == null) eDayData.Event10 = eventVMData;
                                    else if (eDayData.Event11 == null) eDayData.Event11 = eventVMData;
                                    else if (eDayData.Event12 == null) eDayData.Event12 = eventVMData;
                                    else if (eDayData.Event13 == null) eDayData.Event13 = eventVMData;
                                    else if (eDayData.Event14 == null) eDayData.Event14 = eventVMData;
                                    else if (eDayData.Event15 == null) eDayData.Event15 = eventVMData;

                                    if (!EventsById.ContainsKey(eventData.Id)) EventsById[eventData.Id] = new List<EventViewModelData>();
                                    //if (EventsById[eventData.Id].Where(x => x.Day == eventVMData.Day).FirstOrDefault() == null)
                                        EventsById[eventData.Id].Add(eventVMData);
                                }
                            }
                            
                            var dayDataDateTime = eDayData.Date;
                            var nextDayDataTime = dayDataDateTime.AddDays(1);

							eDayData = findEventDayData(tempMonth, nextDayDataTime);
                        }
                    }
                }

                Month = tempMonth;
                this.DisplayMonthName = true;
			}
			catch (ServerRequestException ex)
			{
				this.ErrorMessage = ex.Message;
			}
			catch (Exception exception)
			{
				this.ErrorMessage = "No podemos recuperar los datos de eventos. Por favor intentá más tarde.";
			}

            await Task.Delay(100);
            this.IsLoading = false;

			return this.ErrorMessage;
        }

        private EventDayData findEventDayData(MonthsData week, DateTime date) {
            foreach (EventDayData edd in week.Days){
                if (edd.Date.Date == date.Date) return edd;
            }
            return null;
        }

		/// <summary>
		/// Cleans view model cache
		/// </summary>
		/// <returns>
		/// No value is returned
		/// </returns>
		public override async Task DeleteCache()
		{
			EventsService service = new EventsService();
			await service.DeleteCachedDataAsync();
		}

		protected override async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
		{
			App.CurrentPage = PageId.EVENTS;

            if (this.Month == null)
			{
				await this.LoadDataAsync().ConfigureAwait(false);
			}
		}

        private void addEventDayDataToWeek(MonthsData week, EventDayData eventDayData) {
            week.Days.Add(eventDayData);
        }
    }

    public class MonthsData : INotifyPropertyChanged
    {
        private IList<EventDayData> days;
		public IList<EventDayData> Days
		{
			get { return days; }
			set
			{
				days = value;
				NotifyPropertyChanged("Days");
			}
		}

        public bool IsNewMonth { get; set; }

		public event PropertyChangedEventHandler PropertyChanged;
		private void NotifyPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
    }

    public class EventDayData : INotifyPropertyChanged
    {
        public string FormattedDate 
        { 
            get 
            {
                return Date.ToString("dd/MM/yyyy");
            }
        }
        public DateTime Date { get; set; }

        private Thickness margin;
        public Thickness Margin
		{
			get { return margin; }
			set
			{
				margin = value;
				NotifyPropertyChanged("Margin");
			}
		}

        private string name;
		public string Name
		{
			get { return name; }
			set
			{
				name = value;
				NotifyPropertyChanged("Name");
			}
		}

		private string number;
		public string Number
		{
			get { return number; }
			set
			{
				number = value;
				NotifyPropertyChanged("Number");
			}
		}

        public Color DayColor { get; set; }
        public bool LastDay { get; set; }
        public Color DayTextColor { get; set; }

        private EventViewModelData event1;
		public EventViewModelData Event1
		{
			get { return event1; }
			set
			{
				event1 = value;
				NotifyPropertyChanged("Event1");
			}
		}

        private EventViewModelData event2 { get; set; }
		public EventViewModelData Event2
		{
			get { return event2; }
			set
			{
				event2 = value;
				NotifyPropertyChanged("Event2");
			}
		}

		private EventViewModelData event3 { get; set; }
		public EventViewModelData Event3
		{
			get { return event3; }
			set
			{
				event3 = value;
				NotifyPropertyChanged("Event3");
			}
		}

		private EventViewModelData event4 { get; set; }
		public EventViewModelData Event4
		{
			get { return event4; }
			set
			{
				event4 = value;
				NotifyPropertyChanged("Event4");
			}
		}

		private EventViewModelData event5 { get; set; }
		public EventViewModelData Event5
		{
			get { return event5; }
			set
			{
				event5 = value;
				NotifyPropertyChanged("Event5");
			}
		}

		private EventViewModelData event6 { get; set; }
		public EventViewModelData Event6
		{
			get { return event6; }
			set
			{
				event6 = value;
				NotifyPropertyChanged("Event6");
			}
		}

		private EventViewModelData event7 { get; set; }
		public EventViewModelData Event7
		{
			get { return event7; }
			set
			{
				event7 = value;
				NotifyPropertyChanged("Event7");
			}
		}

		private EventViewModelData event8 { get; set; }
		public EventViewModelData Event8
		{
			get { return event8; }
			set
			{
				event8 = value;
				NotifyPropertyChanged("Event8");
			}
		}

		private EventViewModelData event9 { get; set; }
		public EventViewModelData Event9
		{
			get { return event9; }
			set
			{
				event9 = value;
				NotifyPropertyChanged("Event9");
			}
		}

        private EventViewModelData event10;
		public EventViewModelData Event10
		{
			get { return event10; }
			set
			{
				event10 = value;
				NotifyPropertyChanged("Event10");
			}
		}

		private EventViewModelData event11;
		public EventViewModelData Event11
		{
			get { return event11; }
			set
			{
				event11 = value;
				NotifyPropertyChanged("Event11");
			}
		}

		private EventViewModelData event12;
		public EventViewModelData Event12
		{
			get { return event12; }
			set
			{
				event12 = value;
				NotifyPropertyChanged("Event12");
			}
		}

		private EventViewModelData event13;
		public EventViewModelData Event13
		{
			get { return event13; }
			set
			{
				event13 = value;
				NotifyPropertyChanged("Event13");
			}
		}

		private EventViewModelData event14;
		public EventViewModelData Event14
		{
			get { return event14; }
			set
			{
				event14 = value;
				NotifyPropertyChanged("Event14");
			}
		}

		private EventViewModelData event15;
		public EventViewModelData Event15
		{
			get { return event15; }
			set
			{
				event15 = value;
				NotifyPropertyChanged("Event15");
			}
		}

        public event PropertyChangedEventHandler PropertyChanged;
		private void NotifyPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
    }

    public class EventViewModelData : INotifyPropertyChanged
    {
        public ICommand tapCommand { get; set; }
		public ICommand TapCommand
		{
			get { return tapCommand; }
			set
			{
				tapCommand = value;
				NotifyPropertyChanged("TapCommand");
			}
		}

        public bool ShowInscription { get; set; }
        public string Quota { get; set; }

        private string title;
		public string Title
		{
			get { return title; }
			set
			{
				title = value;
				NotifyPropertyChanged("Title");
			}
		}

        private string timeFrom;
		public string From
		{
			get { return timeFrom; }
			set
			{
				timeFrom = value;
				NotifyPropertyChanged("From");
			}
		}

        private string to;
		public string To
		{
			get { return to; }
			set
			{
				to = value;
				NotifyPropertyChanged("To");
			}
		}

        private string classroom;
		public string Classroom
		{
			get { return classroom; }
			set
			{
				classroom = value;
				NotifyPropertyChanged("Classroom");
			}
		}

        private string location;
		public string Location
		{
			get { return location; }
			set
			{
				location = value;
				NotifyPropertyChanged("Location");
			}
		}

        private string itbaClassroom;
		public string ITBAClassroom
		{
			get { return itbaClassroom; }
			set
			{
				itbaClassroom = value;
				NotifyPropertyChanged("ITBAClassroom");
			}
		}

        private string enrolled;
		public string Enrolled
		{
			get { return enrolled; }
			set
			{
				enrolled = value;
				NotifyPropertyChanged("Enrolled");
			}
		}

        private bool isEnrolled;
		public bool IsEnrolled
		{
			get { return isEnrolled; }
			set
			{
				isEnrolled = value;
				NotifyPropertyChanged("IsEnrolled");
			}
		}

        private string id;
		public string Id
		{
			get { return id; }
			set
			{
				id = value;
				NotifyPropertyChanged("Id");
			}
		}

        public string Comments { get; set; }
        public string Url { get; set; }
        public DayOfWeek Day { get; set; }
        public Color DayColor { get; set; }
        public Color DayTextColor { get; set; }
        public string DayName { get; set; }
        public string DateDescription { get; set; }
        public DateTime EventInstanceDate { get; set; }

		public event PropertyChangedEventHandler PropertyChanged;
		private void NotifyPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
    }
}
