﻿using ITBApp.Interfaces;
using ITBApp.Model;
using Xamarin.Forms;

namespace ITBApp
{
	public class GoogleLoginPage : ContentPage
	{

		public GoogleLoginPage()
		{
		}

		protected override bool OnBackButtonPressed()
		{
			DependencyService.Get<IAppMethods>().CloseApp();
			return base.OnBackButtonPressed();
		}
	}
}
