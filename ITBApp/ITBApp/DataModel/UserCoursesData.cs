﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ITBApp.Model.DataModel;

namespace ITBApp.DataModel
{
	[DataContract]
	[KnownType(typeof(CoursesByDayData))]
	[KnownType(typeof(CommissionData))]
	public class UserCoursesData : ServiceResponseData
	{
		[DataMember]
		public CoursesByDayData CoursesByDay { get; set; }

		[DataMember]
		public Dictionary<string, List<CommissionData>> Commissions { get; set; }

        [DataMember]
        public DateTime Expiration { get; set;  }

		public bool IsMale { get; set; }
    }
}
