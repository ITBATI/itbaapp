﻿using System;
using CoreGraphics;
using UIKit;

namespace ITBApp.iOS.ShareExtension
{
    public class PrintSettingsViewController : UIViewController
    {
        public UISegmentedControl segmentControl;
        public UIPickerView myPickerView;

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

			nfloat h = 31.0f;
			nfloat w = View.Bounds.Width;

			UILabel typeLabel = new UILabel(new CGRect(10, 10, w - 20, 30));
			typeLabel.Text = "Tipo de Impresión";
			typeLabel.TextColor = UIColor.Black;

			segmentControl = new UISegmentedControl();
			segmentControl.Frame = new CGRect(10, 45, w - 50, 50);

			segmentControl.InsertSegment("Blanco y Negro", 0, false);
			segmentControl.InsertSegment("Color", 1, false);
			segmentControl.SelectedSegment = 0;

			UILabel numberLabel = new UILabel(new CGRect(10, 100, w - 20, 30));
			numberLabel.Text = "Cantidad de copias";
			numberLabel.TextColor = UIColor.Black;

			// Setup the picker and model
			NumberPickerModel model = new NumberPickerModel();

			myPickerView = new UIPickerView(new CGRect(10, 130, w - 20, 50))
			{
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth,
				ShowSelectionIndicator = true,
				Model = model
				//BackgroundColor = backgroundColor,
				//Hidden = true
			};

			View.AddSubview(typeLabel);
			View.AddSubview(segmentControl);
			View.AddSubview(numberLabel);
			View.AddSubview(myPickerView);
        }
    }

	public class NumberPickerModel : UIPickerViewModel
	{
		public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
		{
			return 10;
		}

		public override string GetTitle(UIPickerView pickerView, nint row, nint component)
		{
            return (row+1).ToString();
		}

		public override nint GetComponentCount(UIPickerView pickerView)
		{
			return 1;
		}

	}
}
