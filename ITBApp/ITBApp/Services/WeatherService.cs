﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using ITBApp.Handlers;
using ITBApp.Helpers;
using ITBApp.Model.Handlers;
using ITBApp.Resources.i18n;
using ITBApp.Util.Helpers;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.Services
{
    public class WeatherService
    {
        public async Task DeleteCachedDataAsync()
		{
			Application.Current.Properties.Remove(ServiceConstants.WEATHER_DATA);
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
		}

		private WeatherData GetSavedWeatherData()
		{
			if (Application.Current.Properties.ContainsKey(ServiceConstants.WEATHER_DATA))
			{
				string json = (string)Application.Current.Properties[ServiceConstants.WEATHER_DATA];
				WeatherData data = (WeatherData)SerializerHelper.Deserialize(json, (new WeatherData()).GetType());

				if (data.Expiration > DateTime.Now || !CrossConnectivity.Current.IsConnected)
				{
					return data;
				}
			}

			return null;
		}

		private async void SaveWeatherData(WeatherData data)
		{
			string json = SerializerHelper.Serialize(data);
			Application.Current.Properties[ServiceConstants.WEATHER_DATA] = json;
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);
		}

		public async Task<WeatherData> GetWeatherAsync()
		{
			WeatherData weatherData = GetSavedWeatherData();

			if (weatherData == null)
			{
				if (!CrossConnectivity.Current.IsConnected)
				{
					throw new ServerRequestException(Translate.weather_no_internet_text);
				}

				try
				{
					string token = await new TokenHolder().GetTokenAsync().ConfigureAwait(false);

					try
					{
						var stringResponse = await HttpUtility
							.MakeGetCallAsync(String.Format("{0}{1}", AppSettings.WeatherURL, token))
							.ConfigureAwait(false);

						weatherData = ParseWeatherResponse(stringResponse);
						weatherData.Expiration = DateTime.Now.AddMinutes(ServiceConstants.WEATHER_EXPIRATION_IN_MINUTES);

						SaveWeatherData(weatherData);
					}
					catch
					{
						throw new ServerRequestException(Translate.weather_service_error);
					}
				}
				catch (TokenExpiredException)
				{
					throw new ServerRequestException(Translate.weather_no_internet_text);
				}

			}

			return weatherData;
		}

		private WeatherData ParseWeatherResponse(string response)
		{
			var result = new WeatherData();

			try
			{
				JObject jsonResponse = JObject.Parse(response);

				result.WeatherText = (string)jsonResponse["WeatherText"];
				result.Temperature = (string)jsonResponse["Temperature"];
				result.Unit = (string)jsonResponse["Unit"];
				result.WeatherIcon = (string)jsonResponse["WeatherIcon"];
			}
			catch (Exception exc)
			{
				throw new ServerRequestException("Error al procesar los datos de información del clima");
			}

			return result;
		}

        public async Task<string> getWeatherImageAsync(string locationId)
        {
            var weatherId = 0;

            try {
                var weather = await GetWeatherAsync().ConfigureAwait(false);

                if (weather != null && weather.WeatherIcon != null)
                {
                    int iconId = int.Parse(weather.WeatherIcon);

                    if (iconId >= 1 && iconId <= 5)
                        weatherId = 0;
                    else if (iconId >= 6 && iconId <= 11)
                        weatherId = 1;
                    else if (iconId >= 12 && iconId <= 14)
                        weatherId = 2;
                    else if (iconId >= 15 && iconId <= 18)
                        weatherId = 3;
                    else if (iconId >= 22 && iconId <= 29)
                        weatherId = 2;
                    else if (iconId >= 33 && iconId <= 37)
                        weatherId = 4;
                    else if (iconId == 38)
                        weatherId = 5;
                    else if (iconId >= 39 && iconId <= 40)
                        weatherId = 6;
                    else if (iconId >= 41 && iconId <= 44)
                        weatherId = 7;
                }
            }
            catch {
                // weatherId = 0
            }

            return "sede_" + locationId + "_" + weatherId + ".jpg";
        }
    }

	[DataContract]
	[KnownType(typeof(WeatherData))]
	public class WeatherData
	{
        [DataMember]
		public string WeatherText { get; set; }
        [DataMember]
		public string Temperature { get; set; }
        [DataMember]
		public string Unit { get; set; }
        [DataMember]
		public string WeatherIcon { get; set; }
        [DataMember]
		public DateTime Expiration { get; set; }
	}
}
