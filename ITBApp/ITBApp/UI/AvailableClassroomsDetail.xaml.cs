﻿using System;
using System.Collections;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ITBApp.UI
{
    public partial class AvailableClassroomsDetail : ContentPage
    {
        public AvailableClassroomsDetail(string title, string location, IEnumerable classrooms)
        {
            InitializeComponent();

            titleClassrooms.Text = title;

            if (string.IsNullOrEmpty(location))
            {
                titleLocation.IsVisible = false;
            }
            else
            {
                titleLocation.Text = location;
            }
            lvClassrooms.ItemsSource = classrooms;

            if (Device.RuntimePlatform == Device.UWP)
            {
                ToolbarItems.Add(new ToolbarItem("Volver", "pop_arrow_left.png", () =>
                {
                    NavigationPage availableclassroomsPage = new NavigationPage(new AvailableClassrooms());
                    availableclassroomsPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                    availableclassroomsPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                    App.MasterDetailPage.Detail = availableclassroomsPage;
                }));
            }
        }

        public void DisableSelection(Object sender, EventArgs args)
        {
        	((ListView)sender).SelectedItem = null;
        }
    }
}
