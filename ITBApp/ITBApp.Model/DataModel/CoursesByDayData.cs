﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel
{
	[DataContract]
	[KnownType(typeof(CourseData))]
	public class CoursesByDayData
	{
		[DataMember]
		private List<CourseData>[] daysOfWeek = new List<CourseData>[7];

		public CoursesByDayData()
		{
			for (int day = 0; day < 7; day++) daysOfWeek[day] = new List<CourseData>();
		}

		public void AddCourseToDay(CourseData course)
		{
			daysOfWeek[course.CurrentTimetable.Day].Add(course);
		}

		public List<CourseData> GetCoursesOfDay(DayOfWeek day)
		{
			return daysOfWeek[(int)day];
		}

		public List<CourseData> GetCoursesOfDay(int day)
		{
			return daysOfWeek[day];
		}

		public bool HasCourses(DayOfWeek day)
		{
			return GetCoursesOfDay(day).Count > 0;
		}

		public void sort()
		{
			for (int day = 0; day < 7; day++) daysOfWeek[day] = daysOfWeek[day].OrderBy(o => o.CurrentTimetable.StartTime).ToList();
		}
	}
}
