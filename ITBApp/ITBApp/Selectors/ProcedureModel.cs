﻿using System;
namespace ITBApp.Selectors
{
    public class ProcedureModel
    {
        public string Name
        {
            get;
            set;
        }

		public string Date
		{
			get;
			set;
		}

		public string Comments
		{
			get;
			set;
		}

		public string Assignee
		{
			get;
			set;
		}

		public string Status
		{
			get;
			set;
		}

		public string Icon
		{
			get;
			set;
		}
    }
}
