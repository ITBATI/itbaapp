﻿using System;
using System.Runtime.Serialization;
using System.Windows.Input;
using Xamarin.Forms;

namespace ITBApp.Model.DataModel
{
	public enum NotificationTypes
	{
		Seguridad,
		Eventos,
		Administrativa,
		SecretaríaAcadémica,
		TI
	}

	[DataContract]
	public class NotificationData
	{
        public const string Elypsis = "...";
        public const string TimeFormat = "HH:mm";
        public const string DateFormat = "dd/MM/yyyy";

        // Background Images
        public const string NotificationSecurityBackgroundImageName = "notification_security_bg.jpg";
        public const string NotificationAdministrativeBackgroundImageName = "notification_administrative_bg.jpg";
        public const string NotificationEventsBackgroundImageName = "notification_events_bg.jpg";
        public const string NotificationSABackgroundImageName = "notification_sa_bg.jpg";
        public const string NotificationITBackgroundImageName = "notification_it_bg.jpg";

        // Icon Images
        public const string NotificationSecurityIconName = "notification_security_icon.png";
        public const string NotificationAdministrativeIconName = "notification_administrative_icon.png";
        public const string NotificationEventsIconName = "notification_events_icon.png";
        public const string NotificationSAIconName = "notification_sa_icon.png";
        public const string NotificationITIconName = "notification_it_icon.png";

		[DataMember]
		public Int64? Id { get; set; }
		[DataMember]
		public string Title { get; set; }
		[DataMember]
		public string Type { get; set; }
		[DataMember]
		public string Description { get; set; }
		[DataMember]
		public string Source { get; set; }
		[DataMember]
		public DateTime Date { get; set; }
		[DataMember]
		public DateTime DueDate { get; set; }
		[DataMember]
		public string Email { get; set; }
		[DataMember]
		public string Url { get; set; }

		public ICommand TapCommand { get; set; }

		public string ShortDescription
		{
			get {
                if (Device.RuntimePlatform == Device.UWP) return Description;
                if (Description.Length <= ModelSettings.NOTIFICATION_LIST_DESCRIPTION_LENGTH) return Description;
				return Description.Substring(0, ModelSettings.NOTIFICATION_LIST_DESCRIPTION_LENGTH) + Elypsis;
			}
		}

		public string FormattedDate
		{
			get {
				if (this.Date.Date == DateTime.Today.Date) 
					return this.Date.ToString(TimeFormat);
				return this.Date.ToString(DateFormat); 
			}
		}

		public string BackgroundImageSource
		{
			get
			{
                string typeCleaned = this.Type.Replace(" ", string.Empty);
				if (typeCleaned == NotificationTypes.Seguridad.ToString())
					return NotificationSecurityBackgroundImageName;
				if (typeCleaned == NotificationTypes.Administrativa.ToString())
					return NotificationAdministrativeBackgroundImageName;
				if (typeCleaned == NotificationTypes.Eventos.ToString())
					return NotificationEventsBackgroundImageName;
				if (typeCleaned == NotificationTypes.SecretaríaAcadémica.ToString())
					return NotificationSABackgroundImageName;
				return NotificationITBackgroundImageName;
			}
		}

        public string IconSourceLarge
        {
            get
            {
                string typeCleaned = this.Type.Replace(" ", string.Empty);
                if (typeCleaned == NotificationTypes.Seguridad.ToString())
                    return NotificationSecurityIconName;
                if (typeCleaned == NotificationTypes.Administrativa.ToString())
                    return NotificationAdministrativeIconName;
                if (typeCleaned == NotificationTypes.Eventos.ToString())
                    return NotificationEventsIconName;
                if (typeCleaned == NotificationTypes.SecretaríaAcadémica.ToString())
                    return NotificationSAIconName;
                return NotificationITIconName;
            }
        }

        public string IconSource { 
			get {
                string windowsPrefix = Device.RuntimePlatform == Device.UWP ? "small" : string.Empty;

                string typeCleaned = this.Type.Replace(" ", string.Empty);
				if (typeCleaned == NotificationTypes.Seguridad.ToString())
					return windowsPrefix + NotificationSecurityIconName;
				if (typeCleaned == NotificationTypes.Administrativa.ToString())
					return windowsPrefix + NotificationAdministrativeIconName;
				if (typeCleaned == NotificationTypes.Eventos.ToString())
					return windowsPrefix + NotificationEventsIconName;
				if (typeCleaned == NotificationTypes.SecretaríaAcadémica.ToString())
					return windowsPrefix + NotificationSAIconName;
				return windowsPrefix + NotificationITIconName;
			} 
		}

		public Color HeaderColor
		{
			get
			{
				Color bgColor;
                string typeCleaned = this.Type.Replace(" ", string.Empty);

                if (typeCleaned == NotificationTypes.Seguridad.ToString())
					bgColor = Color.FromHex("#DC1456");
				else if (typeCleaned == NotificationTypes.Administrativa.ToString())
					bgColor = Color.FromHex("#EF5F33");
				else if (typeCleaned == NotificationTypes.Eventos.ToString())
					bgColor = Color.FromHex("#123533");
                else if (typeCleaned == NotificationTypes.SecretaríaAcadémica.ToString())
					bgColor = Color.FromHex("#3C113F");
				else
					bgColor = Color.FromHex("#002A47");
				
		       	return bgColor.MultiplyAlpha(0.5);
			}
		}
	}
}
