﻿using Newtonsoft.Json.Linq;

namespace ITBApp.Helpers
{
	public class JsonHelper
	{
		public static JArray GetAsArray(JToken json)
		{
			JArray collection = new JArray();
			if (json is JObject)
			{
				collection.Add(JObject.Parse(json.ToString()));
			}
			else
			{
				collection = json.Value<JArray>();
			}

			if (collection == null)
			{
				collection = new JArray();
			}

			return collection;
		}
	}
}
