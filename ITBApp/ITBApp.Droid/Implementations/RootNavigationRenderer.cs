﻿using Android.App;
using Android.Graphics.Drawables;
using ITBApp.Droid.Implementations;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(NavigationPage), typeof(RootNavigationRenderer))]
namespace ITBApp.Droid.Implementations
{
	public class RootNavigationRenderer: NavigationRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<NavigationPage> e)
		{
			base.OnElementChanged(e);

			RemoveAppIconFromActionBar();
		}

		void RemoveAppIconFromActionBar()
		{
			var actionBar = ((Activity)Context).ActionBar;
			actionBar.SetIcon(new ColorDrawable(Color.Transparent.ToAndroid()));
		}
	}
}

