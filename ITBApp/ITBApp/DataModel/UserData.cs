﻿using System.Runtime.Serialization;

namespace ITBApp.DataModel
{
	[DataContract]
	public enum UserGender
	{
		[EnumMember]
		Male,
		[EnumMember]
		Female
	}

	[DataContract]
	public class UserData : ServiceResponseData
	{
		[DataMember]
		public string CardId { get; set; }

		[DataMember]
		public string LastName { get; set; }

		[DataMember]
		public string FirstName { get; set; }

		[DataMember]
		public string DNI { get; set; }

		[DataMember]
		public string Email { get; set; }

		[DataMember]
		public UserGender Gender { get; set; }

		public string FullName
		{
			get
			{
				if (FirstName != null && LastName != null)
				{
					return FirstName + " " + LastName;
				}

				return string.Empty;
			}
		}
	}
}
