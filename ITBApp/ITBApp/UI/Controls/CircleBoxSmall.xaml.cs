﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ITBApp.UI.Controls
{
	public partial class CircleBoxSmall : ContentView
	{
		public static readonly BindableProperty CountProperty = BindableProperty.Create("Count", typeof(string), typeof(CircleBox), "-");
		public static readonly BindableProperty TitleProperty = BindableProperty.Create("Title", typeof(string), typeof(CircleBox), null);
		public static readonly BindableProperty IsActiveProperty = BindableProperty.Create("IsActive", typeof(bool), typeof(CircleBox), false);

		public CircleBoxSmall()
		{
			InitializeComponent();
			Content.BindingContext = this;
		}

		public string Count
		{
			get { return (string)GetValue(CountProperty); }
			set
			{
				SetValue(CountProperty, value);
			}
		}

		public string Title
		{
			get { return (string)GetValue(TitleProperty); }
			set { SetValue(TitleProperty, value); }
		}

		public bool IsActive
		{
			get { return (bool)GetValue(IsActiveProperty); }
			set { SetValue(IsActiveProperty, value); }
		}

		private Command _tappedEvent;
		public Command TappedEvent
		{
			get { return _tappedEvent; }
			set
			{
				_tappedEvent = value;

				TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer
				{
					Command = _tappedEvent
				};
				slCircle.GestureRecognizers.Add(tapGestureRecognizer);
				lblCount.GestureRecognizers.Add(tapGestureRecognizer);
				lblTitle.GestureRecognizers.Add(tapGestureRecognizer);
			}
		}
	}
}
