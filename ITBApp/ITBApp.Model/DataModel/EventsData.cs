﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel
{
    [DataContract]
    [KnownType(typeof(EventsData))]
    public class EventsData
    {
        [DataMember]
        public DateTime Expiration { get; set; }
        [DataMember]
        public List<EventEntryData> Events { get; set; }

    }

	[DataContract]
    [KnownType(typeof(EventEntryData))]
    public class EventEntryData 
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Date { get; set; }
        [DataMember]
        public string DateUntil { get; set; }
        [DataMember]
        public string InscriptionStart { get; set; }
        [DataMember]
        public string InscriptionEnd { get; set; }
        [DataMember]
        public string Quota { get; set; }
        [DataMember]
        public string Enrolled { get; set; }
        [DataMember]
        public List<EventTimeDetail> Time { get; set; }
    }

	[DataContract]
	[KnownType(typeof(EventTimeDetail))]
    public class EventTimeDetail
    {
        [DataMember]
		public string From { get; set; }
        [DataMember]
		public string To { get; set; }
        [DataMember]
		public string ITBAClassroom { get; set; }
        [DataMember]
		public string Classroom { get; set; }
        [DataMember]
        public string DayShortName { get; set; }
        [DataMember]
        public string Building { get; set; }
    }

}
