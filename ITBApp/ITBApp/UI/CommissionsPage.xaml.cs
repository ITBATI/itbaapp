﻿using ITBApp.Model.DataModel;
using ITBApp.Resources.i18n;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Xamarin.Forms;

namespace ITBApp.UI
{
	public partial class CommissionsPage : ContentPage
    {
		ObservableCollection<CommissionViewModel> BindableCommissionsMain = new ObservableCollection<CommissionViewModel>();
		ObservableCollection<CommissionViewModel> BindableCommissionsTotal = new ObservableCollection<CommissionViewModel>();

		public void seeMoreCommissions(object sender, EventArgs args)
		{
			lvMainCommission.ItemsSource = BindableCommissionsTotal;
		}

        public CommissionsPage(string courseName, List<CommissionData> commissions, string commissionId)
        {
            InitializeComponent();

            if (Device.RuntimePlatform == Device.UWP)
            {
                ToolbarItems.Add(new ToolbarItem("Volver", "pop_arrow_left.png", () =>
                {
                    NavigationPage schedulePage = new NavigationPage(new Schedule());
                    schedulePage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                    schedulePage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                    App.MasterDetailPage.Detail = schedulePage;
                }));
            }

            lblClassName.Text = courseName.ToUpper();

			ObservableCollection<CommissionViewModel> BindableCommissions = new ObservableCollection<CommissionViewModel>();

			if (commissions.Count > 0)
			{
				foreach (CommissionData commission in commissions)
				{
					if (commission.Id == commissionId)
					{
						// commission
						BindableCommissionsMain.Add(new CommissionViewModel()
						{
							CommissionName = Translate.commissionspage_commission_name.Replace("{0}", commission.Name),
							IsCommissionTitle = true
						});

                        createDaysForCollection(commission, BindableCommissionsMain);

                        // TODO: Se quitan los profesores hasta que vengan del servicio
						//BindableCommissionsMain.Add(createTeachersTitleViewModel());
                        //createTeachersForCollection(commission.Teachers, BindableCommissionsMain);

                        // More commissions Button
                        if (commissions.Count > 1)
						{
							BindableCommissionsMain.Add(new CommissionViewModel()
							{
								IsMoreCommissionsButton = true
							});
						}

						lvMainCommission.ItemsSource = BindableCommissionsMain;
					}
					else
					{
						// commission
						BindableCommissions.Add(new CommissionViewModel()
						{
							CommissionName = Translate.commissionspage_commission_name.Replace("{0}", commission.Name),
							IsCommissionTitle = true
						});
                        createDaysForCollection(commission, BindableCommissions);
						// TODO: Se quitan los profesores hasta que vengan del servicio
                        //BindableCommissions.Add(createTeachersTitleViewModel());
                        //createTeachersForCollection(commission.Teachers, BindableCommissions);
                        BindableCommissions.Add(createEndLineViewModel());
                    }
				}

				foreach (CommissionViewModel commissionView in BindableCommissionsMain)
				{
					if (!commissionView.IsMoreCommissionsButton)
						BindableCommissionsTotal.Add(commissionView);
					else {
						BindableCommissionsTotal.Add(createEndLineViewModel());
					}
				}
				foreach (CommissionViewModel commissionView in BindableCommissions)
				{
					BindableCommissionsTotal.Add(commissionView);
				}

			}
        }

		private CommissionViewModel createEndLineViewModel() {
			return new CommissionViewModel() {
				IsEndLine = true
			};
		}

		private CommissionViewModel createTeachersTitleViewModel()
		{
			return new CommissionViewModel()
			{
				IsTeachersTitle = true
			};
		}

		private void createTeachersForCollection(List<string> teacherCollection, 
		                                    ObservableCollection<CommissionViewModel> collection)
		{
			foreach (string teacher in teacherCollection)
			{
				collection.Add(new CommissionViewModel
				{
					IsTeacherData = true,
					TeacherName = teacher
				});
			}
		}

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}

		private void createDaysForCollection(CommissionData commissionData,
                                            ObservableCollection<CommissionViewModel> collection)
		{
			string[] daysNames = { Translate.Sunday, Translate.Monday, Translate.Tuesday, Translate.Wednesday,
											Translate.Thursday, Translate.Friday, Translate.Saturday };

            foreach (TimetableData course in commissionData.Timetables)
            {
				collection.Add(new CommissionViewModel()
				{
					IsClassDay = true,
					ClassDay = new ClassDayViewModel()
					{
						DayName = daysNames[course.Day],
						DayMainBackgroundColor = DayData.GetMainBackgroundColorForDay(course.Day),
						DaySecondaryBackgroundColor = Color.FromHex("#353E54"), //DayData.GetSecondaryBackgroundColorForDay(course.Day),
						StartTime = course.StartTime,
						EndTime = course.EndTime,
						Classroom = course.Classroom,
						Location = course.Location
					}
				});
			}
		}
        
    }
}
