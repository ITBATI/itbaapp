﻿using System;

namespace ITBApp.Model.Handlers
{
    public class ServerRequestException: Exception
    {
        public ServerRequestException(string message):base(message)
        {

        }
    }
}
