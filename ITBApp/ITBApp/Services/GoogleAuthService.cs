﻿using System;
using ITBApp.Interfaces;
using Xamarin.Auth;
using Xamarin.Forms;

namespace ITBApp.Services
{
    public class GoogleAuthService
    {
        public OAuth2Authenticator GetAuthenticator() 
        {
            if (Device.RuntimePlatform == Device.iOS)
            {
                string clientId = AppSettings.AuthenticatorClientIdIos;

                string clientSecret = clientSecret = null;

                string redirectUrl = "com.googleusercontent.apps.843843481588-0bm5i908b7sng9bu63il6agcukggeftb:/oauth2redirect";

                return new Xamarin.Auth.OAuth2Authenticator
                 (
                    clientId: clientId,
                    clientSecret: clientSecret,
                    authorizeUrl: new Uri("https://accounts.google.com/o/oauth2/auth"),
                    accessTokenUrl: new Uri("https://www.googleapis.com/oauth2/v4/token"),
                                  redirectUrl: new Uri(redirectUrl),
                    scope: "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/gmail.send",
                    getUsernameAsync: null,
                    isUsingNativeUI: true
                                 )
                 {
                     AllowCancel = false,
                 };
            }

            return null;
        }

        public bool PresentUILoginScreen(OAuth2Authenticator presentedAuthenticator)
        {
            // Presenters Implementation
            Xamarin.Auth.Presenters.OAuthLoginPresenter presenter = null;
            presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
            presenter.Login(presentedAuthenticator);

            return true;
        }

    }
}
