﻿namespace ITBApp
{
	public interface IMailMessageGenerator
	{
		string CreateMailMessage(string from, string to, byte[] attachment = null, string fileName = null, string mediaType = null, string mediaSubType = null);
	}
}
