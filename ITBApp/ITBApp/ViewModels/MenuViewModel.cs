﻿//-----------------------------------------------------------------------
// <copyright file="MenuViewModel.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ITBApp.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using ITBApp.DataModel;
    using ITBApp.Handlers;
    using ITBApp.Model.DataModel;
    using ITBApp.Model.Handlers;
    using ITBApp.Resources.i18n;
    using ITBApp.Services;

    /// <summary>
    /// Menu Context View Model
    /// </summary>
    internal class MenuViewModel : BaseModel
    {
        /// <summary>
        /// Login service instance  
        /// </summary>
        private LoginService service = new LoginService();

        /// <summary>
        /// Exams list Data   
        /// </summary>
        private UserData userData;

        /// <summary>
        /// Avatar image path   
        /// </summary>
        private string avatarImgSource;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuViewModel"/> class  
        /// </summary>
        public MenuViewModel()
        {
        }

        /// <summary>
        /// Gets or sets the current user data   
        /// </summary>
        public UserData CurrentUserData
        {
            get
            {
                return this.userData;
            }

            set
            {
                this.userData = value;
                this.OnPropertyChanged("CurrentUserData");
            }
        }

        /// <summary>
        /// Gets or sets the avatar image path   
        /// </summary>
        public string AvatarImgSource
        {
            get
            {
                return this.avatarImgSource;
            }

            set
            {
                this.avatarImgSource = value;
                this.OnPropertyChanged("AvatarImgSource");
            }
        }

        /// <summary>
        /// Fills view model data
        /// </summary>
        /// <returns>
        /// Error message  
        /// </returns>
        public override async Task<string> LoadDataAsync()
        {
            try
            {
                this.CurrentUserData = await this.service.GetUserDataAsync();

                if (this.CurrentUserData.Gender == UserGender.Female)
                {
                    this.AvatarImgSource = "https://i.imgur.com/qThz1mv.png";
                }
                else
                {
                    this.AvatarImgSource = "https://i.imgur.com/yPS1jqY.png";
                }

                return this.CurrentUserData.FullName;
            }
            catch (TokenExpiredException)
            {
            }
            catch {
                this.ErrorMessage = Translate.login_error_message;
                return this.ErrorMessage;
            }

            return string.Empty;
        }
    }
}
