﻿using System;
using Xamarin.Forms;

namespace ITBApp
{
	/// <summary>
    /// Automatically detects the Phone numbers, web links and email addresses
    /// and make them clickable to be opened up in default device applications
    /// </summary>
	public class HyperLinkLabel : Label
	{
		public HyperLinkLabel()
		{
		}
	}
}
