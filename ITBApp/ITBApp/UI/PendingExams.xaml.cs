﻿using System;
using ITBApp.Helpers;
using ITBApp.ViewModels;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.UI
{
	public partial class PendingExams : ContentPage
	{
		public PendingExams()
		{
			InitializeComponent();

			((BaseModel)BindingContext).Initialize(this);

            if (Device.RuntimePlatform == Device.UWP)
            {
                ToolbarItems.Add(new ToolbarItem("Volver", "pop_arrow_left.png", () =>
                {
                    NavigationPage profilePage = new NavigationPage(new UserProfile());
                    profilePage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                    profilePage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                    App.MasterDetailPage.Detail = profilePage;
                }));
            }

            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
				if (App.CurrentPage == PageId.PENDING_EXAMS && args.IsConnected)
				{
					if (((PendingExamsViewModel)BindingContext).Exams == null
						|| ((PendingExamsViewModel)BindingContext).Exams.Count == 0)
					{
						((PendingExamsViewModel)BindingContext).LoadDataAsync().ConfigureAwait(false);
					}

				}
			};
		}

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}
	}
}
