using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ITBApp.Helpers;
using ITBApp.Resources.i18n;
using ITBApp.Selectors;
using ITBApp.Services;
using ITBApp.Model.Handlers;
using System.Linq;
using System.Globalization;

namespace ITBApp.ViewModels
{
    internal class ProceduresViewModel : BaseModel
    {
        private WorkRequestService service = new WorkRequestService();

        private bool showProcedures = false;
		/// <summary>
		/// List of survey selection models   
		/// </summary>
		private ObservableCollection<ProcedureModel> proceduresModel = new ObservableCollection<ProcedureModel>();


        /// <summary>
        /// Gets a value indicating whether show procedures.
        /// </summary>
        /// <value><c>true</c> if show procedures; otherwise, <c>false</c>.</value>
        public bool ShowProcedures
		{
			get
			{
				return this.showProcedures;
			}

			private set
			{
				this.showProcedures = value;
				this.OnPropertyChanged("ShowProcedures");
			}
		}

        /// <summary>
        /// Gets the procedures model.
        /// </summary>
        /// <value>The procedures model.</value>
		public ObservableCollection<ProcedureModel> ProceduresModel
		{
			get
			{
				return this.proceduresModel;
			}

			private set
			{
				this.proceduresModel = value;
				this.OnPropertyChanged("ProceduresModel");
			}
		}

        public override async Task<string> LoadDataAsync()
        {
			try
			{
    			this.IsLoading = true;
    			this.ErrorMessage = null;
                ShowProcedures = true;

                WorkRequestData data = await service.GetWorkRequestsAsync().ConfigureAwait(false);


                if (data.Changesets.Count == 0)
                {
                    this.ShowProcedures = false;
                    this.IsLoading = false;

                    return string.Empty;
                }

                var rest = new ObservableCollection<ProcedureModel>();
                var changesetsOrdered = data.Changesets.OrderByDescending(x => x.CreatedOn).ToList();

                foreach (WorkRequestChangesetData workData in changesetsOrdered)
                {
                    var status = string.Empty;
                    var icon = string.Empty;
                    if (workData.Resolution.ToUpper() == "ACCEPTED") 
                    {
                        status = "Aprobada";
                        icon = "historial_aprobada.png";
                    }
                    else if (workData.Resolution.ToUpper() == "REJECTED") 
                    {
                        status = "Rechazado";
                        icon = "historial_desaprobada.png";
                    } 
                    else 
                    {
                        status = "Pendiente";
                        icon = "historial_warning.png";
                    }

                    rest.Add(new ProcedureModel(){
                        Name = workData.Title.ToUpper(),
                        Comments = string.IsNullOrWhiteSpace(workData.LastResponse) ? "---" : workData.LastResponse.Trim(),
                        Date = workData.CreatedOn.ToString("dd/MM/yyyy"),
                        Assignee = workData.Assignee,
                        Status = status,
                        Icon = icon
                    });
                }

                ProceduresModel = rest;
			}
			catch (ServerRequestException ex)
			{
				this.ErrorMessage = ex.Message;
			}
			catch (Exception exception)
			{
                this.ErrorMessage = Translate.work_request_service_error;
			}

			this.IsLoading = false;
			this.ShowProcedures = true;

			return this.ErrorMessage;
        }

		/// <summary>
		/// Cleans view model cache
		/// </summary>
		/// <returns>
		/// No value is returned
		/// </returns>
		public override async Task DeleteCache()
		{
			await this.service.DeleteCachedDataAsync();
		}

		/// <summary>
		/// Event when Procedures page is open or maximized
		/// </summary>
		/// <param name="sender">
		/// Object that sends the event
		/// </param>
		/// <param name="eventArgs">
		/// Event parameters
		/// </param>
		protected override async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
		{
			App.CurrentPage = PageId.PROCEDURES;

            if (this.ProceduresModel.Count == 0)
			{
				await this.LoadDataAsync().ConfigureAwait(false);
			}
		}

		/// <summary>
		/// Event when Procedures page is closed or minimized
		/// </summary>
		/// <param name="sender">
		/// Object that sends the event
		/// </param>
		/// <param name="eventArgs">
		/// Event parameters
		/// </param>
		protected override void CurrentPageOnDisappearing(object sender, EventArgs eventArgs)
		{
            App.CurrentPage = PageId.PROCEDURES;
		}
    }
}
