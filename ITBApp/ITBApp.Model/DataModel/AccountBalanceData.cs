﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ITBApp.Model.DataModel
{
	[DataContract]
	[KnownType(typeof(PaymentData))]
	public class AccountBalanceData
	{
		[DataMember]
		public List<PaymentData> Payments { get; set; }

		[DataMember]
		public DateTime Expiration { get; set; }
	}
}
