﻿using System;
using Xamarin.Forms;
namespace ITBApp.Selectors
{
    public class SurveyEntrySelector
    {
		public int Id { get; set; }
		public String Question { get; set; }
        public bool HasResult { get; set; }
        public Color QuestionTextColor { get; set; }
        public Color QuestionBackgroundColor { get; set; }
    }
}