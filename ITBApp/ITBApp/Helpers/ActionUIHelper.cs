﻿using System;
using Xamarin.Forms;

namespace ITBApp.Helpers
{
    public class ActionUIHelper
    {
        private static Color SelectionColor = Color.FromHex("#DC1456").MultiplyAlpha(0.5);

        public static void OpenPage(MainMenuItem menuItem)
		{
			CleanSelections();
			menuItem.ViewItem.BackgroundColor = SelectionColor;

			App.CurrentPage = menuItem.Id;
			App.MasterDetailPage.Detail = menuItem.NavigationPage;
			MainMenuItem.FreeClassroomsItem.NavigationPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
			MainMenuItem.FreeClassroomsItem.NavigationPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

            if (Device.RuntimePlatform != Device.UWP)
				App.MasterDetailPage.IsPresented = false;
		}

        private static void CleanSelections()
		{
			foreach (MainMenuItem item in MainMenuItem.Values())
			{
				if (item.ViewItem != null)
				{
					item.ViewItem.BackgroundColor = Color.Transparent;
				}
			}
		}
    }
}
