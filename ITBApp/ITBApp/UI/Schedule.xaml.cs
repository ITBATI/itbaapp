﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ITBApp.Helpers;
using ITBApp.Model.DataModel;
using ITBApp.Interfaces;
using ITBApp.Resources.i18n;
using ITBApp.Services;
using ITBApp.ViewModels;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.UI
{
	public partial class Schedule : ContentPage
	{
		private static readonly int PAGE_LIMIT = 6;
		private static bool isTapped = false;
		private bool positionLoaded = false;

		public Schedule()
		{
			InitializeComponent();
			Translate.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();

			((BaseModel)BindingContext).Initialize(this);

			ConfigureEventListeners();

			ToolbarItems.Add(new ToolbarItem("Calendar", "gcalendar.png", async () =>
			{
				if (await this.DisplayAlert(
					Translate.classes_by_day_title,
					Translate.schedule_export_question,
					Translate.yes,
					Translate.no))
				{
					if (!CrossConnectivity.Current.IsConnected)
					{
						await DisplayAlert(Translate.classes_by_day_title, Translate.notifications_not_connected, Translate.Ok);
					}
					else
					{
						try
						{
							isTapped = true;
                            acitivyIndicator.IsVisible = true;
                            acitivyIndicator.IsRunning = true;
							DependencyService.Get<IAppMethods>().showQuickMessage(
								await ((ClassesByDayViewModel)BindingContext).ExportSchedule());
                            acitivyIndicator.IsVisible = false;
                            acitivyIndicator.IsRunning = false;
						}
						catch (Exception)
						{
							await DisplayAlert(Translate.classes_by_day_title, Translate.schedule_calendar_authorization, Translate.Ok);
						}
						isTapped = false;
					}
				}
			}));
		}

		void ConfigureEventListeners()
		{
			cvDays.ItemSelected += (sender, args) =>
			{
				SetIndicator(Color.White);
			};

			btnMonday.Clicked += (sender, args) =>
			{
				cvDays.Position = 0;
			};
			btnTuesday.Clicked += (sender, args) =>
			{
				cvDays.Position = 1;
			};
			btnWednesday.Clicked += (sender, args) =>
			{
				cvDays.Position = 2;
			};
			btnThursday.Clicked += (sender, args) =>
			{
				cvDays.Position = 3;
			};
			btnFriday.Clicked += (sender, args) =>
			{
				cvDays.Position = 4;
			};
			btnSaturday.Clicked += (sender, args) =>
			{
				cvDays.Position = 5;
			};

			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
				if (BindingContext != null && App.CurrentPage == PageId.CLASSES_BY_DAY && args.IsConnected)
				{
					if (cvDays.ItemsSource == null || !cvDays.ItemsSource.Cast<object>().Any())
					{
						LoadPage();
					}
				}
			};
		}

		void LoadPage()
		{
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
			((ClassesByDayViewModel)BindingContext).LoadDataAsync();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
		}

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}

		private void SetIndicator(Color color)
		{
			btnMonday.BackgroundColor = Color.Transparent;
			btnTuesday.BackgroundColor = Color.Transparent;
			btnWednesday.BackgroundColor = Color.Transparent;
			btnThursday.BackgroundColor = Color.Transparent;
			btnFriday.BackgroundColor = Color.Transparent;
			btnSaturday.BackgroundColor = Color.Transparent;

			if (!positionLoaded && cvDays.ItemsSource != null && cvDays.ItemsSource.Cast<object>().Count() == PAGE_LIMIT)
			{
				cvDays.Position = App.DaysPosition;
				positionLoaded = true;
			}

			int position = cvDays.Position;

			if (position == 0) btnMonday.BackgroundColor = color;
			else if (position == 1) btnTuesday.BackgroundColor = color;
			else if (position == 2) btnWednesday.BackgroundColor = color;
			else if (position == 3) btnThursday.BackgroundColor = color;
			else if (position == 4) btnFriday.BackgroundColor = color;
			else if (position == 5) btnSaturday.BackgroundColor = color;
		}

		public async static void NavigateToCommissions(Schedule instance, object courseData)
		{
            if (Device.RuntimePlatform == Device.UWP)
            {
                CourseData course = (CourseData)courseData;
                ClassesByDayService classesService = new ClassesByDayService();
                List<CommissionData> commissions = classesService.getCommissionsForCourse(course.Id);

                NavigationPage commissionsPage = new NavigationPage(new CommissionsPage(course.Name, commissions, course.CommissionId));
                commissionsPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                commissionsPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

                App.MasterDetailPage.Detail = commissionsPage;
            }
			else if (!isTapped)
			{
				isTapped = true;
				CourseData course = (CourseData)courseData;
				ClassesByDayService classesService = new ClassesByDayService();

				List<CommissionData> commissions = classesService.getCommissionsForCourse(course.Id);
				await instance.Navigation.PushAsync(new CommissionsPage(course.Name, commissions, course.CommissionId));
				isTapped = false;
			}
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			if (!isTapped)
			{
				App.DaysPosition = cvDays.Position;
				this.BindingContext = null;
				this.Content = null;
			}
            App.HardwareBackPressed = null;
		}

        protected override void OnAppearing()
        {
        	base.OnAppearing();

        	App.HardwareBackPressed = () =>
        	{
        		MainMenuItem item = MainMenuItem.HomeItem;
        		App.CurrentPage = item.Id;
        		item.NavigationPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
        		item.NavigationPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
        		App.MasterDetailPage.Detail = item.NavigationPage;
        		return Task.FromResult<bool?>(false);
        	};
        }

	}
}
