﻿using ITBApp.Model;
using Microsoft.WindowsAzure.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Core;
using Windows.Graphics.Display;
using Windows.Networking.PushNotifications;
using Windows.UI.ViewManagement;
using Xamarin.Forms;

[assembly: Dependency(typeof(ITBApp.UWP.Implementations.AppMethods))]
namespace ITBApp.UWP.Implementations
{
    class AppMethods : IAppMethods
    {
        public void AddPhoneContact(string name, string phoneNumber, string phoneLabel)
        {
            var contact = new Windows.ApplicationModel.Contacts.Contact();
            contact.Name = name;

            var contactPhone = new Windows.ApplicationModel.Contacts.ContactPhone();
            contactPhone.Number = phoneNumber;
            contactPhone.Description = phoneLabel;

            contact.Phones.Add(contactPhone);

            Task.Run(async () =>
            {
                try
                {
                    //Get he contact store for the app (so no lists from outlook and other stuff will be in the returned lists..)
                    var contactstore = await Windows.ApplicationModel.Contacts.ContactManager.RequestStoreAsync(Windows.ApplicationModel.Contacts.ContactStoreAccessType.AppContactsReadWrite);

                    var contactLists = await contactstore.FindContactListsAsync();
                    Windows.ApplicationModel.Contacts.ContactList contactList;

                    //if there is no contact list we create one
                    if (contactLists.Count == 0)
                    {
                        contactList = await contactstore.CreateContactListAsync("MyList");
                    }
                    //otherwise if there is one then we reuse it
                    else
                    {
                        contactList = contactLists.FirstOrDefault();
                    }

                    await contactList.SaveContactAsync(contact);

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        showQuickMessage(ITBApp.Helpers.TranslationHelper.GetNewContactsSuccesful(name));
                    });
                }
                catch (Exception exc)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        showQuickMessage(ITBApp.Helpers.TranslationHelper.GetNewContactsError(name));
                    });
                }
            }
            );   
        }

        public void CloseApp()
        {
            CoreApplication.Exit();
        }

        public string getAppVersion()
        {
            Package package = Package.Current;
            PackageId packageId = package.Id;
            PackageVersion version = packageId.Version;

            return string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
        }

        public object getGroupAppSetting(string key)
        {
            Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            bool hasContainer = localSettings.Containers.ContainsKey(AppSettings.AppSignInName);
            
            if (hasContainer)
            {
                return localSettings.Containers[AppSettings.AppSignInName].Values[key];
            }

            return null;
        }

        public int GetHeight()
        {
            var bounds = ApplicationView.GetForCurrentView().VisibleBounds;
            var scaleFactor = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;
            return Convert.ToInt32(bounds.Height* scaleFactor);
        }

        public int GetWidth()
        {
            var bounds = ApplicationView.GetForCurrentView().VisibleBounds;
            var scaleFactor = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;
            return Convert.ToInt32(bounds.Width* scaleFactor);
        }

        public async void RegisterOnNotificationHubAsync(string token, string tag)
        {
            var channel = await PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();

            string[] tags = { tag };
            var hub = new NotificationHub("ITBAapp", "Endpoint=sb://itbaapp.servicebus.windows.net/;SharedAccessKeyName=ListenPolicy;SharedAccessKey=r+5PL/lvjxJLUAJGVTzL+LOUzARsToBWzSBpmVEsMMA=");
            await hub.UnregisterAllAsync(channel.Uri);
            var result = await hub.RegisterNativeAsync(channel.Uri, tags);
        }

        public void RestartApp()
        {
            //throw new NotImplementedException();
        }

        public void setGroupAppSetting(string key, string value)
        {
            Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            // Create a setting in a container

            Windows.Storage.ApplicationDataContainer container =
               localSettings.CreateContainer(AppSettings.AppSignInName, Windows.Storage.ApplicationDataCreateDisposition.Always);

            if (localSettings.Containers.ContainsKey(AppSettings.AppSignInName))
            {
                localSettings.Containers[AppSettings.AppSignInName].Values[key] = value;
            }
        }

        public void showQuickMessage(string message)
        {
            var dialog = new Windows.UI.Popups.MessageDialog(message);
            dialog.ShowAsync();
        }

        public void UnregisterFromHub(string token)
        {
            //throw new NotImplementedException();
        }
    }
}
