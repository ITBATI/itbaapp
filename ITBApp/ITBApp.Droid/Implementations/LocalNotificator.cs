﻿using Android.App;
using Android.Content;
using ITBApp.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(ITBApp.Droid.Implementations.LocalNotificator))]
namespace ITBApp.Droid.Implementations
{
    public class LocalNotificator : ILocalNotificator
	{
		public void Notificate(string title, string content)
		{
			Intent intent = new Intent(Android.App.Application.Context, typeof(SplashActivity));

			// Create a PendingIntent; we're only using one PendingIntent (ID = 0):
			const int pendingIntentId = 0;
			PendingIntent pendingIntent =
				PendingIntent.GetActivity(Android.App.Application.Context, pendingIntentId, 
				                          intent, PendingIntentFlags.OneShot);

			// Instantiate the builder and set notification elements:
			Notification.Builder builder = new Notification.Builder(Android.App.Application.Context)
				.SetContentTitle(title)
				.SetContentText(content)
				.SetContentIntent(pendingIntent)
				.SetSmallIcon(Resource.Drawable.icono_notificacion);

			// Build the notification:
			Notification notification = builder.Build();

			// Get the notification manager:
			NotificationManager notificationManager =
				Android.App.Application.Context.GetSystemService(Android.Content.Context.NotificationService) as NotificationManager;

			// Publish the notification:
			const int notificationId = 0;
			notificationManager.Notify(notificationId, notification);
		}
	}
}
