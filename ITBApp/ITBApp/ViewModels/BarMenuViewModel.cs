﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ITBApp.Model.Handlers;
using ITBApp.Resources.i18n;
using ITBApp.Services;
using static ITBApp.Services.BarMenuService;

namespace ITBApp.ViewModels
{
    internal class BarMenuViewModel: BaseModel
    {
        private bool showBarMenu = false;

		public bool ShowBarMenu
		{
			get
			{
				return this.showBarMenu;
			}

			private set
			{
				this.showBarMenu = value;
				this.OnPropertyChanged("ShowBarMenu");
			}
		}

        private ObservableCollection<SingleMenuData> menuDataModel;
		public ObservableCollection<SingleMenuData> MenuDataModel
		{
			get
			{
				return this.menuDataModel;
			}

			private set
			{
				this.menuDataModel = value;
				this.OnPropertyChanged("MenuDataModel");
			}
		}
        public string LocationId = string.Empty;

        public override async Task<string> LoadDataAsync()
        {
            try
            {
                this.IsLoading = true;
                this.ErrorMessage = null;

                MenuDataModel = new ObservableCollection<SingleMenuData>();

                var menuData = await new BarMenuService().GetMenuAsync();

                if (LocationId == string.Empty)
                {
                    LocationId = await new GeolocatorService().GetLocationIdAsync();

                    if (LocationId == "0") LocationId = App.lastLocationId;
                }

                foreach (var item in menuData.Menues)
                {
                    if (item.Location == LocationId && DateTime.ParseExact(item.Date,"dd/MM/yyyy",CultureInfo.InvariantCulture) == DateTime.Today)
                        MenuDataModel.Add(item);
                }
					
                this.OnPropertyChanged("MenuData");
            }
            catch (ServerRequestException ex)
            {
                this.ErrorMessage = ex.Message;
            }
            catch (Exception exception)
            {
                this.ErrorMessage = Translate.bar_menu_service_error;
            }

            this.IsLoading = false;
            this.ShowBarMenu = MenuDataModel.Count != 0;

            return this.ErrorMessage;
        }
    }
}
