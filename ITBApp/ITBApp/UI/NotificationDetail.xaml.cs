﻿using ITBApp.Model.DataModel;
using ITBApp.UI;
using Xamarin.Forms;

namespace ITBApp
{
	public partial class NotificationDetail : ContentPage
	{
		public NotificationDetail(NotificationData notification)
		{
			InitializeComponent();

            if (Device.RuntimePlatform == Device.UWP)
            {
                ToolbarItems.Add(new ToolbarItem("Volver", "pop_arrow_left.png", () =>
                {
                    NavigationPage notificationsPage = new NavigationPage(new NotificationsPage());
                    notificationsPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                    notificationsPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                    App.MasterDetailPage.Detail = notificationsPage;
                }));
            }

            lblSource.Text = notification.Type;
			lblCreationDate.Text = notification.FormattedDate;
			lblTitle.Text = notification.Title.ToUpper();
			lblDescription.Text = notification.Description;

			imgIcon.Source = notification.IconSourceLarge;
			imgBackground.Source = notification.BackgroundImageSource;
			cvDate.BackgroundColor = notification.HeaderColor;
			cvSource.BackgroundColor = notification.HeaderColor;
		}
	}
}
