﻿using System;
using ITBApp.Helpers;
using ITBApp.ViewModels;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp.UI
{
	public partial class AccountBalancePage : ContentPage
	{
		public AccountBalancePage()
		{
			InitializeComponent();

			((BaseModel)BindingContext).Initialize(this);

            if (Device.RuntimePlatform == Device.UWP)
            {
                ToolbarItems.Add(new ToolbarItem("Volver", "pop_arrow_left.png", () =>
                {
                    NavigationPage profilePage = new NavigationPage(new UserProfile());
                    profilePage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                    profilePage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                    App.MasterDetailPage.Detail = profilePage;
                }));
            }

            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
				if (App.CurrentPage == PageId.ACCOUNT_BALANCE && args.IsConnected)
				{
					if (((AccountBalanceViewModel)BindingContext).Payments == null
						|| ((AccountBalanceViewModel)BindingContext).Payments.Count == 0)
					{
						((AccountBalanceViewModel)BindingContext).LoadDataAsync().ConfigureAwait(false);
					}

				}
			};
		}

		public void DisableSelection(Object sender, EventArgs args)
		{
			((ListView)sender).SelectedItem = null;
		}

	}
}
