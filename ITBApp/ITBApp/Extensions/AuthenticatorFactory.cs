﻿//-----------------------------------------------------------------------
// <copyright file="AuthenticatorFactory.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ITBApp.Extensions
{
    using System;

    /// <summary>
    /// Manages authenticator instances
    /// </summary>
    public static class AuthenticatorFactory
    {
        /// <summary>
        /// Authenticator instance
        /// </summary>
        private static OAuth2AuthenticatorExtension authenticator;

        /// <summary>
        /// Authenticator Extension instance
        /// </summary>
        private static OAuth2AuthenticatorExtension authenticatorRefresh;

        /// <summary>
        /// Gets Authenticator Extension instance
        /// </summary>
        /// <returns>
        /// Authenticator Extension instance
        /// </returns>
        public static OAuth2AuthenticatorExtension GetAuthenticator()
        {
            if (authenticator == null)
            {
                authenticator = CreateAuthenticator();

                authenticator.Completed += (sender, eventArgs) =>
                {
                    if (eventArgs.IsAuthenticated)
                    {
                        App.SaveTokenAsync(eventArgs.Account);
                        App.SuccessfulLoginAction.Invoke();
                    }
                    else
                    {
                        // The user cancelled
                    }
                };
            }

            return authenticator;
        }

        /// <summary>
        /// Gets Authenticator Extension for Refresh
        /// </summary>
        /// <returns>
        /// Authenticator Extension instance
        /// </returns>
        public static OAuth2AuthenticatorExtension GetAuthenticatorForRefresh() 
        {
            if (authenticatorRefresh == null)
            {
                authenticatorRefresh = CreateAuthenticator();
            }

            return authenticatorRefresh;
        }

        /// <summary>
        /// Creates an Authenticator Extension instance
        /// </summary>
        /// <returns>
        /// Authenticator Extension
        /// </returns>
        private static OAuth2AuthenticatorExtension CreateAuthenticator()
        {
            return new OAuth2AuthenticatorExtension(
                clientId: AppSettings.AuthenticatorClientId, // your OAuth2 client id
                clientSecret: AppSettings.AuthenticatorSecret,
                scope: "openid https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/gmail.send",
                authorizeUrl: new Uri("https://accounts.google.com/o/oauth2/auth"), // the auth URL for the service
                redirectUrl: new Uri(AppSettings.AuthenticatorRedirectUrl),
                accessTokenUrl: new Uri("https://accounts.google.com/o/oauth2/token"));
        }
    }
}
