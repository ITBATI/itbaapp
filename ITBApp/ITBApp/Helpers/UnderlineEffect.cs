﻿using Xamarin.Forms;

namespace ITBApp.Helpers
{
	public class UnderlineEffect : RoutingEffect
	{
		public const string EffectNamespace = "ITBAapp";

		public UnderlineEffect() : base($"{EffectNamespace}.{nameof(UnderlineEffect)}")
		{
		}
	}
}
