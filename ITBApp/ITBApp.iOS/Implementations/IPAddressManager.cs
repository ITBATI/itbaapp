﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using Foundation;
using ITBApp.Interfaces;
using Plugin.Connectivity;
using Xamarin.Forms;
using SystemConfiguration;

[assembly: Dependency(typeof(ITBApp.iOS.Implementations.IPAddressManager))]

namespace ITBApp.iOS.Implementations
{
    [Preserve(AllMembers = true)]
    public class IPAddressManager: IIPAddressManager
    {
		public List<string> GetIPAddress()
		{
            List<string> result = new List<string>();

            // if not connected to WIFI no IP
            if (Reachability.InternetConnectionStatus() != NetworkStatus.ReachableViaWiFiNetwork) return result;

            /*
            string wifiName = GetWifiId();
            if (wifiName == null || ! wifiName.Contains("ITBA")) return result;
            */

			foreach (var netInterface in NetworkInterface.GetAllNetworkInterfaces())
			{
				if (netInterface.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 ||
					netInterface.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
				{
					foreach (var addrInfo in netInterface.GetIPProperties().UnicastAddresses)
					{
						if (addrInfo.Address.AddressFamily == AddressFamily.InterNetwork)
						{
                            result.Add(addrInfo.Address.ToString());

						}
					}
				}
			}

			return result;
		}

		/// <summary>
		/// Returns an identifier (the SSID or BSSID) of the Wifi the device is corrently connected to.
		/// If the current SSID is not available, the BSSID is used as identifier.
		/// </summary>
		/// <returns>identifier (SSID or BSSID) for the corrently connected Wifi</returns>
		private string GetWifiId()
		{
			String[] interfaces;
			CaptiveNetwork.TryGetSupportedInterfaces(out interfaces);

			if (interfaces != null && interfaces.Length >= 1)
			{
				NSDictionary dict;
				CaptiveNetwork.TryCopyCurrentNetworkInfo(interfaces[0], out dict);

				if (dict != null)
				{
					var bssid = (NSString)dict[CaptiveNetwork.NetworkInfoKeyBSSID];
					var ssid = (NSString)dict[CaptiveNetwork.NetworkInfoKeySSID];


					if (!String.IsNullOrEmpty(ssid))
					{
						return ssid;
					}

					if (!String.IsNullOrEmpty(bssid))
					{
						return bssid;
					}
				}
			}

			return null;
		}
    }
}
