using Android.App;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using ITBApp;
using ITBApp.Droid.Implementations;
using Android.Gms.Common.Apis;
using Android.Content;
using Android.Gms.Auth.Api;
using Android.Gms.Auth.Api.SignIn;
using Android.OS;
using System;
using Android.Gms.Common;
using System.Collections.Generic;
using System.Threading.Tasks;
using ITBApp.Services;
using Firebase.Iid;
using Firebase;

[assembly: ExportRenderer(typeof(GoogleLoginPage), typeof(GoogleLoginPageRenderer))]
namespace ITBApp.Droid.Implementations
{
	public class GoogleLoginPageRenderer : PageRenderer,
	Android.Views.View.IOnClickListener,
		Android.Gms.Common.Apis.GoogleApiClient.IConnectionCallbacks, GoogleApiClient.IOnConnectionFailedListener
	{
		private readonly int RC_SIGN_IN = 10;
        private const string GmailScope = "https://www.googleapis.com/auth/gmail.send";
        private const string CalendarScope = "https://www.googleapis.com/auth/calendar";
        private const string HostDomain = "itba.edu.ar";
        private const string ValidatingUserMessage = "Validando usuario...";
        private const string InitSessionMessage = "Iniciando sesión...";
        private const string ResponseExpiresInField = "expires_in";
        private const string ResponseAccessTokenField = "access_token";
        private const string ResponseRefreshTokenField = "refresh_token";

        ProgressDialog mDialog;

		GoogleApiClient mGoogleApiClient;

		protected override void OnElementChanged(ElementChangedEventArgs<Page> element)
		{
			base.OnElementChanged(element);

			// this is a ViewGroup - so should be able to load an AXML file and FindView<>
			var activity = this.Context as MainActivity;

			activity.ActivityResult += OnActivityResult;

            activity.SetContentView(Resource.Layout.activity_main);

			SetLoginDisplayDesign(activity);

			activity.FindViewById(Resource.Id.sign_in_button).SetOnClickListener(this);
			
            Scope[] scopes = { new Scope(GmailScope) };

			GoogleSignInOptions gso =
				new GoogleSignInOptions.Builder(GoogleSignInOptions.DefaultSignIn)
				                       .SetHostedDomain(HostDomain)
                                       .RequestServerAuthCode(AppSettings.AuthenticatorClientId, true)
				                       .RequestScopes(new Scope(CalendarScope),scopes)
                                       .RequestEmail()
                                       .Build();

			mGoogleApiClient = new GoogleApiClient.Builder(activity)
				.AddConnectionCallbacks(this)
				.AddOnConnectionFailedListener(this)
				.AddApi(Auth.GOOGLE_SIGN_IN_API, gso)
				.AddScope(new Scope(CalendarScope))
                .AddScope(new Scope(GmailScope))
				.Build();
		}

		public void OnConnected(Bundle connectionHint)
		{
			
		}

		public void OnConnectionSuspended(int cause)
		{
			
		}

		public void OnConnectionFailed(ConnectionResult result)
		{
			
		}

        protected async void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            mDialog = new ProgressDialog(this.Context);
            mDialog.SetMessage(ValidatingUserMessage);
            mDialog.SetCancelable(false);
            mDialog.Show();
            // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
            if (requestCode == RC_SIGN_IN)
            {
                GoogleSignInResult result = Auth.GoogleSignInApi.GetSignInResultFromIntent(data);
                await HandleSignInResult(result);
            }
        }

        private async Task HandleSignInResult(GoogleSignInResult result)
        {
            if (result.IsSuccess)
            {
                mDialog.SetMessage(InitSessionMessage);
                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount acct = result.SignInAccount;
                String authCode = acct.ServerAuthCode;

                LoginService service = new LoginService();
                IDictionary<string, string> dict = await service.getAccessTokenAndExpireTime(authCode);
                double diffInSeconds = double.Parse(dict[ResponseExpiresInField]);

                if (mGoogleApiClient.IsConnected)
                {
                    await Auth.GoogleSignInApi.SignOut(mGoogleApiClient);
                    mGoogleApiClient.Disconnect();
                }

				var options = new FirebaseOptions.Builder()
					.SetApplicationId(AppSettings.GoogleApisID)
					.SetApiKey(AppSettings.GoogleAppKey)					
					.SetGcmSenderId(Context.GetString(Resource.String.gcm_defaultSenderId))
					.Build();

				FirebaseApp.InitializeApp(this.Context, options);
                await App.SaveTokenAsync(dict[ResponseAccessTokenField], dict[ResponseRefreshTokenField], (int)diffInSeconds);
                mDialog.Dismiss();
            }
            else
            {
				// Signed out, show unauthenticated UI.
				mDialog.Dismiss();         
            }
        }

        class DialogInterfaceOnCancelListener : Java.Lang.Object, IDialogInterfaceOnCancelListener
		{
			public Action<IDialogInterface> OnCancelImpl { get; set; }

			public void OnCancel(IDialogInterface dialog)
			{
				OnCancelImpl(dialog);
			}
		}

		public void OnClick(Android.Views.View v)
		{
			switch (v.Id)
			{
				case Resource.Id.sign_in_button:
					//mShouldResolve = true;

					//mGoogleApiClient.Connect();

					var activity = this.Context as Activity;
                    mGoogleApiClient.Connect();
					Intent signInIntent = Auth.GoogleSignInApi.GetSignInIntent(mGoogleApiClient);

					activity.StartActivityForResult(signInIntent, RC_SIGN_IN);

                    //Intent signInIntent = Auth.GoogleSignInApi.GetSignInIntent(mGoogleApiClient);
                    //StartActivity(signInIntent);

					break;
			}
		}

		private int ConvertPixelsToDp(float pixelValue)
		{
			var dp = (int)((pixelValue) / Resources.DisplayMetrics.Density);
			return dp;
		}

		private void SetLoginDisplayDesign(MainActivity activity)
		{
			var metrics = Resources.DisplayMetrics;
			var widthInDp = ConvertPixelsToDp(metrics.WidthPixels);
			var heightInDp = ConvertPixelsToDp(metrics.HeightPixels);

			//var widthInDp = metrics.WidthPixels;
			//var heightInDp = metrics.HeightPixels;

			Android.Widget.RelativeLayout.LayoutParams lp =
				       new Android.Widget.RelativeLayout.LayoutParams((metrics.WidthPixels) / 2 , ( metrics.HeightPixels) / 2);
			lp.SetMargins(0, heightInDp / 5, 0, 0);
			//lp.Height = (widthInDp - 20) / 2;
			//lp.Width = widthInDp / 2 ; 
			lp.AddRule(Android.Widget.LayoutRules.CenterHorizontal);

			activity.FindViewById(Resource.Id.itba_image).LayoutParameters = lp;
		}

		public int DpFromPx(int px)
		{
			return px / (int)Resources.DisplayMetrics.Density;
		}

		public int PxFromDp(int dp)
		{
			return dp * (int)Resources.DisplayMetrics.Density;
		}
	}
}