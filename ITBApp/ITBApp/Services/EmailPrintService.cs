﻿using System;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ITBApp.Helpers;
using ITBApp.Model.Handlers;
using ITBApp.Resources.i18n;
using ITBApp.Services;
using ITBApp.Enums;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ITBApp
{
    public class EmailPrintService
	{
		private TokenHolder tokenHolder = new TokenHolder();

        public async Task<bool> SendBWEmail(string userMail, byte[] attachment, string fileName, MediaType mediaType, MediaSubType mediaSubType, string token) 
        {
            if (token == null)
                return await this.SendEmail(userMail, AppSettings.BwPrinterEmail, attachment, fileName,mediaType,mediaSubType);
            return await this.SendEmail(userMail, AppSettings.BwPrinterEmail, attachment, fileName, mediaType, mediaSubType, token);
        }

		public async Task<bool> SendColorEmail(string userMail, byte[] attachment, string fileName, MediaType mediaType, MediaSubType mediaSubType, string token)
		{
			if (token == null)
				return await this.SendEmail(userMail, AppSettings.ColorPrinterEmail, attachment, fileName, mediaType, mediaSubType);
			return await this.SendEmail(userMail, AppSettings.ColorPrinterEmail, attachment, fileName, mediaType, mediaSubType, token);
		}

		private async Task<bool> SendEmail(string userMail, string destinationAddress ,byte[] attachment, string fileName, MediaType mediaType, MediaSubType mediaSubType)
		{
			var mailMessage = DependencyService.Get<IMailMessageGenerator>()
											   .CreateMailMessage(userMail,
																  destinationAddress,
																  attachment,
																  fileName,
			                                                      mediaType.ToString().ToLower(),
			                                                      mediaSubType.ToString().ToLower());
			var messageString = Base64EncodeWebSafe(mailMessage);

			string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
			var uriBuilder = new UriBuilder(String.Format(AppSettings.GoogleGmailApiURL, WebUtility.UrlEncode(userMail)));
			HttpValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
			query.Add("uploadType", "multipart");
			query.Add("key", AppSettings.GoogleAppKey);
			uriBuilder.Query = query.ToString();
			var content = new StringContent("{\"raw\":\"" + messageString + "\"}", Encoding.UTF8, "application/json");

			using (var client = new HttpClient())
			{
				client.MaxResponseContentBufferSize = 256000;
				client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

				HttpResponseMessage response = await client.PostAsync(uriBuilder.Uri, content).ConfigureAwait(false);

				if (response.IsSuccessStatusCode)
				{
					return true;
				}
				else
				{
					throw new ServerRequestException("Error al enviar el mail.");
				}
			}
		}

		private async Task<bool> SendEmail(string userMail, string destinationAddress, byte[] attachment, string fileName, MediaType mediaType,
		                                  MediaSubType mediaSubType, string token)
		{
			var mailMessage = DependencyService.Get<IMailMessageGenerator>()
											   .CreateMailMessage(userMail,
																  destinationAddress,
																  attachment,
																  fileName,
																  mediaType.ToString().ToLower(),
																  mediaSubType.ToString().ToLower());
			var messageString = Base64EncodeWebSafe(mailMessage);

			var uriBuilder = new UriBuilder(String.Format(AppSettings.GoogleGmailApiURL, WebUtility.UrlEncode(userMail)));
			HttpValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
			query.Add("uploadType", "multipart");
			query.Add("key", AppSettings.GoogleAppKey);
			uriBuilder.Query = query.ToString();
			var content = new StringContent("{\"raw\":\"" + messageString + "\"}", Encoding.UTF8, "application/json");

			using (var client = new HttpClient())
			{
				client.MaxResponseContentBufferSize = 256000;
				client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

				HttpResponseMessage response = await client.PostAsync(uriBuilder.Uri, content).ConfigureAwait(false);

				if (response.IsSuccessStatusCode)
				{
					return true;
				}
				else
				{
					return false;
					//throw new ServerRequestException("Error al enviar el mail.");
				}
			}
		}

		public async Task<int> GetPrintQuota(string token)
		{
			string stringResponse;

			try
			{
				stringResponse = await HttpUtility
					.MakeGetCallAsync(String.Format("{0}{1}", AppSettings.PrintQuotaURL, token))
					.ConfigureAwait(false);
			}
			catch
			{
				throw new ServerRequestException("Error de conexión al servicio de cuota de impresión.");
			}

			try
			{				
				JToken questions = JToken.Parse(stringResponse);

				return Convert.ToInt32(Double.Parse(questions["quota"].ToString(), CultureInfo.InvariantCulture));
			}
			catch
			{
				throw new ServerRequestException("Error al procesar los datos de la cuota de impresión.");
			}
		}

		public async Task<int> GetPrintQuotaAsync()
		{
			if (!CrossConnectivity.Current.IsConnected)
			{
				throw new ServerRequestException(Translate.accountbalancepage_no_internet_text);
			}

			string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
            return await GetPrintQuota(token);
		}

		private string Base64EncodeWebSafe(string plainText)
		{
			var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
			return System.Convert.ToBase64String(plainTextBytes)
				         .Replace('+', '-')
				         .Replace('/', '_')
				         .Replace("=", "");
		}
	}
}
