﻿//-----------------------------------------------------------------------
// <copyright file="WebRequestExtensions.cs" company="ITBA">
//     Copyright (c) INSTITUTO TECNOLÓGICO DE BUENOS AIRES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ITBApp.Extensions
{
    using System;
    using System.IO;
    using System.Net;
    using System.Threading;

    /// <summary>
    /// Web Request Extensions.
    /// </summary>
    public static class WebRequestExtensions
    {
        /// <summary>
        /// Gets the response.
        /// </summary>
        /// <returns>The response.</returns>
        /// <param name="request">Web Request.</param>
        public static WebResponse GetResponse(this WebRequest request)
        {
            ManualResetEvent evt = new ManualResetEvent(false);
            WebResponse response = null;
            request.BeginGetResponse(
            (IAsyncResult ar) =>
            {
                response = request.EndGetResponse(ar);
                evt.Set();
            }, 
            null);
            evt.WaitOne();
            return response as WebResponse;
        }

        /// <summary>
        /// Gets the request stream.
        /// </summary>
        /// <returns>The request stream.</returns>
        /// <param name="request">Web Request.</param>
        public static Stream GetRequestStream(this WebRequest request)
        {
            ManualResetEvent evt = new ManualResetEvent(false);
            Stream requestStream = null;
            request.BeginGetRequestStream(
            (IAsyncResult ar) =>
            {
                requestStream = request.EndGetRequestStream(ar);
                evt.Set();
            }, 
            null);
            evt.WaitOne();
            return requestStream;
        }
    }
}