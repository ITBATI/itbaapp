﻿using System;
using System.Threading.Tasks;
using ITBApp.Helpers;
using ITBApp.Interfaces;
using ITBApp.Model;
using ITBApp.Resources.i18n;
using Xamarin.Forms;

namespace ITBApp.UI
{
	public partial class HelpPage : ContentPage
	{
		public HelpPage()
		{
			InitializeComponent();

			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (s, e) =>
			{
				Device.OpenUri(new Uri("mailto:ayuda@itba.edu.ar"));
			};
			imgMail.GestureRecognizers.Add(tapGestureRecognizer);

			tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (s, e) =>
			{
				string urlStore = "";

                if (Device.RuntimePlatform == Device.Android)
                    urlStore = AppSettings.BBPlaystoreURL;
                else if (Device.RuntimePlatform == Device.iOS)
                    urlStore = AppSettings.BBApplestoreURL;
                else if (Device.RuntimePlatform == Device.UWP)
                    urlStore = AppSettings.BBWindowsStoreURL;

				Device.OpenUri(new Uri(urlStore));
			};
			slBlackboard.GestureRecognizers.Add(tapGestureRecognizer);

			tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += async (s, e) =>
			{
				if (await this.DisplayAlert(
					Translate.help_whatsapp_title,
					Translate.help_whatsapp_question,
					Translate.Ok,
					Translate.Cancel))
				{
                    DependencyService.Get<IAppMethods>().AddPhoneContact(Translate.itba_help_ti, "1161967321", "Whatsapp");
				}
			};
			imgWhatsapp.GestureRecognizers.Add(tapGestureRecognizer);

			tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (s, e) =>
			{
				Device.OpenUri(new Uri("https://twitter.com/itba_ti"));
			};
			imgTwitter.GestureRecognizers.Add(tapGestureRecognizer);

			tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += async (s, e) =>
			{
				if (await this.DisplayAlert(
                     Translate.itba_help,
					 "2150 - 4800",
                     Translate.call,
                     Translate.Cancel))
				{
					Device.OpenUri(new Uri("tel:21504800"));
				}

			};
			imgPhone.GestureRecognizers.Add(tapGestureRecognizer);

			lblVersion.Text = "ITBAapp v" + DependencyService.Get<IAppMethods>().getAppVersion();
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			this.BindingContext = null;
			this.Content = null;

            App.HardwareBackPressed = null;
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            App.HardwareBackPressed = () =>
            {
                MainMenuItem item = MainMenuItem.HomeItem;
                App.CurrentPage = item.Id;
                item.NavigationPage.SetValue(NavigationPage.BarBackgroundColorProperty, AppSettings.NavigationBarBackgroundColor);
                item.NavigationPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                App.MasterDetailPage.Detail = item.NavigationPage;
                return Task.FromResult<bool?>(false);
            };
        }
	}
}
