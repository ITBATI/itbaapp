﻿using ITBApp.Interfaces;
using Xamarin.Forms;
using Android.Preferences;
using Android.Content;

[assembly: Dependency(typeof(ITBApp.Droid.Implementations.AppGroupSettings))]
namespace ITBApp.Droid.Implementations
{
    public class AppGroupSettings: IAppGroupSettings
    {
        public void Set(string key, string value)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
            ISharedPreferencesEditor editor = prefs.Edit();
            editor.PutString(key, value);
            editor.Apply();
        }

		public object Get(string key)
		{
			// To redirect if push notification
			using (ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context))
			{
				string fromPush = prefs.GetString(key, null);
				return fromPush;
			}
		}

        public void Clear()
        {
			ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
			ISharedPreferencesEditor editor = prefs.Edit();
            editor.Clear();
			editor.Apply();
        }


    }
}
