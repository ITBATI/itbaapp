﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ITBApp.Handlers;
using ITBApp.Helpers;
using ITBApp.Model.DataModel;
using ITBApp.Model.Handlers;
using ITBApp.Util.Helpers;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace ITBApp.Services
{
	public class NotificationsService
	{
		private TokenHolder tokenHolder = new TokenHolder();

		/// <summary>
		/// Gets or sets the notifications last identifier.
		/// </summary>
		/// <value>The notifications last identifier.</value>
		private long? NotificationsLastId
		{
			get
			{
				if (Application.Current.Properties.ContainsKey(ServiceConstants.NOTIFICATIONS_LAST_ID))
				{
					return (long?)Application.Current.Properties[ServiceConstants.NOTIFICATIONS_LAST_ID];
				}
				else
				{
					return null;
				}
			}
			set
			{
				Application.Current.Properties[ServiceConstants.NOTIFICATIONS_LAST_ID] = value;
			}
		}

		public async Task<int> getUnreadNotificationsCount()
		{
			string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);
			NotificationListData notificationsList = GetSavedNotificationsData();

			if (notificationsList != null)
			{
				NotificationsLastId = notificationsList.LastId;
			}

			try
			{
				List<NotificationData> newNotifications = await getNotificationsAsync(token, NotificationsLastId).ConfigureAwait(false);
				return newNotifications.Count;
			}
			catch (TokenExpiredException ex)
			{
				await App.LogoutAndRelogin();
			}
			catch (Exception exception)
			{
				if (notificationsList == null) throw exception;
			}

			return 0;
		}

		public async Task<NotificationListData> getNotificationsAsync()
		{
			NotificationListData notificationsList = GetSavedNotificationsData();

			if (notificationsList != null)
			{
				NotificationsLastId = notificationsList.LastId;
			}


			try
			{
				string token = await tokenHolder.GetTokenAsync().ConfigureAwait(false);

				List<NotificationData> newNotifications = await getNotificationsAsync(token, NotificationsLastId).ConfigureAwait(false);

				if (notificationsList == null)
				{
					notificationsList = new NotificationListData();
					notificationsList.notifications = new List<NotificationData>();
				}

				if (newNotifications != null && newNotifications.Count > 0)
				{
					notificationsList.notifications.InsertRange(0, newNotifications);
					notificationsList.LastId = FindLastId(newNotifications);
				}

				// Chequeo del limite de notificaciones
				while (notificationsList.notifications.Count > ServiceConstants.NOTIFICATION_LIST_LIMIT)
				{
					notificationsList.notifications.RemoveAt(ServiceConstants.NOTIFICATION_LIST_LIMIT);
				}
			}
			catch (TokenExpiredException ex)
			{
				await App.LogoutAndRelogin();
			}
			catch (Exception exception)
			{
				if (notificationsList == null) throw exception;
			}

			string json = SerializerHelper.Serialize(notificationsList);
			Application.Current.Properties[ServiceConstants.NOTIFICATIONS_DATA] = json;
			await Application.Current.SavePropertiesAsync().ConfigureAwait(false);


			return notificationsList;
		}

		private long? FindLastId(List<NotificationData> newNotifications)
		{
			long? id = null;
			foreach (NotificationData data in newNotifications)
			{
				if (id == null || data.Id > id)
				{
					id = data.Id;
				}
			}

			return id;
		}

		private async Task<List<NotificationData>> getNotificationsAsync(string token, long? lastId)
		{
			string stringResponse = await notificationsCallAsync(token, lastId).ConfigureAwait(false);

			List<NotificationData> notifications = processNotificationsCallResponse(stringResponse);

			return notifications;
		}

		private async Task<string> notificationsCallAsync(string token, long? lastId)
		{
			string parameters = token;
			if (lastId != null)
			{
				parameters += "/" + lastId;
			}

			var uriBuilder = new UriBuilder(AppSettings.NotificationsURL + parameters);
			HttpValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
			uriBuilder.Query = query.ToString();

			using (var client = new HttpClient())
			{
				client.MaxResponseContentBufferSize = 256000;
				HttpResponseMessage response = await client.GetAsync(uriBuilder.Uri).ConfigureAwait(false);
				if (response.IsSuccessStatusCode)
				{
					string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					return result;
				}
				else
				{
					throw new ServerRequestException("Error de conexión al servicio de notificaciones");
				}
			}
		}


		private NotificationListData GetSavedNotificationsData()
		{
			if (Application.Current.Properties.ContainsKey(ServiceConstants.NOTIFICATIONS_DATA))
			{
				string json = (string)Application.Current.Properties[ServiceConstants.NOTIFICATIONS_DATA];
				NotificationListData data = (NotificationListData)SerializerHelper.Deserialize(json, (new NotificationListData()).GetType());
				return data;
			}

			return null;
		}

		private List<NotificationData> processNotificationsCallResponse(string stringResponse)
		{
			List<NotificationData> notificationsData = new List<NotificationData>();

			foreach (JToken data in JArray.Parse(stringResponse))
			{
				notificationsData.Add(new NotificationData()
				{
					Title = data["title"].ToString(),
					Type = data["type"].ToString(),
					Description = data["description"].ToString(),
					Source = data["source"].ToString(),
					Date = ValidationHelper.JavaDateToDateTime(data["date"].ToString()),
					DueDate = ValidationHelper.JavaDateToDateTime(data["dueDate"].ToString()),
					Id = Int64.Parse(data["id"].ToString()),
					Email = data["email"].ToString(),
					Url = data["url"].ToString()
				});
			}

			return notificationsData;
		}
	}
}
